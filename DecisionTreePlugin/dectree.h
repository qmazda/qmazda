#ifndef DECTREE_H
#define DECTREE_H

#include <string>
#include <vector>
#include <iostream>
#include <fstream>
#include <sstream>
#include "../MzShared/classifierio.h"
#include "../MzShared/parametertreetemplate.h"
//#include "../../qmazda/MzGenerator/templates.h"

//#define STORE_FEATURE_NAMES

//extern bool verbose;

class DecisionTreeElement
{
public:
    DecisionTreeElement()
    {
        maxClassLut = 0;
        classifierai = NULL;
        classLut = NULL;
//        featureComputationObject = NULL;
    }
    ~DecisionTreeElement()
    {
        if(classifierai != NULL) delete classifierai;
        if(classLut != NULL) delete[] classLut;
//        if(featureComputationObject != NULL)
//            delete featureComputationObject;
    }
    std::string toString(void)
    {
        std::stringstream ss;
        ss << fileName << " ";
//        if(featureComputationObject != NULL)
//        {
//#ifdef STORE_FEATURE_NAMES
//            ss << "F: ";
//            for(unsigned int i = 0; i < computationObjectFeature.size(); i++)
//                ss << computationObjectFeature[i] <<" ";
//#endif
//        }
        if(classifierai != NULL)
        {
            std::vector<std::string> cnames = classifierai->getClassNames();
            ss << "C: ";
            for(unsigned int i = 0; i < cnames.size(); i++)
                ss << cnames[i] <<" ";
        }

        return ss.str();
    }
    void fromString(std::string v)
    {
        fileName = v;
    }
    std::string fileName;

//    StubItemBuilder* featureComputationObject;

//#ifdef STORE_FEATURE_NAMES
//    std::vector<std::string> computationObjectFeature;
//#endif
    ClassifierAccessInterface* classifierai;
    unsigned int* classLut;
    unsigned int maxClassLut;

};


class DecisionTree : public ParameterTree <DecisionTreeElement>
{
public:
    DecisionTree();

    bool loadFromStream(std::ifstream* file, std::vector <std::string>* class_names);

    bool loadFromFile(const char *filename, std::vector<string> *class_names);

    int decisionTreeClassificationRecursive(ParameterTreeItem<DecisionTreeElement>* item, double* values);

    int decisionTreeClassification(double* values);

    bool loadClassifiers(std::vector<string> *allClassNames);

    void renumberClasses(std::vector<string> *allClassNames);

    bool renumberFeatures(std::vector <std::string>* allFeatureNames);

    bool getTreeFeatureNames(std::vector <std::string>* allFeatureNames);

//    std::vector <std::string> allFeatureNames;

//    std::vector <std::string> classNames;
    
    std::string errorMessage;
private:
    bool renumberFeaturesRecursive(ParameterTreeItem<DecisionTreeElement>* item, std::vector <std::string>* allFeatureNames);

    void renumberClassesRecursive(ParameterTreeItem<DecisionTreeElement>* item, std::vector<string> *allClassNames, string parrentClassName);

    int loadClassifiersRecursive(ParameterTreeItem<DecisionTreeElement>* item, std::vector<string> *allClassNames);

    bool getFeatureNamesRecursive(ParameterTreeItem<DecisionTreeElement>* item, std::vector <std::string>* allFeatureNames);

    ClassifierAccessInterface* loadSingleClassifier(ClassifierAccessInterface* object, const char* fileName);



};

#endif // DECTREE_H
