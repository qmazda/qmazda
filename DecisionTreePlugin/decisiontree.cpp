/*
 * qMaZda - Image Analysis and Pattern Recognition
 * 
 * Copyright 2013  Piotr M. Szczypiński <piotr.szczypinski@p.lodz.pl>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <limits>
#include <sstream>

#include "decisiontree.h"
#include "../MzShared/filenameremap.h"

#ifdef _WIN32
#include <windows.h>
#include <direct.h>
// MSDN recommends against using getcwd & chdir names
//#define PATH_MAX 4096
#define dt_getcwd _getcwd
#define dt_chdir _chdir
//#define PATH_MAX 4096
#else
#include "unistd.h"
#define dt_getcwd getcwd
#define dt_chdir chdir
#endif
#include <limits.h>

DecisionTreeClassifier::DecisionTreeClassifier()
    :DecisionTree()
{
    input_feature_names_buffer = NULL;
}
DecisionTreeClassifier::~DecisionTreeClassifier()
{
    if(input_feature_names_buffer != NULL)
        delete input_feature_names_buffer;
}

void DecisionTreeClassifier::segmentImage(unsigned int vectornumber, MazdaMapPixelType** values, MazdaMapPixelType* result)
{
//    breakanalysis = false;
    for(unsigned int v = 0; v < vectornumber; v++)
    {
        result[v] = 0;
        for(unsigned int f = 0; f < input_feature_names_count; f++)
        {
            input_feature_names_buffer[f] = values[f][v];
        }

        result[v] = decisionTreeClassification(input_feature_names_buffer);
//        if(breakanalysis) break;
//        NotifyProgressStep();
    }
}








bool DecisionTreeClassifier::configureForClassification(vector<string> *input_feature_names)
{
    std::vector <std::string> allFeatureNames;
    getTreeFeatureNames(&allFeatureNames);
    renumberClasses(&class_names);
    input_feature_names_count = input_feature_names->size();
    if(input_feature_names_buffer != NULL)
        delete input_feature_names_buffer;
    input_feature_names_buffer = new double[input_feature_names_count];

//    if(verbose)
//    {
//        std::cout << "Classes:" << std::endl;
//        for(unsigned int i = 0; i < class_names.size(); i++)
//            std::cout << "  " << i << " " << class_names[i]  << std::endl;
//        std::cout << std::endl;

//        std::cout << "Features available:" << std::endl;
//        for(unsigned int i = 0; i < input_feature_names->size(); i++)
//            std::cout << "  " << (*input_feature_names)[i]  << std::endl;
//        std::cout << std::endl;

//        std::cout << "Features required:" << std::endl;
//        for(unsigned int i = 0; i < allFeatureNames.size(); i++)
//            std::cout << "  " << allFeatureNames[i]  << std::endl;
//        std::cout << std::endl;
//    }


    if(!renumberFeatures(input_feature_names))
    {
        return false;
    }


    return true;
}

unsigned int DecisionTreeClassifier::classifyFeatureVector(double* featureVector)
{
    return decisionTreeClassification(featureVector);
}

bool DecisionTreeClassifier::loadClassifierFromFile(const char *filename)
{
    class_names.clear();
    ifstream file;

#if defined _MZ_UTF8_TO_U16
        file.open(_mz_utf8_to_u16(filename).c_str());
#else
        file.open(filename);
#endif
    if (!file.is_open())
        return false;
    if (!file.good())
        return false;
    bool ret = false;

    char *prevloc = setlocale(LC_ALL, NULL);
    if(prevloc) prevloc = strdup(prevloc);
    setlocale(LC_ALL, "C");
    std::string line;
    getline(file, line);

    if (!line.empty() && line[line.size() - 1] == '\r')
        line.erase(line.size() - 1);

    if(line == line)
    {
        char cwd[PATH_MAX];
        if (dt_getcwd(cwd, sizeof(cwd)) != NULL)
        {
            std::string file_name = filename;
            size_t slash = file_name.find_last_of("/\\");

            if(slash != std::string::npos)
            {
                std::string file_path = file_name.substr(0, slash);
                dt_chdir(file_path.c_str());
            }
            ret = loadFromStream(&file, &class_names);
            if(slash != std::string::npos)
            {
                dt_chdir(cwd);
            }
        }
        else
        {
            ret = loadFromStream(&file, &class_names);
        }
    }
    setlocale(LC_ALL, prevloc);
    free(prevloc);
    file.close();
    return ret;
}

std::vector<std::string> DecisionTreeClassifier::getClassNames(void)
{
    return class_names;
}

std::vector<std::string> DecisionTreeClassifier::getFeatureNames(void)
{
    std::vector<std::string> feature_names;
    getTreeFeatureNames(&feature_names);
    return feature_names;
}
