#include "dectree.h"
#include "../LdaPlugin/ldaselection.h"
#include "../VschPlugin/vschselection.h"
#include "../SvmPlugin/svmselection.h"
#include "../MzGenerator/featureio.h"
#include "../MzShared/filenameremap.h"

DecisionTree::DecisionTree() :
    ParameterTree <DecisionTreeElement>()
{
}

bool DecisionTree::loadFromStream(std::ifstream* file, std::vector <std::string>* class_names)
{
    parse(file, '+');

    errorMessage.clear();

    class_names->clear();
    //class_names->push_back("%Undefined%");

    if(!loadClassifiers(class_names))
    {
        //if(verbose)
        //std::cerr << "Failed loading classifiers: " << errorMessage << std::endl;
        return false;
    }

//    if(verbose)
//    {
////        std::cout << "Treebb:" << std::endl;
//        std::cout << serialize(' ') << std::endl;
////        std::cout << "Classes:" << std::endl;
////        for(unsigned int i = 0; i < class_names->size(); i++)
////            std::cout << "  " << i << " " << (*class_names)[i]  << std::endl;
////        std::cout << std::endl;
//    }
    return true;
}


bool DecisionTree::loadFromFile(const char *filename, std::vector <std::string>* class_names)
{
    std::ifstream file;
#if defined _MZ_UTF8_TO_U16
        file.open(_mz_utf8_to_u16(filename).c_str());
#else
        file.open(filename);
#endif

    if (!file.is_open())
    {
        //if(verbose)
        std::cerr << "Dectree - failed to open: " << filename << std::endl;
        return false;
    }
    if (!file.good())
    {
        //if(verbose)
        std::cerr << "Dectree - failed to open: " << filename << std::endl;
        return false;
    }

    loadFromStream(&file, class_names);
    return true;
}


int DecisionTree::decisionTreeClassificationRecursive(ParameterTreeItem<DecisionTreeElement>* item, double* values)
{
    DecisionTreeElement* ek = & item->data;
    if(ek->classifierai == NULL || ek->classLut == NULL)
        return -1;
    unsigned int klasa = ek->classifierai->classifyFeatureVector(values);

    //std::cout << ek->fileName << " " << klasa << ", ";

    if(klasa < item->children.size())
    {
        int nk = decisionTreeClassificationRecursive(item->children[klasa], values);
        if(nk >= 0) return nk;
    }
    if(klasa >= ek->maxClassLut)
        klasa = 0;
    else
        klasa = ek->classLut[klasa];
    return (int)klasa;
}

int DecisionTree::decisionTreeClassification(double* values)
{
    int klasa = decisionTreeClassificationRecursive(getRoot()->children[0], values);
    if(klasa < 0) return 0;
    return klasa;
}

ClassifierAccessInterface* DecisionTree::loadSingleClassifier(ClassifierAccessInterface* object, const char* fileName)
{
    if(object->loadClassifierFromFile(fileName))
    {
        return object;
    }
    delete object;
    return NULL;
}

int DecisionTree::loadClassifiersRecursive(ParameterTreeItem<DecisionTreeElement>* item,
                              std::vector <std::string>* allClassNames)
{
    DecisionTreeElement* ek = & item->data;
    if(ek->classifierai != NULL) delete ek->classifierai;
    ek->classifierai = NULL;
    if(ek->classLut != NULL) delete[] ek->classLut;
    ek->classLut = NULL;

    if(ek->fileName.empty())
    {
        return 0;
    }
    if(ek->fileName == "!")
        return 0;

    if(ek->classifierai == NULL)
        ek->classifierai = loadSingleClassifier(dynamic_cast<ClassifierAccessInterface*>(new LdaSelectionReduction()), ek->fileName.c_str());
    if(ek->classifierai == NULL)
        ek->classifierai = loadSingleClassifier(dynamic_cast<ClassifierAccessInterface*>(new VschSelectionReduction()), ek->fileName.c_str());
    if(ek->classifierai == NULL)
        ek->classifierai = loadSingleClassifier(dynamic_cast<ClassifierAccessInterface*>(new SvmSelectionReduction()), ek->fileName.c_str());

    //object->loadClassifierFromFile(fileName)

    if(ek->classifierai == NULL)
    {
        errorMessage = ek->fileName.c_str();
        return -1;
    }

    std::vector<std::string> newClassNames = ek->classifierai->getClassNames();
    unsigned int nc = newClassNames.size();

    ek->classLut = new unsigned int[nc];
    ek->maxClassLut = nc;

    unsigned int nb = item->children.size();
    unsigned int ncb = nc;
    if(nb < nc) ncb = nb;

    int ret = 0;
    unsigned int c = 0;

    for(; c < ncb ; c++)
    {
        unsigned int na = allClassNames->size();
        unsigned int a;

        for(a = 0; a < na; a++)
        {
            if((*allClassNames)[a] == newClassNames[c]) break;
        }

        if(a >= na)
        {
            allClassNames->push_back(newClassNames[c]);

        }
        ret++;

        int added_leafs = loadClassifiersRecursive(item->children[c], allClassNames);
        if(added_leafs < 0)
        {
            return added_leafs;
        }
        if(a >= na && (allClassNames->size() > na+1 || added_leafs > 0))
            allClassNames->erase(allClassNames->begin()+na);
    }
    for(; c < nc ; c++)
    {
        unsigned int na = allClassNames->size();
        unsigned int a;
        for(a = 0; a < na; a++)
        {
            if((*allClassNames)[a] == newClassNames[c]) break;
        }
        if(a >= na)
        {
            allClassNames->push_back(newClassNames[c]);
        }
        ret++;
    }
    if(c < nb)
    {
        errorMessage = item->children[c]->data.fileName;
    }
    return ret;
}

bool DecisionTree::loadClassifiers(std::vector <std::string>* allClassNames)
{
    unsigned int ni = getRoot()->children.size();
    std::vector<std::string> alreadyComputedFeatureNames;

    bool ret = true;
    for(unsigned int i = 0; i < ni; i++)
    {
        int r = loadClassifiersRecursive(getRoot()->children[i], allClassNames);
        if(r < 0)
            ret = false;
        alreadyComputedFeatureNames.clear();
    }

//    ParameterTreeItem<DecisionTreeElement>* rrr = getRoot();

    for(std::vector<std::string>::iterator it = allClassNames->begin() ; it != allClassNames->end(); ++it)
    {
        if(*it == "!")
        {
            allClassNames->erase(it);
            break;
        }
    }
    return ret;
}

void DecisionTree::renumberClassesRecursive(ParameterTreeItem<DecisionTreeElement>* item,
                                            std::vector <std::string>* allClassNames,
                                            std::string parrentClassName)
{
    DecisionTreeElement* ek = & item->data;
    if(ek->classifierai != NULL && ek->classLut != NULL)
    {
        unsigned int nc = ek->classifierai->getClassNames().size();
        if(nc > ek->maxClassLut)
            nc = ek->maxClassLut;

        std::vector<std::string> class_names = ek->classifierai->getClassNames();
        for(unsigned int c = 0; c < nc ; c++)
        {
            ek->classLut[c] = 0;
            std::string newCurrentClassName = class_names[c];
            if(newCurrentClassName == "!")
                newCurrentClassName = parrentClassName;

            bool forwarded = false;

            if(item->children.size() > c)
            {
                if(item->children[c] != NULL)
                {
                    renumberClassesRecursive(item->children[c], allClassNames, newCurrentClassName);
                    forwarded = true;
                }
            }
            if(! forwarded)
            {
                unsigned int na = allClassNames->size();
                unsigned int a;
                for(a = 0; a < na; a++)
                {
                    if((*allClassNames)[a] == newCurrentClassName)
                    {
                        ek->classLut[c] = a;
                        break;
                    }
                }
                if(a >= na)
                {
                    allClassNames->push_back(newCurrentClassName);
                    ek->classLut[c] = a;
                }
            }
            else
            {
                unsigned int na = allClassNames->size();
                unsigned int a;
                for(a = 0; a < na; a++)
                {
                    if((*allClassNames)[a] == newCurrentClassName)
                    {
                        ek->classLut[c] = a;
                        break;
                    }
                }
            }
        }
    }
    else
    {
        unsigned int na = allClassNames->size();
        unsigned int a;
        for(a = 0; a < na; a++)
        {
            if((*allClassNames)[a] == parrentClassName)
                break;
        }
        if(a >= na)
        {
            allClassNames->push_back(parrentClassName);
        }
    }
}

void DecisionTree::renumberClasses(std::vector <std::string>* allClassNames)
{
    allClassNames->clear();
    allClassNames->push_back("!");
    std::string currentClassName = (*allClassNames)[0];

    unsigned int ni = getRoot()->children.size();
    for(unsigned int i = 0; i < ni; i++)
    {
        renumberClassesRecursive(getRoot()->children[i], allClassNames, currentClassName);
    }
}

bool DecisionTree::renumberFeaturesRecursive(ParameterTreeItem<DecisionTreeElement>* item, std::vector <std::string>* allFeatureNames)
{
    DecisionTreeElement* ek = & item->data;
    if(ek->classifierai == NULL || ek->classLut == NULL) return true;

    if(! ek->classifierai->configureForClassification(allFeatureNames))
        return false;

    bool ret = true;
    unsigned int nb = item->children.size();

    for(unsigned int c = 0; c < nb; c++)
    {
        if(! renumberFeaturesRecursive(item->children[c], allFeatureNames))
            ret = false;
    }
    return ret;
}

bool DecisionTree::renumberFeatures(std::vector <std::string>* allFeatureNames)
{
    unsigned int ni = getRoot()->children.size();
    bool ret = true;
    for(unsigned int i = 0; i < ni; i++)
    {
        if(! renumberFeaturesRecursive(getRoot()->children[i], allFeatureNames))
            ret = false;
    }
    return ret;
}

bool DecisionTree::getFeatureNamesRecursive(ParameterTreeItem<DecisionTreeElement>* item, std::vector <std::string>* allFeatureNames)
{
    DecisionTreeElement* ek = & item->data;
    if(ek->classifierai == NULL || ek->classLut == NULL) return true;

    std::vector<std::string> newFeatureNames = ek->classifierai->getFeatureNames();
    unsigned int nfc = newFeatureNames.size();
    unsigned int c;
    for(c = 0; c < nfc ; c++)
    {
        unsigned int afc = allFeatureNames->size();
        unsigned int a;
        for(a = 0; a < afc; a++)
        {
            if((*allFeatureNames)[a] == newFeatureNames[c]) break;
        }
        allFeatureNames->push_back(newFeatureNames[c]);
    }

    bool ret = true;
    unsigned int nb = item->children.size();

    for(unsigned int c = 0; c < nb; c++)
    {
        if(! getFeatureNamesRecursive(item->children[c], allFeatureNames))
            ret = false;
    }
    return ret;
}

bool DecisionTree::getTreeFeatureNames(std::vector <std::string>* allFeatureNames)
{
    unsigned int ni = getRoot()->children.size();
    bool ret = true;
    for(unsigned int i = 0; i < ni; i++)
    {
        if(! getFeatureNamesRecursive(getRoot()->children[i], allFeatureNames)) ret = false;
    }
    return ret;
}
