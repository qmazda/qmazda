cmake_minimum_required(VERSION 3.1)
project(qmazda)

# Shared plugins
add_subdirectory(QSortPlugin)
add_subdirectory(LdaPlugin)
add_subdirectory(VschPlugin)
add_subdirectory(SvmPlugin)
add_subdirectory(DecisionTreePlugin)
add_subdirectory(PcaPlugin)

# Console programs
add_subdirectory(MzGenerator)
add_subdirectory(MzPredict)
add_subdirectory(MzTrainer)

# GUI applications
add_subdirectory(MzGengui)
add_subdirectory(MzMaps)
add_subdirectory(MzReport)
add_subdirectory(MaZda)

install(FILES "${PROJECT_SOURCE_DIR}/Config/defroi.txt" DESTINATION bin)
install(FILES "${PROJECT_SOURCE_DIR}/Config/defloc.txt" DESTINATION bin)
install(FILES "${PROJECT_SOURCE_DIR}/qmazda.pdf" DESTINATION bin)
install(FILES "${PROJECT_SOURCE_DIR}/icons/MZMain.png" DESTINATION bin)
install(FILES "${PROJECT_SOURCE_DIR}/icons/MZGenerator.png" DESTINATION bin)
install(FILES "${PROJECT_SOURCE_DIR}/icons/MZMaps.png" DESTINATION bin)
install(FILES "${PROJECT_SOURCE_DIR}/icons/MZReport.png" DESTINATION bin)
