/*
 * QMAZDA project: Image Analysis and Pattern Recognition
 *
 * Copyright 2013-2016 Piotr M. Szczypiński <piotr.szczypinski@p.lodz.pl>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef MZROIDOCK_H
#define MZROIDOCK_H

#include <QDockWidget>
#include <QDropEvent>
#include <QMimeData>
#include <QUrl>
#include "mainwindow.h"

class MzRoiDock : public QDockWidget
{
    Q_OBJECT
public:
//    explicit MzRoiDock(const QString &title, QWidget *parent = 0, Qt::WindowFlags flags = 0);
    explicit MzRoiDock(const QString &title, QWidget *parent = nullptr, Qt::WindowFlags flags = Qt::WindowFlags());
//    explicit MzRoiDock(QWidget *parent = 0, Qt::WindowFlags flags = 0);
    explicit MzRoiDock(QWidget *parent = nullptr, Qt::WindowFlags flags = Qt::WindowFlags());

protected:

    void dragEnterEvent(QDragEnterEvent* event);
    void dragMoveEvent(QDragMoveEvent* event);
    void dragLeaveEvent(QDragLeaveEvent* event);
    void dropEvent(QDropEvent* event);
    
signals:

public slots:

};
#endif // MZROIDOCK_H
