/*
 * QMAZDA project: Image Analysis and Pattern Recognition
 *
 * Copyright 2013-2016 Piotr M. Szczypiński <piotr.szczypinski@p.lodz.pl>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef PAINTER2D_H
#define PAINTER2D_H

#include "render2d.h"
#include "Painters/paintpencil.h"
#include "Painters/paintrect.h"
#include "Painters/paintelli.h"
#include "Painters/paintactive.h"
#include "Painters/paintfloodfill.h"
#include "Painters/paintpoly.h"
#include "Painters/paintmove.h"
#include "dockeditoptions.h"

class Painter2D : public Renderer2D
{
public:
    Painter2D(RenderingContainer2D* container, DockEditOptions* dockEditOptions, QWidget *parent);
    ~Painter2D();

    void paintEvent(QPaintEvent *ev);
    void mouseMoveEvent(QMouseEvent *ev);
    void mousePressEvent(QMouseEvent *ev);
    void wheelEvent(QWheelEvent *ev);
    void mouseReleaseEvent(QMouseEvent *ev);
    void mouseDoubleClickEvent(QMouseEvent *ev);
    void setDrawingTool(DrawingTool idrawingtool, bool ieraser);
private:
    void QImage2MzRoi(QImage* image, RenderingContainer2D::MzRoi* roi, QRect paintrect, bool erase);
    void createPainter(void);
    void deletePaintTools(void);
    void repaintAction(AfterPaintAction action);
    QRect paintrect;
    iPaint* painttoolobject;
    DrawingTool drawingtool;
    bool eraser;
    DockEditOptions* dockEditOptions;
protected:
    bool eventFilter(QObject* obj, QEvent* e);
};



#endif // PAINTER2D_H
