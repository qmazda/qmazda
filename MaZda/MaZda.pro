#-------------------------------------------------
#
# Project created by QtCreator 2013-02-04T18:11:42
#
#-------------------------------------------------

QT       += core gui opengl widgets

DESTDIR         = ../../Executables
TARGET = MaZda
TEMPLATE = app

# DEFINES += MAZDA_IMAGE_8BIT_PIXEL
# DEFINES += MAZDA_CONVERT_BGR_IMAGE
# DEFINES += MAZDA_HIDE_ON_PROGRESS
DEFINES += MAZDA_IN_MACOS_BUNDLE

SOURCES += main.cpp \
        mainwindow.cpp \
        render2d.cpp \
        render3d.cpp \
        renderarea.cpp \
        roilistwidget.cpp \
    Painters/paintpencil.cpp \
    painter2d.cpp \
    painter3d.cpp \
    Painters/paintelli.cpp \
    Painters/paintfloodfill.cpp \
    Painters/paintmove.cpp \
    Painters/paintpoly.cpp \
    Painters/paintrect.cpp \
    Painters/paintactive.cpp \
    Painters/paint3dactive.cpp \
    Painters/paint3dpen.cpp \
    Painters/paint3dball.cpp \
    Painters/paint3dblock.cpp \
    Painters/paint3dcube.cpp \
    Painters/paint3dfloodfill.cpp \
    Painters/paint3dmove.cpp \
    Painters/paint3dtube.cpp \
    Painters/sphear.cpp \
    featureswizard.cpp\
    randomnames.cpp \
    dockeditoptions.cpp \
    ../MzShared/parametertreewidget.cpp \
    dialogfeatureoptions.cpp \
    ../MzShared/parametertreetemplate.cpp \
    ../MzShared/filenameremap.cpp \
    dialogvoxelsize.cpp \
    mzroidock.cpp
    
HEADERS  += mainwindow.h \
    render2d.h \
    render3d.h \
    renderarea.h \
    renderingcontainer.hpp \
    renderingcontainer.h \
    roilistwidget.h \
    rendererinterface.h \
    ../SharedImage/mazdadummy.h \
    ../SharedImage/mazdaimage.h \
    ../SharedImage/mazdaimageio.h \
    ../SharedImage/mazdaroi.h \
    ../SharedImage/mazdaroiio.h \
    mzroidock.h \
    getsetslice.h \
    Painters/ipaint.h \
    Painters/paintpencil.h \
    painter2d.h \
    painter3d.h \
    Painters/paintelli.h \
    Painters/paintfloodfill.h \
    Painters/paintmove.h \
    Painters/paintpoly.h \
    Painters/paintrect.h \
    Painters/paint3dpen.h \
    Painters/paint3dball.h \
    Painters/paint3dblock.h \
    Painters/paint3dcube.h \
    Painters/paint3dfloodfill.h \
    Painters/paint3dmove.h \
    Painters/paint3dtube.h \
    Painters/sphear.h \
    featureswizard.h \
    randomnames.h \
    roitools.h \
    dockeditoptions.h \
    ../MzShared/parametertreewidget.h \
    dialogfeatureoptions.h \
    ../MzShared/parametertreetemplate.h \
    ../MzShared/filenameremap.h \
    ../MzShared/mzdefines.h \
    dialogvoxelsize.h \
    ../SharedImage/imagedefs.h

FORMS    += mainwindow.ui \
    dockeditoptions.ui \
    dialogfeatureoptions.ui \
    dialogvoxelsize.ui \
    featureswizard.ui

RESOURCES += \
    mazdares.qrc
    
INCLUDEPATH += ../MzShared

macx{
    ICON = mzimage.icns
}
else:unix{
 
}
else:win32{
    RC_FILE = MZMain.rc
    #RC_ICONS += icons/MZMain.ico
}

include(../Pri/config.pri)
include(../Pri/itk.pri)
include(../Pri/opengl.pri)
include(../Pri/opencv.pri)
include(../Pri/tiff.pri)

    
    
