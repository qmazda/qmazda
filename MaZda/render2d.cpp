/*
 * QMAZDA project: Image Analysis and Pattern Recognition
 *
 * Copyright 2013-2016 Piotr M. Szczypiński <piotr.szczypinski@p.lodz.pl>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <QPainter>
#include <QScrollArea>
#include <QScrollBar>
#include <QApplication>

#include "render2d.h"
#include "renderarea.h"

Renderer2DState Renderer2D::persistent_state;

void Renderer2D::initColor(unsigned int color)
{
    persistent_state.color = color;
}

Renderer2D::
Renderer2D(RenderingContainer2D* container, QWidget *parent) :
    QWidget(parent)
{
    state = persistent_state;
    this->container = container;
}
Renderer2D::~Renderer2D(void)
{
    persistent_state = state;
}

bool Renderer2D::SetContainer(RenderingContainerInterface* container)
{
    RenderingContainer2D* container2d = dynamic_cast<RenderingContainer2D*>(container);
    if(container2d == NULL) return false;
    this->container = container2d;
    Repaint();
    return true;
}
//void Renderer2D::SetZoom(double zoom)
//{
//    state.zoom_factor = zoom;
//    if(! screen_image.isNull()) resize(state.zoom_factor * screen_image.size());
//    update();
//}

void Renderer2D::SetZoom(double zoom)
{
    QWidget* papa = parentWidget();
    if(papa != NULL)
    {
        papa = papa->parentWidget();
        if(papa != NULL)
        {
            QScrollArea* pasa = qobject_cast<QScrollArea*>(papa);
            if(pasa != NULL)
            {
                int x, y;
                QScrollBar* sbh = pasa->horizontalScrollBar();
                QScrollBar* sbv = pasa->verticalScrollBar();
                if(sbh != NULL)
                {
                    int sw = sbh->value();
                    x = (sw + (width() - sbh->maximum())/2);
                    x = sw + (x/state.zoom_factor - x/zoom)*zoom;
                }
                if(sbh != NULL)
                {
                    int sh = sbv->value();
                    y = (sh + (height() - sbv->maximum())/2);
                    y = sh + (y/state.zoom_factor - y/zoom)*zoom;
                }
                resize(zoom * screen_image.size());
                if(sbh != NULL)
                    sbh->setValue(x);
                if(sbh != NULL)
                    sbv->setValue(y);
                state.zoom_factor = zoom;
            }
        }
    }
}

//void Renderer2D::wheelEvent(QWheelEvent *ev)
//{
//    ev->accept();
//    if(ev->delta() > 0)
//    {
//        emit setZoomRequest(-1);
//    }
//    else if(ev->delta() < 0)
//    {
//        emit setZoomRequest(+1);
//    }
//}

void Renderer2D::zoomKeepLocation(int x, int y, double previous_zoom, double current_zoom)
{
    if(screen_image.isNull())
        return;
    QWidget* papa = parentWidget();
    if(papa == NULL)
        return;
    papa = papa->parentWidget();
    if(papa == NULL)
        return;
    QScrollArea* pasa = qobject_cast<QScrollArea*>(papa);
    if(pasa == NULL)
        return;
    QScrollBar* sb = pasa->horizontalScrollBar();
    if(sb != NULL)
        sb->setValue(sb->value() + (x/previous_zoom - x/current_zoom)*current_zoom);
    sb = pasa->verticalScrollBar();
    if(sb != NULL)
        sb->setValue(sb->value() + (y/previous_zoom - y/current_zoom)*current_zoom);
}

void Renderer2D::wheelEvent(QWheelEvent *ev)
{
    if(QApplication::keyboardModifiers() & Qt::ControlModifier)
    {
        double previous_zoom = state.zoom_factor;
        ev->accept();
        //if(ev->delta() > 0)
        if(ev->angleDelta().y() > 0)
        {
            if(state.zoom_factor < 13.34)
            {
                state.zoom_factor *= 1.2;
                if(state.zoom_factor > 0.9 && state.zoom_factor < 1.1)
                    state.zoom_factor = 1.0;
                if(! screen_image.isNull())
                {
                    resize(state.zoom_factor * screen_image.size());
                    //zoomKeepLocation(ev->x(), ev->y(), previous_zoom, state.zoom_factor);
                    zoomKeepLocation(ev->position().x(), ev->position().y(), previous_zoom, state.zoom_factor);
                }
                emit setZoomRequest(state.zoom_factor);
                //update();
            }
        }
        //else if(ev->delta() < 0)
        else if(ev->angleDelta().y() < 0)
        {
            if(state.zoom_factor > 0.075)
            {
                state.zoom_factor /= 1.2;
                if(state.zoom_factor > 0.9 && state.zoom_factor < 1.1)
                    state.zoom_factor = 1.0;
                if(! screen_image.isNull())
                {
                    zoomKeepLocation(ev->position().x(), ev->position().y(), previous_zoom, state.zoom_factor);
                    resize(state.zoom_factor * screen_image.size());
                }
                emit setZoomRequest(state.zoom_factor);
                //update();
            }
        }
    }
    else
    {
        QWidget* papa = parentWidget();
        if(papa == NULL)
            return;
        papa = papa->parentWidget();
        if(papa == NULL)
            return;
        RenderArea* pasa = qobject_cast<RenderArea*>(papa);
        if(pasa == NULL)
            return;
        pasa->wheelEventSuperclass(ev);
    }
}


double Renderer2D::GetZoom(void)
{
    return state.zoom_factor;
}
void Renderer2D::SetBackground(unsigned int  color)
{
    state.color = color;
}
unsigned int Renderer2D::GetBackground(void)
{
    return state.color;
}
void Renderer2D::ResetView(void)
{
    state.Reset();
}
void Renderer2D::Repaint()
{
    container->update();
    RenderingContainer2D::MzRGBImage8* canvas = container->getRGBCanvas();
    if(canvas == NULL) return;
    unsigned int size[RenderingContainer2D::MzRGBImage8::Dimensions];
    canvas->GetSize(size);
    uchar* data = (uchar*)(canvas->GetDataPointer());
    screen_image = QImage(data, size[0], size[1], size[0]*3, QImage::Format_RGB888);
    if(! screen_image.isNull()) resize(state.zoom_factor * screen_image.size());
    //paintEvent(NULL);
    update();
}
void Renderer2D::paintEvent(QPaintEvent *e)
{
    QPainter painter(this);
    if(state.zoom_factor == 1.0)
    {
        painter.drawImage(screen_image.rect(), screen_image);
    }
    else
    {
        painter.drawImage(contentsRect(), screen_image);
    }
    //if(e != NULL)
    e->accept();
}

void Renderer2D::mouseReleaseEvent(QMouseEvent *ev)
{
}
void Renderer2D::mouseDoubleClickEvent(QMouseEvent *ev)
{
}
void Renderer2D::mousePressEvent(QMouseEvent *ev)
{
}
void Renderer2D::mouseMoveEvent(QMouseEvent *ev)
{
}


