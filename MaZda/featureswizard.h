#ifndef FEATURESWIZARD_H
#define FEATURESWIZARD_H

#include <QDialog>
#include "parametertreetemplate.h"


namespace Ui {
class FeaturesWizard;
}

class FeaturesWizard : public QDialog
{
    Q_OBJECT

public:
    explicit FeaturesWizard(NameStubTree *featemp, QStringList *featlist, bool local, QWidget *parent = nullptr);
    ~FeaturesWizard();

private slots:
    void on_lineEditCount_textChanged(const QString &arg1);

    void on_comboBoxLbpPattern_currentIndexChanged(int index);

    void on_pushButtonReplace_clicked();

    void on_pushButtonAppend_clicked();

    void on_pushButtonCancel_clicked();

private:
    QStringList *featlist;
    NameStubTree* featemp;
    bool local;
    Ui::FeaturesWizard *ui;
//    unsigned int countChannels();
//    unsigned int countTexture();
//    unsigned int countMorpho();
    int appendGroup(QString* stub, QStringList* features);
    int makeListColors(QStringList *features);
    int makeList(QStringList *features);
};

#endif // FEATURESWIZARD_H
