/*
 * QMAZDA project: Image Analysis and Pattern Recognition
 *
 * Copyright 2013-2016 Piotr M. Szczypiński <piotr.szczypinski@p.lodz.pl>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "roilistwidget.h"

#include <QStyledItemDelegate>

RoiListWidget::RoiListWidget(QWidget *parent) :
    QListWidget(parent)
{
}

QRect RoiListWidget::getIconRect(QListWidgetItem *item)
{
    //QRect r1;
    QRect r2;
    //QRect r3;

    int width = style()->pixelMetric(QStyle::PM_IndicatorWidth);
    int height = style()->pixelMetric(QStyle::PM_IndicatorHeight);
    //QStyleOptionViewItemV4 opt;
    QStyleOptionViewItem opt;
    //QStyleOptionButton opt;
//    opt.QStyleOption::operator=(viewOptions());
//    opt.QStyleOptionViewItemV4::operator =(viewOptions());
    //((QStyledItemDelegate*)itemDelegate())->initStyleOption(&opt, indexFromItem(item));
    opt.rect = visualItemRect(item);
    //opt.decorationPosition = QStyleOptionViewItem::Left;
    opt.decorationSize = QSize(width, height);
    //opt.viewItemPosition = QStyleOptionViewItem::Middle;
    //opt.decorationAlignment = Qt::TextSingleLine;

    //opt.text = QString("Bolek");
    opt.features = opt.HasCheckIndicator | opt.HasDisplay | opt.HasDecoration;
    //r1 = style()->subElementRect(QStyle::SE_ViewItemCheckIndicator, &opt);
    //r1 = style()->subElementRect(QStyle::SE_ItemViewItemCheckIndicator, &opt);
    r2 = style()->subElementRect(QStyle::SE_ItemViewItemDecoration, &opt);
    //r3 = style()->subElementRect(QStyle::SE_ItemViewItemText, &opt);
    return r2;
}
