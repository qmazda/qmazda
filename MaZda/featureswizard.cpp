#include "featureswizard.h"
#include "ui_featureswizard.h"
#include <QListView>

FeaturesWizard::FeaturesWizard(NameStubTree* featemp, QStringList *featlist, bool local, QWidget *parent) :
    QDialog(parent),
    ui(new Ui::FeaturesWizard)
{
    ui->setupUi(this);
    this->local = local;
    this->featemp = featemp;
    this->featlist = featlist;
    if(local)
    {
        ui->checkBoxMorLegacy->setChecked(false);
        ui->checkBoxMorOpenCV->setChecked(false);
        ui->checkBoxMorITK->setChecked(false);
        ui->tabWidget->removeTab(6);
    }
    else
    {
        ui->tabWidget->removeTab(0);
        ui->radioButtonNormImage->setDisabled(true);
    }
    on_lineEditCount_textChanged(nullptr);
}

FeaturesWizard::~FeaturesWizard()
{
    delete ui;
}
/*
unsigned int FeaturesWizard::countChannels()
{
    unsigned int count = 0;
    if(ui->checkBoxY->isChecked()) count++;
    if(ui->checkBoxR->isChecked()) count++;
    if(ui->checkBoxG->isChecked()) count++;
    if(ui->checkBoxB->isChecked()) count++;
    if(ui->checkBoxU->isChecked()) count++;
    if(ui->checkBoxV->isChecked()) count++;
    if(ui->checkBoxH->isChecked()) count++;
    if(ui->checkBoxS->isChecked()) count++;
    if(ui->checkBoxI->isChecked()) count++;
    if(ui->checkBoxQ->isChecked()) count++;
    if(ui->checkBoxu->isChecked()) count++;
    if(ui->checkBoxv->isChecked()) count++;
    if(ui->checkBoxi->isChecked()) count++;
    if(ui->checkBoxh->isChecked()) count++;
    if(ui->checkBoxx->isChecked()) count++;
    if(ui->checkBoxy->isChecked()) count++;
    if(ui->checkBoxz->isChecked()) count++;
    if(ui->checkBoxL->isChecked()) count++;
    if(ui->checkBoxa->isChecked()) count++;
    if(ui->checkBoxb->isChecked()) count++;
    return count;
}
unsigned int FeaturesWizard::countMorpho()
{
    unsigned int count = 0;
    if(ui->checkBoxMorLegacy->isChecked()) count+=73;
    if(ui->checkBoxMorOpenCV->isChecked()) count+=31;
    if(ui->checkBoxMorITK->isChecked()) count+=22;
    return count;
}
unsigned int FeaturesWizard::countTexture()
{
    unsigned int d_mul = 0;
    unsigned int o_mul = 0;
    unsigned int count = 0;

    if(ui->checkBoxGlcm1->isChecked()) o_mul++;
    if(ui->checkBoxGlcm2->isChecked()) o_mul++;
    if(ui->checkBoxGlcm3->isChecked()) o_mul++;
    if(ui->checkBoxGlcm4->isChecked()) o_mul++;
    if(ui->checkBoxGlcm5->isChecked()) o_mul++;
    if(ui->checkBoxGlcmDistance->isChecked()) o_mul++;
    if(ui->checkBoxGlcmH->isChecked()) d_mul++;
    if(ui->checkBoxGlcmV->isChecked()) d_mul++;
    if(ui->checkBoxGlcmZ->isChecked()) d_mul++;
    if(ui->checkBoxGlcmN->isChecked()) d_mul++;
    if(ui->checkBoxGlcmX->isChecked()) d_mul++;
    if(ui->checkBoxAlgHist->isChecked()) count+=14;
    if(ui->checkBoxAlgARM->isChecked()) count+=5;
    if(ui->checkBoxAlgGrad->isChecked()) count+=6;
    if(ui->checkBoxAlgGRLM->isChecked()) count+=o_mul*7;
    if(ui->checkBoxAlgGLCM->isChecked()) count+=(o_mul*d_mul*14);
    if(ui->checkBoxAlgGLCH->isChecked()) count+=(o_mul*d_mul*14);
    if(ui->checkBoxAlgLBP->isChecked())
    {
        unsigned int lbp_count = 0;
        if(ui->comboBoxLbpPattern->currentIndex() == 0)//Oc
        {
            if(ui->comboBoxLbpNeighbors->currentIndex() == 0) lbp_count = 16;
            else lbp_count = 256;
        }
        else if(ui->comboBoxLbpPattern->currentIndex() == 1)//Tr
        {
            if(ui->comboBoxLbpNeighbors->currentIndex() == 0) lbp_count = 15;
            else lbp_count = 255;
        }
        else //Cs
        {
            if(ui->comboBoxLbpNeighbors->currentIndex() == 0) lbp_count = 4;
            else if(ui->comboBoxLbpNeighbors->currentIndex() == 1) lbp_count = 8;
            else lbp_count = 12;
        }
        count += lbp_count;
    }
    if(ui->checkBoxAlgHOG->isChecked())
    {
        unsigned int hog_count = 0;
        if(ui->comboBoxHogBins->currentIndex() == 0) hog_count = 4;//Oc
        else if(ui->comboBoxHogBins->currentIndex() == 1) hog_count = 8;//Oc
        else if(ui->comboBoxHogBins->currentIndex() == 2) hog_count = 16;//Oc
        else hog_count = 32;//Oc
        count += hog_count;
    }
    if(ui->checkBoxAlgHaar->isChecked())
    {
        o_mul = 0;
        d_mul = 0;
        if(ui->checkBoxHaar1->isChecked()) d_mul++;
        if(ui->checkBoxHaar2->isChecked()) d_mul++;
        if(ui->checkBoxHaar3->isChecked()) d_mul++;
        if(ui->checkBoxHaar4->isChecked()) d_mul++;
        if(ui->checkBoxHaar5->isChecked()) d_mul++;
        if(ui->checkBoxHaar6->isChecked()) d_mul++;
        if(ui->checkBoxHaar7->isChecked()) d_mul++;
        if(ui->checkBoxHaar8->isChecked()) d_mul++;
        if(ui->checkBoxHaarHH->isChecked()) o_mul++;
        if(ui->checkBoxHaarHL->isChecked()) o_mul++;
        if(ui->checkBoxHaarLH->isChecked()) o_mul++;
        if(ui->checkBoxHaarLL->isChecked()) o_mul++;
        count += (o_mul*d_mul);
    }
    if(ui->checkBoxAlgGabor->isChecked())
    {
        o_mul = 0;
        d_mul = 0;
        if(ui->checkBoxGabH->isChecked()) d_mul++;
        if(ui->checkBoxGabZ->isChecked()) d_mul++;
        if(ui->checkBoxGabV->isChecked()) d_mul++;
        if(ui->checkBoxGabN->isChecked()) d_mul++;
        if(ui->checkBoxGabU->isChecked()) d_mul++;
        if(ui->checkBoxGabS->isChecked()) d_mul++;
        if(ui->checkBoxGabG->isChecked()) d_mul++;
        if(ui->checkBoxGabJ->isChecked()) d_mul++;
        if(ui->checkBoxGabX->isChecked()) d_mul++;
        if(ui->checkBoxGabO->isChecked()) d_mul++;
        if(ui->checkBoxGabMag->isChecked()) o_mul++;
        if(ui->checkBoxGabPhase->isChecked()) o_mul++;
        if(ui->checkBoxGabRe->isChecked()) o_mul++;
        if(ui->checkBoxGabIm->isChecked()) o_mul++;
        count += (o_mul*d_mul);
    }
    return count;
}
*/
void FeaturesWizard::on_lineEditCount_textChanged(const QString &arg1)
{
    ui->lineEditCount->blockSignals(true);
    int count = makeList(nullptr);
    //unsigned int count = countChannels()*countTexture();
    //count += countMorpho();
    ui->lineEditCount->setText(QString::number(count));
    ui->lineEditCount->blockSignals(false);
}


int FeaturesWizard::appendGroup(QString* stub, QStringList* features)
{
    ParameterTreeItem<NameStub>* matched = nullptr;
    featemp->doesItMatch(stub->toStdString().c_str(), &matched);
    if(matched == nullptr)
        return 0;
    size_t cmax = (*matched).children.size();
    if(cmax < 1)
        return 0;
    int count = 0;
    for(int c = 0; c < cmax; c++)
    {
        if((*matched).children[c]->data.type == NSTName)
        {
            if((*matched).children[c]->data.list.size() > 0)
            {
                if(features != nullptr)
                {
//                    printf("%s %s\n", morphoFeatures[i].toStdString().c_str(), (*matched).children[c]->data.list[0].c_str());
//                    fflush(stdout);
                    QString s(stub->toStdString().c_str());
                    s.append((*matched).children[c]->data.list[0].c_str());
                    features->append(s);
                }
                count ++;
            }
        }
        if((*matched).children[c]->data.type == NSTRange)
        {
            int imax = (*matched).children[c]->data.max;
            for(int i = (*matched).children[c]->data.min; i <= imax; i++)
            {
                if(features != nullptr)
                {
                    QString s(stub->toStdString().c_str());
                    s.append(QString::number(i));
                    features->append(s);
                }
                count ++;
            }
        }
    }
    return count;
}

int FeaturesWizard::makeListColors(QStringList* features)
{
    int count = 0;

    QStringList colorChannels;
    if(ui->checkBoxY->isChecked()) colorChannels.append("Y");
    if(ui->checkBoxR->isChecked()) colorChannels.append("R");
    if(ui->checkBoxG->isChecked()) colorChannels.append("G");
    if(ui->checkBoxB->isChecked()) colorChannels.append("B");
    if(ui->checkBoxU->isChecked()) colorChannels.append("U");
    if(ui->checkBoxV->isChecked()) colorChannels.append("V");
    if(ui->checkBoxH->isChecked()) colorChannels.append("H");
    if(ui->checkBoxS->isChecked()) colorChannels.append("S");
    if(ui->checkBoxI->isChecked()) colorChannels.append("I");
    if(ui->checkBoxQ->isChecked()) colorChannels.append("Q");
    if(ui->checkBoxu->isChecked()) colorChannels.append("u");
    if(ui->checkBoxv->isChecked()) colorChannels.append("v");
    if(ui->checkBoxi->isChecked()) colorChannels.append("i");
    if(ui->checkBoxh->isChecked()) colorChannels.append("h");
    if(ui->checkBoxx->isChecked()) colorChannels.append("x");
    if(ui->checkBoxy->isChecked()) colorChannels.append("y");
    if(ui->checkBoxz->isChecked()) colorChannels.append("z");
    if(ui->checkBoxL->isChecked()) colorChannels.append("L");
    if(ui->checkBoxa->isChecked()) colorChannels.append("a");
    if(ui->checkBoxb->isChecked()) colorChannels.append("b");

    QStringList haralicDistances;
    if(ui->checkBoxGlcm1->isChecked()) haralicDistances.append("1");
    if(ui->checkBoxGlcm2->isChecked()) haralicDistances.append("2");
    if(ui->checkBoxGlcm3->isChecked()) haralicDistances.append("3");
    if(ui->checkBoxGlcm4->isChecked()) haralicDistances.append("4");
    if(ui->checkBoxGlcm5->isChecked()) haralicDistances.append("5");
    if(ui->checkBoxGlcmDistance->isChecked()) haralicDistances.append(ui->spinBoxGlcmDistance->text());

    QStringList haralicDirections;
    if(ui->checkBoxGlcmH->isChecked()) haralicDirections.append("H");
    if(ui->checkBoxGlcmV->isChecked()) haralicDirections.append("V");
    if(ui->checkBoxGlcmZ->isChecked()) haralicDirections.append("Z");
    if(ui->checkBoxGlcmN->isChecked()) haralicDirections.append("N");
    if(ui->checkBoxGlcmX->isChecked()) haralicDirections.append("X");

    QStringList haarScales;
    if(ui->checkBoxHaar1->isChecked()) haarScales.append("1");
    if(ui->checkBoxHaar2->isChecked()) haarScales.append("2");
    if(ui->checkBoxHaar3->isChecked()) haarScales.append("3");
    if(ui->checkBoxHaar4->isChecked()) haarScales.append("4");
    if(ui->checkBoxHaar5->isChecked()) haarScales.append("5");
    if(ui->checkBoxHaar6->isChecked()) haarScales.append("6");
    if(ui->checkBoxHaar7->isChecked()) haarScales.append("7");
    if(ui->checkBoxHaar8->isChecked()) haarScales.append("8");

    QStringList haarBands;
    if(ui->checkBoxHaarHH->isChecked()) haarBands.append("HH");
    if(ui->checkBoxHaarHL->isChecked()) haarBands.append("HL");
    if(ui->checkBoxHaarLH->isChecked()) haarBands.append("LH");
    if(ui->checkBoxHaarLL->isChecked()) haarBands.append("LL");

    QStringList gaborDirections;
    if(ui->checkBoxGabH->isChecked()) gaborDirections.append("H");
    if(ui->checkBoxGabZ->isChecked()) gaborDirections.append("Z");
    if(ui->checkBoxGabV->isChecked()) gaborDirections.append("V");
    if(ui->checkBoxGabN->isChecked()) gaborDirections.append("N");
    if(ui->checkBoxGabU->isChecked()) gaborDirections.append("U");
    if(ui->checkBoxGabS->isChecked()) gaborDirections.append("S");
    if(ui->checkBoxGabG->isChecked()) gaborDirections.append("G");
    if(ui->checkBoxGabJ->isChecked()) gaborDirections.append("J");
    if(ui->checkBoxGabX->isChecked()) gaborDirections.append("X");
    if(ui->checkBoxGabO->isChecked()) gaborDirections.append("O");

    QStringList gaborFeatures;
    if(ui->checkBoxGabMag->isChecked()) gaborFeatures.append("Mag");
    if(ui->checkBoxGabPhase->isChecked()) gaborFeatures.append("Phase");
    if(ui->checkBoxGabRe->isChecked()) gaborFeatures.append("Re");
    if(ui->checkBoxGabIm->isChecked()) gaborFeatures.append("Im");

    QString windowStub;
    QString normStub;
    if(local)
    {
        if(ui->radioButtonSquare->isChecked())
            windowStub.append("s");
        else
            windowStub.append("c");
        windowStub.append(ui->spinBoxWindowRadius->text());

        if(! ui->radioButtonNormImage->isChecked())
        {
            normStub.append(windowStub);
        }
    }
    if(ui->radioButtonNormN->isChecked()) normStub.append("N");
    else if(ui->radioButtonNormS->isChecked()) normStub.append("S");
    else if(ui->radioButtonNormM->isChecked()) normStub.append("M");
    else normStub.append("D");
    normStub.append(ui->spinBox->text());
    if(local)
    {
        if(ui->radioButtonNormImage->isChecked())
        {
            normStub.append(windowStub);
        }
    }

    for(int c = 0; c < colorChannels.count(); c++)
    {
        if(ui->checkBoxAlgHist->isChecked())
        {
            QString stub;
            stub.append(colorChannels[c]);
            stub.append(normStub);
            stub.append("Hist");
            count += appendGroup(&stub, features);
        }
        if(ui->checkBoxAlgGrad->isChecked())
        {
            QString stub;
            stub.append(colorChannels[c]);
            stub.append(normStub);
            stub.append("Grad");
            count += appendGroup(&stub, features);
        }
        if(ui->checkBoxAlgARM->isChecked())
        {
            QString stub;
            stub.append(colorChannels[c]);
            stub.append(normStub);
            stub.append("Arm");
            count += appendGroup(&stub, features);
        }
        if(ui->checkBoxAlgGRLM->isChecked())
        {
            for(int d = 0; d < haralicDirections.count(); d++)
            {
                QString stub;
                stub.append(colorChannels[c]);
                stub.append(normStub);
                stub.append("Grlm");
                stub.append(haralicDirections[d]);
                count += appendGroup(&stub, features);
            }
        }
        if(ui->checkBoxAlgGLCM->isChecked())
        {
            for(int d = 0; d < haralicDirections.count(); d++)
            {
                for(int dd = 0; dd < haralicDistances.count(); dd++)
                {
                    QString stub;
                    stub.append(colorChannels[c]);
                    stub.append(normStub);
                    stub.append("Glcm");
                    stub.append(haralicDirections[d]);
                    stub.append(haralicDistances[dd]);
                    count += appendGroup(&stub, features);
                }
            }
        }
        if(ui->checkBoxAlgGLCH->isChecked())
        {
            for(int d = 0; d < haralicDirections.count(); d++)
            {
                for(int dd = 0; dd < haralicDistances.count(); dd++)
                {
                    QString stub;
                    stub.append(colorChannels[c]);
                    stub.append(normStub);
                    stub.append("Glch");
                    stub.append(haralicDirections[d]);
                    stub.append(haralicDistances[dd]);
                    count += appendGroup(&stub, features);
                }
            }
        }
        if(ui->checkBoxAlgLBP->isChecked())
        {
            QString stub;
            stub.append(colorChannels[c]);
            if(local)
                stub.append(windowStub);
            stub.append("Lbp");
            stub.append(ui->comboBoxLbpPattern->currentText().section(' ', 0, 0));
            stub.append(ui->comboBoxLbpNeighbors->currentText());
            count += appendGroup(&stub, features);
        }
        if(ui->checkBoxAlgHOG->isChecked())
        {
            QString stub;
            stub.append(colorChannels[c]);
            stub.append(normStub);
            stub.append("Hog");
            stub.append(ui->comboBoxHogBins->currentText());
            count += appendGroup(&stub, features);
        }
        if(ui->checkBoxAlgHaar->isChecked())
        {
            for(int d = 0; d < haarScales.count(); d++)
            {
                for(int dd = 0; dd < haarBands.count(); dd++)
                {
                    if(features != nullptr)
                    {
                        QString stub;
                        stub.append(colorChannels[c]);
                        stub.append(normStub);
                        stub.append("DwtHaarS");
                        stub.append(haarScales[d]);
                        stub.append(haarBands[dd]);
                        features->append(stub);
                    }
                    count ++;
                }
            }
        }
        if(ui->checkBoxAlgGabor->isChecked())
        {
            for(int d = 0; d < gaborDirections.count(); d++)
            {
                for(int dd = 0; dd < gaborFeatures.count(); dd++)
                {
                    if(features != nullptr)
                    {
                        QString stub;
                        stub.append(colorChannels[c]);
                        stub.append(normStub);
                        stub.append("Gab");
                        stub.append(ui->spinBoxGabEnvelope->text());
                        stub.append(gaborDirections[d]);
                        stub.append(ui->spinBoxGabPeriod->text());
                        stub.append(gaborFeatures[dd]);
                        features->append(stub);
                    }
                    count ++;
                }
            }
        }
    }
    return count;
}

int FeaturesWizard::makeList(QStringList* features)
{
    int count = 0;
    count += makeListColors(features);

    if(!local)
    {
        QStringList morphoFeatures;
        if(ui->checkBoxMorLegacy->isChecked()) morphoFeatures.append("MorMz");
        if(ui->checkBoxMorOpenCV->isChecked()) morphoFeatures.append("MorCv");
        if(ui->checkBoxMorITK->isChecked()) morphoFeatures.append("MorItk");
        int imax = morphoFeatures.count();
        for(int i = 0; i < imax; i++)
            count += appendGroup(&(morphoFeatures[i]), features);
    }
    return count;
}

void FeaturesWizard::on_comboBoxLbpPattern_currentIndexChanged(int index)
{
    if(index == 2 || index == 1)
    {
        if(ui->comboBoxLbpNeighbors->currentIndex() == 2)
            ui->comboBoxLbpNeighbors->setCurrentIndex(1);
        QListView* view = qobject_cast<QListView *>(ui->comboBoxLbpNeighbors->view());
        view->setRowHidden(2, true);
    }
    else if(index == 0)
    {
        QListView* view = qobject_cast<QListView *>(ui->comboBoxLbpNeighbors->view());
        view->setRowHidden(2, false);
    }
}

void FeaturesWizard::on_pushButtonReplace_clicked()
{
    QStringList features;
    //int count =
    makeList(&features);
    featlist->clear();
    *featlist = features;
/*
    printf("Count = %i\n", count);
    for(int i = 0; i < features.count(); i++)
        printf("%s\n", features[i].toStdString().c_str());
    printf("Count = %i\n", count);
    fflush(stdout);
*/
    Qt::KeyboardModifiers modif = QApplication::keyboardModifiers();
    if(!(modif & Qt::ControlModifier))
        this->close();
}

void FeaturesWizard::on_pushButtonAppend_clicked()
{
    QStringList features;
    makeList(&features);
    featlist->append(features);
    featlist->removeDuplicates();
    Qt::KeyboardModifiers modif = QApplication::keyboardModifiers();
    if(!(modif & Qt::ControlModifier))
        this->close();
}

void FeaturesWizard::on_pushButtonCancel_clicked()
{
    this->close();
}

