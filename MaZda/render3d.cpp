/*
 * QMAZDA project: Image Analysis and Pattern Recognition
 *
 * Copyright 2013-2016 Piotr M. Szczypiński <piotr.szczypinski@p.lodz.pl>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */



#define _USE_MATH_DEFINES
#include <math.h>
#ifdef _WIN32
#include <windows.h>
#endif
#include "render3d.h"
#include <GL/glu.h> //OSX
#ifdef _WIN32
#define lround(x) int(x+0.5)
#endif

//osx test
#include <QWindow>
#include "../MzShared/qt5qt6compatibility.h"

const static double sina = 3.48994967025009716459951816253e-2;
const static double cosa = 0.99939082701909573000624344004393;

Renderer3DState Renderer3D::persistent_state;

void Renderer3D::initColor(unsigned int color)
{
    persistent_state.color = color;
}



double Renderer3D::getRatio(void)
{
#if (QT_VERSION >= QT_VERSION_CHECK(5, 6, 0))
    return devicePixelRatioF();
#elif (QT_VERSION >= QT_VERSION_CHECK(5, 0, 0))
    QWindow* whnd = windowHandle();
    if(whnd != nullptr)
        return whnd->devicePixelRatio();
#endif
    return 1.0;
}


Renderer3D::Renderer3D(RenderingContainer3D* container, QWidget *parent) : QOpenGLWidget(parent), QOpenGLFunctions()
//    QGLWidget(parent)
{
    state = persistent_state;
    showmodellist = false;
    SetContainer(container);
}
Renderer3D::~Renderer3D(void)
{
    persistent_state = state;
}
bool Renderer3D::SetContainer(RenderingContainerInterface *container)
{
//    lastdirectionofvolume = 0;
    repaintvolume = true;
    RenderingContainer3D* container3d = dynamic_cast<RenderingContainer3D*>(container);
    if(container3d == NULL) return false;
    this->container = container3d;
    this->container->getSize(image_size);
    this->container->getSpacing(voxel_spacing);
    double div = 0;
    for(unsigned int d = 0; d < 3 && d < RenderingContainer3D::MzRGBImage8::Dimensions; d++)
    {
        double dd = image_size[d]*voxel_spacing[d];
        div += (dd*dd);
    }
    div = sqrt(div);
    div = 100.0/div;
    for(unsigned int d = 0; d < RenderingContainer3D::MzRGBImage8::Dimensions; d++)
    {
        voxel_spacing[d] *= div;
    }
    return true;
}
void Renderer3D::SetZoom(double zoom)
{
    state.zoom_factor = zoom;
    state.volumeMatrix[14] = -CENTER_OF_PROJECTION_DISTANCE/sqrt(state.zoom_factor);
    update();
}
double Renderer3D::GetZoom(void)
{
    return state.zoom_factor;
}
void Renderer3D::SetBackground(unsigned int  color)
{
    state.color = color;
    glClearColor((float)((state.color>>16) & 0xff)/255.0,
                 (float)((state.color>>8) & 0xff)/255.0,
                 (float)((state.color) & 0xff)/255.0,
                 0.0f );
}
unsigned int Renderer3D::GetBackground(void)
{
    return state.color;
}
void Renderer3D::ResetView(void)
{
    state.Reset();
}
void Renderer3D::Repaint()
{
    //See Painter3D::Repaint() which takes over this function
    container->update();
    update();
    //paintGL();
}

void Renderer3D::wheelEvent(QWheelEvent *ev)
{
    ev->accept();

    if(QApplication::keyboardModifiers() & Qt::ShiftModifier)
    {
        if(QApplication::keyboardModifiers() & Qt::ControlModifier)
        {
            ev->accept();
            if(ev->angleDelta().y() > 0)
            {
                if(state.zoom_factor < 10.92)
                {
                    state.zoom_factor *= 1.4641;
                    if(state.zoom_factor > 0.95 && state.zoom_factor < 1.05)
                        state.zoom_factor = 1.0;
                    SetZoom(state.zoom_factor);
                    emit setZoomRequest(state.zoom_factor);
                }
            }
            else if(ev->angleDelta().y() < 0)
            {
                if(state.zoom_factor > 0.0915)
                {
                    state.zoom_factor /= 1.4641;
                    if(state.zoom_factor > 0.95 && state.zoom_factor < 1.05)
                        state.zoom_factor = 1.0;
                    SetZoom(state.zoom_factor);
                    emit setZoomRequest(state.zoom_factor);
                }
            }
        }
        else if(QApplication::keyboardModifiers() & Qt::AltModifier)
        {
            if(ev->angleDelta().y() > 0)
                MatrixUtils3D<GLdouble>::shift(8, 0, state.volumeMatrix);
            else if(ev->angleDelta().y() < 0)
                MatrixUtils3D<GLdouble>::shift(-8, 0, state.volumeMatrix);
            update();
        }
        else
        {
            if(ev->angleDelta().y() > 0)
                MatrixUtils3D<GLdouble>::shift(0, 8, state.volumeMatrix);
            else if(ev->angleDelta().y() < 0)
                MatrixUtils3D<GLdouble>::shift(0, -8, state.volumeMatrix);
            update();
        }
    }
    else
    {
        if(QApplication::keyboardModifiers() & Qt::ControlModifier)
        {
            ev->accept();
            if(ev->angleDelta().y() > 0)
            {
                if(state.zoom_factor < 14.545455)
                {
                    state.zoom_factor *= 1.1;
                    if(state.zoom_factor > 0.95 && state.zoom_factor < 1.05)
                        state.zoom_factor = 1.0;
                    SetZoom(state.zoom_factor);
                    emit setZoomRequest(state.zoom_factor);
                }
            }
            else if(ev->angleDelta().y() < 0)
            {
                if(state.zoom_factor > 0.06875)
                {
                    state.zoom_factor /= 1.1;
                    if(state.zoom_factor > 0.95 && state.zoom_factor < 1.05)
                        state.zoom_factor = 1.0;
                    SetZoom(state.zoom_factor);
                    emit setZoomRequest(state.zoom_factor);
                }
            }
        }
        else if(QApplication::keyboardModifiers() & Qt::AltModifier)
        {
            if(ev->angleDelta().y() > 0)
                MatrixUtils3D<GLdouble>::rotate(0.049087385, 0, 2, state.volumeMatrix);
            else if(ev->angleDelta().y() < 0)
                MatrixUtils3D<GLdouble>::rotate(-0.049087385, 0, 2, state.volumeMatrix);
            update();
        }
        else
        {
            if(ev->angleDelta().y() > 0)
                MatrixUtils3D<GLdouble>::rotate(0.049087385, 1, 2, state.volumeMatrix);
            else if(ev->angleDelta().y() < 0)
                MatrixUtils3D<GLdouble>::rotate(-0.049087385, 1, 2, state.volumeMatrix);
            update();
        }
    }


}
void Renderer3D::mouseReleaseEvent(QMouseEvent* /*ev*/){}
void Renderer3D::mouseDoubleClickEvent(QMouseEvent* /*ev*/){}
//void Renderer3D::keyPressEvent(QKeyEvent* /*ev*/){}
//void Renderer3D::keyReleaseEvent(QKeyEvent* /*ev*/){}

void Renderer3D::mousePressEvent(QMouseEvent *ev)
{
    mousex = _ev_position_x;
    mousey = _ev_position_y;
}
void Renderer3D::mouseMoveEvent(QMouseEvent *ev)
{
    if(QApplication::keyboardModifiers() & Qt::ShiftModifier)
    {
        MatrixUtils3D<GLdouble>::shift(_ev_position_x-mousex, mousey-_ev_position_y, state.volumeMatrix);
    }
    else
    {
        int width = size().width();
        int height = size().height();
        double dref = sqrt((double)height*height + (double)width*width);

        if(dref > 1)
        {
            double xcen = 2*_ev_position_x - width;
            double ycen = 2*_ev_position_y - height;

            double xsht = _ev_position_x-mousex;
            double ysht = _ev_position_y-mousey;

            double xn = xcen/dref;
            double yn = ycen/dref;
            double xnyn = sqrt(xn*xn + yn*yn);

            if(xnyn > 1.0)
            {
                xn /= xnyn;
                yn /= xnyn;
                xnyn = 1.0;
            }

            double rot = xn * ysht - yn * xsht;
            xsht += (yn*rot);
            ysht -= (xn*rot);

            MatrixUtils3D<GLdouble>::rotate((double)rot/100.0, 0, 1, state.volumeMatrix);
            MatrixUtils3D<GLdouble>::rotate(xsht/100.0, 0, 2, state.volumeMatrix);
            MatrixUtils3D<GLdouble>::rotate(-ysht/100.0, 1, 2, state.volumeMatrix);
        }
    }
    mousex = _ev_position_x;
    mousey = _ev_position_y;
    update();
    ev->accept();
}


void Renderer3D::getMatrix(double m[16])
{
    for(int i = 0; i < 16; i++) m[i] = state.volumeMatrix[i];
}
void Renderer3D::setMatrix(double m[16])
{
    for(int i = 0; i < 16; i++) state.volumeMatrix[i] = m[i];
    update();
}

void Renderer3D::initializeGL()
{
//    initializeGLFunctions();
    initializeOpenGLFunctions();
    GLfloat  diffuseLight[] = {0.7f, 0.7f, 0.7f, 1.0f };
    GLfloat  specular[] = { 0.7f, 0.7f, 0.7f, 1.0f};
    GLfloat  lightPos[] = { 500.0f, -2000.0f, 2000.0f, 1.0f };
    GLfloat  ambientLight[] = {0.3f, 0.3f, 0.3f, 1.0f };
    GLfloat  specref[] =  { 0.4f, 0.4f, 0.4f, 1.0f };

    glEnable(GL_DEPTH_TEST); // Wlaczenie metdy bufora glebokosci
    glEnable(GL_CULL_FACE); // Wlaczenie metody eliminowania niewidocznych scianek
    glEnable(GL_LIGHTING); // Wlaczenie metody obliczania oswietlenia
    //glEnable(GL_NORMALIZE); // Ustawienie automatycznego obliczania normalnych do powierzchni
    //glEnable(GL_AUTO_NORMAL);
    glEnable(GL_RESCALE_NORMAL);
    //glDisable(GL_NORMALIZE);
    //glDisable(GL_AUTO_NORMAL);
    // Przygotowanie i wlaczenie swiatla GL_LIGHT0
    //  glLightModelfv(GL_LIGHT_MODEL_AMBIENT,ambientLight);
    glLightfv(GL_LIGHT0,GL_AMBIENT,ambientLight);
    glLightfv(GL_LIGHT0,GL_DIFFUSE,diffuseLight);
    glLightfv(GL_LIGHT0,GL_SPECULAR,specular);
    glLightfv(GL_LIGHT0,GL_POSITION,lightPos);
    glEnable(GL_LIGHT0);

    // Ustawienie wlaciwosci odbijania swiatla dla materialu
    glEnable(GL_COLOR_MATERIAL);
    glColorMaterial(GL_FRONT, GL_AMBIENT_AND_DIFFUSE);
    glMaterialfv(GL_FRONT, GL_SPECULAR, specref);
    glMateriali(GL_FRONT,GL_SHININESS,64);
    glClearColor((float)((state.color>>16) & 0xff)/255.0,
                 (float)((state.color>>8) & 0xff)/255.0,
                 (float)((state.color) & 0xff)/255.0,
                 0.0f );
    glDisable(GL_BLEND);

    int list = glGenLists(7);
    volumelist[0] = list; list++;
    volumelist[1] = list; list++;
    volumelist[2] = list; list++;
    volumelist[3] = list; list++;
    volumelist[4] = list; list++;
    volumelist[5] = list; list++;
    modellist = list;
}
void Renderer3D::paintGL()
{
    if(size().height()<=0 || size().width()<=0) return;
    glColorMask(1, 1, 1, 1);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    paintGLSetMatrices();
    paintSingleGL();
    glFlush();
}

void Renderer3D::paintGLSetMatrices()
{
    GLdouble rangeh, rangev;
    double ratio = getRatio();
    int width = ratio*size().width();
    int height = ratio*size().height();

    rangeh = FRUSTUM_SIDE_TO_SIDE*sqrt((double)height/(double)width);
    rangev = FRUSTUM_SIDE_TO_SIDE*sqrt((double)width/(double)height);
    glViewport(0, 0, width, height);
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    double front = 30.0;//-state.modelViewMat[14] - 1000.0;
    double back = -state.volumeMatrix[14] + 200.0;
    glFrustum(-rangev, rangev, -rangeh, rangeh, front, back);
    glMatrixMode(GL_MODELVIEW);
    glLoadMatrixd(state.volumeMatrix);
    glClearColor((float)((state.color>>16) & 0xff)/255.0,
                 (float)((state.color>>8) & 0xff)/255.0,
                 (float)((state.color) & 0xff)/255.0,
                 0.0f );
}
void Renderer3D::paintSingleGL()
{
    bool something = false;
    for(int k = 0; k < 3; k++)
    {
        if(state.renderslice[k])
        {
            if(createCrossSection(k+1, state.sliceindex[k])) something = true;
        }
    }
    if(showmodellist)
    {
        glPushMatrix();
        glMultMatrixd(modelMatrix);
        glEnable(GL_NORMALIZE);
        glCallList(modellist);
        glPopMatrix();
        something = true;
    }
    if(state.rendervolume != 0)
    {
        glEnable(GL_BLEND);

// To dziala z modulowaniem kanalu alfa
//        glBlendFunc(GL_ONE, GL_ONE);
//        glColor4f(0.0f, 0.0f, 0.0f, 0.0f);


        glBlendFunc(GL_ONE, GL_ONE);
        //glBlendFunc(GL_SRC_ALPHA_SATURATE, GL_ONE);

//#ifdef _WIN32
        if(state.rendervolume > 0) glBlendEquation(GL_MAX);
        else glBlendEquation(GL_MIN);
//#else
//        if(state.rendervolume > 0) glBlendEquation(GL_MAX);
        //if(state.rendervolume > 0) ::glBlendEquation(GL_MAX);
//        else glBlendEquation(GL_MIN);
//#endif


        glColor4f(0.0f, 0.0f, 0.0f, 0.0f);

//        if(state.rendervolume >= 0)
//        {
//            glBlendFunc(GL_ONE, GL_ONE);
//            glBlendEquation(GL_FUNC_ADD);
//            glColor4f(0.0f, 0.0f, 0.0f, 0.0f);
//        }
//        else
//        {
//            glBlendFunc(GL_ONE, GL_ONE);
//            glBlendEquation(GL_FUNC_REVERSE_SUBTRACT);
//            glColor4f(0.0f, 0.0f, 0.0f, 0.0f);
//        }


        if(repaintvolume)
        {
            glNewList(volumelist[0], GL_COMPILE);
            createVolumetricView(1);
            glEndList();
            glNewList(volumelist[1], GL_COMPILE);
            createVolumetricView(2);
            glEndList();
            glNewList(volumelist[2], GL_COMPILE);
            createVolumetricView(3);
            glEndList();

            glNewList(volumelist[3], GL_COMPILE);
            createVolumetricView(-1);
            glEndList();
            glNewList(volumelist[4], GL_COMPILE);
            createVolumetricView(-2);
            glEndList();
            glNewList(volumelist[5], GL_COMPILE);
            createVolumetricView(-3);
            glEndList();
            repaintvolume = false;
        }


        int dirn = getFacingDirection();
        if(dirn != 0)
        {
            if(dirn > 0) glCallList(volumelist[dirn-1]);
            else glCallList(volumelist[2-dirn]);
            something = true;
        }


/*
        if(lastdirectionofvolume == dirn && glIsList(volumelist))
        {
            glCallList(volumelist);
            something = true;
        }
        else
        {
            glNewList(volumelist, GL_COMPILE_AND_EXECUTE);
            if(createVolumetricView(dirn)) something = true;
            glEndList();
        }
        lastdirectionofvolume = dirn;
*/
        glDisable(GL_BLEND);
    }

    if(!something)
    {
        glBegin(GL_LINES);
        glNormal3f(0, 0, 0);
        glVertex3f(0, 0, 0);
        glVertex3f(0.1, 0, 0);
        glVertex3f(0, 0, 0);
        glVertex3f(0, 0.2, 0);
        glVertex3f(0, 0, 0);
        glVertex3f(0, 0, 0.3);
        glEnd();
    }
}
void Renderer3D::setVolumeView(int view)
{
//    lastdirectionofvolume = 0;
    state.rendervolume = view;
    if(view != 0)
    {
        setRepaintVolume();
    }
    else
    {
        glNewList(volumelist[0], GL_COMPILE);
        glEndList();
        glNewList(volumelist[1], GL_COMPILE);
        glEndList();
        glNewList(volumelist[2], GL_COMPILE);
        glEndList();
        glNewList(volumelist[3], GL_COMPILE);
        glEndList();
        glNewList(volumelist[4], GL_COMPILE);
        glEndList();
        glNewList(volumelist[5], GL_COMPILE);
        glEndList();
    }
    update();
}
void Renderer3D::setRepaintVolume(void)
{
    repaintvolume = true;
//    lastdirectionofvolume = 0;

}
void Renderer3D::setCrossShow(int direction, bool show)
{
    if(abs(direction) > 3 || abs(direction) < 1) return;
    state.renderslice[abs(direction)-1] = show;
    update();
}
void Renderer3D::setCrossNumber(int direction, unsigned int index)
{
    if(abs(direction) > 3 || abs(direction) < 1) return;
    state.sliceindex[abs(direction-1)] = index;
    update();
}

int Renderer3D::getFacingDirection(void)
{
    int direction = 0;
    if(container->getRGBCanvas() == NULL) return direction;
    GLdouble x = fabs(state.volumeMatrix[2]);
    GLdouble y = fabs(state.volumeMatrix[6]);
    GLdouble z = fabs(state.volumeMatrix[10]);
    if(x>z && x>y)
    {
        if(state.volumeMatrix[2] > 0) direction = 1;
        else direction = -1;
    }
    else if(y>z && y>x)
    {
        if(state.volumeMatrix[6] > 0) direction = 2;
        else direction = -2;
    }
    else
    {
        if(state.volumeMatrix[10] > 0) direction = 3;
        else direction = -3;
    }
    return direction;
}

bool Renderer3D::fillCrossSection(GLubyte* bits, unsigned int stride, unsigned int direction, unsigned int sliceindex) //, bool noalpha)
{
    unsigned int stepx;
    unsigned int stepy;
    unsigned int sizex;
    unsigned int sizey;
    unsigned int y;
    uint8* start;

    uint8* image = (uint8*) container->getRGBCanvas()->GetDataPointer();

    unsigned int linesize = 3 * image_size[0];

    switch(direction)
    {
    case 1: //x
        if(sliceindex >= image_size[0]) return false;
        sizex = image_size[1];
        sizey = image_size[2];
        stepx = linesize;
        stepy = linesize*image_size[1];
        start = (uint8*)image + sliceindex*3;
        break;
    case 2: //y
        if(sliceindex >= image_size[1]) return false;
        sizex = image_size[0];
        sizey = image_size[2];
        stepx = 3;
        stepy = linesize*image_size[1];
        start = (uint8*)image + linesize*sliceindex;
        break;
    default: //z
        if(sliceindex >= image_size[2]) return false;
        sizex = image_size[0];
        sizey = image_size[1];
        stepx = 3;
        stepy = linesize;
        start = (uint8*)image + linesize*image_size[1]*sliceindex;
        break;
    }
    uint8* src;
    GLubyte* dst;

    unsigned int sx = stepx - 3;

    for(y = 0; y < sizey; y++)
    {
        dst = bits + y*stride;
        src = start + y*stepy;
        GLubyte* dstmax = dst + sizex*4;

        //for(x = 0; x < sizex; x++)
        while(dst < dstmax)
        {
            *dst = *src;
            dst++;
            src++;
            *dst = *src;
            dst++;
            src++;
            *dst = *src;
            dst++;
            src++;

            *dst = 255;
            dst++;

            src+=sx;
        }
    }

    return true;
}

bool Renderer3D::createCrossSection(unsigned int direction, unsigned int sliceindex)
{
    unsigned int i, d;
    if(container->getRGBCanvas() == NULL) return false;
    unsigned int sizeimg[2];
    //GLuint listindex;
    switch(direction)
    {
    case 1: //x
        //listindex = firstlist+1;
        sizeimg[0] = image_size[1];
        sizeimg[1] = image_size[2];
        break;
    case 2: //y
        //listindex = firstlist+2;
        sizeimg[0] = image_size[0];
        sizeimg[1] = image_size[2];
        break;
    default: //z
        //listindex = firstlist+3;
        sizeimg[0] = image_size[0];
        sizeimg[1] = image_size[1];
        break;
    }
    unsigned int sizetex[2];
    for(d = 0; d < 2; d++)
    {
        sizetex[d] = 1;
        for(i = 0; i < sizeof(unsigned int)*8; i++)
        {
            if(sizetex[d] >= sizeimg[d]) break;
            sizetex[d] <<= 1;
        }
    }

    unsigned int stride = sizetex[0]*4;
    GLubyte* bits = new GLubyte[sizetex[0]*sizetex[1]*4];

    glDisable(GL_CULL_FACE);
    fillCrossSection(bits, stride, direction, sliceindex/*, true*/);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
    glPixelStorei(GL_UNPACK_ALIGNMENT, 4);	// Force 4-byte alignment
    glPixelStorei(GL_UNPACK_ROW_LENGTH, 0);
    glPixelStorei(GL_UNPACK_SKIP_ROWS, 0);
    glPixelStorei(GL_UNPACK_SKIP_PIXELS, 0);
    glDisable(GL_TEXTURE_1D);
    glEnable(GL_TEXTURE_2D);
    glTexEnvi(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_DECAL);
    glFrontFace(GL_CCW);
    glNormal3f(0.0, 0.0, 0.0);
    renderCrossSection(direction, sliceindex, bits, sizeimg, sizetex);
    delete[] bits;
    return true;
}

void Renderer3D::renderCrossSection(unsigned int direction, unsigned int sliceindex, GLubyte* bits, unsigned int sizeimg[2], unsigned int sizetex[2])
{
    glTexImage2D(GL_TEXTURE_2D, 0, 4, sizetex[0], sizetex[1], 0, GL_RGBA, GL_UNSIGNED_BYTE, bits);
    glBegin(GL_QUADS);
    GLfloat sx, sy, sz;
    const GLfloat texture_margin = 0.1;
    switch(direction)
    {
    case 1: //x
        sx = voxel_spacing[0]*((GLfloat)sliceindex - (GLfloat)image_size[0]/2.0f + 0.5f);
        sy = voxel_spacing[1]*(GLfloat)image_size[1]/2.0f;
        sz = voxel_spacing[2]*(GLfloat)image_size[2]/2.0f;
        glTexCoord2f(texture_margin/sizetex[0], texture_margin/sizetex[1]);
        glVertex3f(sx, -sy, -sz);
        glTexCoord2f(((GLfloat)sizeimg[0]-texture_margin)/sizetex[0], texture_margin/sizetex[1]);
        glVertex3f(sx, sy, -sz);
        glTexCoord2f(((GLfloat)sizeimg[0]-texture_margin)/sizetex[0], ((GLfloat)sizeimg[1]-texture_margin)/sizetex[1]);
        glVertex3f(sx, sy, sz);
        glTexCoord2f(texture_margin/sizetex[0], ((GLfloat)sizeimg[1]-texture_margin)/sizetex[1]);
        glVertex3f(sx, -sy, sz);
        break;
    case 2: //y
        sx = voxel_spacing[0]*(GLfloat)image_size[0]/2.0f;
        sy = voxel_spacing[1]*((GLfloat)sliceindex - (GLfloat)image_size[1]/2.0f + 0.5f);
        sz = voxel_spacing[2]*(GLfloat)image_size[2]/2.0f;
        glTexCoord2f(texture_margin/sizetex[0], texture_margin/sizetex[1]);
        glVertex3f(-sx, sy, -sz);
        glTexCoord2f(((GLfloat)sizeimg[0]-texture_margin)/sizetex[0], texture_margin/sizetex[1]);
        glVertex3f(sx, sy, -sz);
        glTexCoord2f(((GLfloat)sizeimg[0]-texture_margin)/sizetex[0], ((GLfloat)sizeimg[1]-texture_margin)/sizetex[1]);
        glVertex3f(sx, sy, sz);
        glTexCoord2f(texture_margin/sizetex[0], ((GLfloat)sizeimg[1]-texture_margin)/sizetex[1]);
        glVertex3f(-sx, sy, sz);
        break;
    default: //z
        sx = voxel_spacing[0]*(GLfloat)image_size[0]/2.0f;
        sy = voxel_spacing[1]*(GLfloat)image_size[1]/2.0f;
        sz = voxel_spacing[2]*((GLfloat)sliceindex - (GLfloat)image_size[2]/2.0f + 0.5f);
        glTexCoord2f(texture_margin/sizetex[0], texture_margin/sizetex[1]);
        glVertex3f(-sx, -sy, sz);
        glTexCoord2f(((GLfloat)sizeimg[0]-texture_margin)/sizetex[0], texture_margin/sizetex[1]);
        glVertex3f(sx, -sy, sz);
        glTexCoord2f(((GLfloat)sizeimg[0]-texture_margin)/sizetex[0], ((GLfloat)sizeimg[1]-texture_margin)/sizetex[1]);
        glVertex3f(sx, sy, sz);
        glTexCoord2f(texture_margin/sizetex[0], ((GLfloat)sizeimg[1]-texture_margin)/sizetex[1]);
        glVertex3f(-sx, sy, sz);
        break;
    }
    glEnd();
}

bool Renderer3D::createVolumetricView(int direction)
{
    unsigned int i, d;
    if(container->getRGBCanvas() == NULL) return false;

    unsigned int sizeimg[3];
    //int direction = getFacingDirection();

    switch(abs(direction))
    {
    case 1: //x
        sizeimg[0] = image_size[1];
        sizeimg[1] = image_size[2];
        sizeimg[2] = image_size[0];
        break;
    case 2: //y
        sizeimg[0] = image_size[0];
        sizeimg[1] = image_size[2];
        sizeimg[2] = image_size[1];
        break;
    default: //z
        sizeimg[0] = image_size[0];
        sizeimg[1] = image_size[1];
        sizeimg[2] = image_size[2];
        break;
    }

    unsigned int sizetex[2];
    for(d = 0; d < 2; d++)
    {
        sizetex[d] = 1;
        for(i = 0; i < sizeof(unsigned int)*8; i++)
        {
            if(sizetex[d] >= sizeimg[d]) break;
            sizetex[d] <<= 1;
        }
    }
    glDisable(GL_CULL_FACE);

    unsigned int stride = sizetex[0]*4;
    GLubyte* bits = new GLubyte[stride*sizetex[1]];

    //glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
    glPixelStorei(GL_UNPACK_ALIGNMENT, 4);	// Force 4-byte alignment
    glPixelStorei(GL_UNPACK_ROW_LENGTH, 0);
    glPixelStorei(GL_UNPACK_SKIP_ROWS, 0);
    glPixelStorei(GL_UNPACK_SKIP_PIXELS, 0);
    glDisable(GL_TEXTURE_1D);
    glEnable(GL_TEXTURE_2D);
    glTexEnvi(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_DECAL);
    glFrontFace(GL_CCW);
    glNormal3f(0.0, 0.0, 0.0);


    unsigned int sliceindex, sliceindexstart, sliceindexmax, sliceindexstep;
    if(direction > 0)
    {
        sliceindexstart = 0;
        sliceindexstep = 1;
    }
    else
    {
        sliceindexstart = sizeimg[2]-1;
        sliceindexstep = -1;
    }
    sliceindexmax = sizeimg[2];
    for(sliceindex = sliceindexstart; sliceindex < sliceindexmax; sliceindex+=sliceindexstep)
    {
        fillCrossSection(bits, stride, abs(direction), sliceindex/*, false*/);
        renderCrossSection(abs(direction), sliceindex, bits, sizeimg, sizetex);
    }

    delete[] bits;
    return true;
}

