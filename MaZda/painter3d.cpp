/*
 * QMAZDA project: Image Analysis and Pattern Recognition
 *
 * Copyright 2013-2016 Piotr M. Szczypiński <piotr.szczypinski@p.lodz.pl>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "painter3d.h"
#include "Painters/paint3dpen.h"
#include "Painters/paint3dfloodfill.h"
#include "Painters/paint3dmove.h"
#include "Painters/paint3dball.h"
#include "Painters/paint3dcube.h"
#include "Painters/paint3dtube.h"
#include "Painters/paint3dactive.h"
#include <GL/gl.h> //OSX
#include <GL/glu.h>  //OSX

#include "../MzShared/qt5qt6compatibility.h"

Painter3D::Painter3D(RenderingContainer3D* container, DockEditOptions *editoptions, QWidget *parent)
    : Renderer3D(container, parent)
{
    eraser = false;
    drawingtool = DT_NONE;
    painttoolobject = NULL;
    MatrixUtils3D<GLdouble>::unit4x4(modelMatrix);
    qApp->installEventFilter(this);
    dockEditOptions = editoptions;
}

Painter3D::~Painter3D()
{
    deletePaintTools();
}

void Painter3D::Repaint()
{
    bool canvas_changing = (container->whatToUpdate() & FROMCHANNEL2GRAYSCALE);
    container->update();
    if(painttoolobject != NULL && canvas_changing)
        painttoolobject->imageDataChanged();
    update();
}


void Painter3D::showModel(bool show)
{
    showmodellist = show;
    Repaint();
}

void Painter3D::setDrawingTool(DrawingTool idrawingtool, bool ieraser)
{
    eraser = ieraser;

    if(idrawingtool != DT_IGNORE)
    {
        if(drawingtool != idrawingtool && painttoolobject != NULL)
        {
            repaintAction(CANCEL);
        }
        drawingtool = idrawingtool;

        switch(drawingtool)
        {
        case DT_PEN: setCursor(Qt::CrossCursor); break;
        case DT_RECT: setCursor(Qt::CrossCursor); break;
        case DT_ELLI: setCursor(Qt::CrossCursor); break;
        case DT_SNAKE: setCursor(Qt::CrossCursor); break;
        case DT_FILL: setCursor(Qt::CrossCursor); break;
        case DT_POLY: setCursor(Qt::CrossCursor); break;
        case DT_MOVE: setCursor(Qt::OpenHandCursor); break; //Qt::DragMoveCursor
        default: unsetCursor(); break;
        }
    }
}

bool Painter3D::eventFilter(QObject* /*obj*/, QEvent* e)
{
    static int lastkeyp = -1;
    static int lastkeyr = -1;
    static bool accepted = false;
    if(painttoolobject == NULL)
    {
        accepted = false;
        lastkeyr = -1;
        lastkeyp = -1;
        return false;
    }
    if (e->type()==QEvent::KeyPress)
    {
        lastkeyr = -1;
        QKeyEvent* ev = static_cast<QKeyEvent*>(e);
        if(!(ev->key() == lastkeyp))
        {
            lastkeyp = ev->key();
            ev->setAccepted(false);
            repaintAction(painttoolobject->keyPressEvent(ev));
            if(e->isAccepted())
            {
                accepted = true;
                return true;
            }
        }
        else if(accepted) return true;
    }
    else if(e->type()==QEvent::KeyRelease)
    {
        lastkeyp = -1;
        QKeyEvent* ev = static_cast<QKeyEvent*>(e);
        if(!(ev->key() == lastkeyr))
        {
            lastkeyr = ev->key();
            ev->setAccepted(false);
            repaintAction(painttoolobject->keyReleaseEvent(ev));
            if(e->isAccepted())
            {
                accepted = true;
                return true;
            }
        }
        else if(accepted) return true;
    }
    accepted = false;
    return false;
    //return QObject::eventFilter(obj, e);
}

void Painter3D::paintEvent(QPaintEvent *ev)
{
    Renderer3D::paintEvent(ev);
}

void Painter3D::mousePressEvent(QMouseEvent *ev)
{
    mousex = _ev_position_x;
    mousey = _ev_position_y;
    RenderingContainer3D::MzRoi* activeroi = container->getActiveRoi();
    ev->ignore();

    if(cursor().shape() == Qt::OpenHandCursor)
    {
        if(ev->button() == Qt::LeftButton)
            setCursor(Qt::DragMoveCursor);
    }

    //if(activeroi != NULL && (ev->buttons() & Qt::LeftButton))
    if(activeroi != NULL)
    {
        if(painttoolobject == NULL && (!(QApplication::keyboardModifiers() & Qt::ControlModifier)))
        {
            switch(drawingtool)
            {
            case DT_FILL:
                painttoolobject = new Paint3DFloodFill(container, this, &eraser);
                repaintAction(painttoolobject->mousePressEvent(ev));
                break;
            case DT_MOVE:
                MatrixUtils3D<GLdouble>::unit4x4(modelMatrix);
                painttoolobject = new Paint3DMove(container, this, modellist, modelMatrix, state.volumeMatrix, &eraser);
                repaintAction(painttoolobject->mousePressEvent(ev));
                break;
            case DT_PEN:
                painttoolobject = new Paint3DPen(container, this, &eraser);
                repaintAction(painttoolobject->mousePressEvent(ev));
                break;
            case DT_RECT:
                if(! (QApplication::keyboardModifiers() & Qt::ShiftModifier))
                    MatrixUtils3D<GLdouble>::unit4x4(modelMatrix);
                painttoolobject = new Paint3DCube(container, this, modellist, modelMatrix, state.volumeMatrix, &eraser);
                repaintAction(painttoolobject->mousePressEvent(ev));
                break;
            case DT_ELLI:
                if(! (QApplication::keyboardModifiers() & Qt::ShiftModifier))
                    MatrixUtils3D<GLdouble>::unit4x4(modelMatrix);
                painttoolobject = new Paint3DBall(container, this, modellist, modelMatrix, state.volumeMatrix, &eraser);
                repaintAction(painttoolobject->mousePressEvent(ev));
                break;
            case DT_SNAKE:
            {
                int r[4];
                r[0] = dockEditOptions->getValue("Snake:Smoothness").toInt();
                QString s = dockEditOptions->getValue("Snake:Region");
                if(s == "dark") r[1] = -1;
                else if(s == "bright") r[1] = 1;
                else r[1] = 0;
                s = dockEditOptions->getValue("Snake:Level");
                if(s == "detect") r[3] = 1;
                else r[3] = 0;
                r[2] = dockEditOptions->getValue("Snake:Density").toInt();

                if(! (QApplication::keyboardModifiers() & Qt::ShiftModifier))
                    MatrixUtils3D<GLdouble>::unit4x4(modelMatrix);
                painttoolobject = new Paint3DActiveSurface(container, r, this, modellist, modelMatrix, state.volumeMatrix, &eraser);
                repaintAction(painttoolobject->mousePressEvent(ev));
            }
                break;
            case DT_POLY:
                if(! (QApplication::keyboardModifiers() & Qt::ShiftModifier))
                    MatrixUtils3D<GLdouble>::unit4x4(modelMatrix);
                painttoolobject = new Paint3DTube(container, this, modellist, modelMatrix, state.volumeMatrix, &eraser);
                repaintAction(painttoolobject->mousePressEvent(ev));
                break;
            default: break;
            }
        }
        else if(painttoolobject != NULL)
        {
           repaintAction(painttoolobject->mousePressEvent(ev));
        }
    }
    if(! ev->isAccepted())
    {
        Renderer3D::mousePressEvent(ev);
    }
    ev->accept();
}

void Painter3D::mouseMoveEvent(QMouseEvent *ev)
{
    ev->ignore();
    if(painttoolobject != NULL)
    {
        repaintAction(painttoolobject->mouseMoveEvent(ev));
    }
    if(! ev->isAccepted())
    {
        Renderer3D::mouseMoveEvent(ev);
    }
    ev->accept();
}

void Painter3D::wheelEvent(QWheelEvent *ev)
{
    ev->ignore();
    if(painttoolobject != NULL)
    {
        repaintAction(painttoolobject->wheelEvent(ev));
    }
    if(! ev->isAccepted())
    {
        Renderer3D::wheelEvent(ev);
    }
    ev->accept();
}

void Painter3D::mouseReleaseEvent(QMouseEvent *ev)
{
    ev->ignore();
    if(painttoolobject != NULL)
    {
        repaintAction(painttoolobject->mouseReleaseEvent(ev));
    }
    if(cursor().shape() == Qt::DragMoveCursor)
    {
        if(ev->button() == Qt::LeftButton)
            setCursor(Qt::OpenHandCursor);
    }
    if(! ev->isAccepted())
    {
        Renderer3D::mouseReleaseEvent(ev);
    }
    ev->accept();
}

void Painter3D::mouseDoubleClickEvent(QMouseEvent *ev)
{
    ev->ignore();
    if(painttoolobject != NULL)
    {
        repaintAction(painttoolobject->mouseDoubleClickEvent(ev));
    }
    if(! ev->isAccepted())
    {
        Renderer3D::mouseDoubleClickEvent(ev);
    }
    ev->accept();
}

void Painter3D::deletePaintTools(void)
{
    if(painttoolobject == NULL) return;
    delete painttoolobject;
    painttoolobject = NULL;
}

void Painter3D::repaintAction(AfterPaintAction action)
{
    if(painttoolobject != NULL)
    {
        QString* info = painttoolobject->getStatus();
        if(info != NULL)
        {
            if(! info->isEmpty())
            {
                emit setStatusTextRequest(info);
                info->clear();
            }
        }
    }

    switch(action)
    {
    case REDRAW:
        update();
        break;

    case FINALIZE:
        container->setRoisRedraw();
        Repaint();
        deletePaintTools();
        container->setModified();
        emit setRoiListRequest();
        break;

    case CANCEL:
        showModel(false);
        update();
        deletePaintTools();
        break;

    case REFRESH:
        container->setRoisRedraw();
        Repaint();
        //imageutils->saveUndo();
        deletePaintTools();
        break;

    case DONOTHING:
        break;

    default:
        Repaint();
    }
}

/**
 * @brief Painter3D::getSize
 * @param size
 */
void Painter3D::getSize(unsigned int* size)
{
    for(int d = 0; d < 3; d++) size[d] = image_size[d];
}
/**
 * @brief Painter3D::getSpacing
 * @param spacing
 */
void Painter3D::getSpacing(double* spacing)
{
    for(int d = 0; d < 3; d++) spacing[d] = voxel_spacing[d];
}

/**
 * @brief Painter3D::MzImage2MzRoi scans an image region (beginr-endr) for voxels of specified color to create ROI (VOI)
 * @param beginr bottom-left-back vertex
 * @param endr top-right-front vertex
 * @param color color of mask to be converted to roi
 * @param erase eraser mode set by user
 */
void Painter3D::MzImage2MzRoi(int beginr[], int endr[], RenderingContainer3D::MzRGBImage8::PixelType color, bool erase)
{
    RenderingContainer3D::MzRGBImage8* canvas = container->getRGBCanvas();
    if(canvas == NULL) return;
    RenderingContainer3D::MzRoi* roi = container->getActiveRoi();
    if(roi == NULL) return;

    unsigned int isize[RenderingContainer3D::MzRoi::Dimensions];
    canvas->GetSize(isize);
//    printf("MzImage2MzRoi %i %i %i %i %i %i\n", beginr[0], beginr[1], beginr[2], endr[0], endr[1], endr[2]);
//    fflush(stdout);

    for(int d = 0; d < 3; d++)
    {
        if(beginr[d] < 0) beginr[d] = 0;
        if(beginr[d] >= (int)isize[d]) beginr[d] = isize[d]-1;
        if(endr[d] < 0) endr[d] = 0;
        if(endr[d] >= (int)isize[d]) endr[d] = isize[d]-1;
        if(beginr[d] > endr[d])
            return;
    }
    int begin[RenderingContainer3D::MzRoi::Dimensions];
    int end[RenderingContainer3D::MzRoi::Dimensions];

    if(erase)
    {
        if(roi->IsEmpty())
            return;
        roi->GetBegin(begin);
        roi->GetEnd(end);
    }
    else
    {
        if(roi->IsEmpty())
        {
            RenderingContainer3D::MzRoi* oldroi = roi;
            for(int d = 0; d < 3; d++)
            {
                begin[d] = beginr[d];
                end[d] = endr[d];
            }
            roi = new RenderingContainer3D::MzRoi(begin, end);
            roi->SetName(oldroi->GetName());
            roi->SetColor(oldroi->GetColor());
            roi->SetVisibility(oldroi->GetVisibility());
        }
        else
        {
            roi->GetBegin(begin);
            roi->GetEnd(end);

//            printf("MzImage2MzRoi2 %i %i %i %i %i %i\n", begin[0], begin[1], begin[2], end[0], end[1], end[2]);
//            fflush(stdout);

            for(int d = 0; d < 3; d++)
            {
                if(begin[d] > beginr[d]) begin[d] = beginr[d];
                if(end[d] < endr[d]) end[d] = endr[d];
            }
            roi = MazdaRoiResizer< RenderingContainer3D::MzRoi >::Resize(roi, begin, end);
        }
    }
    MazdaRoiRegionIterator <RenderingContainer3D::MzRoi> iterator(roi, begin, end);
    //RenderingContainer3D::MzRGBImage8* canvas = container->getRGBCanvas();
    MazdaImageRegionIterator<RenderingContainer3D::MzRGBImage8> caniter(canvas, (unsigned int*) begin, (unsigned int*) end);
    if(erase)
    {
        while(! iterator.IsBehind())
        {
            unsigned char* c1 = caniter.GetPixelPointer()->channel;
            unsigned char* c2 = color.channel;
            if(c1[0] == c2[0] && c1[1] == c2[1] && c1[2] == c2[2])
                iterator.ClrPixel();
            ++iterator;
            ++caniter;
        }
        roi = MazdaRoiResizer< RenderingContainer3D::MzRoi >::Crop(roi);
    }
    else
    {
        while(! iterator.IsBehind())
        {
            unsigned char* c1 = caniter.GetPixelPointer()->channel;
            unsigned char* c2 = color.channel;
            if(c1[0] == c2[0] && c1[1] == c2[1] && c1[2] == c2[2])
                iterator.SetPixel();
            ++iterator;
            ++caniter;
        }
    }
    container->setActiveRoi(roi);
}


/**
 * @brief Painter3D::Coordinates3Dto2D computes coordinates in render window based on 3D coordinates
 * @param input 3D coordinates
 * @param output 2D coordinates and distance
 * @param update_matrices true to update matrices
 * @return zero on success
 */
//bool Painter3D::Coordinates3Dto2D(double input[3], double output[3], bool update_matrices)
//{
//    static GLdouble projectionMat[16];
//    static GLint viewportMat[4];
//    static GLdouble modelViewMat[16];
//    static GLfloat sx, sy, sz;
//    if(size().height()<=0 || size().width()<=0) return false;

//    RenderingContainer3D::MzRGBImage8* canvas = container->getRGBCanvas();
//    if(canvas != NULL) return false;

//    unsigned int imagesize[RenderingContainer3D::Dimensions];
//    double voxelsize[RenderingContainer3D::Dimensions];
//    canvas->GetSize(imagesize);
//    canvas->GetSpacing(voxelsize);

//    if(update_matrices)
//    {
//        paintGLSetMatrices();
//        glGetDoublev(GL_MODELVIEW_MATRIX, modelViewMat);
//        glGetDoublev(GL_PROJECTION_MATRIX, projectionMat);
//        glGetIntegerv(GL_VIEWPORT, viewportMat);

//        sx = voxelsize[0]*(GLfloat)imagesize[0]/2.0f-0.5;
//        sy = voxelsize[1]*(GLfloat)imagesize[1]/2.0f-0.5;
//        sz = voxelsize[2]*(GLfloat)imagesize[2]/2.0f-0.5;
//    }
//    else
//    {
//        GLdouble winX, winY, winZ;//2D point
//        GLdouble posX, posY, posZ;//3D point
//        posX = input[0] - sx;
//        posY = input[1] - sy;
//        posZ = input[2] - sz;
//        if(gluProject(posX, posY, posZ, modelViewMat, projectionMat, viewportMat, &winX, &winY, &winZ) == GLU_FALSE) return false;
//        output[0] = winX;
//        output[1] = viewportMat[3] - winY - 1;
//        output[2] = winZ;
//    }
//    return true;
//}


/**
 * @brief Painter3D::Coordinates2Dto3D finds voxel index pointed at render window
 * @param input coordinates in render window
 * @param output voxel index
 * @return true on success
 */
bool Painter3D::Coordinates2Dto3D(int input[2], int output[3])
{
    if(size().height()<=0 || size().width()<=0) return false;
    RenderingContainer3D::MzRGBImage8* canvas = container->getRGBCanvas();
    if(canvas == NULL) return false;

    GLdouble projectionMat[16];
    GLint viewportMat[4];
    GLdouble modelViewMat[16];
    paintGLSetMatrices();
    glGetDoublev(GL_MODELVIEW_MATRIX, modelViewMat);
    glGetDoublev(GL_PROJECTION_MATRIX, projectionMat);
    glGetIntegerv(GL_VIEWPORT, viewportMat);

    GLdouble inpd[3];
    GLdouble outd[3];
    GLdouble vecd[3];

    inpd[0] = input[0];
    inpd[1] = viewportMat[3] - input[1] - 1;
    inpd[2] = 0.0;
    if(gluUnProject(inpd[0], inpd[1], inpd[2], modelViewMat, projectionMat, viewportMat, &(outd[0]), &(outd[1]), &(outd[2])) == GLU_FALSE) return false;
    inpd[2] = 1.0;
    if(gluUnProject(inpd[0], inpd[1], inpd[2], modelViewMat, projectionMat, viewportMat, &(vecd[0]), &(vecd[1]), &(vecd[2])) == GLU_FALSE) return false;
    vecd[0] -= outd[0];
    vecd[1] -= outd[1];
    vecd[2] -= outd[2];

    double temp[3];
    int result[3][3];
    double dist[3];

    if(state.renderslice[2] && vecd[2] != 0.0)
    {
        temp[2] = ((double)state.sliceindex[2]-(double)image_size[2]/2.0+0.5)*voxel_spacing[2];
        temp[0] = outd[0] + (temp[2]-outd[2])*vecd[0] / vecd[2];
        temp[1] = outd[1] + (temp[2]-outd[2])*vecd[1] / vecd[2];
        dist[2] = sqrt((outd[0]-temp[0])*(outd[0]-temp[0])+
                       (outd[1]-temp[1])*(outd[1]-temp[1])+
                       (outd[2]-temp[2])*(outd[2]-temp[2]));
        result[2][0] = floor(temp[0]/voxel_spacing[0] + (double)image_size[0]/2.0);
        result[2][1] = floor(temp[1]/voxel_spacing[1] + (double)image_size[1]/2.0);
        result[2][2] = state.sliceindex[2];
    }
    else dist[2] = -1;

    if(state.renderslice[1] && vecd[1] != 0.0)
    {
        temp[1] = ((double)state.sliceindex[1]-(double)image_size[1]/2.0+0.5)*voxel_spacing[1];
        temp[0] = outd[0] + (temp[1]-outd[1])*vecd[0] / vecd[1];
        temp[2] = outd[2] + (temp[1]-outd[1])*vecd[2] / vecd[1];

        dist[1] = sqrt((outd[0]-temp[0])*(outd[0]-temp[0])+
                       (outd[1]-temp[1])*(outd[1]-temp[1])+
                       (outd[2]-temp[2])*(outd[2]-temp[2]));
        result[1][0] = floor(temp[0]/voxel_spacing[0] + (double)image_size[0]/2.0);
        result[1][1] = state.sliceindex[1];
        result[1][2] = floor(temp[2]/voxel_spacing[2] + (double)image_size[2]/2.0);
    }
    else dist[1] = -1;

    if(state.renderslice[0] && vecd[0] != 0.0)
    {
        temp[0] = ((double)state.sliceindex[0]-(double)image_size[0]/2.0+0.5)*voxel_spacing[0];
        temp[1] = outd[1] + (temp[0]-outd[0])*vecd[1] / vecd[0];
        temp[2] = outd[2] + (temp[0]-outd[0])*vecd[2] / vecd[0];
        dist[0] = sqrt((outd[0]-temp[0])*(outd[0]-temp[0])+
                       (outd[1]-temp[1])*(outd[1]-temp[1])+
                       (outd[2]-temp[2])*(outd[2]-temp[2]));
        result[0][0] = state.sliceindex[0];
        result[0][1] = floor(temp[1]/voxel_spacing[1] + (double)image_size[1]/2.0);
        result[0][2] = floor(temp[2]/voxel_spacing[2] + (double)image_size[2]/2.0);
    }
    else dist[0] = -1;

    int sorter[3];
    int z;
    sorter[0] = 0;
    sorter[1] = 1;
    sorter[2] = 2;
    if(dist[sorter[0]]>dist[sorter[1]])
    {
        z = sorter[1];
        sorter[1] = sorter[0];
        sorter[0] = z;
    }
    if(dist[sorter[0]]>dist[sorter[2]])
    {
        z = sorter[2];
        sorter[2] = sorter[0];
        sorter[0] = z;
    }
    if(dist[sorter[1]]>dist[sorter[2]])
    {
        z = sorter[2];
        sorter[2] = sorter[1];
        sorter[1] = z;
    }
    for(int ii = 0; ii < 3; ii++)
    {
        int i = sorter[ii];
        if(dist[i] >= 0.0)
        {
           if((unsigned int)result[i][0] < image_size[0] && (unsigned int)result[i][1] < image_size[1] && (unsigned int)result[i][2] < image_size[2])
           {
               output[0] = result[i][0];
               output[1] = result[i][1];
               output[2] = result[i][2];
               return true;
           }
        }
    }
    return false;
}

/**
 * @brief Painter3D::Coordinates2Dto3D computes coordiantes in world based on mouse coordinates, given a reference point
 * @param input mouse coordinates
 * @param reference some coordinates to compute distance from camera
 * @param output coordinates in world coordinate system
 * @return distance from reference
 */
bool Painter3D::Coordinates2Dto3D(const int input[2], const GLdouble reference[3], GLdouble output[3], GLdouble outputref[3])
{
    GLdouble projectionMat[16];
    GLint viewportMat[4];
    GLdouble modelViewMat[16];
    paintGLSetMatrices();
    glGetDoublev(GL_MODELVIEW_MATRIX, modelViewMat);
    glGetDoublev(GL_PROJECTION_MATRIX, projectionMat);
    glGetIntegerv(GL_VIEWPORT, viewportMat);

    if(gluProject(reference[0], reference[1], reference[2], modelViewMat, projectionMat, viewportMat, &(outputref[0]), &(outputref[1]), &(outputref[2])) == GLU_FALSE) return false;
    output[0] = input[0];
    output[1] = viewportMat[3] - input[1] - 1;
    outputref[1] = viewportMat[3] - outputref[1] - 1;
    if(gluUnProject(output[0], output[1], outputref[2], modelViewMat, projectionMat, viewportMat, &(output[0]), &(output[1]), &(output[2])) == GLU_FALSE) return false;
    return true;
}
