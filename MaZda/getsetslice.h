/*
 * QMAZDA project: Image Analysis and Pattern Recognition
 *
 * Copyright 2013-2016 Piotr M. Szczypiński <piotr.szczypinski@p.lodz.pl>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef GETSETSLICE_H
#define GETSETSLICE_H

#include "../SharedImage/mazdadummy.h"
#include "../SharedImage/mazdaimage.h"
#include "../SharedImage/mazdaimageio.h"
#include "../SharedImage/mazdaroi.h"
#include "../SharedImage/mazdaroiio.h"
#include "renderingcontainer.h"

typedef RenderingContainer<2> RenderingContainer2D;
typedef RenderingContainer<3> RenderingContainer3D;

/**
 * @brief The GetSetSlice class controls container copying between 2D and 3D containers/renderers
 */
class GetSetSlice
{
public:
    /**
     * @brief from3Dto2D copies 3D slice to 2D image
     * @param matrix rotation matrix from opengl renderer
     * @param showslice vector to determine if slice X, Y, Z is visible
     * @param sliceno vector to determine which slice X, Y, Z is visible
     * @param input pointer to container with 3D data
     * @param output pointer to container for 2D data
     */
    void from3Dto2D(double matrix[16], bool showslice[3], unsigned int sliceno[3], RenderingContainer3D* input, RenderingContainer2D* output)
    {
        this->container3d = input;
        this->container2d = output;
        setDirections(matrix, showslice);
        slice = sliceno[abs(directions[2])-1];
        getImageSlice();
        getRoisSlice();
        output->whattoupdate = IMAGE_LOADED;
        output->assembleImage();
    }
    /**
     * @brief backTo3D copies 2D image to 3D slice, must be preceded by call to from3Dto2D.
     */
    void backTo3D(void)
    {
        setRoisSlice();
        container2d->whattoupdate = IMAGE_LOADED;
        container2d->assembleImage();
    }

private:
    int directions[3];
    unsigned int iterdir[3];
    unsigned int size3d[3];
    unsigned int slice;
    RenderingContainer3D* container3d;
    RenderingContainer2D* container2d;

    /**
     * @brief setRoisSlice copies regions from 2D image to 3D image slice
     */
    void setRoisSlice(void)
    {
        unsigned int roi_count = container3d->roitable.size();
        if(roi_count > container2d->roitable.size()) roi_count = container2d->roitable.size();
        if(roi_count <= 0) return;
        for(unsigned int r = 0; r < roi_count; r++)
        {
            RenderingContainer3D::MzRoi* roi3d = container3d->getRoi(r);
            RenderingContainer2D::MzRoi* roi2d = container2d->getRoi(r);

            if(roi3d == NULL || roi2d == NULL) continue;

            int begin3d[RenderingContainer3D::MzRoi::Dimensions];
            int end3d[RenderingContainer3D::MzRoi::Dimensions];
            int begin2d[RenderingContainer2D::MzRoi::Dimensions];
            int end2d[RenderingContainer2D::MzRoi::Dimensions];

            roi2d->GetBegin(begin2d);
            roi2d->GetEnd(end2d);

            if(roi3d->IsEmpty())
            {
                if(roi2d->IsEmpty())
                {
//                    printf("setRoisSlice A\n");
//                    fflush(stdout);
                }
                else
                {
//                    printf("setRoisSlice B\n");
//                    fflush(stdout);

                    for(int d = 0; d < 2; d++)
                    {
                        if(directions[d] < 0)
                        {
                            begin3d[iterdir[d]] = size3d[iterdir[d]]-end2d[d]-1;
                            end3d[iterdir[d]] = size3d[iterdir[d]]-begin2d[d]-1;
                        }
                        else
                        {
                            begin3d[iterdir[d]] = begin2d[d];
                            end3d[iterdir[d]] = end2d[d];
                        }
                    }
                    begin3d[iterdir[2]] = slice;
                    end3d[iterdir[2]] = slice;
                    roi3d->Reallocate(begin3d, end3d);

                    for(int d = 0; d < 2; d++)
                    {
                        if(directions[d] < 0)
                        {
                            int z = begin3d[iterdir[d]];
                            begin3d[iterdir[d]] = end3d[iterdir[d]];
                            end3d[iterdir[d]] = z;
                        }
                    }
                    MazdaRoiRegionFlippedIterator< RenderingContainer3D::MzRoi > iterator3d2(roi3d, begin3d, end3d, iterdir);
                    MazdaRoiIterator< RenderingContainer2D::MzRoi > iterator2d(roi2d);
                    while(! iterator2d.IsBehind())
                    {
                        if(iterator2d.GetPixel()) iterator3d2.SetPixel();
                        ++iterator2d;
                        ++iterator3d2;
                    }
                }
            }
            else
            {
                roi3d->GetBegin(begin3d);
                roi3d->GetEnd(end3d);

                if(roi2d->IsEmpty())
                {
//                    printf("setRoisSlice C\n");
//                    fflush(stdout);

                    begin3d[iterdir[2]] = slice;
                    end3d[iterdir[2]] = slice;
                    MazdaRoiRegionIterator<RenderingContainer3D::MzRoi> iterator3d1(roi3d, begin3d, end3d);
                    while(! iterator3d1.IsBehind())
                    {
                        iterator3d1.ClrPixel();
                        ++iterator3d1;
                    }
                }
                else
                {
//                    printf("setRoisSlice D\n");
//                    fflush(stdout);
                    bool upsize = false;
                    for(int d = 0; d < 2; d++)
                    {
                        if(directions[d] < 0)
                        {
                            int be = size3d[iterdir[d]]-end2d[d]-1;
                            if(begin3d[iterdir[d]] > be) {begin3d[iterdir[d]] = be; upsize = true;}
                            be = size3d[iterdir[d]]-begin2d[d]-1;
                            if(end3d[iterdir[d]] < be) {end3d[iterdir[d]] = be; upsize = true;}
                        }
                        else
                        {
                            if(begin3d[iterdir[d]] > begin2d[d]) {begin3d[iterdir[d]] = begin2d[d]; upsize = true;}
                            if(end3d[iterdir[d]] < end2d[d]) {end3d[iterdir[d]] = end2d[d]; upsize = true;}
                        }
                    }
                    if((int)slice < begin3d[iterdir[2]]) {begin3d[iterdir[2]] = (int)slice; upsize = true;}
                    if((int)slice > end3d[iterdir[2]]) {end3d[iterdir[2]] = (int)slice; upsize = true;}
                    if(upsize)
                    {
                        roi3d = MazdaRoiResizer< RenderingContainer3D::MzRoi >::Resize(roi3d, begin3d, end3d);
                        container3d->setRoi(r, roi3d);
                    }

                    begin3d[iterdir[2]] = slice;
                    end3d[iterdir[2]] = slice;

                    MazdaRoiRegionIterator<RenderingContainer3D::MzRoi> iterator3d1(roi3d, begin3d, end3d);
                    while(! iterator3d1.IsBehind())
                    {
                        iterator3d1.ClrPixel();
                        ++iterator3d1;
                    }

                    for(int d = 0; d < 2; d++)
                    {
                        if(directions[d] < 0)
                        {
                            begin3d[iterdir[d]] = size3d[iterdir[d]]-begin2d[d]-1;
                            end3d[iterdir[d]] = size3d[iterdir[d]]-end2d[d]-1;
                        }
                        else
                        {
                            begin3d[iterdir[d]] = begin2d[d];
                            end3d[iterdir[d]] = end2d[d];
                        }
                    }
                    MazdaRoiRegionFlippedIterator< RenderingContainer3D::MzRoi > iterator3d2(roi3d, begin3d, end3d, iterdir);
                    MazdaRoiIterator< RenderingContainer2D::MzRoi > iterator2d(roi2d);
                    while(! iterator2d.IsBehind())
                    {
                        if(iterator2d.GetPixel()) iterator3d2.SetPixel();
                        ++iterator2d;
                        ++iterator3d2;
                    }
                }
            }
        }
    }




    /**
     * @brief getRoisSlice copies regions from 3D slice to 2D image
     */
    void getRoisSlice(void)
    {
        int roi_count = container3d->roitable.size();
        if(roi_count <= 0) return;
        container2d->removeRois();

        for(int r = 0; r < roi_count; r++)
        {
            container2d->addRoi();
            RenderingContainer3D::MzRoi* inroi = container3d->getRoi(r);
            RenderingContainer2D::MzRoi* outroi = container2d->getRoi(r);
            outroi->SetColor(inroi->GetColor());
            outroi->SetName(inroi->GetName());
            outroi->SetVisibility(inroi->GetVisibility());

            if(inroi->IsEmpty()) continue;

            int inbegin[RenderingContainer3D::MzRoi::Dimensions];
            int inend[RenderingContainer3D::MzRoi::Dimensions];
            inroi->GetBegin(inbegin);
            inroi->GetEnd(inend);

            if((int)slice <= inend[iterdir[2]] && (int)slice >= inbegin[iterdir[2]])
            {
                int temp;
                inbegin[iterdir[2]] = slice;
                inend[iterdir[2]] = slice;

                int outbegin[RenderingContainer2D::MzRoi::Dimensions];
                int outend[RenderingContainer2D::MzRoi::Dimensions];

                for(int d = 0; d < 2; d++)
                {
                    if(directions[d] < 0)
                    {
                        outbegin[d] = size3d[iterdir[d]]-inend[iterdir[d]]-1;
                        outend[d] = size3d[iterdir[d]]-inbegin[iterdir[d]]-1;
                        temp = inbegin[iterdir[d]];
                        inbegin[iterdir[d]] = inend[iterdir[d]];
                        inend[iterdir[d]] = temp;
                    }
                    else
                    {
                        outbegin[d] = inbegin[iterdir[d]];
                        outend[d] = inend[iterdir[d]];
                    }
                }
                outroi->Reallocate(outbegin, outend);
                MazdaRoiRegionFlippedIterator<RenderingContainer3D::MzRoi> iterator3d(inroi, inbegin, inend, iterdir);
                MazdaRoiIterator<RenderingContainer2D::MzRoi> iterator2d(outroi);
                while(! iterator2d.IsBehind())
                {
                    if(iterator3d.GetPixel()) iterator2d.SetPixel();
                    ++iterator2d;
                    ++iterator3d;
                }
            }
        }
    }

    /**
     * @brief setDirections establishes which 3D slice is visible and how to use flipped iterators
     * @param matrix rotation matrix from opengl renderer
     * @param renderslice vector to determine which slice X, Y, Z is visible
     */
    void setDirections(double matrix[16], bool renderslice[3])
    {
        int visiblesliceplane = -1;
        double max = -1;
        double xyz[3];

        xyz[0] = fabs(matrix[2]);
        xyz[1] = fabs(matrix[6]);
        xyz[2] = fabs(matrix[10]);

        for(int r = 0; r < 3; r++)
        {
            if(max < xyz[r] && renderslice[r])
            {
                max = xyz[r];
                visiblesliceplane = r;
            }
        }
        if(visiblesliceplane < 0)
        {
            for(int r = 0; r < 3; r++)
            {
                if(max < xyz[r])
                {
                    max = xyz[r];
                    visiblesliceplane = r;
                }
            }
        }
        directions[2] = visiblesliceplane+1;

        int rr = 0;
        int cc = 0;
        max = -1;
        for(int c = 0; c < 2; c++)
        {
            for(int r = 0; r < 3; r++)
            {
                if(r == visiblesliceplane) continue;
                if(fabs(matrix[4*r+c]) > max)
                {
                    rr = r;
                    cc = c;
                    max = fabs(matrix[4*r+c]);
                }
            }
        }
        if(((rr == 1 && cc == 0)||(rr == 2 && cc == 1)) && visiblesliceplane == 0) // 4 9
        {
            directions[0] = matrix[4] > 0 ? 2 : -2;
            directions[1] = matrix[9] > 0 ? -3 : 3;
        }
        if(((rr == 1 && cc == 1)||(rr == 2 && cc == 0)) && visiblesliceplane == 0) // 8 5
        {
            directions[0] = matrix[8] > 0 ? 3 : -3;
            directions[1] = matrix[5] > 0 ? -2 : 2;
        }
        if(((rr == 0 && cc == 0)||(rr == 2 && cc == 1)) && visiblesliceplane == 1) // 0 9
        {
            directions[0] = matrix[0] > 0 ? 1 : -1;
            directions[1] = matrix[9] > 0 ? -3 : 3;
        }
        if(((rr == 0 && cc == 1)||(rr == 2 && cc == 0)) && visiblesliceplane == 1) // 8 1
        {
            directions[0] = matrix[8] > 0 ? 3 : -3;
            directions[1] = matrix[1] > 0 ? -1 : 1;
        }
        if(((rr == 0 && cc == 0)||(rr == 1 && cc == 1)) && visiblesliceplane == 2) // 0 5
        {
            directions[0] = matrix[0] > 0 ? 1 : -1;
            directions[1] = matrix[5] > 0 ? -2 : 2;
        }
        if(((rr == 0 && cc == 1)||(rr == 1 && cc == 0)) && visiblesliceplane == 2) // 4 1
        {
            directions[0] = matrix[4] > 0 ? 2 : -2;
            directions[1] = matrix[1] > 0 ? -1 : 1;
        }
        iterdir[0] = abs(directions[0])-1;
        iterdir[1] = abs(directions[1])-1;
        iterdir[2] = abs(directions[2])-1;
    }

    /**
     * @brief getImageSlice copies 3D image slice to 2D image
     */
    void getImageSlice(void)
    {
        unsigned int iterdir[3];
        iterdir[0] = abs(directions[0])-1;
        iterdir[1] = abs(directions[1])-1;
        iterdir[2] = abs(directions[2])-1;

        double spacing3d[3];
        container3d->getSize(size3d);
        container3d->getSpacing(spacing3d);
        unsigned int size2d[2];
        double spacing2d[2];
        size2d[0] = size3d[iterdir[0]];
        size2d[1] = size3d[iterdir[1]];
        spacing2d[0] = spacing2d[iterdir[0]];
        spacing2d[1] = spacing2d[iterdir[1]];

        unsigned int beginu[3];
        unsigned int endu[3];
        beginu[iterdir[2]] = slice;
        endu[iterdir[2]] = slice;
        if(directions[1] > 0)
        {
            beginu[iterdir[1]] = 0;
            endu[iterdir[1]] = size3d[iterdir[1]]-1;
        }
        else
        {
            endu[iterdir[1]] = 0;
            beginu[iterdir[1]] = size3d[iterdir[1]]-1;
        }
        if(directions[0] > 0)
        {
            beginu[iterdir[0]] = 0;
            endu[iterdir[0]] = size3d[iterdir[0]]-1;
        }
        else
        {
            endu[iterdir[0]] = 0;
            beginu[iterdir[0]] = size3d[iterdir[0]]-1;
        }

        if(container3d->image_source_color != NULL)
        {
            if(container2d->image_source_color != NULL) delete container2d->image_source_color;
            container2d->image_source_color = new RenderingContainer2D::MzRGBImage(size2d, spacing2d);
            if(container2d->image_source_gray != NULL) delete container2d->image_source_gray;
            container2d->image_source_gray = NULL;
            MazdaImageRegionFlippedIterator<RenderingContainer3D::MzRGBImage> iterator3d(container3d->image_source_color, beginu, endu, iterdir);
            MazdaImageIterator<RenderingContainer2D::MzRGBImage> iterator2d(container2d->image_source_color);
            while(! iterator2d.IsBehind())
            {
                iterator2d.SetPixel(iterator3d.GetPixel());
                ++iterator2d;
                ++iterator3d;
            }
        }
        else if(container3d->image_source_gray != NULL)
        {
            if(container2d->image_source_gray != NULL) delete container2d->image_source_gray;
            container2d->image_source_gray = new RenderingContainer2D::MzImage(size2d, spacing2d);
            if(container2d->image_source_color != NULL) delete container2d->image_source_color;
            container2d->image_source_color = NULL;
            MazdaImageRegionFlippedIterator<RenderingContainer3D::MzImage> iterator3d(container3d->image_source_gray, beginu, endu, iterdir);
            MazdaImageIterator<RenderingContainer2D::MzImage> iterator2d(container2d->image_source_gray);
            while(! iterator2d.IsBehind())
            {
                iterator2d.SetPixel(iterator3d.GetPixel());
                ++iterator2d;
                ++iterator3d;
            }
        }
    }
};

#endif // GETSETSLICE_H
