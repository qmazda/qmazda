/*
 * QMAZDA project: Image Analysis and Pattern Recognition
 *
 * Copyright 2013-2016 Piotr M. Szczypiński <piotr.szczypinski@p.lodz.pl>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef RENDERER_3D_H
#define RENDERER_3D_H

#include <QObject>
#include <QWidget>
#include <QOpenGLWidget>
#include <QOpenGLFunctions>
#include <QVector3D>
#include <QMouseEvent>
#include <QWheelEvent>
#include <QApplication>
#include "renderingcontainer.h"
#include "rendererinterface.h"

//#define FRUSTUM_FRONT_DISTANCE 30.0f
//#define FRUSTUM_BACK_DISTANCE 3000.0f
#define FRUSTUM_SIDE_TO_SIDE 5.0f
#define CENTER_OF_PROJECTION_DISTANCE 200.0f


template <class T>
class MatrixUtils3D
{
public:
    static void unit4x4(T mat[])
    {
        mat[0] = 1; mat[1] = 0; mat[2] = 0; mat[3] = 0;
        mat[4] = 0; mat[5] = 1; mat[6] = 0; mat[7] = 0;
        mat[8] = 0; mat[9] = 0; mat[10] = 1; mat[11] = 0;
        mat[12] = 0; mat[13] = 0; mat[14] = 0; mat[15] = 1;
    }
    static void shift(int qx, int qy, T mat[])
    {
        mat[12] -= ((qx*mat[14])/1000);
        mat[13] -= ((qy*mat[14])/1000);
    }

    static void rotate(T rot, int l1, int l2, T W[16])
    {
        T a, b;
        T sina = sin(rot);
        T cosa = cos(rot);

        for(int k =0; k<12; k+=4)
        {
            a = cosa * W[k + l1] + sina * W[k + l2];
            b = -sina * W[k + l1] + cosa * W[k + l2];
            W[k + l1] = a;
            W[k + l2] = b;
        }
    }
    static void rotatemodel(T rot, int l1, int l2, T mato[16], T mat[16])
    {
        T matt[16];
        T mat_1[16];

        invert3x3(mat_1, mat);
        multiply3x3(matt, mato, mat);
        rotate(rot, l1, l2, matt);
        multiply3x3(mato, matt, mat_1);
    }
    static void resize(T prop, T mato[16])
    {
        for(int k =0; k<12; k++)
        {
            mato[k] *= prop;
        }
    }
    static T determinant4x4(const T W[14])
    {
        return
                W[0*4+0]*
                (
                W[1*4+1]*(W[2*4+2]*W[3*4+3]-W[2*4+3]*W[3*4+2])
                -W[1*4+2]*(W[2*4+1]*W[3*4+3]-W[2*4+3]*W[3*4+1])
                +W[1*4+3]*(W[2*4+1]*W[3*4+2]-W[2*4+2]*W[3*4+1])
                )
                -W[0*4+1]*
                (
                W[1*4+0]*(W[2*4+2]*W[3*4+3]-W[2*4+3]*W[3*4+2])
                -W[1*4+2]*(W[2*4+0]*W[3*4+3]-W[2*4+3]*W[3*4+0])
                +W[1*4+3]*(W[2*4+0]*W[3*4+2]-W[2*4+2]*W[3*4+0])
                )
                +W[0*4+2]*
                (
                W[1*4+0]*(W[2*4+1]*W[3*4+3]-W[2*4+3]*W[3*4+1])
                -W[1*4+1]*(W[2*4+0]*W[3*4+3]-W[2*4+3]*W[3*4+0])
                +W[1*4+3]*(W[2*4+0]*W[3*4+1]-W[2*4+1]*W[3*4+0])
                )
                -W[0*4+3]*
                (
                W[1*4+0]*(W[2*4+1]*W[3*4+2]-W[2*4+2]*W[3*4+1])
                -W[1*4+1]*(W[2*4+0]*W[3*4+2]-W[2*4+2]*W[3*4+0])
                +W[1*4+2]*(W[2*4+0]*W[3*4+1]-W[2*4+1]*W[3*4+0])
                );
    }
    static T subdeterminant3of4(const T W[16], int w, int k)
    {
        int w0, w1, w2;
        int k0, k1, k2;

        w0=0; if(w0==w) w0++;
        w1=w0+1; if(w1==w) w1++;
        w2=w1+1; if(w2==w) w2++;

        k0=0; if(k0==k) k0++;
        k1=k0+1; if(k1==k) k1++;
        k2=k1+1; if(k2==k) k2++;

        return
                W[w0*4+k0]*(W[w1*4+k1]*W[w2*4+k2]-W[w1*4+k2]*W[w2*4+k1])
                -W[w0*4+k1]*(W[w1*4+k0]*W[w2*4+k2]-W[w1*4+k2]*W[w2*4+k0])
                +W[w0*4+k2]*(W[w1*4+k0]*W[w2*4+k1]-W[w1*4+k1]*W[w2*4+k0]);
    }
    static T invert3x3(T I[16], const T W[16])
    {
        T det =
                W[0]*(W[5]*W[10]-W[6]*W[9])
                +W[1]*(W[6]*W[8]-W[4]*W[10])
                +W[2]*(W[4]*W[9]-W[5]*W[8]);

        if(det==0) return 0;
        I[0] = (W[5]*W[10]-W[6]*W[9])/det;
        I[4] = (W[6]*W[8]-W[4]*W[10])/det;
        I[8] = (W[4]*W[9]-W[5]*W[8])/det;
        I[12] = 0.0;
        I[1] = (W[2]*W[9]-W[1]*W[10])/det;
        I[5] = (W[0]*W[10]-W[2]*W[8])/det;
        I[9] = (W[1]*W[8]-W[0]*W[9])/det;
        I[13] = 0.0;
        I[2] = (W[1]*W[6]-W[2]*W[5])/det;
        I[6] = (W[2]*W[4]-W[0]*W[6])/det;
        I[10] = (W[0]*W[5]-W[1]*W[4])/det;
        I[14] = 0.0;
        I[3] = 0.0;
        I[7] = 0.0;
        I[11] = 0.0;
        I[15] = 1.0;
        return det;
    }
    static int invert4x4(T W[16], const T W1[16])
    {
        int k, l;
        T det;
        det=determinant4x4(W1);
        if(det==0) return 1;

        for(k=0; k<4; k++)
            for(l=0; l<4; l++)
            {
                if((k+l)%2) W[l*4+k]=-subdeterminant3of4(W1, k, l)/det;
                else W[l*4+k]=subdeterminant3of4(W1, k, l)/det;
            }
        return 0;
    }
    static void multiply4x4(T W[16], T W1[16], T W2[16])
    {
        for(int i1 = 0; i1 < 4; i1++)
            for(int i2 = 0; i2 < 4; i2++)
                W[i1*4+i2] = W2[i1*4]*W1[i2]
                        + W2[i1*4+1]*W1[i2+4]
                        + W2[i1*4+2]*W1[i2+8]
                        + W2[i1*4+3]*W1[i2+12];
    }
    static void multiply3x3(T W[16], const T W1[16], const T W2[16])
    {
        for(int i1 = 0; i1 < 3; i1++)
            for(int i2 = 0; i2 < 3; i2++)
                W[i1*4+i2] = W1[i1*4]*W2[i2]+ W1[i1*4+1]*W2[i2+4]+ W1[i1*4+2]*W2[i2+8];
    }
    static void copy3x3(T W[16], const T W1[16])
    {
        for(int i1 = 0; i1 < 3; i1++)
            for(int i2 = 0; i2 < 3; i2++)
                W[i1*4+i2] = W1[i1*4+i2];
    }
    static void copy4x4(T W[16], const T W1[16])
    {
        for(int i1 = 0; i1 < 16; i1++) W[i1] = W1[i1];
    }
};




typedef RenderingContainer<3> RenderingContainer3D;

class Renderer3DState
{
public:
    Renderer3DState(void)
    {
        color = 0x17;
        Reset();
    }
    void Reset(void)
    {
        zoom_factor = 1.0;
        for(int i = 0; i < 4; i++)
            for(int j = 0; j < 4; j++)
            {
                if(i == j) volumeMatrix[i*4+j] = 1.0;
                else volumeMatrix[i*4+j] = 0.0;
            }
        volumeMatrix[14] = -CENTER_OF_PROJECTION_DISTANCE;
    }
    double zoom_factor;
    unsigned int color;
    unsigned int sliceindex[3];
    bool renderslice[3];
    int rendervolume;

    GLdouble volumeMatrix[16];
};

//#ifdef _WIN32
//class Renderer3D : public QGLWidget, RendererInterface, protected QGLFunctions
//#else
class Renderer3D : public QOpenGLWidget, RendererInterface, protected QOpenGLFunctions //: public QOpenGLWidget, RendererInterface
//#endif
{
    Q_OBJECT
public:
    void mouseMoveEvent(QMouseEvent *ev);
    void mousePressEvent(QMouseEvent *ev);
    void wheelEvent(QWheelEvent *ev);
    void mouseReleaseEvent(QMouseEvent *ev);
    void mouseDoubleClickEvent(QMouseEvent *ev);
//    void keyPressEvent(QKeyEvent *ev);
//    void keyReleaseEvent(QKeyEvent *ev);

    explicit Renderer3D(RenderingContainer3D* container, QWidget *parent = 0);
    ~Renderer3D(void);
    void ResetView(void);
    bool SetContainer(RenderingContainerInterface* container);
    void SetZoom(double zoom);
    double GetZoom(void);
    void SetBackground(unsigned int  color);
    unsigned int GetBackground(void);
    void Repaint();
    void setRepaintVolume(void);

    void setCrossShow(int direction, bool show);
    void setCrossNumber(int direction, unsigned int index);
    void setVolumeView(int view);

    void getMatrix(double m[16]);
    void setMatrix(double m[16]);
    static void initColor(unsigned int color);

signals:
    void setZoomRequest(double zoom);
    void setRoiListRequest(void);
    void setStatusTextRequest(QString* string);
//    void setRedoUndoRequest(void);
protected:
    Renderer3DState state;
    RenderingContainer3D* container;
    unsigned int image_size[RenderingContainer3D::MzRGBImage8::Dimensions];
    double voxel_spacing[RenderingContainer3D::MzRGBImage8::Dimensions];
    //int lastdirectionofvolume;
    bool repaintvolume;
    int volumelist[6];

    void paintGLSetMatrices();
    void initializeGL();
    void paintGL();
    int getFacingDirection(void);
    bool createVolumetricView(int direction);
    void renderCrossSection(unsigned int direction, unsigned int sliceindex, GLubyte* bits, unsigned int sizeimg[2], unsigned int sizetex[2]);
    bool fillCrossSection(GLubyte *bits, unsigned int stride, unsigned int direction, unsigned int sliceindex); //, bool noalpha);
    bool createCrossSection(unsigned int direction, unsigned int sliceindex);
    void paintSingleGL(void);
    int mousex, mousey;

    int modellist;
    bool showmodellist;
    GLdouble modelMatrix[16];
private:
    double getRatio();
    static Renderer3DState persistent_state;

};

#endif // RENDERER_3D_H
