/*
 * QMAZDA project: Image Analysis and Pattern Recognition
 *
 * Copyright 2013-2016 Piotr M. Szczypiński <piotr.szczypinski@p.lodz.pl>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "dialogfeatureoptions.h"
#include "ui_dialogfeatureoptions.h"
#include <QFile>
#include <QTextStream>
#include <QFileDialog>
#include <QDropEvent>
#include <QMimeData>
#include "stdio.h"
#include "../MzShared/filenameremap.h"

DialogFeatureOptions::DialogFeatureOptions(NameStubTree *featuresTemplate, QWidget *parent) :
    QDialog(parent),
    ui(new Ui::DialogFeatureOptions)
{
    ui->setupUi(this);
    ui->treeWidgetNames->setSeparator(' ');
    ui->treeWidgetNames->setTemplate(featuresTemplate);
    ui->treeWidgetNames->setItemDelegateForColumn(0, new EditDelegate(this, ui->treeWidgetNames, ' '));
    root = ui->treeWidgetNames->createMethod("", "Tree of algorithms for feature extraction");
    ui->treeWidgetNames->setCurrentItem(root);
    ui->treeWidgetNames->expandAll();
}

void DialogFeatureOptions::setTreeRootIcon(const QIcon &icon)
{
    root->setIcon(0, icon);
}


DialogFeatureOptions::~DialogFeatureOptions()
{
/*
    QStringList list;
    ui->treeWidgetRoi->getUniqueLeafs(&list);

    std::ofstream file;
    file.open("/home/piotr/Program/qmazda/MaZda/fr.txt");
    if (!file.is_open()) return;
    if (!file.good()) return;
    for (int i = 0; i < list.size(); ++i)
        file << list.at(i).toStdString() << std::endl;
*/
    delete ui;
}

std::vector<std::string> DialogFeatureOptions::loadFeatureList(const char* filename)
{
    std::string name;
    std::ifstream file;
    std::vector<std::string> result;
#if defined _MZ_UTF8_TO_U16
    file.open(_mz_utf8_to_u16(filename).c_str());
#else
    file.open(filename);
#endif

    if (!file.is_open()) return result;
    if (!file.good()) return result;
    do
    {
        std::string sline;
        std::getline(file, sline);
        std::replace(sline.begin(), sline.end(), ',', ' ');
        std::replace(sline.begin(), sline.end(), ';', ' ');
        std::replace(sline.begin(), sline.end(), '\"', ' ');
        std::stringstream ss(sline);
        while(!ss.eof())
        {
            name.clear();
            ss >> std::skipws >> name;
            if(name.length() > 0) result.push_back(name);
        }
    }
    while(!file.eof());
    file.close();
    return result;
}

bool DialogFeatureOptions::saveFeatureList(const char *filename, QStringList* lista)
{
    QFile f;
    if(lista->size() <= 0) return false;
    f.setFileName(filename);
    if(f.open(QFile::WriteOnly))
    {
        QTextStream textstream(&f);
        for (int i = 0; i < lista->size(); ++i) textstream << lista->at(i) << '\n';
        f.close();
        return true;
    }
    else return false;
}

void DialogFeatureOptions::setFeatureList(QStringList* list)
{
    ui->treeWidgetNames->setUniqueLeafs(list);
}

void DialogFeatureOptions::getFeatureList(QStringList* list)
{
    ui->treeWidgetNames->getUniqueLeafs(list);
}

void DialogFeatureOptions::on_toolButtonPlus_clicked()
{
// Not elagent way to force finish editing
    blockSignals(true);
    ui->treeWidgetNames->setEnabled(false);
    ui->treeWidgetNames->setEnabled(true);
    blockSignals(false);

    QTreeWidgetItem* item = ui->treeWidgetNames->currentItem();
    ui->treeWidgetNames->addMatchedChildren(item);
}

void DialogFeatureOptions::on_toolButtonMinus_clicked()
{
    QTreeWidgetItem* item = ui->treeWidgetNames->currentItem();
    ui->treeWidgetNames->removeItem(item);
}

void DialogFeatureOptions::on_toolButtonExpand_clicked()
{
    QTreeWidgetItem* item = ui->treeWidgetNames->topLevelItem(0);
    if(item->isExpanded()) ui->treeWidgetNames->collapseAll();
    else ui->treeWidgetNames->expandAll();
}


void DialogFeatureOptions::loadOptions(const char* filename)
{
    std::vector<std::string> lista = loadFeatureList(filename);
    if(lista.size() <= 0)
        return;
    QStringList qlista;
    for (unsigned int i = 0; i < lista.size(); ++i)
        qlista.push_back(lista[i].c_str());
    ui->treeWidgetNames->setUniqueLeafs(&qlista);
}

void DialogFeatureOptions::on_toolButton_Load_clicked()
{
    QString fileName = QFileDialog::getOpenFileName(this, tr("Open feature list"),  NULL, tr("Text file (*.txt);;All files (*)"));
    if (fileName.isEmpty())
        return;
    loadOptions(fileName.toStdString().c_str());
}

void DialogFeatureOptions::on_toolButton_Save_clicked()
{
    QString fileName = QFileDialog::getSaveFileName(this, tr("Save feature list"),  NULL, tr("Text file (*.txt);;All files (*)"));
    if (fileName.isEmpty()) return;
    QStringList qlista;
    ui->treeWidgetNames->getUniqueLeafs(&qlista);
    saveFeatureList(fileName.toStdString().c_str(), &qlista);
}

void DialogFeatureOptions::dragEnterEvent(QDragEnterEvent* event)
{
    event->acceptProposedAction();
}

void DialogFeatureOptions::dragMoveEvent(QDragMoveEvent* event)
{
    event->acceptProposedAction();
}

void DialogFeatureOptions::dragLeaveEvent(QDragLeaveEvent* event)
{
    event->accept();
}

void DialogFeatureOptions::dropEvent(QDropEvent* event)
{
    const QMimeData* mimeData = event->mimeData();

    if (mimeData->hasUrls())
    {
        QList<QUrl> urlList = mimeData->urls();

        int ulsize = urlList.size();

        if(ulsize >= 1)
        {
            QString filename = urlList.at(0).toLocalFile();
            loadOptions(filename.toStdString().c_str());
        }
    }
}
