/*
 * QMAZDA project: Image Analysis and Pattern Recognition
 *
 * Copyright 2013-2017 Piotr M. Szczypiński <piotr.szczypinski@p.lodz.pl>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "paint3dfloodfill.h"
#include <QWindow>
#include <QScreen>

Paint3DFloodFill::Paint3DFloodFill(RenderingContainer3D *icontainer, Painter3D *ipainter, bool *ierase)
{
    container = icontainer;
    renderer = ipainter;
    erase = ierase;
    unsigned int color = container->getActiveRoi()->GetColor();
    colorpixel.channel[2] = color&0xff; color >>= 8;
    colorpixel.channel[1] = color&0xff; color >>= 8;
    colorpixel.channel[0] = color&0xff;
}

AfterPaintAction Paint3DFloodFill::imageDataChanged(void)
{
    return DONOTHING;
}

MzBounds Paint3DFloodFill::getModifiedRegion()
{
    return cube;
}

AfterPaintAction Paint3DFloodFill::mouseMoveEvent(QMouseEvent * /*ev*/)
{
    return DONOTHING;
}

AfterPaintAction Paint3DFloodFill::mousePressEvent(QMouseEvent *ev)
{
    if((ev->buttons() & Qt::LeftButton) && (!QApplication::keyboardModifiers()))
    {
        ev->accept();
#if (QT_VERSION >= QT_VERSION_CHECK(5, 0, 0))
        double ratio = 1.0;
        QWindow* wh = renderer->windowHandle();
        if(wh != nullptr)
            ratio = wh->devicePixelRatio();
#ifdef _WIN32
#else
        else
        {
            QScreen* sc = renderer->screen();
            if(sc != nullptr)
                ratio = sc->devicePixelRatio();
        }
#endif
#else
        double ratio = 1.0;
#endif
        int input[2];
        unsigned int output[RenderingContainer3D::Dimensions];
        input[0] = ratio*_ev_position_x;
        input[1] = ratio*_ev_position_y;
        if(renderer->Coordinates2Dto3D(input, (int*)output))
        {
            unsigned int seed[3];
            seed[0] = output[0];
            seed[1] = output[1];
            seed[2] = output[2];
            FloodFill(seed, &cube);
            return REDRAW;
        }
        else
        {
            cube.min[0] = cube.max[0] = 0;
            cube.min[1] = cube.max[1] = 0;
            cube.min[2] = cube.max[2] = 0;
        }
    }
    return DONOTHING;
}

AfterPaintAction Paint3DFloodFill::mouseReleaseEvent(QMouseEvent *ev)
{
    ev->accept();
    renderer->MzImage2MzRoi((int*)cube.min, (int*)cube.max, colorpixel, *erase);
    return FINALIZE;
}

AfterPaintAction Paint3DFloodFill::mouseDoubleClickEvent(QMouseEvent * /*ev*/){return DONOTHING;}
AfterPaintAction Paint3DFloodFill::wheelEvent(QWheelEvent * /*ev*/){return DONOTHING;}

void Paint3DFloodFill::FloodFill(unsigned int seed[], MzBounds *bounds)
{
    std::vector < stackElem > voxelstack;
    RenderingContainer3D::MzRGBImage8* image = container->getRGBCanvas();
    if(image == NULL) return;
    unsigned int size[RenderingContainer3D::Dimensions];
    image->GetSize(size);
    for(unsigned int d = 0; d < RenderingContainer3D::Dimensions; d++)
    {
        if(seed[d] >= size[d]) return;
        bounds->max[d] = bounds->min[d] = seed[d];
    }
    RenderingContainer3D::MazdaRGBPixelType8* color;
    RenderingContainer3D::MazdaRGBPixelType8 seedcolor = image->GetPixel(seed);
    if(seedcolor.channel[0] == colorpixel.channel[0] && seedcolor.channel[1] == colorpixel.channel[1] && seedcolor.channel[2] == colorpixel.channel[2]) return;
    color = image->GetPixelPointer(seed);
    *color = colorpixel;
    stackElem current;
    for(unsigned int d = 0; d < RenderingContainer3D::Dimensions; d++) current.voxel[d] = seed[d];
    try{
        voxelstack.push_back(current);
        do
        {

            current = voxelstack.back();
            voxelstack.pop_back();

            for(unsigned int d = 0; d < RenderingContainer3D::Dimensions; d++)
            {
                current.voxel[d]++;
                if(current.voxel[d] < size[d])
                {
                    color = image->GetPixelPointer(current.voxel);
                    if(seedcolor.channel[0] == color->channel[0] && seedcolor.channel[1] == color->channel[1] && seedcolor.channel[2] == color->channel[2])
                    {
                        voxelstack.push_back(current);
                        *color = colorpixel;
                        if(bounds->max[d] < current.voxel[d]) bounds->max[d] = current.voxel[d];
                        if(bounds->min[d] > current.voxel[d]) bounds->min[d] = current.voxel[d];
                    }
                }
                current.voxel[d]-=2;
                if(current.voxel[d] < size[d])
                {
                    color = image->GetPixelPointer(current.voxel);
                    if(seedcolor.channel[0] == color->channel[0] && seedcolor.channel[1] == color->channel[1] && seedcolor.channel[2] == color->channel[2])
                    {
                        voxelstack.push_back(current);
                        *color = colorpixel;
                        if(bounds->max[d] < current.voxel[d]) bounds->max[d] = current.voxel[d];
                        if(bounds->min[d] > current.voxel[d]) bounds->min[d] = current.voxel[d];
                    }
                }
                current.voxel[d]++;
            }
         }while(voxelstack.size() > 0);
    }
    catch(...){}
}

