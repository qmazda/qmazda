/*
 * QMAZDA project: Image Analysis and Pattern Recognition
 *
 * Copyright 2013-2017 Piotr M. Szczypiński <piotr.szczypinski@p.lodz.pl>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "paintmove.h"
#include <QApplication>
#include <QPainter>

AfterPaintAction PaintMove::imageDataChanged(void)
{
    return DONOTHING;
}

AfterPaintAction PaintMove::mouseMoveEvent(QMouseEvent *ev)
{
    if(ev->buttons() & Qt::LeftButton)
    {
        ev->accept();
        QPainter pixPaint(canvas);
        QPen pen(color);

        if(QApplication::keyboardModifiers() & Qt::ControlModifier)
            pen.setColor(nextcolor);

        pixPaint.setPen(pen);
        unsigned int size[RenderingContainer2D::MzRoi::Dimensions];
        int begin[RenderingContainer2D::MzRoi::Dimensions];
        int end[RenderingContainer2D::MzRoi::Dimensions];
        int beginc[RenderingContainer2D::MzRoi::Dimensions];
        int endc[RenderingContainer2D::MzRoi::Dimensions];
        roi->GetBegin(begin);
        roi->GetSize(size);
        beginc[0] = begin[0]+npoint.x()-point.x();
        beginc[1] = begin[1]+npoint.y()-point.y();
        pixPaint.drawImage(beginc[0], beginc[1], background, beginc[0], beginc[1], size[0], size[1]);
        npoint.setX(_ev_position_x / zoom);
        npoint.setY(_ev_position_y / zoom);
        beginc[0] = begin[0]+npoint.x()-point.x();
        beginc[1] = begin[1]+npoint.y()-point.y();
        endc[0] = beginc[0] + size[0];
        endc[1] = beginc[1] + size[1];
        if(beginc[0] < 0) beginc[0] = 0;
        if(beginc[1] < 0) beginc[1] = 0;
        if(endc[1] > canvas->height()) endc[1] = canvas->height();
        if(endc[0] > canvas->width()) endc[0] = canvas->width();
        endc[0]--; endc[1]--;
        begin[0] = beginc[0]-npoint.x()+point.x();
        begin[1] = beginc[1]-npoint.y()+point.y();
        end[0] = endc[0]-npoint.x()+point.x();
        end[1] = endc[1]-npoint.y()+point.y();

        MazdaRoiRegionIterator <RenderingContainer2D::MzRoi> iterator(roi, begin, end);
        for(int y = beginc[1]; y <= endc[1]; y++)
        {
            for(int x = beginc[0]; x <= endc[0]; x++)
            {
                if(iterator.GetPixel())
                    pixPaint.drawPoint(x, y);
                ++iterator;
            }
        }
        info = QString("%1,%2 - %3,%4 [%5,%6]").arg(point.x()).arg(point.y()).arg(npoint.x()).arg(npoint.y()).arg(npoint.x()-point.x()).arg(npoint.y()-point.y());
        return REDRAW;
    }
    return DONOTHING;
}

AfterPaintAction PaintMove::mousePressEvent(QMouseEvent *ev)
{
    if(ev->buttons() & Qt::LeftButton)
    {
        point.setX(_ev_position_x / zoom);
        point.setY(_ev_position_y / zoom);
        npoint = point;
        return mouseMoveEvent(ev);
    }
    return DONOTHING;
}

AfterPaintAction PaintMove::mouseReleaseEvent(QMouseEvent *ev)
{
    ev->accept();

    int begin[RenderingContainer2D::MzRoi::Dimensions];
    unsigned int size[RenderingContainer2D::MzRoi::Dimensions];
    roi->GetBegin(begin);
    roi->GetSize(size);

    rect->setLeft(begin[0]+npoint.x()-point.x());
    rect->setTop(begin[1]+npoint.y()-point.y());
    rect->setWidth(size[0]);
    rect->setHeight(size[1]);
    if(rect->left() < 0) rect->setLeft(0);
    if(rect->left() > canvas->width()) rect->setLeft(canvas->width()-1);
    if(rect->right() < 0) rect->setRight(0);
    if(rect->right() > canvas->width()) rect->setRight(canvas->width()-1);
    if(rect->top() < 0) rect->setTop(0);
    if(rect->top() > canvas->height()) rect->setTop(canvas->height()-1);
    if(rect->bottom() < 0) rect->setBottom(0);
    if(rect->bottom() > canvas->height()) rect->setBottom(canvas->height()-1);

    if(QApplication::keyboardModifiers() & Qt::ControlModifier)
    {
        int shift[RenderingContainer2D::MzRoi::Dimensions];
        shift[0] = npoint.x()-point.x();
        shift[1] = npoint.y()-point.y();
        MazdaRoiRegionCopier< RenderingContainer2D::MzRoi, RoiAddOperation<RenderingContainer2D::MzRoi::BlockType> > copier;
        RenderingContainer2D::MzRoi* nroi = copier.Copy(roi);
        nroi->SetColor(nextcolor.rgb() & 0xffffff);
        nroi->Shift(shift);
        container->appendRoi(nroi);
        return NEWROI;
    }
    else if(QApplication::keyboardModifiers() & Qt::ShiftModifier)
    {
        return FINALIZE;
    }
    else
    {
        roi->Erase();
        return FINALIZE;
    }
    return DONOTHING;
}

AfterPaintAction PaintMove::mouseDoubleClickEvent(QMouseEvent *ev){return DONOTHING;}
AfterPaintAction PaintMove::wheelEvent(QWheelEvent *ev){return DONOTHING;}

PaintMove::PaintMove(RenderingContainer2D *icontainer, double izoom, QImage *icanvas, QRect *irect)
{
    zoom = izoom;
    canvas = icanvas;
    rect = irect;
    background = icanvas->copy();
    container = icontainer;
    roi = container->getActiveRoi();
    color = roi->GetColor() | 0xff000000;
    nextcolor = RoiDefaultColors[container->getNumberOfRois()%(sizeof(RoiDefaultColors)/sizeof(unsigned int))] | 0xff000000;
    if(nextcolor == color) nextcolor = RoiDefaultColors[(container->getNumberOfRois()+1)%(sizeof(RoiDefaultColors)/sizeof(unsigned int))] | 0xff000000;
}
