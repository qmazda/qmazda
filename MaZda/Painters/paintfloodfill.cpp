/*
 * QMAZDA project: Image Analysis and Pattern Recognition
 *
 * Copyright 2013-2017 Piotr M. Szczypiński <piotr.szczypinski@p.lodz.pl>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "paintfloodfill.h"
#include <QApplication>
#include <QPainter>
#ifdef USE_OPENCV_CPP_API
#include <opencv2/imgproc/imgproc.hpp>
#else
#include <opencv2/imgproc/imgproc_c.h>
#endif


AfterPaintAction PaintFloodFill::imageDataChanged(void)
{
    return DONOTHING;
}

AfterPaintAction PaintFloodFill::mouseMoveEvent(QMouseEvent *ev)
{
    info = QString("%1,%2").arg(_ev_position_x / zoom).arg(_ev_position_y / zoom);
    return DONOTHING;
}

AfterPaintAction PaintFloodFill::mousePressEvent(QMouseEvent *ev)
{
    if(ev->buttons() & Qt::LeftButton)
    {
        ev->accept();
        QPoint point;
        point.setX(_ev_position_x / zoom);
        point.setY(_ev_position_y / zoom);
        unsigned char* bits = (unsigned char*) canvas->bits();
        int channels = canvas->depth();
        if(channels == 24 || channels == 32)
        {
            channels /= 8;
            int stride = canvas->bytesPerLine(); //((canvas->width()*channels + 3) >> 2) << 2;

            #ifdef USE_OPENCV_CPP_API
                cv::Mat cvimg(canvas->height(), canvas->width(),
                              channels == 4 ?CV_8UC4 :CV_8UC3, (void*)bits, stride);
                cv::floodFill(cvimg, cv::Point(point.x(), point.y()),
                              cv::Scalar(color.red(), color.green(), color.blue()));

            #else
                IplImage cvimg;
                cvInitImageHeader(&cvimg, cvSize(canvas->width(),
                                  canvas->height()), IPL_DEPTH_8U, channels);
                cvSetData(&cvimg, bits, stride);
                cvFloodFill(&cvimg, cvPoint(point.x(), point.y()),
                            cvScalar(color.red(), color.green(), color.blue()));

            #endif

            rect->setTop(0);
            rect->setLeft(0);
            rect->setBottom(canvas->height()-1);
            rect->setRight(canvas->width()-1);
        }
        return FINALIZE;
    }
    return DONOTHING;
}

AfterPaintAction PaintFloodFill::mouseReleaseEvent(QMouseEvent *ev)
{
    ev->accept();
    return CANCEL;
}

AfterPaintAction PaintFloodFill::mouseDoubleClickEvent(QMouseEvent *ev){return DONOTHING;}
AfterPaintAction PaintFloodFill::wheelEvent(QWheelEvent *ev){return DONOTHING;}

PaintFloodFill::PaintFloodFill(QColor icolor, double izoom, QImage *icanvas, QRect *irect)
{
    zoom = izoom;
    color = icolor;
    canvas = icanvas;
    rect = irect;
}
