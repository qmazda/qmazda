/*
 * QMAZDA project: Image Analysis and Pattern Recognition
 *
 * Copyright 2013-2017 Piotr M. Szczypiński <piotr.szczypinski@p.lodz.pl>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef PaintActiveContour_H
#define PaintActiveContour_H

#include "ipaint.h"
#include <QImage>
#include <QStatusBar>
#include <QColor>

#include "../SharedImage/mazdaimage.h"
typedef MazdaImage<unsigned char, 2> MzImage2D8;
typedef float acconFloat;
const acconFloat accon2pi = 6.28318530717958647692529;
//const unsigned int profilecount = 256;
const float profilestep = 0.5;

#pragma pack(push, 4)
/**
 * @brief The RadialContourParams struct
 * @param xo, yo - środek obiektu wskazany myszą przez użytkownika
 * @param ro - wstępny, inicjowany promień konturu (jeśli wartość ujemna to kontur inicjowany w maksimum gradientu)
 * @param ex - waga oddziaływania jasności obrazu (ujemna dla ciemnych obszarów na jasnym tle, dodatnia dla jasnych)
 * @param eg - waga oddziaływania gradientu obrazu (ujemna dla ciemnych obszarów na jasnym tle, dodatnia dla jasnych)
 * @param et - próg jasności obrazu (powiązany z wagą ex)
 * @param ti - indeks <0, 31> stablicowanych współczynników wygładzających (większy indeks - bardziej gładki)
 * @param sm - liczba węzłów, punktów wiodących, minimum 25
 * @param i1, i2 - liczba iteracji głównych i wygaszania
 */
struct RadialContourParams
{
    acconFloat transform[6];
    acconFloat ex;
    acconFloat eg;
    acconFloat et;
    unsigned int ti;
    unsigned int i1;
    unsigned int i2;
};

/**
 * @brief The AcconImage struct
 * @param data - wskaźnik do bufora obrazu 8 bitów na piksel
 * @param height, width - wysokość i szerokość obrazu
 * @param stride - liczba bajtów w linii obrazu
 */
struct AcconImage
{
    unsigned char* data;
    unsigned int height;
    unsigned int width;
    unsigned int stride;
};
#pragma pack(pop)

class PaintActiveContour : public iPaint
{
private:
    int image_direction;
    RadialContourParams params =
    {
        {(acconFloat)1.0, (acconFloat)0.0, (acconFloat)0.0, (acconFloat)0.0, (acconFloat)1.0, (acconFloat)0.0},
        (acconFloat)0.01, //acconFloat ex;
        (acconFloat)-0.1, //acconFloat eg;
        (acconFloat)24, //acconFloat et;
        20, //unsigned int ti;
        220, //unsigned int i1;
        10 //unsigned int i2;
    };

public:
    AfterPaintAction keyPressEvent(QKeyEvent*){return DONOTHING;}
    AfterPaintAction keyReleaseEvent(QKeyEvent*){return DONOTHING;}
    AfterPaintAction mouseMoveEvent(QMouseEvent *ev);
    AfterPaintAction mousePressEvent(QMouseEvent *ev);
    AfterPaintAction mouseReleaseEvent(QMouseEvent *ev);
    AfterPaintAction mouseDoubleClickEvent(QMouseEvent *ev);
    AfterPaintAction wheelEvent(QWheelEvent *ev);
    AfterPaintAction imageDataChanged(void);
    MzBounds getModifiedRegion(void) {MzBounds b; return b;}
    QString* getStatus(void) {return &info;}
    PaintActiveContour(QColor icolor, int params[], double izoom, QImage *icanvas, MzImage2D8* gray, QRect *irect);
    ~PaintActiveContour();

private:
    acconFloat acconImageLinearInterpolation(unsigned char* buf, unsigned int stride, const acconFloat x, const acconFloat y);
    //void acconComputeRadialGradientProfile(const AcconImage* image, const acconFloat* sina, const acconFloat* cosa, acconFloat* prof, acconFloat* prr);
    void acconComputeRadialGradientProfile(const AcconImage* image,
                                         const acconFloat *sina,
                                         const acconFloat *cosa,
                                         const acconFloat *dist,
                                         const unsigned int profilecount,
                                         acconFloat **profile,
                                         acconFloat* level, acconFloat* gradient);
    acconFloat acconProfileLinearInterpolation(const acconFloat *prof, const unsigned int profilecount, const acconFloat r);
    void acconFindRadialContour(const AcconImage* image, RadialContourParams* params);

    void drawContour(QRect *rectn, bool filled);
    acconFloat* xy;
    acconFloat** profile;
    acconFloat* pr1;
    acconFloat* pr2;
    acconFloat* prx;
    acconFloat* pry;
    acconFloat* prp;

    int radia, radib;
    double zoom;
    double angle;
    QPoint point;
    QPoint endpoint;
    QRect* rect;
    QRect rectp;
    QColor color;
    QImage *canvas;
    MzImage2D8* gray;
    QImage background;
    QString info;
    unsigned int nodes_count;
    bool threshold_setup;
};

#endif // PaintActiveContour_H
