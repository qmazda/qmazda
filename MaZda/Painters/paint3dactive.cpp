/*
 * QMAZDA project: Image Analysis and Pattern Recognition
 *
 * Copyright 2013-2017 Piotr M. Szczypiński <piotr.szczypinski@p.lodz.pl>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "paint3dactive.h"


const int gradientKernelSize = 20;
const double gradientKernel[gradientKernelSize] =
    {
        -0.000963189814, -0.002321379259, -0.004941967782, -0.009256359190, -0.015162057243,
        -0.021510979161, -0.025986883789, -0.025825761259, -0.019311740994, -0.007186350878,
        0.007186350878, 0.019311740994, 0.025825761259, 0.025986883789, 0.021510979161,
        0.015162057243, 0.009256359190, 0.004941967782, 0.002321379259, 0.000963189814
    };

/*
const int gradientOKernelSize = 19;
const double gradientOKernel[gradientOKernelSize] =
    {
    -0.0016422845365, -0.0036316735205, -0.007099163486, -0.0122092082165,
    -0.018336518202, -0.023748931475, -0.025906322524, -0.0225687511265,
    -0.013249045936, 0, 0.013249045936,
    0.0225687511265, 0.025906322524, 0.023748931475, 0.018336518202,
    0.0122092082165, 0.007099163486, 0.0036316735205, 0.0016422845365
    };

const int deriv2KernelSize = 21;
const double deriv2Kernel[deriv2KernelSize] =
    {
        -0.000963189814, -0.001358189445, -0.002620588523, -0.004314391408, -0.005905698053,
        -0.006348921918, -0.004475904628, 0.000161122529999998, 0.006514020265, 0.012125390116,
        0.014372701756,
        0.012125390116, 0.006514020265, 0.000161122529999998, -0.004475904628, -0.006348921918,
        -0.005905698053, -0.004314391408, -0.002620588523, -0.001358189445, -0.000963189814
    };
*/
Paint3DActiveSurface::Paint3DActiveSurface(RenderingContainer3D* icontainer, int params[4], Painter3D* ipainter, unsigned int list,  GLdouble* modelMat, GLdouble *volumeMat, bool *ierase) :
    Paint3dBlock(icontainer, ipainter, list, modelMat, volumeMat, ierase),
    Sphear(params[2] < 0 ? 0 : (params[2] > 31 ? 31 : params[2]))
{
    redraw_ellipsoid = false;
    nodes_count = verticesNumber();
//    pr0.reserve(nodes_count);
    pr1 = new float[nodes_count];
    pr2 = new float[nodes_count];

//    profile = new unsigned char*[nodes_count];
//    profile[0] = new unsigned char[nodes_count*profile_length];
    profile = new float*[nodes_count];
    profile[0] = new float[nodes_count*profile_length];
    for(unsigned int s = 1; s < nodes_count; s++)
        profile[s] = profile[0] + s*profile_length;

    smoothing_strength = ((float)params[0]*params[0]+1)/3100;
    image_direction = params[1];
    threshold_setup = params[3];

//    profile.reserve(nodes_count);
//    for(unsigned int s = 0; s < nodes_count; s++)
//        profile[s].reserve(profile_length);
}
Paint3DActiveSurface::~Paint3DActiveSurface()
{
    delete[] profile[0];
    delete[] profile;
    delete[] pr2;
    delete[] pr1;
}

void Paint3DActiveSurface::resetSurface(void)
{
//    pra = 0.0;
    direction = 0;
    unsigned int imax = profile_length-gradientKernelSize+1;
    float* grads = new float[imax];

//    float dmax = 0;
    MzImage3D8* image = container->getGrayCanvas();
    unsigned int size[3];
    image->GetSize(size);

    double threshold = 0;

    for(unsigned int s = 0; s < nodes_count; s++)
    {
        double ip[3];
        double op[3];
        vertex(s, ip);
//        Transform(ip, op, mat);
        op[0] = (ip[0]*modelMat[0]+ip[1]*modelMat[4]+ip[2]*modelMat[8]);
        op[1] = (ip[0]*modelMat[1]+ip[1]*modelMat[5]+ip[2]*modelMat[9]);
        op[2] = (ip[0]*modelMat[2]+ip[1]*modelMat[6]+ip[2]*modelMat[10]);
//        double d = (op[0]*op[0] + op[1]*op[1] + op[2]*op[2]);
//        pra += d;
//        d = sqrt(d);
//        if(d > dmax)
//            dmax = d;
//        MzImage3D8* image = container->getGrayCanvas();
//        unsigned int size[3];
//        image->GetSize(size);

        for(unsigned int i = 0; i < profile_length; i++)
        {
            double mul = 2.0*upsizing_sphear*i/profile_length;
            double ipa[3];
            ipa[0] = op[0] * mul;
            ipa[1] = op[1] * mul;
            ipa[2] = op[2] * mul;
            ipa[0] += modelMat[12];
            ipa[1] += modelMat[13];
            ipa[2] += modelMat[14];
            int opa[3];
            ImageToVoxelSpace(ipa, opa);

            if(opa[0] < 0)
                opa[0] = 0;
            if(opa[0]>=size[0])
                opa[0] = size[0]-1;
            if(opa[1] < 0)
                opa[1] = 0;
            if(opa[1]>=size[1])
                opa[1] = size[1]-1;
            if(opa[2] < 0)
                opa[2] = 0;
            if(opa[2]>=size[2])
                opa[2] = size[2]-1;
            profile[s][i] = image->GetPixel((unsigned int *)opa);
        }

        threshold += profile[s][profile_length/2];

//        for(unsigned int i = 0; i < profile_length; i++)
//            profile[s][i] = (i < 20 ? 50 :150);
//        for(unsigned int i = 0; i < 41; i++)
//            printf("%i ", (int)profile[s][i]);
//        printf("\n");
//        fflush(stdout);

        for(unsigned int i = 0; i < imax; i++)
        {
            float grad = 0.0;
            for(unsigned int g = 0; g < gradientKernelSize; g++)
            {
                grad += (gradientKernel[g] * profile[s][i+g]);
            }
            grads[i] = grad;
        }
        unsigned int pol = (gradientKernelSize-1)/2;
        for(unsigned int i = 1; i < imax; i++)
            profile[s][i+pol] += gradient_stregth*(grads[i-1]-grads[i]);
//            profile[s][i+pol] += (gradient_stregth*(grads[i-1]-grads[i])+0.5);

//        for(unsigned int i = 0; i < 41; i++)
//            printf("%i ", (int)profile[s][i]);
//        printf("\n");
//        fflush(stdout);
        direction += grads[imax/2];
    }
    delete[] grads;
//    pra = upsizing_sphear * sqrt(pra / nodes_count);
//    dmax = upsizing_sphear * sqrt(dmax);
    for(unsigned int s = 0; s < nodes_count; s++)
    {
        pr1[s] = upsizing_sphear;
    }

    if(threshold_setup)
        image_threshold = threshold/nodes_count;
    else
        image_threshold = 127.5;

}
/*
float Paint3DActiveSurface::filtrationKrnel(float distance, float sharpness)
{
    float a = sharpness*asin(distance);
    if(a < 0.0001)
        return 1;
    return exp(-a)*sin(a)/a;
}
*/
void Paint3DActiveSurface::fitSurface(void)
{
    float ie;
    if(image_direction > 0)
        ie = image_strength;
    else if(image_direction < 0)
        ie = -image_strength;
    else
        ie = direction < 0 ? image_strength : -image_strength;

    for(unsigned int i = 0; i < iterations; i++)
    {
//        printf("I = %i\n", i);
//        fflush(stdout);

        for(unsigned int s = 0; s < nodes_count; s++)
        {
            float r = pr1[s]*profile_length/(2.0*upsizing_sphear);
            int rl = floor(r);
            int rh = ceil(r);
            float rwl = r - rl;
            if(rl < 0) rl = 0;
            if(rh < 0) rh = 0;
            if(rl >= profile_length) rl = profile_length - 1;
            if(rh >= profile_length) rh = profile_length - 1;
            float val = (1.0 - rwl)*(float)profile[s][rl] + (rwl)*(float)profile[s][rh];
            val -= image_threshold;
            pr2[s] = pr1[s] + val * ie;

//            if(pr2[s] < 0.01*pra)
//                pr2[s] = 0.01*pra;
//            if(pr2[s] > 2.0*pra)
//                pr2[s] = 2.0*pra;
        }

        for(unsigned int s = 0; s < nodes_count; s++)
        {
            /*
            // ZA DLUGI CZAS WYGLADZANIA
            double divider = 0.0;
            double newvalue = 0.0;
            double v[3];
            vertex(s, v);
            for(unsigned int ss = 0; ss < nodes_count; ss++)
            {
                double vv[3];
                vertex(ss, vv);
                vv[0] -= v[0];
                vv[1] -= v[1];
                vv[2] -= v[2];
                double distance = sqrt(vv[0]*vv[0] + vv[1]*vv[1] + vv[2]*vv[2]);
                float w = filtrationKrnel(distance, sharpness);
                divider += w;
                newvalue += w*pr2[ss];
            }
            */

            double divider = 1;
            double newvalue = pr2[s];
            for(unsigned int ss = 0; ss < 6; ss++)
            {
                int sss = neighbor(s, ss);
                if(sss < 0) break;
                divider += smoothing_strength;
                newvalue += smoothing_strength*pr2[sss];
            }
            newvalue /= divider;
            float dr = (upsizing_sphear - pr2[s])/upsizing_sphear;
            newvalue += (dr * anchor_strength);

            if(i > iterationx)
            {
                float w = float(iterations - i) / float(iterations - iterationx);
                pr1[s] = w * newvalue + (1.0 - w) * pr1[s];
            }
            else
            {
                pr1[s] = newvalue;
            }
            if(pr1[s] < 0.1*upsizing_sphear)
                pr1[s] = 0.1*upsizing_sphear;
            if(pr1[s] > 1.9*upsizing_sphear)
                pr1[s] = 1.9*upsizing_sphear;
        }
    }
}

void Paint3DActiveSurface::createBall(void)
{
    resetSurface();
    fitSurface();
    unsigned int tn = edgesNumber();
    unsigned int* tr = new unsigned int[tn*2];
    edges(tr);
    double v[3];

    glEnable(GL_CULL_FACE);
    glDisable(GL_TEXTURE_1D);
    glDisable(GL_TEXTURE_2D);
    glFrontFace(GL_CCW);
    glBegin(GL_LINES);

    if(redraw_ellipsoid)
    {
        double d = upsizing_sphear;
        for(unsigned int k = 0; k < tn; k++)
        {
            unsigned int s = tr[k*2];
            vertex(s, v);
            glNormal3f(v[0], v[1], v[2]);
            v[0] *= d;
            v[1] *= d;
            v[2] *= d;
            glVertex3f(v[0], v[1], v[2]);
            s = tr[k*2+1];
            vertex(s, v);
            glNormal3f(v[0], v[1], v[2]);
            v[0] *= d;
            v[1] *= d;
            v[2] *= d;
            glVertex3f(v[0], v[1], v[2]);
        }
    }
    else
    {
        for(unsigned int k = 0; k < tn; k++)
        {
            unsigned int s = tr[k*2];
            double d = (pr1[s]);
            vertex(s, v);
            glNormal3f(v[0], v[1], v[2]);
            v[0] *= d;
            v[1] *= d;
            v[2] *= d;
            glVertex3f(v[0], v[1], v[2]);
            s = tr[k*2+1];
            d = (pr1[s]);
            vertex(s, v);
            glNormal3f(v[0], v[1], v[2]);
            v[0] *= d;
            v[1] *= d;
            v[2] *= d;
            glVertex3f(v[0], v[1], v[2]);

    //        s = tr[k*2+1];
    //        v[0] = pr0[s] * pr1[s] * prx[s];
    //        v[1] = pr0[s] * pr1[s] * pry[s];
    //        v[2] = pr0[s] * pr1[s] * prz[s];
    //        glNormal3f(prx[s], pry[s], prz[s]);
    //        glVertex3f(v[0], v[1], v[2]);
        }
    }
    glNormal3f(0, 0, 0);
    glVertex3f(upsizing_sphear, 0, 0);
    glVertex3f(-upsizing_sphear, 0, 0);
    glVertex3f(0, upsizing_sphear, 0);
    glVertex3f(0, -upsizing_sphear, 0);
    glVertex3f(0, 0, upsizing_sphear);
    glVertex3f(0, 0, -upsizing_sphear);

    glEnd();
    delete[] tr;
}


void Paint3DActiveSurface::ballFill(void)
{
/*
    double mat[16];
    mat[0] = 1;
    mat[1] = 0;
    mat[2] = 0;
    mat[3] = modelMat[3];
    mat[4] = 0;
    mat[5] = 1;
    mat[6] = 0;
    mat[7] = modelMat[7];
    mat[8] = 0;
    mat[9] = 0;
    mat[10] = 1;
    mat[11] = modelMat[11];
    mat[12] = modelMat[12];
    mat[13] = modelMat[13];
    mat[14] = modelMat[14];
    mat[15] = modelMat[15];
*/
    unsigned int tn = trianglesNumber();
    unsigned int* tr = new unsigned int[tn*3];
    triangles(tr);
    double pi[4][3];
    double po[4][3];

    pi[3][0] = 0.0;
    pi[3][1] = 0.0;
    pi[3][2] = 0.0;

    for(unsigned int k = 0; k < tn; k++)
    {
        for(unsigned int v = 2; v < 3; v--)
        {
            unsigned int s = tr[k*3+v];
            double d = (pr1[s]);
            vertex(s, pi[v]);
            pi[v][0] *= d;
            pi[v][1] *= d;
            pi[v][2] *= d;
        }
        TransformTetrahedronAndFill(pi, po, modelMat);
        for(int ii = 0; ii < 4; ii++)
        {
            if(k == 0 && ii == 0)
            {
                for(int iii = 0; iii < 3; iii++)
                {
                    int b = round(po[ii][iii]);
                    cube.min[iii] = cube.max[iii] = b;
                }
            }
            else
            {
                for(int iii = 0; iii < 3; iii++)
                {
                    int b = round(po[ii][iii]);
                    if(cube.min[iii] > b) cube.min[iii] = b;
                    if(cube.max[iii] < b) cube.max[iii] = b;
                }
            }
        }
    }
}

AfterPaintAction Paint3DActiveSurface::changeShape(int /*delta*/, bool /*pressed*/)
{
    return DONOTHING;
}

void Paint3DActiveSurface::createBlock(void)
{
    createBall();
}
void Paint3DActiveSurface::rasterizeBlock(void)
{
    ballFill();
}

AfterPaintAction Paint3DActiveSurface::imageDataChanged(void)
{
    updateShape();
    return REDRAW;
}

AfterPaintAction Paint3DActiveSurface::mouseMoveEvent(QMouseEvent *ev)
{
    redraw_ellipsoid = false;
    if(ev->buttons() & Qt::LeftButton)
    {
        unsigned int m = QApplication::keyboardModifiers();
        if(
            (!((m&Qt::ControlModifier) || (m&Qt::AltModifier) || (m&Qt::ShiftModifier))) ||
            ((m&Qt::AltModifier) && (m&Qt::ShiftModifier))
          )
        {
            redraw_ellipsoid = true;
        }
    }
    AfterPaintAction ret = Paint3dBlock::mouseMoveEvent(ev);
    if(ret == REDRAW)
        updateShape();
    return ret;
}

AfterPaintAction Paint3DActiveSurface::mouseReleaseEvent(QMouseEvent *ev)
{
//    if(ev->buttons() & Qt::LeftButton)
//    {
        if(redraw_ellipsoid)
        {
            redraw_ellipsoid = false;
            updateShape();
            return REDRAW;
        }
//    }
    return DONOTHING;
}

AfterPaintAction Paint3DActiveSurface::wheelEvent(QWheelEvent *ev)
{
    AfterPaintAction ret = Paint3dBlock::wheelEvent(ev);
    if(ret == REDRAW)
        updateShape();
    return ret;
}
