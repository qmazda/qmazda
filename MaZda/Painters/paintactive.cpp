/*
 * QMAZDA project: Image Analysis and Pattern Recognition
 *
 * Copyright 2013-2017 Piotr M. Szczypiński <piotr.szczypinski@p.lodz.pl>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "paintactive.h"
#include <QApplication>
#include <QPainter>

//#include <math.h>
#include <cmath>


#define sqrt2rec 0.70710678
#define pi4 0.78539816
#define pi2 1.5707963
#define pi 3.1415927
#define twopi 6.2831853
#define elipointsno 1024


const int gradientKernelSize = 20;
const double gradientKernel[gradientKernelSize] =
    {
        -0.000963189814, -0.002321379259, -0.004941967782, -0.009256359190, -0.015162057243,
        -0.021510979161, -0.025986883789, -0.025825761259, -0.019311740994, -0.007186350878,
        0.007186350878, 0.019311740994, 0.025825761259, 0.025986883789, 0.021510979161,
        0.015162057243, 0.009256359190, 0.004941967782, 0.002321379259, 0.000963189814
    };

/*
const int deriv2KernelSize = 21;
const double deriv2Kernel[deriv2KernelSize] =
    {
        -0.000963189814, -0.001358189445, -0.002620588523, -0.004314391408, -0.005905698053,
        -0.006348921918, -0.004475904628, 0.000161122529999998, 0.006514020265, 0.012125390116,
        0.014372701756,
        0.012125390116, 0.006514020265, 0.000161122529999998, -0.004475904628, -0.006348921918,
        -0.005905698053, -0.004314391408, -0.002620588523, -0.001358189445, -0.000963189814
    };
*/
const int tensionKernelSize = 8;
const int tensionKernelsCount = 32;
const acconFloat tensionKernels[tensionKernelsCount][tensionKernelSize] =
{
//Tau1 = 0.005102, Tau2 = 0.002551
    {0.975568771362, 0.014491376467, -0.002211468294, -0.000068986767, 0.000004474943, 0.000000238280, -0.000000007586, -0.000000000706},
//Tau1 = 0.005906, Tau2 = 0.002953
    {0.971903741360, 0.016635881737, -0.002503010212, -0.000090698028, 0.000005624708, 0.000000356783, -0.000000010014, -0.000000001196},
//Tau1 = 0.006837, Tau2 = 0.003418
    {0.967720270157, 0.019074155018, -0.002822882961, -0.000118899115, 0.000006983492, 0.000000531282, -0.000000012587, -0.000000002006},
//Tau1 = 0.007915, Tau2 = 0.003957
    {0.962954819202, 0.021839099005, -0.003170447890, -0.000155356349, 0.000008538575, 0.000000786096, -0.000000014625, -0.000000003327},
//Tau1 = 0.009162, Tau2 = 0.004581
    {0.957538902760, 0.024965053424, -0.003543655854, -0.000202230629, 0.000010238032, 0.000001154581, -0.000000014644, -0.000000005445},
//Tau1 = 0.010606, Tau2 = 0.005303
    {0.951399862766, 0.028487123549, -0.003938570153, -0.000262123038, 0.000011965510, 0.000001681398, -0.000000009717, -0.000000008768},
//Tau1 = 0.012278, Tau2 = 0.006139
    {0.944461524487, 0.032440226525, -0.004348803312, -0.000338102982, 0.000013504159, 0.000002424526, 0.000000005555, -0.000000013849},
//Tau1 = 0.014214, Tau2 = 0.007107
    {0.936645388603, 0.036857921630, -0.004764875397, -0.000433705049, 0.000014487036, 0.000003456255, 0.000000040631, -0.000000021361},
//Tau1 = 0.016454, Tau2 = 0.008227
    {0.927872598171, 0.041770812124, -0.005173515994, -0.000552872894, 0.000014331650, 0.000004861724, 0.000000111357, -0.000000031984},
//Tau1 = 0.019048, Tau2 = 0.009524
    {0.918065845966, 0.047204766423, -0.005556941964, -0.000699827855, 0.000012157642, 0.000006732860, 0.000000243195, -0.000000046095},
//Tau1 = 0.022050, Tau2 = 0.011025
    {0.907151937485, 0.053178772330, -0.005892171524, -0.000878830499, 0.000006689377, 0.000009154467, 0.000000475332, -0.000000063091},
//Tau1 = 0.025526, Tau2 = 0.012763
    {0.895065128803, 0.059702582657, -0.006150452886, -0.001093805535, -0.000003850214, 0.000012178206, 0.000000865329, -0.000000080106},
//Tau1 = 0.029549, Tau2 = 0.014775
    {0.881750166416, 0.066774331033, -0.006296914537, -0.001347803045, -0.000021841335, 0.000015779384, 0.000001493295, -0.000000089801},
//Tau1 = 0.034207, Tau2 = 0.017103
    {0.867165803909, 0.074378117919, -0.006290542893, -0.001642275834, -0.000050342205, 0.000019791467, 0.000002463526, -0.000000076904},
//Tau1 = 0.039599, Tau2 = 0.019799
    {0.851288676262, 0.082481943071, -0.006084620953, -0.001976176398, -0.000093104383, 0.000023814908, 0.000003900062, -0.000000013332},
//Tau1 = 0.045840, Tau2 = 0.022920
    {0.834115862846, 0.091036126018, -0.005627744831, -0.002344907261, -0.000154485795, 0.000027101240, 0.000005930959, 0.000000147764},
//Tau1 = 0.053066, Tau2 = 0.026533
    {0.815667986870, 0.099972464144, -0.004865469877, -0.002739187563, -0.000239212328, 0.000028420915, 0.000008654607, 0.000000477782},
//Tau1 = 0.061430, Tau2 = 0.030715
    {0.795990467072, 0.109204411507, -0.003742633853, -0.003143957118, -0.000351940515, 0.000025935156, 0.000012081229, 0.000001075402},
//Tau1 = 0.071113, Tau2 = 0.035557
    {0.775153994560, 0.118628330529, -0.002206274774, -0.003537465585, -0.000496587192, 0.000017106006, 0.000016044965, 0.000002061442},
//Tau1 = 0.082323, Tau2 = 0.041161
    {0.753253817558, 0.128125980496, -0.000208979094, -0.003890727879, -0.000675421092, -0.000001307181, 0.000020088064, 0.000003559017},
//Tau1 = 0.095299, Tau2 = 0.047649
    {0.730407536030, 0.137568101287, 0.002287564799, -0.004167519044, -0.000887952745, -0.000033106924, 0.000023329487, 0.000005651424},
//Tau1 = 0.110320, Tau2 = 0.055160
    {0.706751585007, 0.146818801761, 0.005309124477, -0.004325070418, -0.001129714889, -0.000082315659, 0.000024345292, 0.000008312111},
//Tau1 = 0.127710, Tau2 = 0.063855
    {0.682436764240, 0.155740737915, 0.008865971118, -0.004315529019, -0.001391077996, -0.000152593086, 0.000021104279, 0.000011307367},
//Tau1 = 0.147840, Tau2 = 0.073920
    {0.657623112202, 0.164200276136, 0.012950682081, -0.004088184796, -0.001656280947, -0.000246406562, 0.000011015226, 0.000014083638},
//Tau1 = 0.171143, Tau2 = 0.085572
    {0.632474720478, 0.172072649002, 0.017537014559, -0.003592334688, -0.001902882825, -0.000364000414, -0.000008855242, 0.000015666978},
//Tau1 = 0.198119, Tau2 = 0.099060
    {0.607154250145, 0.179246246815, 0.022579915822, -0.002780567855, -0.002101802500, -0.000502263953, -0.000041348241, 0.000014617766},
//Tau1 = 0.229348, Tau2 = 0.114674
    {0.581818342209, 0.185626477003, 0.028016831726, -0.001612162217, -0.002218074864, -0.000653655152, -0.000088547895, 0.000009094833},
//Tau1 = 0.265499, Tau2 = 0.132749
    {0.556613862514, 0.191138073802, 0.033770017326, -0.000056273639, -0.002212338150, -0.000805358985, -0.000150918568, -0.000002918293},
//Tau1 = 0.307348, Tau2 = 0.153674
    {0.531674385071, 0.195726588368, 0.039749778807, 0.001905401587, -0.002042992972, -0.000938868499, -0.000226332122, -0.000023189728},
//Tau1 = 0.355794, Tau2 = 0.177897
    {0.507118582726, 0.199358552694, 0.045858234167, 0.004276738036, -0.001668849378, -0.001030132640, -0.000309120456, -0.000052617746},
//Tau1 = 0.411876, Tau2 = 0.205938
    {0.483048915863, 0.202020823956, 0.051993429661, 0.007046253420, -0.001052025240, -0.001050352934, -0.000389319786, -0.000090458314},
//Tau1 = 0.476798, Tau2 = 0.238399
    {0.459551602602, 0.203719288111, 0.058053448796, 0.010187225416, -0.000160785596, -0.000967411674, -0.000452270935, -0.000133497189}
};

acconFloat PaintActiveContour::acconImageLinearInterpolation(unsigned char* buf, unsigned int stride, const acconFloat x, const acconFloat y)
{
    acconFloat wtl, wtr, wbl, wbr, value, dy;
    unsigned char* b;
    int ix, iy;
    ix = (int) floor(x);
    iy = (int) floor(y);
    wtr = wbr = x - ix;
    wtl = wbl = (1.0 - wbr);
    dy = y - iy;
    wbl *= dy;
    wbr *= dy;
    dy = 1.0 - dy;
    wtl *= dy;
    wtr *= dy;
    b = buf + iy*stride+ix;
    value  = (wtl**b + wtr**(b+1));
    b += stride;
    value += (wbl**b + wbr**(b+1));
    return value;
}


acconFloat PaintActiveContour::acconProfileLinearInterpolation(const acconFloat* prof, const unsigned int profilecount, const acconFloat r)
{
    acconFloat wl, wr;
    int ir;
    acconFloat rr = r/profilestep;
    if(rr <= 0)
        return 0.0;
    if(rr >= profilecount-1.1)
        return 0.0;
    ir = (int) floor(rr);
    wr = rr - ir;
    wl = (1.0 - wr);
    return (wl*prof[ir] + wr*prof[ir+1]);
}


void PaintActiveContour::acconComputeRadialGradientProfile(const AcconImage* image,
                                                           const acconFloat* sina,
                                                           const acconFloat* cosa,
                                                           const acconFloat* dist,
                                                           const unsigned int profilecount,
                                                           acconFloat** profile,
                                                           acconFloat* level, acconFloat* gradient)
{
    *level = 0.0;
    *gradient = 0.0;
    acconFloat* profl = (acconFloat*)malloc(sizeof(acconFloat)*(profilecount+gradientKernelSize));
    for(unsigned int s = 0; s < nodes_count; s++)
    {
        acconFloat dx, dy;
        dy = sqrt(sina[s]*sina[s] + cosa[s]*cosa[s]);
        dx = sina[s]/dy;
        dy = cosa[s]/dy;
        acconFloat* pprofl = profl;
        for(int r = -gradientKernelSize/2; r < (int)profilecount+gradientKernelSize/2; r++)
        {
            acconFloat x, y;
            x = (dx * r * profilestep)+params.transform[2];
            y = (dy * r * profilestep)+params.transform[5];
            if(x < 0.0)
                x = 0;
            if(y < 0.0)
                y = 0;
            if(x > (acconFloat)image->width-1.1)
                x = (acconFloat)image->width-1.1;
            if(y > (acconFloat)image->height-1.1)
                y = (acconFloat)image->height-1.1;
            *pprofl = acconImageLinearInterpolation(image->data, image->stride, x, y);
            pprofl++;
        }
        //acconFloat* pprof = prof+s*profilecount;
        for(unsigned int r = 0; r < profilecount; r++)
        {
            acconFloat grad = 0;
            acconFloat* pprofl = profl+r;
            for(unsigned int g = 0; g < gradientKernelSize; g++, pprofl++)
            {
                grad += (gradientKernel[g] * *pprofl);
            }
            profile[s][r] = grad;
        }
        pprofl = profl+gradientKernelSize/2;

        int d = dist[s]/profilestep+0.5;
        if(d >= profilecount)
            d = profilecount-1;
        if(d < 0)
            d = 0;

        *gradient += profile[s][d];
        *level += pprofl[d];

        acconFloat preftmp = profile[s][0];
        for(unsigned int r = 1; r < profilecount; r++, pprofl++)
        {
            acconFloat temp = profile[s][r];
            profile[s][r-1] = params.eg * (preftmp - temp) + params.ex * (*pprofl - params.et);
            preftmp = temp;
        }
        profile[s][profilecount-1] = 0;
    }
    *gradient /= nodes_count;
    *level /= nodes_count;
    free(profl);
}

void PaintActiveContour::acconFindRadialContour(const AcconImage* image, RadialContourParams* params)
{
    unsigned int i;
    acconFloat ra = sqrt(params->transform[0]*params->transform[0] + params->transform[3]*params->transform[3]);
    acconFloat rb = sqrt(params->transform[1]*params->transform[1] + params->transform[4]*params->transform[4]);
    acconFloat rm = ra > rb ? ra : rb;
    acconFloat rab = sqrt(ra*ra+rb*rb)/2.0;
    unsigned int profilecount = 2*((unsigned int)rm/profilestep + 1);


/*
// VECTOR ALOKUJE NA STOSIE I BRAKUJE PAMIECI!!!
    std::vector < std::vector <acconFloat>> profile;
    profile.resize(nodes_count);
    for(i = 0; i < nodes_count; i++)
        profile[i].resize(profilecount);
    std::vector <acconFloat> pr1;
    std::vector <acconFloat> pr2;
    pr1.resize(nodes_count, rab);
    pr2.resize(nodes_count, rab);
    std::vector <acconFloat> prx;
    prx.resize(nodes_count);
    std::vector <acconFloat> pry;
    pry.resize(nodes_count);
    std::vector <acconFloat> prp;
    prp.resize(nodes_count);
*/

    for(unsigned int s = 0; s < nodes_count; s++)
    {
        pr1[s] = rab;
        if(profile[s] != nullptr)
            delete[] profile[s];
        profile[s] = new float[profilecount];
    }

    for(i = 0; i < nodes_count; i++)
    {
        acconFloat x = sin((acconFloat)i*accon2pi/nodes_count);
        acconFloat y = cos((acconFloat)i*accon2pi/nodes_count);

        prx[i] = (x*params->transform[0]+y*params->transform[1]);
        pry[i] = (x*params->transform[3]+y*params->transform[4]);
        if(prx[i]*prx[i]+pry[i]*pry[i] < 2)
            return;

        prp[i] = sqrt(prx[i]*prx[i]+pry[i]*pry[i]);
        prx[i] /= rab;
        pry[i] /= rab;

    }
    acconFloat level, gradient;
    acconComputeRadialGradientProfile(image, prx, pry, prp, profilecount, profile, &level, &gradient); //null albo prr (params->ro < 0 ? prr : nullptr)

    for(i = 0; i < nodes_count; i++)
        prp[i] /= rab;

    if(threshold_setup)
        params->et = level;
    else
        params->et = 127.5;

    if(image_direction > 0)
        params->ex = fabs(params->ex);
    else if(image_direction < 0)
        params->ex = -fabs(params->ex);
    else
        params->ex = gradient > 0.0 ? -fabs(params->ex) : fabs(params->ex);

//    printf("LG %f %f\n", level, gradient);
//    fflush(stdout);

//    printf("acconComputeRadialGradientProfile\n");
//    fflush(stdout);

    unsigned int kernel = params->ti;
    if(kernel >= tensionKernelsCount)
        kernel = tensionKernelsCount-1;
    for(i = 0; i < params->i1; i++)
    {
        for(unsigned int s = 0; s < nodes_count; s++)
        {
            acconFloat r = pr1[s]*prp[s];
            acconFloat v = acconProfileLinearInterpolation(profile[s], profilecount, r);
            pr2[s] = pr1[s] + v/prp[s];
        }
        for(unsigned int s = 0; s < nodes_count; s++)
        {
            acconFloat v = pr2[s] * tensionKernels[kernel][0];
            for(unsigned int j = 1; j < tensionKernelSize; j++)
            {
                v += (pr2[(s+j)%nodes_count] + pr2[(nodes_count+s-j)%nodes_count]) * tensionKernels[kernel][j];
            }
            pr1[s] = v;
        }
    }
    for(i = 0; i < params->i2; i++)
    {
        acconFloat wa = (acconFloat)(i+1) / (acconFloat)(params->i2+1);
        acconFloat wb = 1.0 - wa;
        for(unsigned int s = 0; s < nodes_count; s++)
        {
            acconFloat r = pr1[s]*prp[s];
            acconFloat v = acconProfileLinearInterpolation(profile[s], profilecount, r);
            pr2[s] = pr1[s] + v/prp[s];
        }
        for(unsigned int s = 0; s < nodes_count; s++)
        {
            acconFloat v = pr2[s] * tensionKernels[kernel][0];
            for(unsigned int j = 1; j < tensionKernelSize; j++)
            {
                v += (pr2[(s+j)%nodes_count] + pr2[(nodes_count+s-j)%nodes_count]) * tensionKernels[kernel][j];
            }
            pr1[s] = wa*pr1[s] + wb*v;
        }
    }

    for(i = 0; i < nodes_count; i++)
    {
//        sina[i] *= pr1[i];
//        cosa[i] *= pr1[i];

//        acconFloat x = (xy[i]*params->transform[0]+xy[i+nodes_count]*params->transform[1])*pr1[i]*prp[i] +params->transform[2];
//        acconFloat y = (xy[i]*params->transform[3]+xy[i+nodes_count]*params->transform[4])*pr1[i]*prp[i] +params->transform[5];




//        acconFloat x = prx[i]*pr1[i]*prp[i] +params->transform[2];
//        acconFloat y = pry[i]*pr1[i]*prp[i] +params->transform[5];
        acconFloat x = prx[i]*pr1[i] + params->transform[2];
        acconFloat y = pry[i]*pr1[i] + params->transform[5];


        xy[i] = x;
        xy[i+nodes_count] = y;


    }
//    printf("End\n");
//    fflush(stdout);
}

AfterPaintAction PaintActiveContour::imageDataChanged(void)
{
    return DONOTHING;
}

void PaintActiveContour::drawContour(QRect* rectn, bool filled)
{
//    if(abs(radia) > 2048 || abs(radib) > 2048)
//        return;

    QPainter pixPaint(canvas);
    QPen pen(color);
    pen.setWidth(0);
    pen.setCapStyle(Qt::RoundCap);
    pen.setJoinStyle(Qt::RoundJoin);
    pixPaint.setPen(pen);

    if(filled)
    {
        QBrush brush(color);
        pixPaint.setBrush(brush);
    }
    pixPaint.drawImage(rectp.left(), rectp.top(), background, rectp.left(), rectp.top(), rectp.width(), rectp.height());
    //pixPaint.drawPolygon(pp, elipointsno);

    QTransform transform;
    transform.translate(point.x(), point.y());
    transform.rotate(angle*180.0/pi);
    transform.scale(radia, radib);
    //pixPaint.setTransform(transform);

    params.transform[0] = transform.m11();
    params.transform[1] = transform.m21();
    params.transform[2] = transform.m31();
    params.transform[3] = transform.m12();
    params.transform[4] = transform.m22();
    params.transform[5] = transform.m32();

    for(unsigned int s = 0; s < nodes_count; s++)
    {
        xy[s] = sin((acconFloat)s*accon2pi/nodes_count);
        xy[s+nodes_count] = cos((acconFloat)s*accon2pi/nodes_count);
    }

    AcconImage image;
    unsigned int size[2];
    gray->GetSize(size);
    image.data = gray->GetDataPointer();
    image.width = size[0];
    image.height = size[1];
    image.stride = size[0];
    acconFindRadialContour(&image, &params);

    //pixPaint.drawEllipse(QPointF(0.0, 0.0), radia, radib);
    //pixPaint.drawEllipse(QPointF(0.0, 0.0), 1, 1);
    //QRect rectn(QPoint(transform.m31(), transform.m32()), QPoint(transform.m31(), transform.m32()));
    QPolygonF poly(nodes_count);

    if(rectn == nullptr)
    {
        for(unsigned int s = 0; s < nodes_count; s++)
        {
            poly[s] = QPointF(xy[s], xy[s+nodes_count]);
        }
    }
    else
    {
        for(unsigned int s = 0; s < nodes_count; s++)
        {
            if(rectn->left()>xy[s]-0.5)
                rectn->setLeft(xy[s]-0.5);
            if(rectn->right()<xy[s]+0.5)
                rectn->setRight(xy[s]+0.5);
            if(rectn->bottom()<xy[s+nodes_count]+0.5)
                rectn->setBottom(xy[s+nodes_count]+0.5);
            if(rectn->top()>xy[s+nodes_count]-0.5)
                rectn->setTop(xy[s+nodes_count]-0.5);

            poly[s] = QPointF(xy[s], xy[s+nodes_count]);
        }
//        printf("Recn %i %i %i %i\n", rectn->left(), rectn->top(), rectn->right(), rectn->bottom());
//        fflush(stdout);
    }
/*
    *rect = rectn.united(rectp);
    rectp = rectn;
*/

    pixPaint.drawPolygon(poly);

    pixPaint.setTransform(transform);
    if(!filled)
    {
        //pixPaint.drawEllipse(QPointF(0.0, 0.0), 1, 1);
        pixPaint.drawLine(-1, 0, 1, 0);
        pixPaint.drawLine(0, -1, 0, 1);
    }


//    printf("%f %f   %f %f\n", (float)radia, (float)radib,
//           sqrt(params.transform[0]*params.transform[0] + params.transform[3]*params.transform[3]),
//           sqrt(params.transform[1]*params.transform[1] + params.transform[4]*params.transform[4]));
//    fflush(stdout);

}

AfterPaintAction PaintActiveContour::mouseMoveEvent(QMouseEvent *ev)
{
    if(ev->buttons() & Qt::LeftButton)
    {
        ev->accept();

        Qt::KeyboardModifiers modif = QApplication::keyboardModifiers();
        if((modif & Qt::ControlModifier) && (modif & Qt::ShiftModifier))
        {
            endpoint.setX(_ev_position_x / zoom);
            endpoint.setY(_ev_position_y / zoom);
            double x = endpoint.x()-point.x();
            double y = endpoint.y()-point.y();
            radia = radib = sqrt(x*x + y*y);
        }
        else if(modif & Qt::ShiftModifier)
        {
            QPoint newpoint;
            newpoint.setX(_ev_position_x / zoom);
            newpoint.setY(_ev_position_y / zoom);
            double x = endpoint.x()-newpoint.x();
            double y = endpoint.y()-newpoint.y();
            point.setX(point.x() - x);
            point.setY(point.y() - y);
            endpoint = newpoint;
            x = endpoint.x()-point.x();
            y = endpoint.y()-point.y();
            double a = atan2(y, x)-angle;
            double dd = sqrt(x*x + y*y);
            radia = cos(a)*dd;
            a += pi2;
            radib = cos(a)*dd;
        }
        else if(modif & Qt::ControlModifier)
        {
            QPoint newpoint;
            newpoint.setX(_ev_position_x / zoom);
            newpoint.setY(_ev_position_y / zoom);
            double x = endpoint.x()-point.x();
            double y = endpoint.y()-point.y();
            double xn = newpoint.x()-point.x();
            double yn = newpoint.y()-point.y();
            if((xn != 0 || yn != 0) && (x != 0 || y != 0))
            {
                angle += (atan2(yn, xn) - atan2(y, x));
                if(angle > pi) angle -= twopi;
                if(angle < -pi) angle += twopi;
            }
            endpoint = newpoint;
            x = endpoint.x()-point.x();
            y = endpoint.y()-point.y();
            double a = atan2(y, x)-angle;
            double dd = sqrt(x*x + y*y);
            radia = cos(a)*dd;
            a += pi2;
            radib = cos(a)*dd;
        }
        else
        {
            endpoint.setX(_ev_position_x / zoom);
            endpoint.setY(_ev_position_y / zoom);
            double x = endpoint.x()-point.x();
            double y = endpoint.y()-point.y();
            double a = atan2(y, x)-angle;
            double dd = sqrt(x*x + y*y);
            radia = cos(a)*dd;
            a += pi2;
            radib = cos(a)*dd;
        }

        QRect rectn;
        double cosangle = cos(angle);
        double sinangle = sin(angle);
        double m = fabs(cosangle*radia)+fabs(sinangle*radib);
        rectn.setLeft(point.x()-m+0.5);
        rectn.setRight(point.x()+m+0.5);
        m = fabs(cosangle*radib)+fabs(sinangle*radia);
        rectn.setTop(point.y()-m+0.5);
        rectn.setBottom(point.y()+m+0.5);

        if(abs(radia) > 2048 || abs(radib) > 2048)
            return DONOTHING;


        drawContour(&rectn, false);

//        printf("Rect %i %i %i %i\n", rectn.left(), rectn.top(), rectn.right(), rectn.bottom());
//        fflush(stdout);


        *rect = rectn.united(rectp);
        rectp = rectn;

        info = QString("%1,%2 R[%3,%4]").arg(point.x()).arg(point.y()).arg(radia).arg(radib);//.arg(int(180*angle/3.1415927));
        return REDRAW;
    }
    return DONOTHING;
}


AfterPaintAction PaintActiveContour::mousePressEvent(QMouseEvent *ev)
{
    if(ev->buttons() & Qt::LeftButton)
    {
        ev->accept();
        point.setX(_ev_position_x / zoom);
        point.setY(_ev_position_y / zoom);
        endpoint = point;
        rectp.setBottomRight(point);
        rectp.setTopLeft(point);

        QPainter pixPaint(canvas);
        QPen pen(color);
        //pen.setWidth(thick);
        //pen.setCapStyle(Qt::RoundCap);
        pixPaint.setPen(pen);
        pixPaint.drawPoint(point);
        *rect = rectp;
        //rect->adjust(-thick/2, -thick/2, thick/2, thick/2);
        return REDRAW;
    }
    return DONOTHING;
}

AfterPaintAction PaintActiveContour::mouseReleaseEvent(QMouseEvent *ev)
{
//    if(ev->buttons() & Qt::LeftButton)
    {
        ev->accept();
        drawContour(nullptr, true);
        return FINALIZE;
    }
    return DONOTHING;
}

AfterPaintAction PaintActiveContour::mouseDoubleClickEvent(QMouseEvent *ev){return DONOTHING;}
AfterPaintAction PaintActiveContour::wheelEvent(QWheelEvent *ev){return DONOTHING;}

PaintActiveContour::PaintActiveContour(QColor icolor, int pars[4], double izoom, QImage *icanvas, MzImage2D8 *gray, QRect *irect)
{
    nodes_count = (pars[2] < 0 ? 0 : (pars[2] > 31 ? 31 : pars[2]));
    nodes_count = 17 + nodes_count*nodes_count;
    threshold_setup = pars[3];
    zoom = izoom;
    color = icolor;
    canvas = icanvas;
    rect = irect;
    background = icanvas->copy();
    this->gray = gray;
    angle = 0.0;
    //xy.reserve(nodes_count*2);
    xy = new acconFloat[nodes_count*2];
    for(unsigned int s = 0; s < nodes_count; s++)
    {
        xy[s] = sin((acconFloat)s*accon2pi/nodes_count);
        xy[s+nodes_count] = cos((acconFloat)s*accon2pi/nodes_count);
    }
    if((unsigned int)pars[0] < tensionKernelsCount)
        params.ti = pars[0];

    image_direction = pars[1];
    pr1 = new acconFloat[nodes_count];
    pr2 = new acconFloat[nodes_count];
    prx = new acconFloat[nodes_count];
    pry = new acconFloat[nodes_count];
    prp = new acconFloat[nodes_count];

    profile = new acconFloat*[nodes_count];
    for(unsigned int s = 0; s < nodes_count; s++)
        profile[s] = nullptr;
}

PaintActiveContour::~PaintActiveContour()
{
    for(unsigned int s = 0; s < nodes_count; s++)
        if(profile[s] != nullptr) delete[] profile[s];
    delete[] profile;
    delete[] prp;
    delete[] pry;
    delete[] prx;
    delete[] pr2;
    delete[] pr1;
    delete[] xy;
}
