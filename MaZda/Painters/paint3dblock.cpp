/*
 * QMAZDA project: Image Analysis and Pattern Recognition
 *
 * Copyright 2013-2017 Piotr M. Szczypiński <piotr.szczypinski@p.lodz.pl>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "paint3dblock.h"


Paint3dBlock::Paint3dBlock(RenderingContainer3D *icontainer, Painter3D *ipainter, unsigned int list, GLdouble* modelMat, GLdouble *volumeMat, bool *ierase)
{
    initial = true;
    container = icontainer;
    painter = ipainter;
    erase = ierase;
    unsigned int color = container->getActiveRoi()->GetColor();
    colorpixel.channel[2] = color&0xff; color >>= 8;
    colorpixel.channel[1] = color&0xff; color >>= 8;
    colorpixel.channel[0] = color&0xff;
    this->modelMat = modelMat;
    this->volumeMat = volumeMat;
    this->list = list;
    middlepressed = false;

    image = container->getRGBCanvas();
    painter->getSize(size);
    painter->getSpacing(voxelsize);
}

AfterPaintAction Paint3dBlock::imageDataChanged(void)
{
    return DONOTHING;
}

void Paint3dBlock::updateShape()
{
    glNewList(list, GL_COMPILE);
    glColor4f(((float)colorpixel.channel[0])/255.0f, ((float)colorpixel.channel[1])/255.0f, ((float)colorpixel.channel[2])/255.0f, 1.0f);
    createBlock();
    glEndList();
    painter->showModel(true);
}

MzBounds Paint3dBlock::getModifiedRegion()
{
    return cube;
}

AfterPaintAction Paint3dBlock::keyPressEvent(QKeyEvent *ev)
{
    switch(ev->key())
    {
    case Qt::Key_Return:
    case Qt::Key_Enter:
    case Qt::Key_Insert:
        ev->accept();
        glNewList(list, GL_COMPILE);
        glEndList();
        painter->showModel(false);
        rasterizeBlock();
//        printf("keyPressEvent %i %i %i %i %i %i\n", cube.min[0], cube.min[1], cube.min[2], cube.max[0], cube.max[1], cube.max[2]);
//        fflush(stdout);
        painter->MzImage2MzRoi(cube.min, cube.max, colorpixel, *erase);
        return FINALIZE;
    case Qt::Key_Escape:
    case Qt::Key_Delete:
    case Qt::Key_Backspace:
        ev->accept();
        return CANCEL;
    case Qt::Key_Left:
        MatrixUtils3D<GLdouble>::rotatemodel(-0.01, 0, 2, modelMat, volumeMat);
        ev->accept();
        return REDRAW;
    case Qt::Key_Right:
        MatrixUtils3D<GLdouble>::rotatemodel(0.01, 0, 2, modelMat, volumeMat);
        ev->accept();
        return REDRAW;
    case Qt::Key_Up:
        MatrixUtils3D<GLdouble>::rotatemodel(0.01, 1, 2, modelMat, volumeMat);
        ev->accept();
        return REDRAW;
    case Qt::Key_Down:
        MatrixUtils3D<GLdouble>::rotatemodel(-0.01, 1, 2, modelMat, volumeMat);
        ev->accept();
        return REDRAW;
    }
    return DONOTHING;
}
AfterPaintAction Paint3dBlock::keyReleaseEvent(QKeyEvent * /*ev*/)
{
    return DONOTHING;
}


AfterPaintAction Paint3dBlock::mouseMoveEvent(QMouseEvent *ev)
{
    if(ev->buttons() & Qt::LeftButton)
    {
        if(QApplication::keyboardModifiers() & Qt::ControlModifier)
        {
            mousex = _ev_position_x;
            mousey = _ev_position_y;
            return DONOTHING;
        }
        ev->accept();
        if(QApplication::keyboardModifiers() & Qt::AltModifier)
        {
            int x = _ev_position_x;
            int y = _ev_position_y;
            GLdouble mat_1[16];
            GLdouble matt[16];
            GLdouble majj[16];
            GLdouble makk[16];
            MatrixUtils3D<GLdouble>::invert3x3(mat_1, volumeMat);
            matt[1] = 0; matt[2] = 0; matt[3] = 0;
            matt[4] = 0; matt[6] = 0; matt[7] = 0;
            matt[8] = 0; matt[9] = 0; matt[10] = 1; matt[11] = 0;
            matt[12] = 0; matt[13] = 0; matt[14] = 0; matt[15] = 1;
            matt[0] = pow(1.002, (double)(x-mousex));
            matt[5] = pow(1.002, (double)(mousey-y));
            MatrixUtils3D<GLdouble>::multiply3x3(majj, modelMat, volumeMat);
            MatrixUtils3D<GLdouble>::multiply3x3(makk, majj, matt);
            MatrixUtils3D<GLdouble>::multiply3x3(modelMat, makk, mat_1);
            mousex = x;
            mousey = y;
            return REDRAW;
        }
        else if(QApplication::keyboardModifiers() & Qt::ShiftModifier)
        {
            int input[2];
            input[0] = mousex;
            input[1] = mousey;
            GLdouble outputemp[3];
            GLdouble outputr[3];
            GLdouble output[3];
            painter->Coordinates2Dto3D(input, &(modelMat[12]), outputr, outputemp);
            input[0] = mousex = _ev_position_x;
            input[1] = mousey = _ev_position_y;
            painter->Coordinates2Dto3D(input, &(modelMat[12]), output, outputemp);

            modelMat[12] += (output[0] - outputr[0]);
            modelMat[13] += (output[1] - outputr[1]);
            modelMat[14] += (output[2] - outputr[2]);
            return REDRAW;
        }
        else
        {
            int x = _ev_position_x;
            int y = _ev_position_y;
            MatrixUtils3D<GLdouble>::rotatemodel((double)(x-mousex)/100.0, 0, 2, modelMat, volumeMat);
            MatrixUtils3D<GLdouble>::rotatemodel((double)(mousey-y)/100.0, 1, 2, modelMat, volumeMat);
            mousex = x;
            mousey = y;
            return REDRAW;
        }
        return DONOTHING;
    }
    mousex = _ev_position_x;
    mousey = _ev_position_y;
    return DONOTHING;
}

AfterPaintAction Paint3dBlock::mousePressEvent(QMouseEvent *ev)
{
    if(ev->button() == Qt::RightButton)
    {
        ev->accept();
        return CANCEL;
    }
    if(ev->button() == Qt::LeftButton)
    {
        mousex = _ev_position_x;
        mousey = _ev_position_y;
        if(QApplication::keyboardModifiers() & Qt::ControlModifier)
        {
            return DONOTHING;
        }
        ev->accept();
        if(initial)
        {
            updateShape();
            initial = false;
            return REDRAW;
        }
    }
    return DONOTHING;
}
AfterPaintAction Paint3dBlock::mouseReleaseEvent(QMouseEvent * /*ev*/)
{
    return DONOTHING;
}

AfterPaintAction Paint3dBlock::mouseDoubleClickEvent(QMouseEvent *ev)
{
    if(QApplication::keyboardModifiers() & Qt::ControlModifier) return DONOTHING;
    if(ev->buttons() != Qt::LeftButton) return DONOTHING;
    ev->accept();
    glNewList(list, GL_COMPILE);
    glEndList();
    painter->showModel(false);
    rasterizeBlock();
    painter->MzImage2MzRoi(cube.min, cube.max, colorpixel, *erase);
    return FINALIZE;
}

AfterPaintAction Paint3dBlock::wheelEvent(QWheelEvent *ev)
{
    if(QApplication::keyboardModifiers() & Qt::ControlModifier) return DONOTHING;

    if(QApplication::keyboardModifiers() & Qt::AltModifier)
    {
        ev->accept();
        return changeShape(ev->angleDelta().y(), ev->buttons() == Qt::MiddleButton);
    }
    else if(QApplication::keyboardModifiers() & Qt::ShiftModifier)
    {
        double qz;
        if(ev->buttons() == Qt::MiddleButton) qz = -(double) ev->angleDelta().y()/2.0;
        else qz = -(double) ev->angleDelta().y()/20.0;
        GLdouble mat_1[16];

        MatrixUtils3D<GLdouble>::invert3x3(mat_1, volumeMat);
        modelMat[12] += (mat_1[8]*qz);
        modelMat[13] += (mat_1[9]*qz);
        modelMat[14] += (mat_1[10]*qz);
        ev->accept();
        return REDRAW;
    }
    else
    {
        int k;
        double mnoznik;
        if(ev->buttons() == Qt::MiddleButton) mnoznik = pow(1.001, -ev->angleDelta().y());
        else mnoznik = pow(1.0001, -ev->angleDelta().y());
        for(k = 0; k < 3; k++) modelMat[k] *= mnoznik;
        for(k = 4; k < 7; k++) modelMat[k] *= mnoznik;
        for(k = 8; k < 11; k++) modelMat[k] *= mnoznik;
        ev->accept();
        return REDRAW;
    }
    return DONOTHING;
}

//======================================================================================
//======================================================================================
//======================================================================================
/**
 * @brief Paint3dBlock::FillSliceTriangle
 * @param z
 * @param punkty
 */
void Paint3dBlock::FillSliceTriangle(unsigned int z, double punkty[3][2])
{
    RenderingContainer3D::MazdaRGBPixelType8* vox;
    if(image == NULL) return;
    if(z >= size[2]) return;
    int m[3];
    m[0] =  0;
    if(punkty[0][1]>punkty[1][1]) m[0] = 1;
    if(punkty[m[0]][1]>punkty[2][1]) m[0] = 2;
    m[2] =  2;
    if(punkty[1][1]>punkty[2][1]) m[2] = 1;
    if(punkty[0][1]>punkty[m[2]][1]) m[2] = 0;
    if(m[0]!=0 && m[2]!=0) m[1] = 0;
    else if(m[0]!=2 && m[2]!=2) m[1] = 2;
    else m[1] = 1;

    double nn, nd, ng;
    nn = punkty[m[2]][1]-punkty[m[0]][1];
    if(nn!=0)
    {
        unsigned int veci[RenderingContainer3D::Dimensions];
        for(unsigned int d = 0; d < RenderingContainer3D::Dimensions; d++) veci[d] = 0;
        veci[2] = z;
        nn = (punkty[m[2]][0]-punkty[m[0]][0])/nn;
        nd = punkty[m[1]][1]-punkty[m[0]][1];
        ng = punkty[m[2]][1]-punkty[m[1]][1];
        if(nd != 0)
        {
            nd = (punkty[m[1]][0]-punkty[m[0]][0])/nd;
            if(nd>nn)
            {
                int ystart = (int)ceil(punkty[m[0]][1]);
                int ystop = (int)floor(punkty[m[1]][1]);
                if(ystart < 0) ystart=0;
                if(ystop >= (int)size[1]) ystop=size[1]-1;
                for(veci[1] = ystart; (int)veci[1] <= ystop; veci[1]++)
                {
                    int start = (int)ceil(punkty[m[0]][0]+((double)veci[1]-punkty[m[0]][1])*nn);
                    int stop = (int)floor(punkty[m[0]][0]+((double)veci[1]-punkty[m[0]][1])*nd);
                    if(start < 0) start = 0;
                    if(stop >= (int)size[0]) stop = size[0]-1;
                    veci[0] = start;
                    vox = image->GetPixelPointer(veci);
                    for(int x = start; x <= stop; x++)
                    {
                        *vox = colorpixel;
                        vox++;
                    }
                }
            }
            else if(nd<nn)
            {
                int ystart = (int)ceil(punkty[m[0]][1]);
                int ystop = (int)floor(punkty[m[1]][1]);
                if(ystart<0) ystart=0;
                if(ystop >= (int)size[1]) ystop=size[1]-1;
                for(veci[1] = ystart; (int)veci[1] <= ystop; veci[1]++)
                {
                    int start = (int)ceil(punkty[m[0]][0]+((double)veci[1]-punkty[m[0]][1])*nd);
                    int stop = (int)floor(punkty[m[0]][0]+((double)veci[1]-punkty[m[0]][1])*nn);
                    if(start<0) start=0;
                    if(stop >= (int)size[0]) stop=size[0]-1;
                    veci[0] = start;
                    vox = image->GetPixelPointer(veci);
                    for(int x = start; x <= stop; x++)
                    {
                        *vox = colorpixel;
                        vox++;
                    }
                }
            }
        }
        if(ng != 0)
        {
            ng = (punkty[m[2]][0]-punkty[m[1]][0])/ng;
            if(ng>nn)
            {
                int ystart = (int)ceil(punkty[m[1]][1]);
                int ystop = (int)floor(punkty[m[2]][1]);
                if(ystart<0) ystart=0;
                if(ystop >= (int)size[1]) ystop=size[1]-1;
                for(veci[1] = ystart; (int)veci[1] <= ystop; veci[1]++)
                {
                    int start = (int)ceil(punkty[m[1]][0]+((double)veci[1]-punkty[m[1]][1])*ng);
                    int stop = (int)floor(punkty[m[0]][0]+((double)veci[1]-punkty[m[0]][1])*nn);
                    if(start<0) start=0;
                    if(stop >= (int)size[0]) stop=size[0]-1;
                    veci[0] = start;
                    vox = image->GetPixelPointer(veci);
                    for(int x = start; x <= stop; x++)
                    {
                        *vox = colorpixel;
                        vox++;
                    }
                }
            }
            else if(ng<nn)
            {
                int ystart = (int)ceil(punkty[m[1]][1]);
                int ystop = (int)floor(punkty[m[2]][1]);
                if(ystart<0) ystart=0;
                if(ystop >= (int)size[1]) ystop=size[1]-1;
                for(veci[1] = ystart; (int)veci[1] <= ystop; veci[1]++)
                {
                    int start = (int)ceil(punkty[m[0]][0]+((double)veci[1]-punkty[m[0]][1])*nn);
                    int stop = (int)floor(punkty[m[1]][0]+((double)veci[1]-punkty[m[1]][1])*ng);
                    if(start<0) start=0;
                    if(stop >= (int)size[0]) stop=size[0]-1;
                    veci[0] = start;
                    vox = image->GetPixelPointer(veci);
                    for(int x = start; x <= stop; x++)
                    {
                        *vox = colorpixel;
                        vox++;
                    }
                }
            }
        }
    }
}
/**
 * @brief Paint3dBlock::FillTetrahedron
 * @param punkty
 */
void Paint3dBlock::FillTetrahedron(double punkty[4][3])
{
    int z;
    int m1, m2, m3, m4;
    double n[4][2];
    double t;
    double p[3][2];

    m1 = (punkty[0][2]>punkty[1][2] ?1 :0);
    m1 = (punkty[m1][2]>punkty[2][2] ?2 :m1);
    m1 = (punkty[m1][2]>punkty[3][2] ?3 :m1);
    m4 = (punkty[0][2]>punkty[1][2] ?0 :1);
    m4 = (punkty[m4][2]>punkty[2][2] ?m4 :2);
    m4 = (punkty[m4][2]>punkty[3][2] ?m4 :3);
    if(m1!=0 && m4!=0) m2 = 0;
    else if(m1!=1 && m4!=1) m2 = 1;
    else if(m1!=2 && m4!=2) m2 = 2;
    if(m1!=3 && m2!=3 && m4!=3) m3 = 3;
    else if(m1!=2 && m2!=2 && m4!=2) m3 = 2;
    else if(m1!=1 && m2!=1 && m4!=1) m3 = 1;
    if(punkty[m2][2]>punkty[m3][2]) {z=m3;m3=m2;m2=z;}

    t = punkty[m4][2] - punkty[m1][2];
    if(t == 0) goto skok3;
    n[2][0] = (punkty[m4][0] - punkty[m1][0])/t;
    n[2][1] = (punkty[m4][1] - punkty[m1][1])/t;
    t = punkty[m3][2] - punkty[m1][2];
    if(t == 0) goto skok2;
    n[1][0] = (punkty[m3][0] - punkty[m1][0])/t;
    n[1][1] = (punkty[m3][1] - punkty[m1][1])/t;
    t = punkty[m2][2] - punkty[m1][2];
    if(t == 0) goto skok1;
    n[0][0] = (punkty[m2][0] - punkty[m1][0])/t;
    n[0][1] = (punkty[m2][1] - punkty[m1][1])/t;
    for(z = (int)ceil(punkty[m1][2]); z <= (int)floor(punkty[m2][2]); z++)
    {
        p[0][0] = punkty[m1][0]+ ((double)z-punkty[m1][2])*n[0][0];
        p[0][1] = punkty[m1][1]+ ((double)z-punkty[m1][2])*n[0][1];
        p[1][0] = punkty[m1][0]+ ((double)z-punkty[m1][2])*n[1][0];
        p[1][1] = punkty[m1][1]+ ((double)z-punkty[m1][2])*n[1][1];
        p[2][0] = punkty[m1][0]+ ((double)z-punkty[m1][2])*n[2][0];
        p[2][1] = punkty[m1][1]+ ((double)z-punkty[m1][2])*n[2][1];
        FillSliceTriangle(z, p);
    }
skok1:

    t = punkty[m4][2] - punkty[m2][2];
    if(t == 0) goto skok3;
    n[3][0] = (punkty[m4][0] - punkty[m2][0])/t;
    n[3][1] = (punkty[m4][1] - punkty[m2][1])/t;
    t = punkty[m3][2] - punkty[m2][2];
    if(t == 0) goto skok2b;
    n[0][0] = (punkty[m3][0] - punkty[m2][0])/t;
    n[0][1] = (punkty[m3][1] - punkty[m2][1])/t;
    for(z = (int)ceil(punkty[m2][2]); z <= (int)floor(punkty[m3][2]); z++)
    {
        p[0][0] = punkty[m2][0]+ ((double)z-punkty[m2][2])*n[0][0];
        p[0][1] = punkty[m2][1]+ ((double)z-punkty[m2][2])*n[0][1];
        p[1][0] = punkty[m1][0]+ ((double)z-punkty[m1][2])*n[1][0];
        p[1][1] = punkty[m1][1]+ ((double)z-punkty[m1][2])*n[1][1];
        p[2][0] = punkty[m1][0]+ ((double)z-punkty[m1][2])*n[2][0];
        p[2][1] = punkty[m1][1]+ ((double)z-punkty[m1][2])*n[2][1];
        FillSliceTriangle(z, p);
        p[1][0] = punkty[m2][0]+ ((double)z-punkty[m2][2])*n[3][0];
        p[1][1] = punkty[m2][1]+ ((double)z-punkty[m2][2])*n[3][1];
        FillSliceTriangle(z, p);
    }
skok2:
    t = punkty[m4][2] - punkty[m2][2];
    if(t == 0) goto skok3;
    n[3][0] = (punkty[m4][0] - punkty[m2][0])/t;
    n[3][1] = (punkty[m4][1] - punkty[m2][1])/t;
skok2b:
    t = punkty[m4][2] - punkty[m3][2];
    if(t == 0) goto skok3;
    n[1][0] = (punkty[m4][0] - punkty[m3][0])/t;
    n[1][1] = (punkty[m4][1] - punkty[m3][1])/t;
    for(z = (int)ceil(punkty[m3][2]); z <= (int)floor(punkty[m4][2]); z++)
    {
        p[0][0] = punkty[m3][0]+ ((double)z-punkty[m3][2])*n[1][0];
        p[0][1] = punkty[m3][1]+ ((double)z-punkty[m3][2])*n[1][1];
        p[1][0] = punkty[m1][0]+ ((double)z-punkty[m1][2])*n[2][0];
        p[1][1] = punkty[m1][1]+ ((double)z-punkty[m1][2])*n[2][1];
        p[2][0] = punkty[m2][0]+ ((double)z-punkty[m2][2])*n[3][0];
        p[2][1] = punkty[m2][1]+ ((double)z-punkty[m2][2])*n[3][1];
        FillSliceTriangle(z, p);
    }
skok3:
    return;
}

/**
 * @brief Paint3dBlock::TransformTetrahedronAndFill used for block rasterization, the block is divided into tetrahedrons
 * @param ip - tetrahedron vertices coordinates in visualized model space
 * @param op - tetrahedron vertices coordinates in voxel space of raster image
 * @param mat - transformation matrix, the same used in OpenGL rendering for model-to-world space transformation
 */
void Paint3dBlock::TransformTetrahedronAndFill(double ip[4][3], double op[4][3], double mat[16])
{
    for(int p = 0; p < 4; p++)
    {
        Transform(ip[p], op[p], mat);
        ImageToVoxelSpace(op[p], op[p]);
        op[p][0] -= 0.5;
        op[p][1] -= 0.5;
        op[p][2] -= 0.5;
    }
    FillTetrahedron(op);
}

void Paint3dBlock::DeTransform(const double ip[3], double op[3], const double mato[16], const double mat[16])
{
    double ipp[3];
    ipp[0] = ip[0] - mat[12];
    ipp[1] = ip[1] - mat[13];
    ipp[2] = ip[2] - mat[14];
    op[0] = ipp[0]*mato[0]+ipp[1]*mato[4]+ipp[2]*mato[8];
    op[1] = ipp[0]*mato[1]+ipp[1]*mato[5]+ipp[2]*mato[9];
    op[2] = ipp[0]*mato[2]+ipp[1]*mato[6]+ipp[2]*mato[10];
}

void Paint3dBlock::Transform(double ip[3], double op[3], double mat[16])
{
   op[0] = (ip[0]*mat[0]+ip[1]*mat[4]+ip[2]*mat[8]+mat[12]);
   op[1] = (ip[0]*mat[1]+ip[1]*mat[5]+ip[2]*mat[9]+mat[13]);
   op[2] = (ip[0]*mat[2]+ip[1]*mat[6]+ip[2]*mat[10]+mat[14]);
}
void Paint3dBlock::ImageToVoxelSpace(const double ip[3], int op[3])
{
    op[0] = round(ip[0]/voxelsize[0] + (double)size[0]/2.0);
    op[1] = round(ip[1]/voxelsize[1] + (double)size[1]/2.0);
    op[2] = round(ip[2]/voxelsize[2] + (double)size[2]/2.0);
}
void Paint3dBlock::ImageToVoxelSpace(const double ip[3], double op[3])
{
    op[0] = (ip[0]/voxelsize[0] + (double)size[0]/2.0);
    op[1] = (ip[1]/voxelsize[1] + (double)size[1]/2.0);
    op[2] = (ip[2]/voxelsize[2] + (double)size[2]/2.0);
}
void Paint3dBlock::VoxelToImageSpace(const int ip[3], double op[3])
{
    op[0] = (ip[0]-(double)size[0]/2.0)*voxelsize[0];
    op[1] = (ip[1]-(double)size[1]/2.0)*voxelsize[1];
    op[2] = (ip[2]-(double)size[2]/2.0)*voxelsize[2];
}

void Paint3dBlock::VoxelToImageSpace(const double ip[3], double op[3])
{
    op[0] = (ip[0]-(double)size[0]/2.0)*voxelsize[0];
    op[1] = (ip[1]-(double)size[1]/2.0)*voxelsize[1];
    op[2] = (ip[2]-(double)size[2]/2.0)*voxelsize[2];
}


