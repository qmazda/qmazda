/*
 * QMAZDA project: Image Analysis and Pattern Recognition
 *
 * Copyright 2013-2017 Piotr M. Szczypiński <piotr.szczypinski@p.lodz.pl>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "paintpoly.h"

#include "paintfloodfill.h"
#include <QApplication>
#include <QPainter>
#include <opencv2/imgproc/imgproc_c.h>

AfterPaintAction PaintPoly::keyPressEvent(QKeyEvent *ev)
{
    switch(ev->key())
    {
    case Qt::Key_Return:
    case Qt::Key_Enter:
    case Qt::Key_Insert:
        ev->accept();
        return mouseDoubleClickEvent(NULL);
    case Qt::Key_Escape:
    case Qt::Key_Delete:
    case Qt::Key_Backspace:
        ev->accept();
        return CANCEL;
    }
    return DONOTHING;
}
AfterPaintAction PaintPoly::keyReleaseEvent(QKeyEvent *ev)
{
    return DONOTHING;
}

AfterPaintAction PaintPoly::imageDataChanged(void)
{
    return DONOTHING;
}

AfterPaintAction PaintPoly::mouseMoveEvent(QMouseEvent *ev)
{
//    info = QString("%1,%2").arg(_ev_position_x / zoom).arg(_ev_position_y / zoom);
    return DONOTHING;
}

AfterPaintAction PaintPoly::mousePressEvent(QMouseEvent *ev)
{
    if(ev->buttons() & Qt::LeftButton)
    {
        ev->accept();
        info = QString("%1,%2").arg(_ev_position_x / zoom).arg(_ev_position_y / zoom);
        QPoint point;
        point.setX(_ev_position_x / zoom);
        point.setY(_ev_position_y / zoom);
        int n = pointlist.size();

        if(n <= 0)
        {
            rect->setBottomRight(point);
            rect->setTopLeft(point);
            QPainter pixPaint(canvas);
            QPen pen(color);
            pixPaint.setPen(pen);
            pixPaint.drawPoint(point);
            pointlist.append(point);

            //n = pointlist.size();
        }
        else
        {
            if(point.x() < rect->left()) rect->setLeft(point.x());
            if(point.x() > rect->right()) rect->setRight(point.x());
            if(point.y() < rect->top()) rect->setTop(point.y());
            if(point.y() > rect->bottom()) rect->setBottom(point.y());
            QPainter pixPaint(canvas);
            QPen pen(color);
            pixPaint.setPen(pen);
            pixPaint.drawLine(point, pointlist.last());
            pointlist.append(point);
        }
        return REDRAW;
    }
    return DONOTHING;
}

AfterPaintAction PaintPoly::mouseReleaseEvent(QMouseEvent *ev)
{
    return DONOTHING;
}
AfterPaintAction PaintPoly::mouseDoubleClickEvent(QMouseEvent *ev)
{
    QPainter pixPaint(canvas);
    QPen pen(color);
    pixPaint.setPen(pen);
    QBrush brush(color);
    pixPaint.setBrush(brush);
    pixPaint.drawPolygon(pointlist);
    //pixPaint.drawCubicBezier(pointlist);
    return FINALIZE;
}
AfterPaintAction PaintPoly::wheelEvent(QWheelEvent *ev){return DONOTHING;}

PaintPoly::PaintPoly(QColor icolor, double izoom, QImage *icanvas, QRect *irect)
{
    zoom = izoom;
    color = icolor;
    canvas = icanvas;
    rect = irect;
}
