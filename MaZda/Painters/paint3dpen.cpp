/*
 * QMAZDA project: Image Analysis and Pattern Recognition
 *
 * Copyright 2013-2017 Piotr M. Szczypiński <piotr.szczypinski@p.lodz.pl>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "paint3dpen.h"
#include <QWindow>
#include <QScreen>

Paint3DPen::Paint3DPen(RenderingContainer3D* icontainer, Painter3D* ipainter, bool *ierase)
{
    container = icontainer;
    painter = ipainter;
    erase = ierase;
    unsigned int color = container->getActiveRoi()->GetColor();

    colorpixel.channel[2] = color&0xff; color >>= 8;
    colorpixel.channel[1] = color&0xff; color >>= 8;
    colorpixel.channel[0] = color&0xff;
}

AfterPaintAction Paint3DPen::imageDataChanged(void)
{
    return DONOTHING;
}

MzBounds Paint3DPen::getModifiedRegion()
{
    return cube;
}

AfterPaintAction Paint3DPen::drawLine(int p[3], int k[3])
{

    if(!(p[0] == k[0] || p[1] == k[1] || p[2] == k[2]) || p[0] < 0)
    {
        container->getRGBCanvas()->SetPixel((unsigned int*)k, colorpixel);
    }
    else
    {
        int maxsteps;
        double shift[RenderingContainer3D::Dimensions];
        if(abs(p[0] - k[0])  > abs(p[1] - k[1]) && abs(p[0] - k[0])  > abs(p[2] - k[2]))
        {
            maxsteps = abs(p[0] - k[0]);
        }
        else if(abs(p[1] - k[1])  > abs(p[2] - k[2]))
        {
            maxsteps = abs(p[1] - k[1]);
        }
        else
        {
            maxsteps = abs(p[2] - k[2]);
        }
        shift[0] = double(k[0] - p[0]) / maxsteps;
        shift[1] = double(k[1] - p[1]) / maxsteps;
        shift[2] = double(k[2] - p[2]) / maxsteps;

        RenderingContainer3D::MzRGBImage8* canvas = container->getRGBCanvas();

        for(int m = 1; m <= maxsteps; m++)
        {
           unsigned int output[RenderingContainer3D::Dimensions];
           output[0] = p[0] + m * shift[0] + 0.5;
           output[1] = p[1] + m * shift[1] + 0.5;
           output[2] = p[2] + m * shift[2] + 0.5;
           canvas->SetPixel(output, colorpixel);
        }
    }
    return DONOTHING;
}

AfterPaintAction Paint3DPen::mouseMoveEvent(QMouseEvent *ev)
{
    if((ev->buttons() & Qt::LeftButton) && (!QApplication::keyboardModifiers()))
    {
        ev->accept();

#if (QT_VERSION >= QT_VERSION_CHECK(5, 0, 0))
        double ratio = 1.0;
        QWindow* wh = painter->windowHandle();
        if(wh != nullptr)
            ratio = wh->devicePixelRatio();
#ifdef _WIN32
#else
        else
        {
            QScreen* sc = painter->screen();
            if(sc != nullptr)
                ratio = sc->devicePixelRatio();
        }
#endif
#else
        double ratio = 1.0;
#endif

        int input[2];
        int output[RenderingContainer3D::Dimensions];
        input[0] = ratio*_ev_position_x;
        input[1] = ratio*_ev_position_y;

        if(container->getRGBCanvas() != NULL)
        {
            if(painter->Coordinates2Dto3D(input, output))
            {
                for(unsigned int d = 0; d < RenderingContainer3D::Dimensions; d++)
                {
                    if(cube.min[d] > output[d]) cube.min[d] = output[d];
                    if(cube.max[d] < output[d]) cube.max[d] = output[d];
                }

                drawLine(lastcoords, output);

                for(unsigned int d = 0; d < RenderingContainer3D::Dimensions; d++)
                {
                    lastcoords[d] = output[d];
                }
            }
            else lastcoords[0] = -1;
            return REDRAW;
        }
    }
    lastcoords[0] = -1;
    return DONOTHING;
}

AfterPaintAction Paint3DPen::mousePressEvent(QMouseEvent *ev)
{
    if((ev->buttons() & Qt::LeftButton) && (!QApplication::keyboardModifiers()))
    {
        ev->accept();
#if (QT_VERSION >= QT_VERSION_CHECK(5, 0, 0))
        double ratio = 1.0;
        QWindow* wh = painter->windowHandle();
        if(wh != nullptr)
            ratio = wh->devicePixelRatio();
#ifdef _WIN32
#else
        else
        {
            QScreen* sc = painter->screen();
            if(sc != nullptr)
                ratio = sc->devicePixelRatio();
        }
#endif
#else
        double ratio = 1.0;
#endif
        int input[2];
        unsigned int output[RenderingContainer3D::Dimensions];
        input[0] = ratio*_ev_position_x;
        input[1] = ratio*_ev_position_y;
        if(painter->Coordinates2Dto3D(input, (int*)output))
        {
            cube.min[0] = cube.max[0] = lastcoords[0] = output[0];
            cube.min[1] = cube.max[1] = lastcoords[1] = output[1];
            cube.min[2] = cube.max[2] = lastcoords[2] = output[2];
            container->getRGBCanvas()->SetPixel(output, colorpixel);
        }
        else
        {
            cube.min[0] = cube.max[0] = 0;
            cube.min[1] = cube.max[1] = 0;
            cube.min[2] = cube.max[2] = 0;
            lastcoords[0] = -1;

        }
        return mouseMoveEvent(ev);
    }
    return DONOTHING;
}

AfterPaintAction Paint3DPen::mouseReleaseEvent(QMouseEvent *ev)
{
    ev->accept();
    painter->MzImage2MzRoi((int*) cube.min, (int*) cube.max, colorpixel, *erase);
    return FINALIZE;
}

AfterPaintAction Paint3DPen::mouseDoubleClickEvent(QMouseEvent *ev){return DONOTHING;}
AfterPaintAction Paint3DPen::wheelEvent(QWheelEvent *ev){return DONOTHING;}

