/*
 * QMAZDA project: Image Analysis and Pattern Recognition
 *
 * Copyright 2013-2017 Piotr M. Szczypiński <piotr.szczypinski@p.lodz.pl>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef Paint3DActiveSurface_H
#define Paint3DActiveSurface_H

#include "paint3dblock.h"
#include "sphear.h"

class Paint3DActiveSurface : public Paint3dBlock, Sphear
{
    typedef MazdaImage<unsigned char, 3> MzImage3D8;
    const unsigned int profile_length = 256;//128;
    bool threshold_setup;
    int image_direction;
    const float image_strength = 0.002;
    float image_threshold;
    const float gradient_stregth = 0; //1; //0.1;
    float smoothing_strength;
    const float anchor_strength = 0;//0.2;
    const unsigned int iterations = 110;
    const unsigned int iterationx = 100;
    float direction;
//    const float sharpness = 0.5;

public:
    AfterPaintAction mouseMoveEvent(QMouseEvent *ev);
    AfterPaintAction wheelEvent(QWheelEvent *ev);
    AfterPaintAction mouseReleaseEvent(QMouseEvent *ev);
    AfterPaintAction imageDataChanged(void);
    Paint3DActiveSurface(RenderingContainer3D* icontainer, int params[], Painter3D* ipainter, unsigned int list, GLdouble *modelMat, GLdouble *volumeMat, bool *ierase);
    ~Paint3DActiveSurface();
private:
    const double upsizing_sphear = 10.0;
    unsigned int nodes_count;
    //double pra;
    float* pr1;
    float* pr2;
    //unsigned char** profile;
    float** profile;
    bool redraw_ellipsoid;

    void createBlock(void);
    void rasterizeBlock(void);
    AfterPaintAction changeShape(int delta, bool pressed);
    void createBall(void);
    void ballFill(void);
//    float filtrationKrnel(float distance, float sharpness);
    void resetSurface(void);
    void fitSurface(void);
};

#endif // Paint3DActiveSurface_H
