/*
 * QMAZDA project: Image Analysis and Pattern Recognition
 *
 * Copyright 2013-2016 Piotr M. Szczypiński <piotr.szczypinski@p.lodz.pl>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "randomnames.h"
//#include "../MzShared/filenameremap.h"
#include <QApplication>
#include <QFile>
#include <QDir>
#include <time.h>

RandomNames randomNames;

RandomNames::RandomNames()
{
    srand(QApplication::applicationPid()*time(NULL));
}

RandomNames::~RandomNames()
{
    for(unsigned int r = 0; r < filesToremove.size(); r++)
        QFile::remove(filesToremove[r].c_str());
    filesToremove.clear();
}

std::string RandomNames::getTempName(std::string prefix, std::string sufix, bool toRemove)
{
    QString pathq = QDir::tempPath();
    std::string path = pathq.toStdString();
    //std::string path = QDir::tempPath().toStdString();
    char* n = new char[path.size()+prefix.size()+sufix.size()+12];
    int r = rand();
    sprintf(n, "%s%s%.6X%s", path.c_str(), prefix.c_str(), 0xffffff&r, sufix.c_str());
    std::string ret = n;
    if(toRemove) filesToremove.push_back(ret);
    delete[] n;
    return ret;
}

void RandomNames::deleteFile(std::string fname)
{
    QFile::remove(fname.c_str());
}
