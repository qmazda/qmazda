/*
 * QMAZDA project: Image Analysis and Pattern Recognition
 *
 * Copyright 2013-2016 Piotr M. Szczypiński <piotr.szczypinski@p.lodz.pl>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QDropEvent>
#include <QMimeData>
#include <QDesktopServices>
#include <iomanip>
#include "roitools.h"
#include "dialogfeatureoptions.h"
#include "../MzShared/filenameremap.h"
#include "dialogvoxelsize.h"


#include <itkVersion.h>
#include <GL/gl.h> //OSX
#include <opencv2/core/version.hpp>

const int channel_items_number = 21;
const char channel_items_names[channel_items_number][combo_names_length] =
{
    "Y - brightness",
    "R - red",
    "G - green",
    "B - blue",
    "U - U (YUV)",
    "V - V (YUV)",
    "I - I (YIQ)",
    "Q - Q (YIQ)",
    "H - Hue (YUV)",
    "h - Hue (YIQ)",
    "S - Saturation",
    "u - U/Y",
    "v - V/Y",
    "i - I/Y",
    "q - Q/Y",
    "L - L* (CIE)",
    "a - a* (CIE)",
    "b - b* (CIE)",
    "x - X (CIE)",
    "y - Y (CIE)",
    "z - Z (CIE)"
};


//bool MainWindowEventHandler::eventFilter(QObject* obj, QEvent* e)
//{
//    if (e->type()==QEvent::KeyPress)
//    {
//        QKeyEvent* ev = static_cast<QKeyEvent*>(e);
//        printf("eventFilter:keyPressEvent %x\n", ev->key());
//        fflush(stdout);
//    }
//    return QObject::eventFilter(obj, e);
//}

//#include "opencv2/highgui/highgui.hpp"
//#include "opencv2/imgproc/imgproc_c.h"
//void MainWindow::keyPressEvent(QKeyEvent* ev)
//{
//    printf("MainWindow:keyPressEvent %x\n", ev->key());
//    fflush(stdout);


////    switch(ev->key())
////    {
////    case Qt::Key_B: printf("B pressed\n"); break;
////    default: break;
////    }
//}
//void MainWindow::keyReleaseEvent(QKeyEvent* /*ev*/){}

void MainWindow::on_actionAbout_triggered()
{
    std::stringstream ss;
    ss << "<h2>qmazda MaZda</h2> " << std::endl;
    ss << QMAZDA_VERSION << "<br>" << std::endl;
    ss << QMAZDA_COPYRIGHT << "<br>" << std::endl;
    ss << "Built on " << __DATE__ << " at " << __TIME__ << "<br> <br>" << std::endl;
    ss << "The program is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING THE WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE." << "<br> <br>" << std::endl;
    ss << "Home: <a href=\"http://www.eletel.p.lodz.pl/pms/SoftwareQmazda.html\"> http://www.eletel.p.lodz.pl/pms/SoftwareQmazda.html </a> <br>" << std::endl;
    ss << "Sources: <a href=\"https://gitlab.com/qmazda/qmazda\"> https://gitlab.com/qmazda/qmazda </a> <br>" << std::endl;
    ss << "<br>Built with:<br>" << std::endl;
    ss << "- Qt " << QT_VERSION_STR << " <a href=\"https://www.qt.io/developers/\">https://www.qt.io/developers</a> <br>" << std::endl;
    ss << "- LibTIFF " <<  TIFFLIB_VERSION << " <a href=\"http://www.libtiff.org/\">http://www.libtiff.org</a> <br>" << std::endl;
    ss << "- Insight Toolkit " <<  ITK_VERSION << " <a href=\"https://itk.org/\">https://itk.org</a> <br>" << std::endl;
    ss << "- OpenCV " <<   CV_VERSION << " <a href=\"http://opencv.org/\">http://opencv.org</a> <br>" << std::endl;
//    char* glversion = (char*) glGetString(GL_VERSION);
//    if(glversion != NULL)  //OSX
//        ss << "- OpenGL " << glversion <<std::endl;
//    else
        ss << "- OpenGL" << std::endl;
    QMessageBox::about(this, QString("About MaZda"), ss.str().c_str());
}

void MainWindow::on_actionContents_triggered()
{
    //QDesktopServices::openUrl(QUrl(getExecutableWithPath("/qmazda.pdf"), QUrl::TolerantMode));
    QDesktopServices::openUrl(QUrl::fromLocalFile(getExecutableWithPath("/qmazda.pdf", false)));  //OSX
}

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    container2d = NULL;
    container3d = NULL;
    container = NULL;
    renderer2d = NULL;
    renderer3d = NULL;
//    renderer = NULL;
    renderArea = NULL;
//    setZoomRequested = false;

    ui->setupUi(this);
    if(sizeof(MazdaImagePixelType) == 1)
    {
        ui->horizontalSliderMax->setMaximum(256);
        ui->horizontalSliderMin->setMaximum(256);
        ui->spinBoxMax->setMaximum(256);
        ui->spinBoxMax->setSingleStep(1);
        ui->spinBoxMin->setMaximum(256);
        ui->spinBoxMin->setSingleStep(1);
    }
    zoomSlider.setOrientation(Qt::Horizontal);
    zoomSlider.setMinimumWidth(200);
    zoomSlider.setMinimum(-128);
    zoomSlider.setMaximum(128);
    zoomSlider.setValue(0);
    zoomSlider.setSizePolicy(QSizePolicy::Fixed, QSizePolicy::Minimum);

//http://doc.qt.io/qt-4.8/stylesheet-reference.html    
//https://forum.qt.io/topic/12380/using-a-stylesheet-to-customize-a-qslider

    zoomSlider.setStyleSheet(
        "QSlider::groove:horizontal {"
        "background: #82BAD5;"
        "border: 1px solid #CCCECE;"
        "height: 2px;"
        "margin: 5 5 5 5;"
        "}"
        "QSlider::handle:horizontal {"
        "background: #82BAD5;"
        "width: 7px;"
        "margin: -4px 0 -4px 0;"
        "border: 1px solid #77839C;"
        "border-radius: 4px;"
        "}");


    dockEditOptions = new DockEditOptions(this);
    dockEditOptions->setHidden(true);

    //renderArea = new RenderArea;
    //renderArea->setFrameShape(QFrame::NoFrame);
    //setCentralWidget(renderArea);

    //ui->klient->addWidget(renderArea);


    zoomReset.setIcon(QIcon(":/icons/icons/zoom-original.png"));
    zoomReset.setStyleSheet("border: none");
    ui->statusBar->addPermanentWidget(&zoomSlider);
    ui->statusBar->addPermanentWidget(&zoomReset);
    connect(&zoomSlider, SIGNAL(valueChanged(int)), this, SLOT(zoomSliderValueChanged(int)));//???
    connect(&zoomReset, SIGNAL(clicked()), this, SLOT(zoomResetClicked()));//???
    memset(histogram, 0, histogram_size * sizeof(int));

    QPalette pal(palette());
    //unsigned int bc = palette().background().color().rgb();
    unsigned int bc = palette().color(QPalette::Window).rgb();

    bc = bc & 0xfcfcfcfc;
    bc = (bc >> 2);
//    bc = bc & 0xfefefefe;
//    bc = (bc >> 1);
    Renderer3D::initColor(bc);
    Renderer2D::initColor(bc);

    for(int f = 0; f < channel_items_number; f++)
    {
        ui->comboBox->addItem(channel_items_names[f]);
    }
//    createDrawingToolsTree();

    //on_defaultOptionsButton_clicked();

//    ui->actionPencil->setChecked(true);
//    setTemporatyFileNames();

    ui->viewMenu->addSeparator();
    ui->viewMenu->addAction(ui->regionsDockWidget->toggleViewAction());
    ui->viewMenu->addAction(ui->imageDockWidget->toggleViewAction());
    ui->viewMenu->addSeparator();
    ui->viewMenu->addAction(ui->fileToolBar->toggleViewAction());
    ui->viewMenu->addAction(ui->drawingToolBar->toggleViewAction());
//    ui->viewMenu->addAction(ui->analysisToolBar->toggleViewAction());

    ui->menuEdit->addSeparator();
    ui->menuEdit->addAction(dockEditOptions->toggleViewAction());

    initializeFeatures();
    dialog_roi = new FeaturesWizard(&roiFeaturesTemplate, &featureListRoi, false, this);
    dialog_local = new FeaturesWizard(&localFeaturesTemplate, &featureListLocal, true, this);
}

MainWindow::~MainWindow()
{
    delete dialog_local;
    delete dialog_roi;
//    deleteTemporatyFileNames();
    if(renderer2d != NULL) delete renderer2d;
    if(renderer3d != NULL) delete renderer3d;
    if(container2d != NULL) delete container2d;
    if(container3d != NULL) delete container3d;
    delete ui;
}

void MainWindow::rendererRepaint(void)
{
    if(signalsBlocked()) return;
    if(renderer2d != NULL)
        renderer2d->Repaint();
    else if(renderer3d != NULL)
        renderer3d->Repaint();
}


void MainWindow::switchRender2D(void)
{
    if(container2d == NULL) return;

    blockSignals(true);
    if(!ui->actionRender_2D->isChecked())
        ui->actionRender_2D->setChecked(true);
    if(ui->actionRender_3D->isChecked())
        ui->actionRender_3D->setChecked(false);
    if(renderer2d != NULL)
        delete renderer2d;
    if(renderer3d != NULL)
        delete renderer3d;
    renderer2d = NULL;
    renderer3d = NULL;

    if(renderArea != NULL) delete renderArea;
    renderArea = new RenderArea;
    renderArea->setFrameShape(QFrame::NoFrame);
    ui->klient->addWidget(renderArea);

    renderer2d = new Painter2D(container2d, dockEditOptions, NULL);
    connect(renderer2d, SIGNAL(setZoomRequest(double)), this, SLOT(setZoomRequest(double)));
    connect(renderer2d, SIGNAL(setRoiListRequest()), this, SLOT(setRoiListRequest()));
    connect(renderer2d, SIGNAL(setStatusTextRequest(QString*)), this, SLOT(setStatusTextRequest(QString*)));

//    connect(renderer2d, SIGNAL(setRedoUndoRequest()), this, SLOT(setRedoUndoRequest()));
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//    renderArea->setStyleSheet("background-color:blue");
//    renderer2d->setStyleSheet("background-color:blue");

//    printf("Style:\n%s\n", ui->centralWidget->styleSheet().toStdString().c_str());
//    fflush(stdout);


//    QPalette pal(renderArea->palette());
//    unsigned int bc = renderer2d->GetBackground();
//    //pal.setColor(QPalette::Background, QColor(bc));
//    pal.setColor(QPalette::Window, QColor(bc));
//    renderArea->setPalette(pal);

    renderArea->setWidgetResizable(false);
    renderArea->setAlignment(Qt::AlignCenter);
    renderArea->setWidget(renderer2d);

//    printf("Style:\n%s\n", renderArea->styleSheet().toStdString().c_str());
//    fflush(stdout);

    ui->toolButton_Color->setEnabled(false);
    ui->actionRender_3D->setChecked(false);
    ui->frame3DMode->setEnabled(false);
    ui->toolButton_Alfa->setEnabled(false);
    ui->toolButton_Afla->setEnabled(false);

    ui->actionRectangle->setIcon(QIcon(":/icons/icons/rectangle2d.png"));
    ui->actionLine->setIcon(QIcon(":/icons/icons/polygon2d.png"));
    ui->actionElipse->setIcon(QIcon(":/icons/icons/ellipse2d.png"));
    ui->actionActiveContour->setIcon(QIcon(":/icons/icons/elastic2d.png"));
    if(container2d != NULL)
    {
        container2d->getImageHistogram(histogram, histogram_size, histogram_minmax);
        ui->spinBoxMin->setMinimum(histogram_minmax[0]);
        ui->spinBoxMax->setMinimum(histogram_minmax[0]);
        ui->horizontalSliderMin->setMinimum(histogram_minmax[0]);
        ui->horizontalSliderMax->setMinimum(histogram_minmax[0]);
        ui->spinBoxMin->setMaximum(histogram_minmax[1]+1);
        ui->spinBoxMax->setMaximum(histogram_minmax[1]+1);
        ui->horizontalSliderMin->setMaximum(histogram_minmax[1]+1);
        ui->horizontalSliderMax->setMaximum(histogram_minmax[1]+1);
        ui->spinBoxMin->setValue(histogram_minmax[0]);
        ui->spinBoxMax->setValue(histogram_minmax[1]+1);
        ui->horizontalSliderMin->setValue(histogram_minmax[0]);
        ui->horizontalSliderMax->setValue(histogram_minmax[1]+1);
        container2d->setImageGrayWindowHigh(ui->horizontalSliderMax->value());
        container2d->setImageGrayWindowLow(ui->horizontalSliderMin->value());
        if(ui->imageOffButton->isChecked()) container2d->setImageVisibilityMode(OFF);
        else if(ui->imageLevelsButton->isChecked()) container2d->setImageVisibilityMode(THREELEVEL);
        else container2d->setImageVisibilityMode(NORMAL);
    }
    blockSignals(false);
    uncheckDrawingTools(NULL, DT_NONE);
    renderer2d->Repaint();
    redrawHistogram();
}

void MainWindow::switchRender3D(void)
{
    if(container3d == NULL)
        return;
    blockSignals(true);
    if(!ui->actionRender_3D->isChecked())
    {
        ui->actionRender_3D->setChecked(true);
    }
    if(ui->actionRender_2D->isChecked())
        ui->actionRender_2D->setChecked(false);
    if(renderer2d != NULL)
        delete renderer2d;
    if(renderArea != NULL)
        delete renderArea;
    renderArea = NULL;
    if(renderer3d != NULL)
        delete renderer3d;
    renderer2d = NULL;
    renderer3d = NULL;

// Problem skaczącego obrazu w 2D jest tu. Jeśli połaczy się jakoś renderer3d
// z renderArea a potem przełączy na renderer2d to obrazek skacze.
// Trzeba coś zresetować w renderArea przed podłączeniem render2d... ale nie wiem co.
// Problem rozwiązany: renderArea jast usuwane i towrzone na nowo podczas przełączenia
// na rendering 2D. Utoworzono dodatkowy widget wstawiony w klienta okna.

    renderer3d = new Painter3D(container3d, dockEditOptions, NULL);
    connect(renderer3d, SIGNAL(setZoomRequest(double)), this, SLOT(setZoomRequest(double)));
    connect(renderer3d, SIGNAL(setRoiListRequest()), this, SLOT(setRoiListRequest()));
    connect(renderer3d, SIGNAL(setStatusTextRequest(QString*)), this, SLOT(setStatusTextRequest(QString*)));

//    connect(renderer3d, SIGNAL(setRedoUndoRequest()), this, SLOT(setRedoUndoRequest()));

    //renderArea->setWidgetResizable(true);
    // renderArea->setWidget(renderer3d);
    ui->klient->addWidget(renderer3d);
    ui->toolButton_Color->setEnabled(true);
    ui->actionRender_2D->setChecked(false);
    ui->frame3DMode->setEnabled(true);
    ui->toolButton_Alfa->setEnabled(true);
    ui->toolButton_Afla->setEnabled(true);
    ui->actionRectangle->setIcon(QIcon(":/icons/icons/cube3d.png"));
    ui->actionLine->setIcon(QIcon(":/icons/icons/tube3d.png"));
    ui->actionElipse->setIcon(QIcon(":/icons/icons/ball3d.png"));
    ui->actionActiveContour->setIcon(QIcon(":/icons/icons/elastic3d.png"));
    if(container3d != NULL)
    {
        container3d->getImageHistogram(histogram, histogram_size, histogram_minmax);
        ui->spinBoxMin->setMinimum(histogram_minmax[0]);
        ui->spinBoxMax->setMinimum(histogram_minmax[0]);
        ui->horizontalSliderMin->setMinimum(histogram_minmax[0]);
        ui->horizontalSliderMax->setMinimum(histogram_minmax[0]);
        ui->spinBoxMin->setMaximum(histogram_minmax[1]+1);
        ui->spinBoxMax->setMaximum(histogram_minmax[1]+1);
        ui->horizontalSliderMin->setMaximum(histogram_minmax[1]+1);
        ui->horizontalSliderMax->setMaximum(histogram_minmax[1]+1);
        ui->spinBoxMin->setValue(histogram_minmax[0]);
        ui->spinBoxMax->setValue(histogram_minmax[1]+1);
        ui->horizontalSliderMin->setValue(histogram_minmax[0]);
        ui->horizontalSliderMax->setValue(histogram_minmax[1]+1);

        container3d->setImageGrayWindowHigh(ui->horizontalSliderMax->value());
        container3d->setImageGrayWindowLow(ui->horizontalSliderMin->value());
        if(renderer3d != NULL) renderer3d->setRepaintVolume();
        if(ui->imageOffButton->isChecked()) container3d->setImageVisibilityMode(OFF);
        else if(ui->imageLevelsButton->isChecked()) container3d->setImageVisibilityMode(THREELEVEL);
        else container3d->setImageVisibilityMode(NORMAL);
    }
    //redrawHistogram();
    blockSignals(false);
    uncheckDrawingTools(NULL, DT_NONE);
    renderer3d->Repaint();
    redrawHistogram();
}

void MainWindow::dragEnterEvent(QDragEnterEvent* event)
{
    event->acceptProposedAction();
}

void MainWindow::dragMoveEvent(QDragMoveEvent* event)
{
    event->acceptProposedAction();
}

void MainWindow::dragLeaveEvent(QDragLeaveEvent* event)
{
    event->accept();
}

void MainWindow::dropEvent(QDropEvent* event)
{
    const QMimeData* mimeData = event->mimeData();

    if (mimeData->hasUrls())
    {
        QList<QUrl> urlList = mimeData->urls();

        int ulsize = urlList.size();

        if(ulsize >= 1)
        {
            QString filename = urlList.at(0).toLocalFile();
            if(MazdaRoiIO<RenderingContainer3D::MzRoi>::isTiffRoi(filename.toStdString()))
            {
                if(loadRoi(filename, event->possibleActions() & Qt::MoveAction))
                {
                    event->acceptProposedAction();
                    return;
                }
            }
            else
            {
                if(ulsize == 1)
                {
                    if(loadImage(&filename))
                        event->acceptProposedAction();
                }
                else
                {
                    QStringList fileNames;
                    for(int i = 0; i < ulsize; i++)
                        fileNames.append(urlList.at(i).toLocalFile());
                    if(loadImages(&fileNames))
                        event->acceptProposedAction();
                }
            }
        }
    }
}

bool MainWindow::loadImage(const QString* fileName)
{
    QCursor oc = cursor();

    std::string fileNameLocal = fileName->toStdString();
    //std::string fileNameRI = fileName->toStdString();
    //printf("File %s %s\n", fileName->toStdString().c_str(), fileNameRI.c_str());
    setCursor(Qt::WaitCursor);
    try
    {
        MazdaDummy* image = MazdaImageIO< MIType3D, MazdaImagePixelType >::Read(fileNameLocal);
        if(dynamic_cast < MIType2D* >(image) || dynamic_cast < MIType2DRGB* >(image))
        {
            if(container2d != NULL)
                delete container2d;
            container2d = new RenderingContainer2D;
            container = container2d;
            if(dynamic_cast < MIType2DRGB* >(image))
            {
                container2d->setImage(dynamic_cast < MIType2DRGB* >(image));
                imageInfo = (dynamic_cast < MIType2DRGB* >(image))->GetName();
                (dynamic_cast < MIType2DRGB* >(image))->SetName("");
                ui->comboBox->setEnabled(true);
            }
            else if(dynamic_cast < MIType2D* >(image))
            {
                container2d->setImage(dynamic_cast < MIType2D* >(image));
                imageInfo = (dynamic_cast < MIType2D* >(image))->GetName();
                (dynamic_cast < MIType2D* >(image))->SetName("");
                ui->comboBox->setEnabled(false);
            }
            switchRender2D();
            if(container3d != NULL)
                delete container3d;
            container3d = NULL;
            updateXYZASliders();
            zoomSlider.setValue(0);
            if(renderer2d != NULL)
            {
                renderer2d->ResetView();
                renderer2d->Repaint();
            }
            QString openFileName = QFileInfo(*fileName).fileName();
            this->setWindowTitle(QString("%1 - %2").arg(openFileName).arg(tr("MaZda")));
            if(container2d->getNumberOfRois() <= 0)
                on_addRoiButton_clicked();
            ui->actionRender_3D->setEnabled(false);
            ui->actionVoxel_size->setEnabled(false);
            ui->actionRender_2D->setEnabled(true);
            ui->comboBox->setCurrentIndex(0);
            container->setModified();
            setCursor(oc);
            return true;
        }
        if(dynamic_cast < MIType3D* >(image) || dynamic_cast < MIType3DRGB* >(image))
        {
            if(container3d != NULL)
                delete container3d;
            container3d = new RenderingContainer3D;
            container = container3d;
            if(dynamic_cast < MIType3DRGB* >(image))
            {
                container3d->setImage(dynamic_cast < MIType3DRGB* >(image));
                imageInfo = (dynamic_cast < MIType3DRGB* >(image))->GetName();
                (dynamic_cast < MIType3DRGB* >(image))->SetName("");
                ui->comboBox->setEnabled(true);
            }
            else if(dynamic_cast < MIType3D* >(image))
            {
                container3d->setImage(dynamic_cast < MIType3D* >(image));
                imageInfo = (dynamic_cast < MIType3D* >(image))->GetName();
                (dynamic_cast < MIType3D* >(image))->SetName("");
                ui->comboBox->setEnabled(false);
            }
            switchRender3D();
            if(container2d != NULL)
                delete container2d;
            container2d = NULL;
            updateXYZASliders();
            zoomSlider.setValue(0);
            if(renderer3d != NULL) renderer3d->ResetView();
            QString openFileName = QFileInfo(*fileName).fileName();
            this->setWindowTitle(QString("%1 - %2").arg(openFileName).arg(tr("MaZda")));
            if(container3d->getNumberOfRois() <= 0)
                on_addRoiButton_clicked();
            ui->actionRender_3D->setEnabled(true);
            ui->actionVoxel_size->setEnabled(true);
            ui->actionRender_2D->setEnabled(true);
            ui->comboBox->setCurrentIndex(0);
            container->setModified();
            setCursor(oc);
            return true;
        }
    }
    catch(...){}
    setCursor(oc);
    return false;
}

bool MainWindow::loadImages(const QStringList *fileNames)
{
    QCursor oc = cursor();

    setCursor(Qt::WaitCursor);
    try
    {
        std::vector <std::string> fileNamesLocal;
        int fmax = fileNames->count();
        for(int f = 0; f < fmax; f++)
            fileNamesLocal.push_back(((*fileNames)[f]).toStdString());

        MIType3D* image = MazdaImageIO< MIType3D, MazdaImagePixelType >::Read(fileNamesLocal);

        if(image != NULL)
        {
            if(container3d != NULL)
                delete container3d;
            container3d = new RenderingContainer3D;
            container = container3d;
            container3d->setImage(image);
            imageInfo = image->GetName();
            image->SetName("");
            ui->comboBox->setEnabled(false);
            switchRender3D();
            if(container2d != NULL)
                delete container2d;
            container2d = NULL;
            updateXYZASliders();
            zoomSlider.setValue(0);
            if(renderer3d != NULL) renderer3d->ResetView();
            this->setWindowTitle("File list - MaZda");
            if(container3d->getNumberOfRois() <= 0)
                on_addRoiButton_clicked();
            ui->actionRender_3D->setEnabled(true);
            ui->actionVoxel_size->setEnabled(true);
            ui->actionRender_2D->setEnabled(true);
            ui->comboBox->setCurrentIndex(0);
            container->setModified();
        }

    }
    catch(...)
    {
        setCursor(oc);
        return false;
    }
    setCursor(oc);
    return true;
}



/**
 * @brief MainWindow::updateXYZASliders set view options for newly loaded image
 */
void MainWindow::updateXYZASliders(void)
{
    unsigned int size[RenderingContainer3D::Dimensions];
    size[2] = 1; size[1] = 1; size[0] = 1;

    container->getSize(size);

    if(renderer3d != NULL)
    {
        renderer3d->setCrossNumber(1, size[0]/2);
        renderer3d->setCrossNumber(2, size[1]/2);
        renderer3d->setCrossNumber(3, size[2]/2);
        renderer3d->setCrossShow(1, true);
        renderer3d->setCrossShow(2, true);
        renderer3d->setCrossShow(3, true);
        renderer3d->setVolumeView(0);
        renderer3d->Repaint();
    }
    blockSignals(true);
    ui->spinBox_X->setMaximum(size[0]-1);
    ui->horizontalSlider_X->setMaximum(size[0]-1);
    ui->spinBox_X->setMinimum(0);
    ui->horizontalSlider_X->setMinimum(0);
    ui->spinBox_X->setValue(size[0]/2);

    ui->spinBox_Y->setMaximum(size[1]-1);
    ui->horizontalSlider_Y->setMaximum(size[1]-1);
    ui->spinBox_Y->setMinimum(0);
    ui->horizontalSlider_Y->setMinimum(0);
    ui->spinBox_Y->setValue(size[1]/2);

    ui->spinBox_Z->setMaximum(size[2]-1);
    ui->horizontalSlider_Z->setMaximum(size[2]-1);
    ui->spinBox_Z->setMinimum(0);
    ui->horizontalSlider_Z->setMinimum(0);
    ui->spinBox_Z->setValue(size[2]/2);

    ui->toolButton_Alfa->setChecked(false);
    ui->toolButton_Afla->setChecked(false);
    blockSignals(false);
}

bool MainWindow::saveRoi(const char* fileName)
{
    if(container2d != NULL)
    {
        return MazdaRoiIO<RenderingContainer2D::MzRoi>::Write(fileName, container2d->getRoitable(), NULL);
    }
    else if(container3d != NULL)
    {
        return MazdaRoiIO<RenderingContainer3D::MzRoi>::Write(fileName, container3d->getRoitable(), NULL);
    }
    return false;
}

bool MainWindow::saveImage(const char* fileName)
{
    if(container2d != NULL)
    {
        MIType2D* i = container2d->getGrayImage();
        MIType2DRGB* ir = container2d->getRGBImage();
        try
        {
            if(ir != NULL) return MazdaImageIO< MIType2DRGB, MazdaImagePixelType >::Write(fileName, ir);
            else if(i != NULL) return MazdaImageIO< MIType2D, MazdaImagePixelType >::Write(fileName, i);
        }
        catch(...){}
    }
    if(container3d != NULL)
    {
        MIType3D* i = container3d->getGrayImage();
        MIType3DRGB* ir = container3d->getRGBImage();
        try
        {
            if(ir != NULL) return MazdaImageIO< MIType3DRGB, MazdaImagePixelType >::Write(fileName, ir);
            else if(i != NULL) return MazdaImageIO< MIType3D, MazdaImagePixelType >::Write(fileName, i);
        }
        catch(...){}
    }
    return false;
}


bool MainWindow::on_actionSaveImage_triggered()
{
    if(container2d == NULL && container3d == NULL) return false;
    QString fileName = QFileDialog::getSaveFileName(this,
                                                    tr("Save image"),
                                                    NULL,//domyslna nazwa pliku
                                                    tr("Tiff file (*.tif);;Nifti file (*.nii);;All Files (*)"));

    bool saved = false;
    if (!fileName.isEmpty())
    {
        QCursor oc = cursor();
        setCursor(Qt::WaitCursor);
        bool done = saveImage(fileName.toStdString().c_str());
        setCursor(oc);

        if(!done)
        {
            QMessageBox msgBox;
            msgBox.setText(QString("Cannot save the image in this format."));
            msgBox.setWindowTitle(QString("Save image"));
            msgBox.setIcon(QMessageBox::Warning);
            msgBox.exec();
        }
    }
    return saved;
}

void MainWindow::on_actionLoadImage_triggered()
{

    QStringList filenames = QFileDialog::getOpenFileNames
    //QString fileName = QFileDialog::getOpenFileName
                                                   (this,
                                                    tr("Load image"),
                                                    NULL,//domyslna nazwa pliku
                                                    tr(
                                                        "Image files (*.tif *.tiff *.bmp *.png"
                                                        " *.dcm *.dicom"
                                                        " *.nii *.nifti *.hdr *.nii.gz"
                                                        ");;All files (*)"));

    if( !filenames.isEmpty() )
    {
        if(filenames.count() == 1)
            loadImage(&filenames.at(0));
        else
            loadImages(&filenames);
    }

    //if (!fileName.isEmpty()) loadImage(&fileName);
}

void MainWindow::adjustScrollBar(QScrollBar *scrollBar, double factor)
{
    scrollBar->setValue(int(factor * scrollBar->value() + ((factor - 1) * scrollBar->pageStep()/2)));
}

void MainWindow::on_actionZoom_in_triggered()
{
    zoomSlider.setValue(zoomSlider.value()+8);
}

void MainWindow::on_actionZoom_out_triggered()
{
    zoomSlider.setValue(zoomSlider.value()-8);
}

void MainWindow::on_actionZoom_1_triggered()
{
    zoomSlider.setValue(0);
}

void MainWindow::recreateRoiControls(void)
{
    blockSignals(true);
    int current = ui->roiListWidget->currentRow();
    ui->roiListWidget->clear();
    if(container != NULL)
    {
        unsigned int nroi = container->getNumberOfRois();
        if(nroi > 0)
        {
            for(unsigned int r = 0; r < nroi; r++)
            {
                unsigned int color = container->getRoiColor(r);
                bool check = container->getRoiVisibility(r);
                std::string name = container->getRoiName(r);
                color |= 0xff000000;
                QListWidgetItem *item = new QListWidgetItem(ui->roiListWidget);
                item->setText(QString::fromStdString(name));
                item->setFlags(Qt::ItemIsSelectable | Qt::ItemIsEnabled | Qt::ItemIsEditable | Qt::ItemIsUserCheckable);
                if(check) item->setCheckState(Qt::Checked);
                else item->setCheckState(Qt::Unchecked);
                setColorToRoiItem(item, QColor(color));
            }
            unsigned int act = container->getActiveRoiIndex();
            ui->roiListWidget->setCurrentRow(act);
        }
        ui->actionRedo->setEnabled(container->canRedo());
        ui->actionUndo->setEnabled(container->canUndo());
    }
    ui->roiListWidget->setCurrentRow(current);
    blockSignals(false);
}

bool MainWindow::loadRoiAsk(const QString fileName, bool erase)
{
    /* Zawiesza program w Qt6 ????
    bool a = MazdaRoiIO<RenderingContainer3D::MzRoi>::isTiffRoi(fileNameRemapRead(&fileName));
    if(! a)
    {
        QMessageBox msgBox(this);
        msgBox.setWindowTitle(QString("ROI import"));
        msgBox.setText(QString("Import may take time"));
        msgBox.setIcon(QMessageBox::Question);
        msgBox.setStandardButtons(QMessageBox::Ok | QMessageBox::Cancel);
        msgBox.setDefaultButton(QMessageBox::Ok);
        if(msgBox.exec() != QMessageBox::Ok) return false;
    }*/
    return loadRoi(fileName, erase);
}

bool MainWindow::loadRoi(const QString fileName, bool erase)
{
    QCursor oc = cursor();
    setCursor(Qt::WaitCursor);

    try
    {
        if(container3d != NULL && container2d == NULL)
        {
            unsigned int size[RenderingContainer3D::Dimensions];
            container3d->getSize(size);
            std::vector <RenderingContainer3D::MzRoi*> rois  = MazdaRoiIO<RenderingContainer3D::MzRoi>::Read(fileName.toStdString()); //??? ,size does not work
            if(erase) container3d->removeRois();
            container3d->appendRois(rois);
            container3d->setModified();
        }
        else if(container3d == NULL && container2d != NULL)
        {
            unsigned int size[RenderingContainer2D::Dimensions];
            container2d->getSize(size);
            std::vector <RenderingContainer2D::MzRoi*> rois  = MazdaRoiIO<RenderingContainer2D::MzRoi>::Read(fileName.toStdString());  //??? ,size does not work
            if(erase) container2d->removeRois();
            container2d->appendRois(rois);
            container2d->setModified();
        }
        else
        {
            setCursor(oc);
            return false;
        }
    }
    catch(...)
    {
        setCursor(oc);
        return false;
    }
    recreateRoiControls();
    rendererRepaint();
    setCursor(oc);
    return true;
}

void MainWindow::on_actionLoadRoi_triggered()
{
    QString fileName = QFileDialog::getOpenFileName(this,
                                                    tr("Load roi"),
                                                    NULL,//domyslna nazwa pliku
                                                    tr("Region of interest (*.tif *.tiff *.roi);;All Files (*)"));
    if (!fileName.isEmpty())
    {
        loadRoiAsk(fileName);
    }
}

void MainWindow::on_actionSaveRoi_triggered()
{
    QString roi_format = "Multi-page TIFF (*.tif *.tiff)";
    QString fileName = QFileDialog::getSaveFileName(this,
                                                    tr("Save ROIs"),
                                                    NULL,//domyslna nazwa pliku
                                                    roi_format);
    if (!fileName.isEmpty())
    {
        QCursor oc = cursor();
        setCursor(Qt::WaitCursor);
        bool saved = false;
        if(container2d != NULL)
        {
            saved = MazdaRoiIO<RenderingContainer2D::MzRoi>::Write(fileName.toStdString(), container2d->getRoitable(), NULL);
        }
        else if(container3d != NULL)
        {
            saved = MazdaRoiIO<RenderingContainer3D::MzRoi>::Write(fileName.toStdString(), container3d->getRoitable(), NULL);
        }
        setCursor(oc);

        if(!saved)
        {
            QMessageBox msgBox;
            msgBox.setText(QString("Saving ROI failed"));
            msgBox.setWindowTitle(QString("Save ROI"));
            msgBox.setIcon(QMessageBox::Warning);
            msgBox.exec();
        }
    }
}

void MainWindow::uncheckDrawingTools(QAction* checked, DrawingTool tool)
{
    if(checked != NULL)
    {
        if(checked->isChecked())
        {
            if(renderer2d != NULL)
            {
                renderer2d->setDrawingTool(tool, false);
            }
            if(renderer3d != NULL)
            {
                renderer3d->setDrawingTool(tool, false);
            }
        }
        else
        {
            if(renderer2d != NULL)
            {
                renderer2d->setDrawingTool(DT_NONE, false);
            }
            if(renderer3d != NULL)
            {
                renderer3d->setDrawingTool(DT_NONE, false);
            }
        }
    }
    else
    {
        if(renderer2d != NULL)
        {
            renderer2d->setDrawingTool(DT_NONE, false);
        }
        if(renderer3d != NULL)
        {
            renderer3d->setDrawingTool(DT_NONE, false);
        }
    }

    if(checked != (ui->actionPencil)) ui->actionPencil->setChecked(false);
    if(checked != (ui->actionFloodFill)) ui->actionFloodFill->setChecked(false);
    if(checked != (ui->actionRectangle)) ui->actionRectangle->setChecked(false);
    if(checked != (ui->actionElipse)) ui->actionElipse->setChecked(false);
    if(checked != (ui->actionActiveContour)) ui->actionActiveContour->setChecked(false);
    if(checked != (ui->actionLine)) ui->actionLine->setChecked(false);
    if(checked != (ui->actionMove)) ui->actionMove->setChecked(false);
    ui->actionErase->setChecked(false);
}

void MainWindow::on_actionPencil_triggered()
{
    uncheckDrawingTools(ui->actionPencil, DT_PEN);
}

void MainWindow::on_actionMove_triggered()
{
    uncheckDrawingTools(ui->actionMove, DT_MOVE);
}

void MainWindow::on_actionFloodFill_triggered()
{
    uncheckDrawingTools(ui->actionFloodFill, DT_FILL);
}

void MainWindow::on_actionRectangle_triggered()
{
    uncheckDrawingTools(ui->actionRectangle, DT_RECT);
}

void MainWindow::on_actionElipse_triggered()
{
    uncheckDrawingTools(ui->actionElipse, DT_ELLI);
}

void MainWindow::on_actionActiveContour_triggered()
{
    uncheckDrawingTools(ui->actionActiveContour, DT_SNAKE);
}

void MainWindow::on_actionLine_triggered()
{
    uncheckDrawingTools(ui->actionLine, DT_POLY);
}

void MainWindow::on_actionErase_triggered()
{
    if(renderer2d != NULL) renderer2d->setDrawingTool(DT_IGNORE, ui->actionErase->isChecked());
    if(renderer3d != NULL) renderer3d->setDrawingTool(DT_IGNORE, ui->actionErase->isChecked());
}

void MainWindow::on_actionEraseRegion_triggered()
{
    QListWidgetItem *item = ui->roiListWidget->currentItem();
    if(item != NULL)
    {
        int index = ui->roiListWidget->row(item);
        if(index >= 0)
        {
            if(renderer2d != NULL && container2d != NULL)
            {
                if(container2d->getActiveRoi() != NULL)
                {
                    container2d->getActiveRoi()->Erase();
                    container2d->setRoisRedraw();
                    renderer2d->Repaint();
                }
            }
            if(renderer3d != NULL && container3d != NULL)
            {
                if(container3d->getActiveRoi() != NULL)
                {
                    container3d->getActiveRoi()->Erase();
                    container3d->setRoisRedraw();
                    renderer3d->Repaint();
                }
            }
        }
    }
}


/*===========================================================================*/
/*===========================================================================*/
/*========    ROI DOCKED WINDOW RELATED FUNCTIONS                       =====*/
/*===========================================================================*/
/*===========================================================================*/

/*===========================================================================*/
void MainWindow::setColorToRoiItem(QListWidgetItem *item, QColor color)
{
    int width = ui->roiListWidget->style()->pixelMetric(QStyle::PM_IndicatorWidth);
    int height = ui->roiListWidget->style()->pixelMetric(QStyle::PM_IndicatorHeight);
    QPixmap pixmap(width, height);
    QIcon icon;
    QPainter pixPaint(&pixmap);
    QBrush brush(color);
    pixPaint.setBrush(brush);
    QPen pend(color.darker(150));
    pixPaint.setPen(pend);
    pixPaint.drawRect(0.0, 0.0, width - 1, height - 1);
    pend.setColor(color.lighter());
    pixPaint.setPen(pend);
    pixPaint.drawLine(0.0, 0.0, width - 1, 0.0);
    pixPaint.drawLine(0.0, 0.0, 0.0, height - 1);
    icon.addPixmap(pixmap);
    item->setIcon(icon);
}

/*===========================================================================*/
void MainWindow::on_roiListWidget_itemClicked(QListWidgetItem *item)
{
    if (item == NULL) return;
    QRect rect = ui->roiListWidget->getIconRect(item);
    QPoint mouse = ui->roiListWidget->mapFromGlobal(QCursor::pos());
    if(mouse.x() >= rect.left() && mouse.x() <= rect.right())
    {
        int row = ui->roiListWidget->row(item);
        unsigned int initcolor = container->getRoiColor(row);
        initcolor |= 0xff000000;
        initcolor &= 0xffffffff;
        QColor color = QColorDialog::getColor(QColor(initcolor));
        if(color.isValid())
        {
            color.setHsv(color.hsvHue(), color.hsvSaturation(), 255);
            QRgb rgb = color.rgb();
            setColorToRoiItem(item, color);
            if(row >= 0 && row < ui->roiListWidget->count())
            {
                if(container2d != NULL) container2d->setRoiColor(row, rgb);
                if(container3d != NULL)
                {
                    container3d->setRoiColor(row, rgb);
                    if(renderer3d != NULL) renderer3d->setRepaintVolume();
                }
                rendererRepaint();
            }
        }
    }
}

/*===========================================================================*/
void MainWindow::on_addRoiButton_clicked()
{
    if(container == NULL) return;
    QColor color(RoiDefaultColors[ui->roiListWidget->count() % 16]);
    QRgb rgb = color.rgb();
    QString name(tr("New"));
//    QListWidgetItem *item = new QListWidgetItem(ui->roiListWidget);
//    item->setText(name);
//    item->setFlags(Qt::ItemIsSelectable | Qt::ItemIsEnabled | Qt::ItemIsEditable | Qt::ItemIsUserCheckable);

//    item->setCheckState(Qt::Checked);
//    setColorToRoiItem(item, color);

    //bool set = false;
    if(container2d != NULL)
    {
        container2d->addRoi();
        unsigned int rm = container2d->getNumberOfRois();
        if(rm > 0)
        {
            rm--;
            container2d->setRoiColor(rm, rgb);
            container2d->setRoiName(rm, name.toStdString());
            container2d->setRoiVisibility(rm, 1);
            container2d->setActiveRoiIndex(rm);
        }
        //set = true;
    }
    if(container3d != NULL)
    {
        container3d->addRoi();
        unsigned int rm = container3d->getNumberOfRois();
        if(rm > 0)
        {
            rm--;
            container3d->setRoiColor(rm, rgb);
            container3d->setRoiName(rm, name.toStdString());
            container3d->setRoiVisibility(rm, 1);
            container3d->setActiveRoiIndex(rm);
        }
        //set = true;
    }

    recreateRoiControls();

    rendererRepaint();
}

/*===========================================================================*/
void MainWindow::on_removeRoiButton_clicked()
{
    QListWidgetItem *item = ui->roiListWidget->currentItem();
    if(item != NULL)
    {
        int index = ui->roiListWidget->row(item);
        if(index >= 0)
        {
            if(container2d != NULL) container2d->removeRoi(index);
            if(container3d != NULL)
            {
                container3d->removeRoi(index);
                if(renderer3d != NULL) renderer3d->setRepaintVolume();
            }
        }
        delete item;
        item = ui->roiListWidget->currentItem();
        index = ui->roiListWidget->row(item);
        if(index >= 0)
        {
            if(container2d != NULL) container2d->setActiveRoiIndex(index);
            if(container3d != NULL)
            {
                container3d->setActiveRoiIndex(index);
            }
        }
    }
    rendererRepaint();
}

/*===========================================================================*/
void MainWindow::on_changeRoiVisibilityButton_clicked()
{
    static int state = 0;
    state++; state %= 3;
    int imax = ui->roiListWidget->count();
    QListWidgetItem *cur = ui->roiListWidget->currentItem();
    for(int i = 0; i < imax; i++)
    {
        QListWidgetItem *item = ui->roiListWidget->item(i);
        switch(state)
        {
        case 0:
            item->setCheckState(Qt::Checked);
            if(container2d != NULL) container2d->setRoiVisibility(i, 1);
            if(container3d != NULL)
            {
                if(container3d->getRoiVisibility(i) != 1)
                {
                    container3d->setRoiVisibility(i, 1);
                    if(renderer3d != NULL) renderer3d->setRepaintVolume();
                }
            }
            rendererRepaint();
            break;
        case 1:
            item->setCheckState(Qt::Unchecked);
            if(container2d != NULL) container2d->setRoiVisibility(i, 0);
            if(container3d != NULL)
            {
                if(container3d->getRoiVisibility(i) != 0)
                {
                    container3d->setRoiVisibility(i, 0);
                    if(renderer3d != NULL) renderer3d->setRepaintVolume();
                }
            }
            rendererRepaint();
            break;
        default:
            if(item == cur)
            {
                item->setCheckState(Qt::Checked);
                if(container2d != NULL) container2d->setRoiVisibility(i, 1);
                if(container3d != NULL)
                {
                    if(container3d->getRoiVisibility(i) != 1)
                    {
                        container3d->setRoiVisibility(i, 1);
                        if(renderer3d != NULL) renderer3d->setRepaintVolume();
                    }
                }
                rendererRepaint();
            }
            else
            {
                item->setCheckState(Qt::Unchecked);
                if(container2d != NULL) container2d->setRoiVisibility(i, 0);
                if(container3d != NULL)
                {
                    if(container3d->getRoiVisibility(i) != 0)
                    {
                        container3d->setRoiVisibility(i, 0);
                        if(renderer3d != NULL) renderer3d->setRepaintVolume();
                    }
                }
                rendererRepaint();
            }
        }
    }
}

/*===========================================================================*/
void MainWindow::on_roiListWidget_itemChanged(QListWidgetItem *item)
{
    if(container == NULL) return;
    int index = ui->roiListWidget->row(item);
    if(index < 0) return;

    if(Qt::Checked == item->checkState())
    {
        if(container2d != NULL) container2d->setRoiVisibility(index, 1);
        if(container3d != NULL)
        {
            if(container3d->getRoiVisibility(index) != 1)
            {
                container3d->setRoiVisibility(index, 1);
                if(renderer3d != NULL) renderer3d->setRepaintVolume();
            }
        }
    }
    else
    {
        if(container2d != NULL) container2d->setRoiVisibility(index, 0);
        if(container3d != NULL)
        {
            if(container3d->getRoiVisibility(index) != 0)
            {
                container3d->setRoiVisibility(index, 0);
                if(renderer3d != NULL) renderer3d->setRepaintVolume();
            }
        }
    }
    if(container2d != NULL) container2d->setRoiName(index, item->text().toStdString());
    if(container3d != NULL) container3d->setRoiName(index, item->text().toStdString());
    if(ui->roiListWidget->row(ui->roiListWidget->currentItem()) == -1)
        ui->roiListWidget->setCurrentItem(item);
    rendererRepaint();
}

/*===========================================================================*/
void MainWindow::on_roiListWidget_currentItemChanged(QListWidgetItem *current, QListWidgetItem */*previous*/)
{
    int index = ui->roiListWidget->row(current);
    if(index >= 0)
    {
        current->setCheckState(Qt::Checked);
        if(container2d != NULL) container2d->setActiveRoiIndex(index);
        if(container3d != NULL) container3d->setActiveRoiIndex(index);
    }
}

/*===========================================================================*/
void MainWindow::on_toolButton_clicked()
{
    int histogram[histogram_size];
    unsigned int minmaxarea[4];
    if(container == NULL) return;
    if(!container->getActiveRegionHistogram(histogram, histogram_size, minmaxarea)) return;
    double minwindow = container->getImageGrayWindowLow()/256.0;
    double maxwindow = container->getImageGrayWindowHigh()/256.0;
    double dif1 = (maxwindow - minwindow);
    double mul1 = dif1 != 0 ? (double)histogram_size/dif1 : 0;
    int max = 0;
    for(unsigned int x = 0; x < histogram_size; x++)
        if(max < histogram[x]) max = histogram[x];
    QImage image(histogram_size, 128, QImage::Format_ARGB32);

    int index = container->getActiveRoiIndex();
    unsigned int color = container->getRoiColor(index);
    color = (color&0xffffff)|0x2f000000;

    for(unsigned int x = 0; x < histogram_size; x++)
    {
        double pix = x;
        if(dif1 != 0)
        {
            pix = (pix - minwindow) * mul1;
            pix = (pix > 255 ? 255 :(pix < 0 ? 0 : pix));
        }
        else
        {
            pix = (pix > minwindow) ? 255 : 0;
        }
        unsigned char pix8 = pix;
        pix = 0xff000000 | (pix8 << 16) | (pix8 << 8) | pix8;
        int maxy = histogram[x];
        if(maxy > 0)
        {
            maxy = ((long int) maxy) * (long int) 128 / (long int)max;
            maxy++;
        }
        else maxy = 0;
        maxy = 128 - maxy;
        int y = 0;
        for(; y < maxy; y++) image.setPixel(x, y, color);
        for(; y < 128; y++) image.setPixel(x, y, pix);
    }

    std::stringstream ss;
    ss << "Name: " << container->getRoiName(index) << std::endl;
    ss << "Index: " << index+1 << "/" << container->getNumberOfRois() << std::endl;
    ss << "Minimum grey-level: " << minmaxarea[0] << std::endl;
    ss << "Maximum grey-level: " << minmaxarea[1] << std::endl;
    ss << "Maximum value: " << max << std::endl;
    ss << "Area: " << minmaxarea[2] << std::endl;
    ss << "Color(hex): " << std::uppercase << std::setfill('0') << std::setw(6) << std::hex << (color&0xffffff) << std::endl;

    QMessageBox msgBox;
    msgBox.setText(QString(ss.str().c_str()));
    msgBox.setWindowTitle(QString("Region"));
    msgBox.setIconPixmap(QPixmap::fromImage(image));
    msgBox.exec();
}


/*===========================================================================*/
/*===========================================================================*/
/*=======  RELATED TO IMAGE CONTROL DOCKED WINDOW ===========================*/
/*===========================================================================*/
/*===========================================================================*/
void MainWindow::redrawHistogram(void)
{
    if(container == NULL) return;
    ImageVisibilityMode visibility = container->getImageVisibilityMode();

    double minwindow = container->getImageGrayWindowLow();
    double maxwindow = container->getImageGrayWindowHigh();

    double span = (double)histogram_size / (histogram_minmax[1]-histogram_minmax[0]+1);

    minwindow -= histogram_minmax[0];
    maxwindow -= histogram_minmax[0];
    minwindow *= span;
    maxwindow *= span;

/*
    if(sizeof(MazdaImagePixelType) > 1)
    {
        minwindow /= 256.0;
        maxwindow /= 256.0;
    }
*/


    double dif1 = (maxwindow - minwindow);
    double mul1 = dif1 != 0 ? (double)histogram_size/dif1 : 0;
    int max = 127;
    for(unsigned int x = 0; x < histogram_size; x++)
        if(max < histogram[x]) max = histogram[x];

    QImage image(histogram_size, 128, QImage::Format_ARGB32);
    for(unsigned int x = 0; x < histogram_size; x++)
    {
        double pix = x;
        if(visibility == NORMAL)
        {
            if(dif1 != 0)
            {
                pix = (pix - minwindow) * mul1;
                pix = (pix > 255 ? 255 :(pix < 0 ? 0 : pix));
            }
            else
            {
                pix = (pix > minwindow) ? 255 : 0;
            }
        }
        else if(visibility == THREELEVEL)
        {
            if(dif1 != 0)
            {
                pix = (pix - minwindow) * mul1;
                pix = (pix > 255 ? 255 :(pix < 0 ? 0 : 127)); //<<
            }
            else
            {
                pix = (pix > minwindow) ? 255 : 0;
            }
        }
        else
        {
            pix = 127;
        }
        unsigned char pix8 = pix;
        pix = 0xff000000 | (pix8 << 16) | (pix8 << 8) | pix8;
        int maxy = histogram[x];
        if(maxy > 0)
        {
            maxy = ((long int) maxy-1) * (long int) 128 / (long int)max;
            maxy++;
        }
        else maxy = 0;
        maxy = 128 - maxy;
        int y = 0;
        for(; y < maxy; y++) image.setPixel(x, y, 0);
        for(; y < 128; y++) image.setPixel(x, y, pix);
    }
    ui->histogramLabel->setPixmap(QPixmap::fromImage(image));
}


/*void MainWindow::redrawHistogram(void)
{
    const int maxx_size = 4;
    if(container == NULL) return;

    ImageVisibilityMode visibility = container->getImageVisibilityMode();
    double minwindow = container->getImageGrayWindowLow()/256.0;
    double maxwindow = container->getImageGrayWindowHigh()/256.0;

    double dif1 = (maxwindow - minwindow);
    double mul1 = dif1 != 0 ? (double)histogram_size/dif1 : 0;
    double maxx[8];
    int m;
    for(m = 0; m < 8; m++) maxx[m] = 0;

    for(int i = 0; i < histogram_size; i++)
    {
        for(m = 0; m < maxx_size; m++)
        {
            if(maxx[m] < histogram[i]) break;
        }
        for(int mm = maxx_size-1; mm > m; mm--)
        {
            maxx[mm] = maxx[mm-1];
        }
        if(m < maxx_size) maxx[m] = histogram[i];
    }
    double max = 1.5 * maxx[maxx_size-1];
    if(maxx[maxx_size-1]*1.5 > maxx[0]) max = maxx[0];
    if(max > 0) max = 128.0/max;
    else max = 0;

    QImage image(histogram_size, 128, QImage::Format_ARGB32);
    for(unsigned int x = 0; x < histogram_size; x++)
    {
        for(int y = 0; y < 128; y++)
        {

            double pix = x;
            if(visibility == NORMAL)
            {
                if(dif1 != 0)
                {
                    pix = (pix - minwindow) * mul1;
                    pix = (pix > 255 ? 255 :(pix < 0 ? 0 : pix));
                }
                else
                {
                    pix = (pix > minwindow) ? 255 : 0;
                }
            }
            else if(visibility == THREELEVEL)
            {
                if(dif1 != 0)
                {
                    pix = (pix - minwindow) * mul1;
//                    pix = (pix > 255 ? maxbrightness :(pix < 0 ? minbrightness : med2));
                    pix = (pix > 255 ? 255 :(pix < 0 ? 0 : 127)); //<<
                }
                else
                {
                    pix = (pix > minwindow) ? 255 : 0;
                }
//                pix *= mul2;
//                pix += minbrightness;
            }
            else
            {
//                pix = med2;
                pix = 127; //<<
            }
            unsigned char pix8 = pix;

            if(128-y < histogram[x] * max+0.999) pix = 0xff000000 | (pix8 << 16) | (pix8 << 8) | pix8;
            else pix = 0x00000000;

            image.setPixel(x, y, pix);
        }
    }
    ui->histogramLabel->setPixmap(QPixmap::fromImage(image));
}*/

void MainWindow::on_comboBox_currentIndexChanged(const QString &arg1)
{
    if(container == NULL) return;
    char ch = arg1[0].toLatin1(); // .toAscii(); in qt4
    if(container2d != NULL) container2d->setImageChannel(ch);
    if(container3d != NULL) container3d->setImageChannel(ch);

    if(container != NULL)
    {
        container->getImageHistogram(histogram, histogram_size, histogram_minmax);
        ui->spinBoxMin->setMinimum(histogram_minmax[0]);
        ui->spinBoxMax->setMinimum(histogram_minmax[0]);
        ui->horizontalSliderMin->setMinimum(histogram_minmax[0]);
        ui->horizontalSliderMax->setMinimum(histogram_minmax[0]);
        ui->spinBoxMin->setMaximum(histogram_minmax[1]+1);
        ui->spinBoxMax->setMaximum(histogram_minmax[1]+1);
        ui->horizontalSliderMin->setMaximum(histogram_minmax[1]+1);
        ui->horizontalSliderMax->setMaximum(histogram_minmax[1]+1);
        ui->spinBoxMin->setValue(histogram_minmax[0]);
        ui->spinBoxMax->setValue(histogram_minmax[1]+1);
        ui->horizontalSliderMin->setValue(histogram_minmax[0]);
        ui->horizontalSliderMax->setValue(histogram_minmax[1]+1);
        redrawHistogram();
    }
    rendererRepaint();
}

void MainWindow::on_imageLevelsButton_clicked()
{
    ui->imageOffButton->setChecked(false);
    ui->imageLevelsButton->setChecked(true);
    ui->ImageGrayButton->setChecked(false);
    if(container2d != NULL) container2d->setImageVisibilityMode(THREELEVEL);
    if(container3d != NULL)
    {
        container3d->setImageVisibilityMode(THREELEVEL);
        if(renderer3d != NULL) renderer3d->setRepaintVolume();
    }
    rendererRepaint();
    redrawHistogram();
}

void MainWindow::on_imageOffButton_clicked()
{
    ui->imageOffButton->setChecked(true);
    ui->imageLevelsButton->setChecked(false);
    ui->ImageGrayButton->setChecked(false);
    if(container2d != NULL) container2d->setImageVisibilityMode(OFF);
    if(container3d != NULL)
    {
        container3d->setImageVisibilityMode(OFF);
        if(renderer3d != NULL) renderer3d->setRepaintVolume();
    }
    rendererRepaint();
    redrawHistogram();
}

void MainWindow::on_ImageGrayButton_clicked()
{
    ui->imageOffButton->setChecked(false);
    ui->imageLevelsButton->setChecked(false);
    ui->ImageGrayButton->setChecked(true);
    if(container2d != NULL) container2d->setImageVisibilityMode(NORMAL);
    if(container3d != NULL)
    {
        container3d->setImageVisibilityMode(NORMAL);
        if(renderer3d != NULL) renderer3d->setRepaintVolume();
    }
    rendererRepaint();
    redrawHistogram();
}

void MainWindow::on_horizontalSliderMin_valueChanged(int value)
{
    if(container2d != NULL) container2d->setImageGrayWindowLow(value);
    if(container3d != NULL)
    {
        container3d->setImageGrayWindowLow(value);
        if(renderer3d != NULL) renderer3d->setRepaintVolume();
    }

    rendererRepaint();
    redrawHistogram();
}

void MainWindow::on_horizontalSliderMax_valueChanged(int value)
{
    if(container2d != NULL) container2d->setImageGrayWindowHigh(value);
    if(container3d != NULL)
    {
        container3d->setImageGrayWindowHigh(value);
        if(renderer3d != NULL) renderer3d->setRepaintVolume();
    }
    rendererRepaint();
    redrawHistogram();
}

void MainWindow::on_actionRoi_from_sliders_triggered()
{
    if(container2d != NULL)
    {
        RoiTools < RenderingContainer2D>::roiFromSliders(container2d); //??
        container2d->setRoisRedraw();
    }
    else if(container3d != NULL)
    {
        RoiTools < RenderingContainer3D>::roiFromSliders(container3d); //??
        container3d->setRoisRedraw();
    }
    rendererRepaint();
}
void MainWindow::onMorphology(int mode)
{
    double radius = dockEditOptions->getValue("Morphology:Radius").toDouble();
    if(radius < 1) radius = 1;
    int forwhat;
    QString forwhats = dockEditOptions->getValue("Morphology:Apply");
    if(forwhats == "all") forwhat = 2;
    else if(forwhats == "visible") forwhat = 1;
    else forwhat = 0;

    QApplication::setOverrideCursor(Qt::WaitCursor);
    if(container2d != NULL && renderer2d != NULL)
    {
        RenderingContainer2D::MzRoi* sel = RoiTools < RenderingContainer2D>::createBallElement(radius);
        RoiTools < RenderingContainer2D>::roiMorphology(container2d, sel, mode, forwhat);
        delete sel;
        container2d->setRoisRedraw();
    }
    else if(container3d != NULL && renderer3d != NULL)
    {
        RenderingContainer3D::MzRoi* sel = RoiTools < RenderingContainer3D>::createBallElement(radius);
        RoiTools < RenderingContainer3D>::roiMorphology(container3d, sel, mode, forwhat);
        delete sel;
        container3d->setRoisRedraw();
        renderer3d->setRepaintVolume();
    }
    rendererRepaint();
    QApplication::restoreOverrideCursor();
}
void MainWindow::on_actionOpen_triggered()
{
    onMorphology(1);
    onMorphology(-1);
    if(container != NULL)
    {
        container->setModified();
        recreateRoiControls();
    }
}

void MainWindow::on_actionClose_triggered()
{
    onMorphology(-1);
    onMorphology(1);
    if(container != NULL)
    {
        container->setModified();
        recreateRoiControls();
    }
}

void MainWindow::on_actionDilate_triggered()
{
    onMorphology(1);
    if(container != NULL)
    {
        container->setModified();
        recreateRoiControls();
    }
}

void MainWindow::on_actionErode_triggered()
{
    onMorphology(-1);
    if(container != NULL)
    {
        container->setModified();
        recreateRoiControls();
    }
}

void MainWindow::on_actionMedian_triggered()
{
    onMorphology(0);
    if(container != NULL)
    {
        container->setModified();
        recreateRoiControls();
    }
}


void MainWindow::on_actionSplit_roi_triggered()
{
    QApplication::setOverrideCursor(Qt::WaitCursor);
    if(container2d != NULL && renderer2d != NULL)
    {
        RoiTools < RenderingContainer2D>::splitRois(container2d);
        container2d->setRoisRedraw();
        if(container3d != NULL)
        {
            for(unsigned int i = container3d->getNumberOfRois(); i < container2d->getNumberOfRois(); i++)
            {
                RenderingContainer3D::MzRoi* ar = new RenderingContainer3D::MzRoi();
                ar->SetName(container2d->getRoi(i)->GetName());
                ar->SetColor(container2d->getRoi(i)->GetColor());
                ar->SetVisibility(container2d->getRoi(i)->GetVisibility());
                container3d->appendRoi(ar);
            }
        }
    }
    else if(container3d != NULL && renderer3d != NULL)
    {
        RoiTools < RenderingContainer3D>::splitRois(container3d);
        container3d->setRoisRedraw();
        renderer3d->setRepaintVolume();
    }
    rendererRepaint();
    QApplication::restoreOverrideCursor();
    if(container != NULL) container->setModified();
    recreateRoiControls();
}

void MainWindow::on_actionMerge_visible_triggered()
{
    QApplication::setOverrideCursor(Qt::WaitCursor);
    if(container2d != NULL && renderer2d != NULL)
    {
        RoiTools < RenderingContainer2D>::mergeRois(container2d);
        container2d->setRoisRedraw();
        if(container3d != NULL)
        {
            for(unsigned int i = container3d->getNumberOfRois(); i < container2d->getNumberOfRois(); i++)
            {
                RenderingContainer3D::MzRoi* ar = new RenderingContainer3D::MzRoi();
                ar->SetName(container2d->getRoi(i)->GetName());
                ar->SetColor(container2d->getRoi(i)->GetColor());
                ar->SetVisibility(container2d->getRoi(i)->GetVisibility());
                container3d->appendRoi(ar);
            }
        }
    }
    else if(container3d != NULL && renderer3d != NULL)
    {
        RoiTools < RenderingContainer3D>::mergeRois(container3d);
        container3d->setRoisRedraw();
        renderer3d->setRepaintVolume();
    }
    rendererRepaint();
    QApplication::restoreOverrideCursor();
    if(container != NULL) container->setModified();
    recreateRoiControls();
}


void MainWindow::on_actionRemove_small_triggered()
{
    int r;
    r = dockEditOptions->getValue("Remove:Size").toInt();
    if(r < 1) r = 1;

    QApplication::setOverrideCursor(Qt::WaitCursor);
    if(container2d != NULL && renderer2d != NULL)
    {
        std::vector<unsigned int> toremove = RoiTools < RenderingContainer2D>::removeSmall(container2d, r);
        container2d->setRoisRedraw();
        if(container3d != NULL)
        {
            for(unsigned int r = 0; r < toremove.size(); r++)
            {
                container3d->removeRoi(toremove[r]);
            }
        }
    }
    else if(container3d != NULL && renderer3d != NULL)
    {
        RoiTools < RenderingContainer3D>::removeSmall(container3d, r);
        container3d->setRoisRedraw();
        renderer3d->setRepaintVolume();
    }
    rendererRepaint();
    QApplication::restoreOverrideCursor();
    if(container != NULL) container->setModified();
    recreateRoiControls();
}

void MainWindow::zoomResetClicked()
{
    if(renderer2d != NULL) renderer2d->ResetView();
    if(renderer3d != NULL) renderer3d->ResetView();
    zoomSlider.setValue(0);
    rendererRepaint();
}

void MainWindow::setRoiListRequest(void)
{
    recreateRoiControls();
}
void MainWindow::setStatusTextRequest(QString* txt)
{
    ui->statusBar->showMessage(*txt);
}

void MainWindow::on_actionUndo_triggered()
{
    if(container != NULL)
    {
        container->undo();
        //setRedoUndoRequest();
        recreateRoiControls();
        rendererRepaint();
    }
}

void MainWindow::on_actionRedo_triggered()
{
    if(container != NULL)
    {
        container->redo();
        //setRedoUndoRequest();
        recreateRoiControls();
        rendererRepaint();
    }
}

void MainWindow::on_actionRender_2D_triggered(bool checked)
{
    if(checked)
    {
        if(renderer3d != NULL && container3d != NULL)
        {
            double matrix[16];
            unsigned int sliceno[3];
            bool sliceshow[3];
            renderer3d->getMatrix(matrix);
            sliceno[0] = ui->spinBox_X->value();
            sliceno[1] = ui->spinBox_Y->value();
            sliceno[2] = ui->spinBox_Z->value();
            sliceshow[0] = (Qt::Checked == ui->checkBox_X->checkState());
            sliceshow[1] = (Qt::Checked == ui->checkBox_Y->checkState());
            sliceshow[2] = (Qt::Checked == ui->checkBox_Z->checkState());
            if(container2d == NULL) container2d = new RenderingContainer2D;
            getSetSlice.from3Dto2D(matrix, sliceshow, sliceno, container3d, container2d);
            container2d->setActiveRoiIndex(container3d->getActiveRoiIndex());
            container = container2d;
            switchRender2D();
            container->setModified();
            recreateRoiControls();
        }
    }
    else ui->actionRender_2D->setChecked(true);
}

void MainWindow::on_actionRender_3D_triggered(bool checked)
{
    if(checked)
    {
        if(renderer2d != NULL && container2d != NULL && container3d != NULL)
        {
            bool modified = container2d->canUndo();
            getSetSlice.backTo3D();
            container = container3d;
            container3d->setActiveRoiIndex(container2d->getActiveRoiIndex());
            switchRender3D();

            if(renderer3d != NULL && container2d != NULL)
            {
                delete container2d;
                container2d = NULL;
            }
            if(modified)
            {
                container->setModified();
                recreateRoiControls();
            }
        }
    }
    else
    {
        ui->actionRender_3D->setChecked(true);
    }
}

void MainWindow::on_actionBackground_color_triggered()
{
//    if(renderer2d != NULL)
//    {
//        QColor color(renderer2d->GetBackground());
//        color = QColorDialog::getColor(color);
//        //ui->centralWidget->setStyleSheet("background-color:red");//////////////////////////////////////////////////
//        if(color.isValid()) renderer2d->SetBackground(color.rgb());
//        QPalette pal(ui->centralWidget->palette());
//        unsigned int bc = renderer2d->GetBackground();
//        //pal.setColor(QPalette::Background, QColor(bc));
//        pal.setColor(QPalette::Window, QColor(bc));
//        ui->centralWidget->setPalette(pal);
////        if(renderArea != NULL)
////        {
////            QPalette pal(renderArea->palette());
////            unsigned int bc = renderer2d->GetBackground();
////            //pal.setColor(QPalette::Background, QColor(bc));
////            pal.setColor(QPalette::Window, QColor(bc));
////            renderArea->setPalette(pal);
////        }
//        renderer2d->Repaint();
//    }
//    else
    if(renderer3d != NULL)
    {
        QColor color(renderer3d->GetBackground());
        color = QColorDialog::getColor(color);
        if(color.isValid()) renderer3d->SetBackground(color.rgb());
        renderer3d->Repaint();
    }
}

void MainWindow::on_toolButton_Color_clicked()
{
    on_actionBackground_color_triggered();
}

void MainWindow::on_spinBox_X_valueChanged(int arg1)
{
    if(renderer3d != NULL)
    {
        renderer3d->setCrossNumber(1, arg1);
        renderer3d->Repaint();
    }
}

void MainWindow::on_spinBox_Y_valueChanged(int arg1)
{
    if(renderer3d != NULL)
    {
        renderer3d->setCrossNumber(2, arg1);
        renderer3d->Repaint();
    }
}

void MainWindow::on_spinBox_Z_valueChanged(int arg1)
{
    if(renderer3d != NULL)
    {
        renderer3d->setCrossNumber(3, arg1);
        renderer3d->Repaint();
    }
}

void MainWindow::on_checkBox_X_clicked(bool checked)
{
    if(renderer3d != NULL)
    {
        renderer3d->setCrossShow(1, checked);
        renderer3d->Repaint();
    }
}

void MainWindow::on_checkBox_Y_clicked(bool checked)
{
    if(renderer3d != NULL)
    {
        renderer3d->setCrossShow(2, checked);
        renderer3d->Repaint();
    }
}

void MainWindow::on_checkBox_Z_clicked(bool checked)
{
    if(renderer3d != NULL)
    {
        renderer3d->setCrossShow(3, checked);
        renderer3d->Repaint();
    }
}

void MainWindow::on_toolButton_Alfa_clicked(bool checked)
{
    if(renderer3d != NULL)
    {
        renderer3d->setVolumeView(checked ? 1 : 0);
        renderer3d->Repaint();
    }
    ui->toolButton_Afla->setChecked(false);
}

void MainWindow::on_toolButton_Afla_clicked(bool checked)
{
    if(renderer3d != NULL)
    {
        renderer3d->setVolumeView(checked ? -1 : 0);
        renderer3d->Repaint();
    }
    ui->toolButton_Alfa->setChecked(false);
}

void MainWindow::initializeFeatures(void)
{
    QString result;
    bool failed = false;
    if(queryGenerator(&result, "roi"))
    {
        std::stringstream ss(result.toStdString());
        roiFeaturesTemplate.parse(&ss, '.');
    }
    else failed = true;

    result.clear();
    if(queryGenerator(&result, "local"))
    {
        std::stringstream ss(result.toStdString());
        localFeaturesTemplate.parse(&ss, '.');
    }
    else failed = true;
    if(failed)
    {
        result.clear();
        result = "Failed to execute generator:\n";
        result += getExecutableWithPath(query_name, true);
        result += QString("\n");
        result += QString("Ensure that your system or antivirus program is not blocking the MzGenerator and MzGengui applications.");
        QMessageBox msgBox;
        msgBox.setText(result);
        msgBox.setWindowTitle(QString("Generator"));
        msgBox.setIcon(QMessageBox::Information);
        msgBox.exec();
    }
    else
    {
        int i, imax;
        QString fileName = getExecutableWithPath("/defroi.txt", false);
        QString fileNamel = getExecutableWithPath("/defloc.txt", false);

        std::vector<std::string> featureList = DialogFeatureOptions::loadFeatureList(fileName.toStdString().c_str());
        imax = featureList.size();
        for(i = 0; i < imax; i++)
        {
            if(roiFeaturesTemplate.doesItMatch(featureList[i].c_str()) == 0)
            {
                featureListRoi.push_back(featureList[i].c_str());
            }
        }
        featureList = DialogFeatureOptions::loadFeatureList(fileNamel.toStdString().c_str());
        imax = featureList.size();
        for(i = 0; i < imax; i++)
        {
            if(localFeaturesTemplate.doesItMatch(featureList[i].c_str()) == 0)
            {
                featureListLocal.push_back(featureList[i].c_str());
            }
        }
    }
}

bool MainWindow::queryGenerator(QString* result, const char *mode)
{
//    std::stringstream command;
//    command << getExecutableWithPath(query_name).toStdString();
//    command << " -q -m " << mode << "    ";
////    const char* cc = command.str().c_str();
////    printf(cc);
////    fflush(stdout);
//    QProcess process(this);
//    process.start(command.str().c_str());

    QProcess process(this);
    QString command = getExecutableWithPath(query_name, true);
//    command.append(" -q -m ");
//    command.append(mode);
//    process.start(command); //deprecated

    QStringList arguments;
    arguments.append(QString("-q"));
    arguments.append(QString("-m"));
    arguments.append(QString(mode));
    process.start(command, arguments);

    if (!process.waitForFinished(10000))
    {
        fprintf(stderr, "%s\n", process.errorString().toStdString().c_str());
        process.kill();
        process.waitForFinished(5000);
        return false;
    }
    else
    {

        //OSX
//        if(process.readAllStandardError().size()>0)
//        {
//            fprintf(stderr, "%s\n", process.errorString().toStdString().c_str());
//            process.kill();
//            process.waitForFinished(1000);
//            return false;
//        }
//        else
        {
            *result = process.readAllStandardOutput();
            return true;
        }
    }
}

void MainWindow::on_actionRoi_based_computation_triggered()
{
    std::string tempFtsFile = randomNames.getTempName("/f", ".txt", false);
    if(!DialogFeatureOptions::saveFeatureList(tempFtsFile.c_str(), &featureListRoi)) return;
    std::string tempRoiFile = randomNames.getTempName("/r", ".tif", false);
    if(!saveRoi(tempRoiFile.c_str()))
    {
        randomNames.deleteFile(tempFtsFile);
        return;
    }
    std::string tempImgFile = randomNames.getTempName("/i", ".tif", false);
    if(!saveImage(tempImgFile.c_str()))
    {
        randomNames.deleteFile(tempFtsFile);
        randomNames.deleteFile(tempRoiFile);
        return;
    }
    std::string tempCsvFile = randomNames.getTempName("/c", ".csv", false);
    QString command = getExecutableWithPath(generator_name, true);

    QStringList arguments;
    arguments.append(QString("-m"));
    arguments.append(QString("roi"));
    arguments.append(QString("-i"));
    arguments.append(QString::fromStdString(tempImgFile));
    arguments.append(QString("-f"));
    arguments.append(QString::fromStdString(tempFtsFile));
    arguments.append(QString("-r"));
    arguments.append(QString::fromStdString(tempRoiFile));
    arguments.append(QString("-o"));
    arguments.append(QString::fromStdString(tempCsvFile));
    if(ui->actionSingle_thread->isChecked())
    {
        arguments.append(QString("-k"));
    }
    arguments.append(QString("-x"));
    arguments.append(QString("--delete-input-files"));
    arguments.append(QString("-e"));
    arguments.append(QString("*"));

    ui->statusBar->showMessage(QString(command).append(" ").append(arguments.join(" ")));
    //printf("%s\n", QString(command).append(" ").append(arguments.join(" ")).toStdString().c_str());
    fflush(stdout);

    QProcess process(this);
    process.startDetached(command, arguments);

    /*
    command += " -m roi -i ";
    command += tempImgFile.c_str();
    command += " -r ";
    command += tempRoiFile.c_str();
    command += " -f ";
    command += tempFtsFile.c_str();
    command += " -o ";
    command += tempCsvFile.c_str();
    if(ui->actionSingle_thread->isChecked())
        command += " -k -x --delete-input-files -e *";
    else
        command += " -x --delete-input-files -e *";

//    command += " -x --delete-input-files -e '";
//    command += getExecutableWithPath(report_viewer_name);
//    command += " ";
//    command += tempCsvFile.c_str();
//    command += "'";

    ui->statusBar->showMessage(command);
    printf("%s\n", command.toStdString().c_str());
    fflush(stdout);

    QProcess process(this);
    process.startDetached(command);
    */
}

void MainWindow::on_actionMap_computation_triggered()
{
    std::string tempFtsFile = randomNames.getTempName("/f", ".txt", false);
    if(!DialogFeatureOptions::saveFeatureList(tempFtsFile.c_str(), &featureListLocal)) return;
    std::string tempImgFile = randomNames.getTempName("/r", ".tif", false);
    if(!saveImage(tempImgFile.c_str()))
    {
        randomNames.deleteFile(tempFtsFile);
        return;
    }
    std::string tempMapFile = randomNames.getTempName("/m", ".tif", false);
    QString command = getExecutableWithPath(generator_name, true);

    /*
    command += " -m local -i ";
    command += tempImgFile.c_str();
    command += " -f ";
    command += tempFtsFile.c_str();
    command += " -o ";
    command += tempMapFile.c_str();
    if(ui->actionSingle_thread->isChecked())
        command += " -k -x --delete-input-files -e *";
    else
        command += " -x --delete-input-files -e *";

//    command += " -x --delete-input-files -e \"";
//    command += getExecutableWithPath(map_viewer_name);
//    command += " ";
//    command += tempMapFile.c_str();
//    command += "\"";

    ui->statusBar->showMessage(command);
    printf("%s\n", command.toStdString().c_str());
    fflush(stdout);

    QProcess process(this);
    process.startDetached(command);
    */

    QStringList arguments;
    arguments.append(QString("-m"));
    arguments.append(QString("local"));
    arguments.append(QString("-i"));
    arguments.append(QString::fromStdString(tempImgFile));
    arguments.append(QString("-f"));
    arguments.append(QString::fromStdString(tempFtsFile));
    arguments.append(QString("-o"));
    arguments.append(QString::fromStdString(tempMapFile));
    if(ui->actionSingle_thread->isChecked())
    {
        arguments.append(QString("-k"));
    }
    arguments.append(QString("-x"));
    arguments.append(QString("--delete-input-files"));
    arguments.append(QString("-e"));
    arguments.append(QString("*"));

    ui->statusBar->showMessage(QString(command).append(" ").append(arguments.join(" ")));
    //printf("%s\n", QString(command).append(" ").append(arguments.join(" ")).toStdString().c_str());
    fflush(stdout);

    QProcess process(this);
    process.startDetached(command, arguments);
}

void MainWindow::on_actionPoint_driven_computation_triggered()
{
    std::string tempFtsFile = randomNames.getTempName("/f", ".txt", false);
    if(!DialogFeatureOptions::saveFeatureList(tempFtsFile.c_str(), &featureListLocal)) return;
    std::string tempRoiFile = randomNames.getTempName("/i", ".tif", false);
    if(!saveRoi(tempRoiFile.c_str()))
    {
        randomNames.deleteFile(tempFtsFile);
        return;
    }
    std::string tempImgFile = randomNames.getTempName("/r", ".tif", false);
    if(!saveImage(tempImgFile.c_str()))
    {
        randomNames.deleteFile(tempFtsFile);
        randomNames.deleteFile(tempRoiFile);
        return;
    }
    std::string tempCsvFile = randomNames.getTempName("/c", ".csv", false);
    QString command = getExecutableWithPath(generator_name, true);


/*
    command += " -m local -i ";
    command += tempImgFile.c_str();
    command += " -r ";
    command += tempRoiFile.c_str();
    command += " -f ";
    command += tempFtsFile.c_str();
    command += " -o ";
    command += tempCsvFile.c_str();
    if(ui->actionSingle_thread->isChecked())
        command += " -k -x --delete-input-files -e *";
    else
        command += " -x --delete-input-files -e *";
//    command += " -x --delete-input-files -e \"";
//    command += getExecutableWithPath(report_viewer_name);
//    command += " ";
//    command += tempCsvFile.c_str();
//    command += "\"";

    ui->statusBar->showMessage(command);
    printf("%s\n", command.toStdString().c_str());
    fflush(stdout);

    QProcess process(this);
    process.startDetached(command);
*/


    QStringList arguments;
    arguments.append(QString("-m"));
    arguments.append(QString("local"));
    arguments.append(QString("-i"));
    arguments.append(QString::fromStdString(tempImgFile));
    arguments.append(QString("-r"));
    arguments.append(QString::fromStdString(tempRoiFile));
    arguments.append(QString("-f"));
    arguments.append(QString::fromStdString(tempFtsFile));
    arguments.append(QString("-o"));
    arguments.append(QString::fromStdString(tempCsvFile));
    if(ui->actionSingle_thread->isChecked())
    {
        arguments.append(QString("-k"));
    }
    arguments.append(QString("-x"));
    arguments.append(QString("--delete-input-files"));
    arguments.append(QString("-e"));
    arguments.append(QString("*"));

    ui->statusBar->showMessage(QString(command).append(" ").append(arguments.join(" ")));
    //printf("%s\n", QString(command).append(" ").append(arguments.join(" ")).toStdString().c_str());
    fflush(stdout);

    QProcess process(this);
    process.startDetached(command, arguments);



//    QFile f;
//    f.setFileName(tempFtsFile);
//    if(f.open(QFile::WriteOnly))
//    {
//        QStringList lista;
//        QTextStream textstream(&f);
//        getUniqueNames(&lista, generate_local);
//        for (int i = 0; i < lista.size(); ++i) textstream << lista.at(i) << '\n';
//        f.close();
//    }
//    else return;

//    if(!imageutils()->saveRoi(tempRoiFile.toStdString().c_str())) return;
//    if(!imageutils()->saveImage(tempImgFile.toStdString().c_str())) return;

//    QString cname = QDir::tempPath();
//    char randomname[32];
//    sprintf(randomname, "/L%.6X.csv", 0xffffff&rand());
//    cname += randomname;

//    QString command = getExecutableWithPath(compute_gen_name);
//    command += " -m local -i ";
//    command += tempImgFile;
//    command += " -r ";
//    command += tempRoiFile;
//    command += " -f ";
//    command += tempFtsFile;
//    command += " -o ";
//    command += cname;
//    command += " -x -e \"";
//    command += getExecutableWithPath(report_viewer_name);
//    command += " ";
//    command += cname;
//    command += "\"";

//    ui->statusBar->showMessage(command);
////    printf("%s\n", command.toStdString().c_str());
////    fflush(stdout);
//    QProcess process(this);
//    process.startDetached(command);
}


/*

bool MainWindow::queryGenerator(QString* result, QString featurename, int mode)
{
    QString command = getExecutableWithPath(query_gen_name);
    command += " -q ";
    command += featurename;

    switch(mode)
    {
    case 1: command += " -m local"; break;
    default: command += " -m roi";
    }
    QProcess process(this);
    process.start(command);

//    printf("%s\n", command.toStdString().c_str());

    if (!process.waitForFinished(1000))
    {
        *result = "Error starting: ";
        *result += process.errorString();
        process.kill();
        process.waitForFinished(1000);

//        printf("FalseA\n");
//        printf("%s\n", result->toStdString().c_str());
//        fflush(stdout);

        return false;
    }
    else
    {
        if(process.readAllStandardError().size()>0)
        {
            *result = "Error executing: ";
            *result += process.readAllStandardError();
            *result += process.errorString();

//            printf("FalseB\n");
//            printf("%s\n", result->toStdString().c_str());
//            fflush(stdout);

            return false;
        }
        else
        {
            *result = process.readAllStandardOutput();


//            printf("True\n");
//            printf("%s\n", result->toStdString().c_str());
//            fflush(stdout);

            return true;
        }
    }
}

*/

void MainWindow::on_actionImage_info_triggered()
{
//    QMessageBox::about(this, "Image info", imageInfo.c_str());

    QMessageBox msgBox;
    msgBox.setText(QString(imageInfo.c_str()));
    msgBox.setWindowTitle(QString("Image info"));
    msgBox.setIcon(QMessageBox::Information);
    msgBox.exec();
}


//void MainWindow::on_actionOptionsTools_triggered()
//{
//    dockEditOptions->show();
//}

//void MainWindow::on_actionFeatureOptions_triggered()
//{
//    DialogFeatureOptions dialog(&roiFeaturesTemplate, &localFeaturesTemplate, this);
//    dialog.setFeatureListRoi(&featureListRoi);
//    dialog.setFeatureListLocal(&featureListLocal);
//    if(dialog.exec() == QDialog::Accepted)
//    {
//        featureListRoi.clear();
//        featureListLocal.clear();
//        dialog.getFeatureListRoi(&featureListRoi);
//        dialog.getFeatureListLocal(&featureListLocal);
//    }
//}

QString MainWindow::getExecutableWithPath(const char* name, bool use_quotation)
{
    QDir execDir(qApp->applicationDirPath());
    QString pathq = execDir.absolutePath();

    if((pathq.contains(' ') || pathq.contains('\t')) && use_quotation)
        return QString("\"") + QString::fromStdString(pathq.toStdString()) + QString(name) + QString("\"");
    else
        return QString::fromStdString(pathq.toStdString()) + QString(name);
    //return execDir.absolutePath() + QString(name);
}

void MainWindow::on_actionMaZda_new_instance_triggered()
{
    MainWindow* w = new MainWindow;
    w->show();
}

void MainWindow::on_actionMap_viewer_triggered()
{
    QProcess P(this);
    QString command = getExecutableWithPath(map_viewer_name, true);
//    P.startDetached(command);
    P.startDetached(command, QStringList());
}

void MainWindow::on_actionReport_editor_triggered()
{
    QProcess P(this);
    QString command = getExecutableWithPath(report_viewer_name, true);
//    P.startDetached(command);
    P.startDetached(command, QStringList());
}

void MainWindow::on_actionColor_overlay_triggered()
{
    QString fileName = QFileDialog::getSaveFileName(this,
                                                    tr("Export ROIs"),
                                                    NULL,//domyslna nazwa pliku
                                                    "Image files (*.tif *.tiff *.png *.nii *.dcm);;All Files (*)");
    if (!fileName.isEmpty())
    {
        QCursor oc = cursor();
        setCursor(Qt::WaitCursor);
        if(container2d != NULL)
        {
            unsigned int size[2];
            container2d->getSize(size);
            MazdaRoiIO<RenderingContainer2D::MzRoi>::ExportAsColor(fileName.toStdString(), container2d->getRoitable(), size);
        }
        else if(container3d != NULL)
        {
            unsigned int size[3];
            container3d->getSize(size);
            MazdaRoiIO<RenderingContainer3D::MzRoi>::ExportAsColor(fileName.toStdString(), container3d->getRoitable(), size);
        }
        setCursor(oc);
    }
}

void MainWindow::on_actionGray_levels_triggered()
{
    QString fileName = QFileDialog::getSaveFileName(this,
                                                    tr("Export ROIs"),
                                                    NULL,//domyslna nazwa pliku
                                                    "Image files (*.tif *.tiff *.png *.nii *.dcm);;All Files (*)");
    if (!fileName.isEmpty())
    {
        QCursor oc = cursor();
        setCursor(Qt::WaitCursor);
        if(container2d != NULL)
        {
            unsigned int size[2];
            container2d->getSize(size);
            MazdaRoiIO<RenderingContainer2D::MzRoi>::ExportAsGreyLevels(fileName.toStdString(), container2d->getRoitable(), size);
        }
        else if(container3d != NULL)
        {
            unsigned int size[3];
            container3d->getSize(size);
            MazdaRoiIO<RenderingContainer3D::MzRoi>::ExportAsGreyLevels(fileName.toStdString(), container3d->getRoitable(), size);
        }
        setCursor(oc);
    }
}

void MainWindow::on_actionText_triggered()
{
    QString fileName = QFileDialog::getSaveFileName(this,
                                                    tr("Export ROIs"),
                                                    NULL,//domyslna nazwa pliku
                                                    "Text file (*.txt);;All Files (*)");
    if (!fileName.isEmpty())
    {
        QCursor oc = cursor();
        setCursor(Qt::WaitCursor);
        if(container2d != NULL)
        {
            MazdaRoiIO<RenderingContainer2D::MzRoi>::ExportAsText(fileName.toStdString(), container2d->getRoitable());
        }
        else if(container3d != NULL)
        {
            MazdaRoiIO<RenderingContainer3D::MzRoi>::ExportAsText(fileName.toStdString(), container3d->getRoitable());
        }
        setCursor(oc);
    }
}

void MainWindow::on_actionVoxel_size_triggered()
{
    if(container3d == NULL) return;
    double spacing[4];
    container3d->getSpacing(spacing);
    DialogVoxelSize dialog(spacing, this);
    int r = dialog.exec();
    if(QDialog::Accepted == r)
    {
        container3d->setSpacing(spacing);
        if(renderer3d != NULL)
        {
            renderer3d->SetContainer(container3d);
            renderer3d->Repaint();
        }
    }
}

void MainWindow::on_actionExit_triggered()
{
    close();
}

void MainWindow::setZoomRequest(double zoom)
{
    zoomSlider.blockSignals(true);
    zoomSlider.setValue(round(log(zoom)*46.166241));
    zoomSlider.blockSignals(false);
}

void MainWindow::zoomSliderValueChanged(int value)
{
    double zoom = exp((double) value*0.021660849);
    if(renderer2d != NULL)
        renderer2d->SetZoom(zoom);
    if(renderer3d != NULL)
        renderer3d->SetZoom(zoom);
    rendererRepaint();
}

void MainWindow::on_actionSingle_thread_triggered()
{
}

void MainWindow::on_actionRoi_features_wizard_triggered()
{
    int r = dialog_roi->exec();
}

void MainWindow::on_actionLocal_features_wizard_triggered()
{
    int r = dialog_local->exec();
}

void MainWindow::on_actionLocal_features_list_triggered()
{
    DialogFeatureOptions dialog(&localFeaturesTemplate, this);
    dialog.setWindowTitle(QString("Locally extracted features"));
    dialog.setWindowIcon(QIcon(":/icons/icons/configure.png"));
    dialog.setTreeRootIcon(QIcon(":/icons/icons/local-mode.png"));
    dialog.setFeatureList(&featureListLocal);
    if(dialog.exec() == QDialog::Accepted)
    {
        featureListLocal.clear();
        dialog.getFeatureList(&featureListLocal);
    }
}

void MainWindow::on_actionRoi_features_list_triggered()
{
    DialogFeatureOptions dialog(&roiFeaturesTemplate, this);
    dialog.setWindowTitle(QString("Features extracted in regions of interest"));
    dialog.setWindowIcon(QIcon(":/icons/icons/configure.png"));
    dialog.setTreeRootIcon(QIcon(":/icons/icons/roi-mode.png"));
    dialog.setFeatureList(&featureListRoi);
    if(dialog.exec() == QDialog::Accepted)
    {
        featureListRoi.clear();
        dialog.getFeatureList(&featureListRoi);
    }
}

