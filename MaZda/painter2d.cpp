/*
 * QMAZDA project: Image Analysis and Pattern Recognition
 *
 * Copyright 2013-2016 Piotr M. Szczypiński <piotr.szczypinski@p.lodz.pl>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "painter2d.h"
#include <QApplication>

Painter2D::Painter2D(RenderingContainer2D *container, DockEditOptions *editoptions, QWidget *parent)
    : Renderer2D(container, parent)
{
    eraser = false;
    drawingtool = DT_NONE;
    painttoolobject = NULL;
    dockEditOptions = editoptions;
    qApp->installEventFilter(this);
}
Painter2D::~Painter2D()
{
    deletePaintTools();
}
bool Painter2D::eventFilter(QObject* /*obj*/, QEvent* e)
{
    static int lastkeyp = -1;
    static int lastkeyr = -1;
    static bool accepted = false;

    if(painttoolobject == NULL)
    {
        accepted = false;
        lastkeyr = -1;
        lastkeyp = -1;
        return false;
    }
    if (e->type()==QEvent::KeyPress)
    {
        lastkeyr = -1;
        QKeyEvent* ev = static_cast<QKeyEvent*>(e);
        if(!(ev->key() == lastkeyp))
        {
            lastkeyp = ev->key();
            ev->setAccepted(false);
            repaintAction(painttoolobject->keyPressEvent(ev));
            if(e->isAccepted())
            {
                accepted = true;
                return true;
            }
        }
        else if(accepted) return true;
    }
    else if(e->type()==QEvent::KeyRelease)
    {
        lastkeyp = -1;
        QKeyEvent* ev = static_cast<QKeyEvent*>(e);
        if(!(ev->key() == lastkeyr))
        {
            lastkeyr = ev->key();
            ev->setAccepted(false);
            repaintAction(painttoolobject->keyReleaseEvent(ev));
            if(e->isAccepted())
            {
                accepted = true;
                return true;
            }
        }
        else if(accepted) return true;
    }
    accepted = false;
    return false;
    //return QObject::eventFilter(obj, e);
}
void Painter2D::paintEvent(QPaintEvent *ev)
{
    Renderer2D::paintEvent(ev);
}
void Painter2D::mouseMoveEvent(QMouseEvent *ev)
{
    if(painttoolobject != NULL)
        repaintAction(painttoolobject->mouseMoveEvent(ev));
//    Renderer2D::mouseMoveEvent(ev);
}
void Painter2D::mousePressEvent(QMouseEvent *ev)
{
    createPainter();
    if(painttoolobject != NULL)
    {
        repaintAction(painttoolobject->mousePressEvent(ev));
        if(cursor().shape() == Qt::OpenHandCursor)
        {
            if(ev->button() == Qt::LeftButton)
                setCursor(Qt::DragMoveCursor);
        }
    }
    Renderer2D::mousePressEvent(ev);
}
void Painter2D::wheelEvent(QWheelEvent *ev)
{
    if(painttoolobject != NULL)
    {
        repaintAction(painttoolobject->wheelEvent(ev));
    }
    Renderer2D::wheelEvent(ev);
}
void Painter2D::mouseReleaseEvent(QMouseEvent *ev)
{
    if(painttoolobject != NULL)
    {
        repaintAction(painttoolobject->mouseReleaseEvent(ev));
        if(cursor().shape() == Qt::DragMoveCursor)
        {
            if(ev->button() == Qt::LeftButton)
                setCursor(Qt::OpenHandCursor);
        }
    }
    //Renderer2D::mouseReleaseEvent(ev);
}
void Painter2D::mouseDoubleClickEvent(QMouseEvent *ev)
{
    if(painttoolobject != NULL)
        repaintAction(painttoolobject->mouseDoubleClickEvent(ev));
//    Renderer2D::mouseDoubleClickEvent(ev);
}

void Painter2D::setDrawingTool(DrawingTool idrawingtool, bool ieraser)
{
    eraser = ieraser;
    if(idrawingtool != DT_IGNORE) drawingtool = idrawingtool;

    switch(drawingtool)
    {
    case DT_PEN: setCursor(Qt::CrossCursor); break;
    case DT_RECT: setCursor(Qt::CrossCursor); break;
    case DT_ELLI: setCursor(Qt::CrossCursor); break;
    case DT_SNAKE: setCursor(Qt::CrossCursor); break;
    case DT_FILL: setCursor(Qt::CrossCursor); break;
    case DT_POLY: setCursor(Qt::CrossCursor); break;
    case DT_MOVE: setCursor(Qt::OpenHandCursor); break; //Qt::DragMoveCursor
    default: unsetCursor(); break;
    }
}

void Painter2D::createPainter(void)
{
    RenderingContainer2D::MzRoi* roi = container->getActiveRoi();
    if(painttoolobject == NULL && roi != NULL)
    {
        int r;
        switch(drawingtool)
        {
        case DT_PEN:
            r = dockEditOptions->getValue("Pen:Width").toInt();
            if(r < 1) r = 1;
            if(r > 100) r = 100;
            painttoolobject = new PaintPencil((roi->GetColor() | 0xff000000),
                                                         r,
                                                         state.zoom_factor,
                                                         &screen_image,
                                                         &paintrect);
            break;
        case DT_RECT:
            painttoolobject = new PaintRect((roi->GetColor() | 0xff000000),
                                                         state.zoom_factor,
                                                         &screen_image,
                                                         &paintrect);
            break;
        case DT_ELLI:
            painttoolobject = new PaintElli((roi->GetColor() | 0xff000000),
                                                         state.zoom_factor,
                                                         &screen_image,
                                                         &paintrect);
            break;
        case DT_SNAKE:
        {
            int r[4];
            r[0] = dockEditOptions->getValue("Snake:Smoothness").toInt();
            QString s = dockEditOptions->getValue("Snake:Region");
            if(s == "dark") r[1] = -1;
            else if(s == "bright") r[1] = 1;
            else r[1] = 0;
            s = dockEditOptions->getValue("Snake:Level");
            if(s == "detect") r[3] = 1;
            else r[3] = 0;
            r[2] = dockEditOptions->getValue("Snake:Density").toInt();

            painttoolobject = new PaintActiveContour((roi->GetColor() | 0xff000000),
                                                     r,
                                                     state.zoom_factor,
                                                     &screen_image,
                                                     container->getGrayCanvas(),
                                                     &paintrect);
        }
        break;
        case DT_FILL:
            painttoolobject = new PaintFloodFill((roi->GetColor() | 0xff000000),
                                                         state.zoom_factor,
                                                         &screen_image,
                                                         &paintrect);
            break;
        case DT_POLY:
            painttoolobject = new PaintPoly((roi->GetColor() | 0xff000000),
                                                         state.zoom_factor,
                                                         &screen_image,
                                                         &paintrect);
            break;
        case DT_MOVE:
            painttoolobject = new PaintMove(container,
                                             state.zoom_factor,
                                             &screen_image,
                                             &paintrect);
            break;
        default: ;
        }
    }
}

void Painter2D::deletePaintTools(void)
{
    if(painttoolobject == NULL) return;
    delete painttoolobject;
    painttoolobject = NULL;
}

void Painter2D::QImage2MzRoi(QImage* image, RenderingContainer2D::MzRoi* roi, QRect paintrect, bool erase)
{
    int begin[RenderingContainer2D::MzRoi::Dimensions];
    int end[RenderingContainer2D::MzRoi::Dimensions];
    roi->GetBegin(begin);
    roi->GetEnd(end);
    if(begin[0] < paintrect.left()) begin[0] = paintrect.left();
    if(begin[1] < paintrect.top()) begin[1] = paintrect.top();
    if(end[0] > paintrect.right()) end[0] = paintrect.right();
    if(end[1] > paintrect.bottom()) end[1] = paintrect.bottom();
    if(begin[0] > end[0] || begin[1] > end[1]) return;
    QRgb color = roi->GetColor() & 0xffffff;
    MazdaRoiRegionIterator <RenderingContainer2D::MzRoi> iterator(roi, begin, end);
    if(erase)
    {
        for(int y = begin[1]; y <= end[1]; y++)
        {
            for(int x = begin[0]; x <= end[0]; x++)
            {
                if((image->pixel(x, y) & 0xffffff) == color) iterator.ClrPixel();
                ++iterator;
            }
        }
    }
    else
    {
        for(int y = begin[1]; y <= end[1]; y++)
        {
            for(int x = begin[0]; x <= end[0]; x++)
            {
                if((image->pixel(x, y) & 0xffffff) == color) iterator.SetPixel();
                ++iterator;
            }
        }
    }
}


void Painter2D::repaintAction(AfterPaintAction action)
{
    if(painttoolobject != NULL)
    {
        QString* info = painttoolobject->getStatus();
        if(info != NULL)
        {
            if(! info->isEmpty())
            {
                emit setStatusTextRequest(info);
                info->clear();
            }
        }
    }


    if(paintrect.right() < paintrect.left() || paintrect.bottom() < paintrect.top())
    {
        deletePaintTools();
        return;
    }
    if(paintrect.left() < 0) paintrect.setLeft(0);
    if(paintrect.top() < 0) paintrect.setTop(0);
    if(paintrect.right() >= screen_image.width()) paintrect.setRight(screen_image.width()-1);
    if(paintrect.bottom() >= screen_image.height()) paintrect.setBottom(screen_image.height()-1);

    RenderingContainer2D::MzRoi* roi = container->getActiveRoi();
    switch(action)
    {
    case REDRAW:
        update();
        break;

    case FINALIZE:
        if(roi != NULL)
        {
            if(eraser)
            {
                if(! roi->IsEmpty())
                {
                    QImage2MzRoi(&screen_image, roi, paintrect, true);
                    RenderingContainer2D::MzRoi* nroi = MazdaRoiResizer<RenderingContainer2D::MzRoi>::Crop(roi);
                    if(nroi == NULL)
                    {
                        delete nroi;
                        roi->Erase();
                    }
                    else if(nroi->IsEmpty())
                    {
                        delete nroi;
                        roi->Erase();
                    }
                    else
                    {
                        container->setActiveRoi(nroi);
                    }
                }
            }
            else
            {
                if(roi->IsEmpty())
                {
                    int begin[RenderingContainer2D::MzRoi::Dimensions];
                    int end[RenderingContainer2D::MzRoi::Dimensions];
                    begin[0] = paintrect.left();
                    begin[1] = paintrect.top();
                    end[0] = paintrect.right();
                    end[1] = paintrect.bottom();
                    if(begin[0] > end[0] || begin[1] > end[1]) break;
                    roi->Reallocate(begin, end);
                    QImage2MzRoi(&screen_image, roi, paintrect, false);
                }
                else
                {
                    int begin[RenderingContainer2D::MzRoi::Dimensions];
                    int end[RenderingContainer2D::MzRoi::Dimensions];
                    roi->GetBegin(begin);
                    roi->GetEnd(end);
                    if(begin[0] > paintrect.left()) begin[0] = paintrect.left();
                    if(begin[1] > paintrect.top()) begin[1] = paintrect.top();
                    if(end[0] < paintrect.right()) end[0] = paintrect.right();
                    if(end[1] < paintrect.bottom()) end[1] = paintrect.bottom();
                    if(begin[0] > end[0] || begin[1] > end[1]) break;

                    RenderingContainer2D::MzRoi* nroi = MazdaRoiResizer<RenderingContainer2D::MzRoi>::Resize(roi, begin, end);
                    if(nroi != NULL)
                    {
                        QImage2MzRoi(&screen_image, nroi, paintrect, false);
                        container->setActiveRoi(nroi);
                    }
                }
            }
        }
        container->setRoisRedraw();
        Repaint();
        deletePaintTools();
        container->setModified();
        emit setRoiListRequest();
        break;

    case CANCEL:
        container->setRoisRedraw();
        Repaint();
        deletePaintTools();
        break;

    case NEWROI:
        container->setRoisRedraw();
        Repaint();
        deletePaintTools();
        emit setRoiListRequest();
        break;

    case REFRESH:
//        container->setRoisRedraw();
        Repaint();
//??        imageutils->saveUndo();

        deletePaintTools();
        break;
    default: break;
    }
}
