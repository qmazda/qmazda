#include "trainersocket.h"
#include <QCoreApplication>
#include <QLibrary>
#include <QDir>
#include <QStringList>
//#include <QApplication>
#include <algorithm>
#include <string>
#include <cctype>
#include "../MzShared/filenameremap.h"

#ifdef _WIN32
#include <direct.h>
// MSDN recommends against using getcwd & chdir names
#define dt_getcwd _getcwd
#define dt_chdir _chdir
#else
#include "unistd.h"
#define dt_getcwd getcwd
#define dt_chdir chdir
#endif

extern bool single_thread;
extern bool verbose;

int TrainerSocket::loadFromFile(const char* fileName)
{
    std::string fileNameS = fileName;
    return datatable.loadFromFile(fileNameS);
}
bool TrainerSocket::getRequiredData(DataForSelection* outputdata)
{
    if(datatable.column_count <= 0 || datatable.row_count <= 0 || datatable.row_names == NULL ||
       datatable.column_names == NULL || datatable.feature_values == NULL) return false;

    int c, r;
    QStringList classlist;
    QStringList featurelist;

    int* vectorsinclass = new int[datatable.column_count];
    for(c = 0; c < datatable.column_count; c++) vectorsinclass[c] = 0;

    int* vectorclassindex = new int[datatable.column_count];
    for(c = 0; c < datatable.column_count; c++) vectorclassindex[c] = -1;

    int vectornumber = 0;
    for(c = 0; c < datatable.column_count; c++)
    {
        int index = classlist.indexOf(QString::fromStdString(datatable.column_names[c]));
        if(index < 0)
        {
            vectorsinclass[classlist.size()]++;
            vectorclassindex[c] = classlist.size();
            //classlist << datatable.column_names[c];
            classlist.append(QString::fromStdString(datatable.column_names[c]));
        }
        else
        {
            vectorsinclass[index]++;
            vectorclassindex[c] = index;
        }
        vectornumber++;
    }
    int classnumber = classlist.size();
    int featurenumber = 0;
    for(r = 0; r < datatable.row_count; r++)
    {
        featurelist.append(QString::fromStdString(datatable.row_names[r]));
        featurenumber++;
    }

    outputdata->Reset();
    outputdata->classnumber = classnumber;
    outputdata->featurenumber = featurenumber;
    outputdata->vectornumber = vectornumber;

    outputdata->classendvectorindex = new int[classnumber];
    r = 0;
    for(c = 0; c < classnumber; c++)
    {
        r += vectorsinclass[c];
        vectorsinclass[c] = 0;
        outputdata->classendvectorindex[c] = r;
    }
    int v = 0;
    outputdata->values = new double[featurenumber*vectornumber];
    for(c = 0; c < datatable.column_count; c++)
    {
        if(vectorclassindex[c] >= 0)
        {
            int classs = vectorclassindex[c];
            int vv = vectorsinclass[classs] + (classs == 0 ? 0 : outputdata->classendvectorindex[classs-1]);
            vectorsinclass[classs]++;

            if(vv >= vectornumber)
                vv = vectornumber-1;

            int f = 0;
            for(r = 0; r < datatable.row_count; r++)
            {
                outputdata->values[featurenumber*vv+f] = datatable.feature_values[datatable.row_count*c+r];
                f++;
            }
            v++;
        }
    }

    delete[] vectorclassindex;
    delete[] vectorsinclass;

    outputdata->classnames = new std::string[classnumber];
    for(c = 0; c < classnumber; c++)
    {
        outputdata->classnames[c] = classlist.at(c).toStdString();
    }

    outputdata->featurenames = new std::string[featurenumber];
    for(r = 0; r < featurenumber; r++)
    {
        outputdata->featurenames[r] = featurelist.at(r).toStdString();
    }

    outputdata->featureoriginalindex = new int[featurenumber];
    c = 0;
    for(r = 0; r < datatable.row_count; r++)
    {
        outputdata->featureoriginalindex[c] = r;
        c++;
    }
    return true;
}
TrainerSocket::TrainerSocket()
{
    selectionplugin = NULL;
}
MzSelectionPluginInterface* TrainerSocket::loadPlugin(const char* filename)
{
//    QString fpn;
//    char cwd[PATH_MAX];
//    if (dt_getcwd(cwd, sizeof(cwd)) != NULL)
//    {
//        QDir pluginsDir(cwd);
//        fpn = pluginsDir.absoluteFilePath(filename);
//    }
//    else
//        fpn = filename;

    MzSelectionPluginInterface* plugin_interface = NULL;
    QLibrary* lib = new QLibrary(filename);
    lib->setFileName(filename);
    lib->load();
    if(lib->isLoaded())
    {
        MzNewPluginFunction functionNew = (MzNewPluginFunction) lib->resolve("MzNewPluginObject");
        if (functionNew != NULL)
        {
            plugin_interface = (MzSelectionPluginInterface*) functionNew();
            if (plugin_interface != NULL)
            {

                if(plugin_interface->initiateTablePlugin(dynamic_cast<MzPullDataInterface*>(this),
                                                         dynamic_cast<MzGuiRelatedInterface*>(this)))
                {
                    selectionplugin = plugin_interface;
                    if(verbose)
                    {
                       std::cout << "Plugin: " << selectionplugin->getName() << std::endl;
                    }

                    return plugin_interface;
                }
                delete plugin_interface;
                plugin_interface = NULL;
            }
        }
    }
    lib->unload();
    delete lib;
    return plugin_interface;
}
bool TrainerSocket::setData(DataForSelection* data, SelectedFeatures *selected_features)
{
    return false;
}
bool TrainerSocket::getData(DataForSelection* data)
{
    return getRequiredData(data);
}
bool TrainerSocket::getData(DataForSegmentation* /*data*/)
{
    return false;
}
bool TrainerSocket::getData(std::vector<std::string>* featureNames, DataForSelection* data)
{
    return false;
}
bool TrainerSocket::setSelection(SelectedFeatures* /*selected_features*/)
{
    return true;
}
void TrainerSocket::InitiateAnalysisActions(void* plugin_object,
                                         MzBeforeStaticPluginFunction before_plugin,
                                         MzThreadStaticPluginFunction threaded_plugin,
                                         MzAfterStaticPluginFunction after_plugin)
{
    if((*before_plugin)(plugin_object))
    {
        (*threaded_plugin)(plugin_object, this, static_notify_step, static_notify_text);
        (*after_plugin)(plugin_object);
    }
}
bool TrainerSocket::selectClassifiers(std::vector<std::string>* /*classifierNames*/, std::vector<bool>* selectionResult, std::string /*title*/)
{
    for(bool sr: *selectionResult)
        sr = true;
    return true;
}
bool TrainerSocket::openProgressDialog(int maxTime, mz_uint64 maxSteps, MzSelectionPluginInterface *object, string title)
{
    return true;
}

void TrainerSocket::closeProgressDialog(void)
{
}

bool TrainerSocket::openOptionsDialog(std::string title)
{
    return true;
}
void TrainerSocket::addGroupOptionsDialog(std::string property, std::string tip)
{
}
void TrainerSocket::addPropertyOptionsDialog(std::string property, std::string value, std::string tip)
{
    TrainerSocketPropertyValue pv;
    pv.property = property;

    QString qvalue = QString::fromStdString(value);
    QStringList valist = qvalue.split(";");
    if(valist.size() > 1)
    {
        pv.value = valist[1].toStdString();
    }
    else
        pv.value = value;
    options.push_back(pv);
}
void TrainerSocket::getValueOptionsDialog(std::string property, std::string* value)
{
    for(TrainerSocketPropertyValue op: options)
    {
        if(property == op.property)
        {
            *value = op.value;
            if(verbose)
            {
               std::cout << property << " = " << *value << std::endl;
            }
            return;
        }
    }
}
bool TrainerSocket::loadOptions(const char* filename)
{
    ifstream file;
#if defined _MZ_UTF8_TO_U16
        file.open(_mz_utf8_to_u16(filename).c_str());
#else
        file.open(filename);
#endif
    if (!file.is_open()) return false;
    if (!file.good()) return false;
    char *prevloc = setlocale(LC_ALL, NULL);
    if(prevloc) prevloc = strdup(prevloc);
    setlocale(LC_ALL, "C");
    std::string sline;
    while(std::getline(file, sline))
    {
        std::stringstream ss(sline);
        std::string property;
        std::string value;
        ss >> std::skipws >> property;
        ss >> std::skipws >> value;
        TrainerSocketPropertyValue pv;
        pv.property = property;
        pv.value = value;
        options.push_back(pv);
    }
    setlocale(LC_ALL, prevloc);
    free(prevloc);
    file.close();
    return true;
}

void TrainerSocket::closeOptionsDialog(void)
{
}
void TrainerSocket::showTestResults(std::string* confusionTable, std::string title)
{
}
void TrainerSocket::showAbout(std::string title, std::string text)
{
    std::cout << title << ": " << text << std::endl;
}
void TrainerSocket::showMessage(std::string title, std::string text, unsigned int iconType)
{
    std::cout << title << ": " << text << std::endl;
}
bool TrainerSocket::getOpenFile(std::string* fileName)
{
    return false;
}

bool TrainerSocket::getSaveFile(std::string* fileName, unsigned int *filter)
{
    if(classifier_filename.empty())
        return false;
    *filter = 0;
    *fileName = classifier_filename;
    return true;
}

bool TrainerSocket::saveClassifier(std::string fileName)
{
    classifier_filename = fileName;
    for(TrainerSocketAction action: actions)
    {
        std::string data = action.name;
        std::transform(data.begin(), data.end(), data.begin(), [](unsigned char c){ return std::tolower(c); });
        if(data.find("save") != std::string::npos)
        {
            selectionplugin->callBack(action.index, ! single_thread);
            return true;
        }
    }
    return false;
}
bool TrainerSocket::trainClassifier(void)
{
    for(TrainerSocketAction action: actions)
    {
        std::string data = action.name;
        std::transform(data.begin(), data.end(), data.begin(), [](unsigned char c){ return std::tolower(c); });
        if(data.find("train") != std::string::npos)
        {
            selectionplugin->callBack(action.index, ! single_thread);
            return true;
        }
    }
    return false;
}
void* TrainerSocket::addMenuAction(const char* name, const char* tip, const unsigned int index)
{
    if(name == NULL)
        return NULL;

    TrainerSocketAction action;
    action.name = name;
    action.index = index;
    actions.push_back(action);
    return NULL;
}

void TrainerSocket::menuEnable(void* menu, bool enable)
{
}
