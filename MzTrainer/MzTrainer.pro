TEMPLATE = app
CONFIG += console
CONFIG -= app_bundle
#CONFIG -= qt

DESTDIR         = ../../Executables
TARGET = MzTrainer

HEADERS += \
    mztrainer.h \
    trainersocket.h

SOURCES +=  \
    ../MzShared/dataforconsole.cpp \
    ../MzShared/csvio.cpp \
    mztrainer.cpp \
    trainersocket.cpp
    

#dectree.cpp \
#../../qmazda/MzGenerator/featureio.cpp \

include(../Pri/config.pri)
