/*
 * main.cpp - program do testowania modułu analizy ziaren jęczmienia
 *
 * Autor:  (2016) Piotr M. Szczypiński, Politechnika Łódzka
 *
 * Opracowano w ramach grantu: NCBR PBS3/A8/38/2015
 * Opracowanie przemysłowej metody automatycznej oceny parametrów
 * technologicznych i klasyfikacji ziarna z zastosowaniem analizy
 * obrazów.
 */

#include "../MzShared/mzdefines.h"

//#include <QCoreApplication>
#include <string.h>
#include <string>
#include <vector>
#include <iostream>
#include <fstream>
#include <sstream>
#include <limits>

#include "../MzShared/getoption.h"
#include "trainersocket.h"
#include "../MzShared/multidimselection.h"

char* config = NULL;
char* input = NULL;
char* output = NULL;
char* plugin = NULL;
bool isprinthelp = false;
//bool echo = false;
bool verbose = false;
bool single_thread = false;

//-----------------------------------------------------------------------------
int scan_parameters_local(int argc, char* argv[])
{
    int argi = 1;
    while(argi < argc)
    {
        GET_STRING_OPTION("-c", "--config", config)
        else GET_STRING_OPTION("-p", "--plugin", plugin)
        else GET_STRING_OPTION("-i", "--input", input)
        else GET_STRING_OPTION("-o", "--output", output)
//        else GET_NOARG_OPTION("-e", "--echo", echo, true)
        else GET_NOARG_OPTION("-v", "--verbose", verbose, true)
        else GET_NOARG_OPTION("-k", "--single-thread", single_thread, true)
        else GET_NOARG_OPTION("/?", "--help", isprinthelp, true)
        else return argi;
        argi++;
    }
    return 0;
}

//-----------------------------------------------------------------------------
void printhelp_local(char* name)
{
    //------------------------------------------------------------------------------------------v
    printf("Usage: %s [OPTION]...\n", name);
    printf("Machine learning based on qmazda plugins\n");
    printf("%s\n", QMAZDA_VERSION);
    printf("%s\n", QMAZDA_COPYRIGHT);
    printf("Compilation date and time: %s %s\n", __DATE__, __TIME__);
    printf("Options:\n");
    printf("  -p, --plugin <file>  Plugin (shared library or dll)\n");
    printf("  -c, --config <file>  Configuration file for plugin\n");
    printf("  -i, --input <file>   Input file (csv)\n");
    printf("  -o, --output <file>  Output file for classification rules\n");
    printf("  -v, --verbose        Verbose mode\n");
#ifdef MZ_USE_THREADS
    printf("  -k, --single-thread     Use single thread (slower, requires less mempory)\n");
#endif
    printf("  /?, --help           Display this help and exit\n\n");
}

//-----------------------------------------------------------------------------
int main(int argc, char* argv[])
{
    int ret = scan_parameters_local(argc, argv);
    if(isprinthelp || argc <= 1)
    {
        printhelp_local(argv[0]);
        return 1;
    }
    if(ret != 0)
    {
        if(ret < argc) fprintf(stderr, "Incorrect operand: %s\n", argv[ret]);
        fprintf(stderr, "Try --help for more information.\n");
        return 2;
    }

    if(plugin == NULL)
    {
        fprintf(stderr, "Missing plugin switch -p\n");
        return 3;
    }
    if(input == NULL)
    {
        fprintf(stderr, "Missing input switch -i\n");
        return 4;
    }
    if(output == NULL)
    {
        fprintf(stderr, "Missing output switch -o\n");
        return 5;
    }

    TrainerSocket socket;
    if(socket.loadPlugin(plugin) == NULL)
    {
        fprintf(stderr, "Cannot load plugin: %s\n", plugin);
        return 11;
    }
    if(socket.loadFromFile(input) <= 0)
    {
        fprintf(stderr, "Cannot input data: %s\n", input);
        return 12;
    }
    if(config != NULL)
    {
        if(! socket.loadOptions(config))
        {
            fprintf(stderr, "Cannot set options from: %s\n", config);
            return 13;
        }
    }
    if(! socket.trainClassifier())
    {
        fprintf(stderr, "Training failed\n");
        return 14;
    }

    if(! socket.saveClassifier(output))
    {
        fprintf(stderr, "Cannot save classifier to: %s\n", output);
        return 15;
    }

    return 0;
}

