/*
 * qMaZda - Image Analysis and Pattern Recognition
 *
 * Copyright 2013-2014 Piotr M. Szczypiński <piotr.szczypinski@p.lodz.pl>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "../MzShared/mzdefines.h"
#include "mainwindow.h"
#include "ui_mainwindow.h"

#include <QDropEvent>
#include <QMimeData>
#include <QMessageBox>
#include <QDesktopServices>
#include <sstream>
#include "../MzShared/filenameremap.h"
#include "../SharedImage/mazdaimageio.h"
#include "../MzReport/selectclassifiers.h"

#include <itkVersion.h>
#include <tiff.h>
//------------------------------------------------------------------
//                PLUGIN INTERFACE
//------------------------------------------------------------------

void MainWindow::InitiateAnalysisActions(void* plugin_object,
                                         MzBeforeStaticPluginFunction before_plugin,
                                         MzThreadStaticPluginFunction threaded_plugin,
                                         MzAfterStaticPluginFunction after_plugin)
{
    this->plugin_object = plugin_object;
    this->after_plugin = after_plugin;
    if((*before_plugin)(plugin_object))
    {
        PlugWorker* worker = new PlugWorker(plugin_object, threaded_plugin);
        QThread* thread = new QThread;
        worker->moveToThread(thread);
        connect(thread, SIGNAL(started()), worker, SLOT(process()));
        connect(worker, SIGNAL(finished(bool)), this, SLOT(on_finished(bool)));
        connect(worker, SIGNAL(finished()), thread, SLOT(quit()));
        connect(worker, SIGNAL(finished()), worker, SLOT(deleteLater()));
        connect(thread, SIGNAL(finished()), thread, SLOT(deleteLater()));
        connect(worker, SIGNAL(notify_progress(void)), this, SLOT(notifyProgressSlot(void)));
        connect(worker, SIGNAL(notify_progress(QString)), this, SLOT(notifyProgressSlot(QString)));
        thread->start();
    }
}
void MainWindow::on_finished(bool success)
{
    (*after_plugin)(plugin_object);
    if(success && dataCopy.result != NULL)
    {
        MainWindow* w = new MainWindow;
        w->show();
        w->setData(&dataCopy);
    }
    else
    {
        ClearData();
    }
}


bool MainWindow::setData(DataForSegmentation* data)
{
    unsigned int resultnames_size = data->resultnames.size();
    //if(fmaps.size() <= 0) return false;
    if(data->resultnames.size() <= 0) return false;
    if(data->featurenames.size() <= 0) return false;

    bool change = false;
    for(unsigned int f = 0; f < resultnames_size; f++)
    {
        double spacing[FloatImage::Dimensions];
        for(unsigned int d = 0; d < FloatImage::Dimensions; d++) spacing[d] = 1.0;
        FloatImage* newfmap = new FloatImage(data->size, spacing, data->result[f]);
        newfmap->SetName(data->resultnames[f]);
        fmaps.push_back(newfmap);
        change = true;
    }
    delete[] dataCopy.result;
    dataCopy.result = NULL;
    delete[] dataCopy.values;
    dataCopy.values = NULL;
    dataCopy.featurenames.clear();
    dataCopy.resultnames.clear();
    dataCopy.vectornumber = 0;
    if(change) assimilateImage();
    return change;
}

void MainWindow::notifyProgressSlot(void)
{
    progressdialog->NotifyProgressStep();
}
void MainWindow::notifyProgressSlot(QString message)
{
    progressdialog->NotifyProgressText(message);
}
bool MainWindow::selectClassifiers(std::vector<std::string>* classifierNames, std::vector<bool>* selectionResult, std::string title)
{
    SelectClassifiers dialog(this);
    if(title.length() > 0) dialog.setWindowTitle(QString::fromStdString(title));
    dialog.setOptions(classifierNames, selectionResult);
    if(dialog.exec() == QDialog::Accepted) return true;
    return false;
}
bool MainWindow::openProgressDialog(int maxTime, mz_uint64 maxSteps, MzSelectionPluginInterface *object, string title)
{
    progressdialog = new Progress(this);
    if(progressdialog == NULL) return false;
    if(title.length() > 0) progressdialog->setWindowTitle(QString::fromStdString(title));
    progressdialog->setMaxValues(maxTime, maxSteps, object);
    progressdialog->show();
#ifdef MAZDA_HIDE_ON_PROGRESS
    this->hide();
    progressdialog->setWindowModality(Qt::NonModal);
#else
    progressdialog->setWindowModality(Qt::ApplicationModal);//(Qt::WindowModal);
#endif
    return true;
}
void MainWindow::closeProgressDialog(void)
{
    progressdialog->close();
    delete progressdialog;
    progressdialog = NULL;
#ifdef MAZDA_HIDE_ON_PROGRESS
    this->show();
#endif
}
bool MainWindow::openOptionsDialog(std::string /*title*/)
{
    return false;
}
void MainWindow::addGroupOptionsDialog(std::string /*property*/, std::string /*tip*/)
{
}
void MainWindow::addPropertyOptionsDialog(std::string /*property*/, std::string /*value*/, std::string /*tip*/)
{
}
void MainWindow::getValueOptionsDialog(std::string /*property*/, std::string* /*value*/)
{
}
void MainWindow::closeOptionsDialog(void)
{
}
void MainWindow::showTestResults(std::string* /*confusionTable*/, std::string /*title*/)
{
}
void MainWindow::showAbout(std::string title, std::string text)
{
    QMessageBox::about(this, QString::fromStdString(title), QString::fromStdString(text));
}
void MainWindow::showMessage(std::string title, std::string text, unsigned int iconType)
{
    QMessageBox msgBox;
    msgBox.setText(QString::fromStdString(text));
    msgBox.setWindowTitle(QString::fromStdString(title));
    msgBox.setIcon((QMessageBox::Icon) iconType);
    msgBox.exec();
}
bool MainWindow::getOpenFile(std::string* fileName)
{
    QString filter;
    QString fileNameTemp = QFileDialog::getOpenFileName(this,
                                                 tr("Load classifier"),
                                                 NULL,//domyslna nazwa pliku
                                                 tr("Text file (*.txt) (*.txt);;Double precision hexadecimals (*.txtv) (*.txtv);;All (*) (*)"),
                                                 &filter);
    if (!fileNameTemp.isEmpty())
    {
        *fileName = fileNameTemp.toStdString();//fileNameTemp.toStdString();
        return true;
    }
    return false;
}

bool MainWindow::getSaveFile(std::string* fileName, unsigned int *filter)
{
    QString sfilter;
    QString fileNameTemp = QFileDialog::getSaveFileName(this,
                                                 tr("Save classifier"),
                                                 NULL,//domyslna nazwa pliku
                                                 tr("Text file (*.txt) (*.txt);;Double precision hexadecimals (*.txtv) (*.txtv);;All (*) (*)"),
                                                 &sfilter);
    if(sfilter == "Double precision hexadecimals (*.txtv) (*.txtv)") *filter = 1;
    else *filter = 0;

    if (!fileNameTemp.isEmpty())
    {
        *fileName = fileNameTemp.toStdString();//fileNameTemp.toStdString();
        return true;
    }
    return false;
}

void MainWindow::pluginMenuTriggered()
{
    MzAction* plugin_action = qobject_cast<MzAction*> (sender());
    plugin_action->interfaceMzSelectionPluginInterface->callBack(plugin_action->indexToOnActionCall, ! ui->actionSingle_thread->isChecked());
}

void *MainWindow::addMenuAction(const char* name, const char* tip, const unsigned int index)
{
    if(plugin_menu == NULL) return NULL;

    if(name == NULL)
    {
        plugin_menu->addSeparator();
        return NULL;
    }

    MzAction* plugin_action = new MzAction(tr(name), this);
    plugin_action->setStatusTip(tr(tip));
    plugin_action->interfaceMzSelectionPluginInterface = selectionplugin;
    plugin_action->indexToOnActionCall = index;

    plugin_menu->addAction(plugin_action);
    connect(plugin_action, SIGNAL(triggered()), this, SLOT(pluginMenuTriggered()));
    return plugin_action;
}

void MainWindow::menuEnable(void* menu, bool enable)
{
    ((QAction*)menu)->setEnabled(enable);
}


//------------------------------------------------------------------
//                DATA TRANSFER INTERFACE
//------------------------------------------------------------------

void MainWindow::ClearData(void)
{
    unsigned int resultsnumber = dataCopy.resultnames.size();
    if(dataCopy.result != NULL)
    {
        for(unsigned int i = 0; i < resultsnumber; i++)
        {
            delete[] dataCopy.result[i];
        }
        delete[] dataCopy.result;
        dataCopy.result = NULL;
    }
    dataCopy.featurenames.clear();
    dataCopy.resultnames.clear();
}

bool MainWindow::getData(DataForSegmentation* data)
{
    ClearData();

    unsigned int featurenames_size = data->featurenames.size();
    unsigned int resultnames_size = data->resultnames.size();
    unsigned int map_size = (unsigned int)-1;
    if(fmaps.size() <= 0) return false;
    if(data->resultnames.size() <= 0) return false;
    if(data->featurenames.size() <= 0) return false;

    for(unsigned int f = 0; f < featurenames_size; f++)
    {
        bool present = false;
        for(std::vector<FloatImage*>::iterator c = fmaps.begin(); c != fmaps.end(); ++c)
        {
            if( (*c)->GetName() == data->featurenames[f])
            {
                for(unsigned int d = 0; d < 4; d++)
                    data->size[d] = 0;
                (*c)->GetSize(data->size);
                size_t count = 1;
                for(unsigned int d = 0; d < FloatImage::Dimensions; d++)
                    count *= data->size[d];
                if(map_size == (unsigned int)-1) map_size = count;
                if(map_size != count) return false;
                present = true;
                break;
            }
        }
        if(!present) return false;
    }

    data->vectornumber = map_size;
    data->values = new MazdaMapPixelType*[featurenames_size];

    for(unsigned int f = 0; f < featurenames_size; f++)
    {
        for(std::vector<FloatImage*>::iterator c = fmaps.begin(); c != fmaps.end(); ++c)
        {
            if( (*c)->GetName() == data->featurenames[f])
            {
                data->values[f] = (MazdaMapPixelType*) ((*c)->GetDataPointer());
                break;
            }
        }
    }
    data->result = new MazdaMapPixelType*[resultnames_size];
    for(unsigned int f = 0; f < resultnames_size; f++) data->result[f] = new MazdaMapPixelType[map_size];

    dataCopy = *data;
    return true;
}

bool MainWindow::setData(DataForSelection* /*data*/, SelectedFeatures */*selected_features*/)
{
    return false;
}
bool MainWindow::getData(DataForSelection* /*data*/)
{
    return false;
}
bool MainWindow::getData(std::vector<std::string>* /*featureNames*/, DataForSelection* /*data*/)
{
    return false;
}
bool MainWindow::setSelection(SelectedFeatures* /*selected_features*/)
{
    return false;
}

//------------------------------------------------------------------
//                MAIN WINDOW
//------------------------------------------------------------------

void MainWindow::on_actionAbout_triggered()
{
    std::stringstream ss;
    ss << "<h2>qmazda MzMaps</h2> " << std::endl;
    ss << QMAZDA_VERSION << "<br>" << std::endl;
    ss << QMAZDA_COPYRIGHT << "<br>" << std::endl;
    ss << "Built on " << __DATE__ << " at " << __TIME__ << "<br> <br>" << std::endl;
    ss << "The program is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING THE WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE." << "<br> <br>" << std::endl;
    ss << "Home: <a href=\"http://www.eletel.p.lodz.pl/pms/SoftwareQmazda.html\"> http://www.eletel.p.lodz.pl/pms/SoftwareQmazda.html </a> <br>" << std::endl;
    ss << "Sources: <a href=\"https://gitlab.com/qmazda/qmazda\"> https://gitlab.com/qmazda/qmazda </a> <br>" << std::endl;
    ss << "<br>Built with:<br>" << std::endl;
    ss << "- Qt " << QT_VERSION_STR << " <a href=\"https://www.qt.io/developers/\">https://www.qt.io/developers</a> <br>" << std::endl;
    ss << "- LibTIFF " <<  TIFFLIB_VERSION << " <a href=\"http://www.libtiff.org/\">http://www.libtiff.org</a> <br>" << std::endl;
    ss << "- Insight Toolkit " <<  ITK_VERSION << " <a href=\"https://itk.org/\">https://itk.org</a> <br>" << std::endl;

    QMessageBox::about(this, "About MzMaps", ss.str().c_str());
}

void MainWindow::on_actionContents_triggered()
{
    QDir pluginsDir(qApp->applicationDirPath());
#if defined(__APPLE__) && defined(MAZDA_IN_MACOS_BUNDLE)
    pluginsDir.cdUp();
    pluginsDir.cdUp();
    pluginsDir.cdUp();
#endif
    QDesktopServices::openUrl(QUrl::fromLocalFile(pluginsDir.absolutePath() + "/qmazda.pdf"));
}

//QString MainWindow::getExecutableWithPath(const char* name)
//{
//    QDir execDir(qApp->applicationDirPath());
//    return execDir.absolutePath() + QString(name);
//}

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    zoomSlider.setOrientation(Qt::Horizontal);
    zoomSlider.setMinimumWidth(200);
    zoomSlider.setMinimum(-128);
    zoomSlider.setMaximum(128);
    zoomSlider.setValue(0);
    zoomSlider.setSizePolicy(QSizePolicy::Fixed, QSizePolicy::Minimum);

//http://doc.qt.io/qt-4.8/stylesheet-reference.html
//https://forum.qt.io/topic/12380/using-a-stylesheet-to-customize-a-qslider

    zoomSlider.setStyleSheet(
        "QSlider::groove:horizontal {"
        "background: #82BAD5;"
        "border: 1px solid #CCCECE;"
        "height: 2px;"
        "margin: 5 5 5 5;"
        "}"
        "QSlider::handle:horizontal {"
        "background: #82BAD5;"
        "width: 7px;"
        "margin: -4px 0 -4px 0;"
        "border: 1px solid #77839C;"
        "border-radius: 4px;"
        "}");

    zoomReset.setIcon(QIcon(":/icons/icons/zoom-original.png"));
    zoomReset.setStyleSheet("border: none");
    ui->statusBar->addPermanentWidget(&zoomSlider);
    ui->statusBar->addPermanentWidget(&zoomReset);
    connect(&zoomSlider, SIGNAL(valueChanged(int)), this, SLOT(zoomSliderValueChanged(int)));//???
    connect(&zoomReset, SIGNAL(clicked()), this, SLOT(zoomResetClicked()));//???


    //setZoomRequested = false;
    progressdialog = NULL;
    image_data = NULL;
    zoom_level = 0;
    renderer = new FPIRenderer();
   // renderer->setBackgroundRole(QPalette::Dark);
    renderer->setSizePolicy(QSizePolicy::Ignored, QSizePolicy::Ignored);

    scrollArea = new FPIRenderArea;
    scrollArea->setFrameShape(QFrame::NoFrame);

//    scrollArea = new QScrollArea;
//    scrollArea->setBackgroundRole(QPalette::Dark);
    scrollArea->setAlignment(Qt::AlignCenter);
    scrollArea->setWidget(renderer);
    setCentralWidget(scrollArea);
    connect(renderer, SIGNAL(setZoomRequest(double)), this, SLOT(setZoomRequest(double)));
    connect(renderer, SIGNAL(mousePressed(QString)), this, SLOT(onMousePressed(QString)));

    loadPlugins();
    if(selectionplugins.count() > 0) initiatePlugins();

    if(QApplication::arguments().size() >= 2)
    {
        QString fn = QApplication::arguments()[1];
        if(loadImage(&fn))
        {
            if(QDir::tempPath() == fn.mid(0, QDir::tempPath().length()))
            {
                QFile::remove(fn);
            }
        }
    }
}

MainWindow::~MainWindow()
{
    delete ui;
}


void MainWindow::dropEvent(QDropEvent* event)
{
    const QMimeData* mimeData = event->mimeData();

    if (mimeData->hasUrls())
    {
        QList<QUrl> urlList = mimeData->urls();

        if(urlList.size() >= 1)
        {
            QString filename = urlList.at(0).toLocalFile();

            if(loadImage(&filename))
                event->acceptProposedAction();
            else
            {
                foreach(selectionplugin, selectionplugins)
                {
                    std::string tsn = filename.toStdString();
                    if(selectionplugin->openFile(&tsn))
                    {
                        event->acceptProposedAction();
                        return;
                    }
                }
            }

        }
    }
}




bool MainWindow::initiatePlugins(void)
{
    foreach(selectionplugin, selectionplugins)
    {
        if(selectionplugin == NULL) continue;
        plugin_menu = new QMenu(ui->menuAnalysis);
        plugin_menu->setObjectName("plugin_menu");
        ui->menuAnalysis->addAction(plugin_menu->menuAction());
        plugin_menu->setTitle(selectionplugin->getName());
        if(! selectionplugin->initiateMapsPlugin(
                dynamic_cast<MzPullDataInterface*>(this),
                dynamic_cast<MzGuiRelatedInterface*>(this)))
            delete plugin_menu;
    }
    plugin_menu = NULL;
    return true;
}

void MainWindow::loadPlugins(void)
{
    QDir pluginsDir(qApp->applicationDirPath());
#if defined(__APPLE__) && defined(MAZDA_IN_MACOS_BUNDLE)
    pluginsDir.cdUp();
    pluginsDir.cdUp();
    pluginsDir.cdUp();
#endif
    foreach (QString fileName, pluginsDir.entryList(QDir::Files))
    {
        QString fiext = QFileInfo(fileName).suffix();
        if(fiext != "so" && fiext != "dll" && fiext != "dylib")
            continue;
        QString finame = QFileInfo(fileName).fileName();
        if(QString::compare(finame.left(3), QString("mzp"), Qt::CaseInsensitive) != 0)
            continue;


        MzSelectionPluginInterface* plugin_interface = NULL;
        QLibrary* lib = new QLibrary(pluginsDir.absoluteFilePath(fileName));
        lib->load();
        if(lib->isLoaded())
        {
            MzNewPluginFunction functionNew = (MzNewPluginFunction) lib->resolve("MzNewPluginObject");
            if (functionNew != NULL)
            {
                plugin_interface = (MzSelectionPluginInterface*) functionNew();
                if (plugin_interface != NULL)
                {
                    selectionplugins.append(plugin_interface);
                }
            }
        }
        if(plugin_interface == NULL)
        {
            lib->unload();
            delete lib;
        }
    }
}

void MainWindow::on_mapNamesListWidget_currentRowChanged(int currentRow)
{
    if(currentRow >= 0 && currentRow < (int)fmaps.size())
    {
        double min, span;
        double minwindow = fmaps[currentRow]->minwindow;
        double maxwindow = fmaps[currentRow]->maxwindow;
        unsigned int size[4];
        fmaps[currentRow]->GetSize(size);
        if(size[2] > 0)
        {
            ui->horizontalSliderZ->setMinimum(0);
            ui->horizontalSliderZ->setMaximum(size[2]-1);
            ui->spinBoxZ->setMinimum(0);
            ui->spinBoxZ->setMaximum(size[2]-1);
            ui->horizontalSliderZ->setEnabled(true);
            ui->spinBoxZ->setEnabled(true);
            ui->labelZ->setEnabled(true);
        }
        else
        {
            ui->horizontalSliderZ->setMinimum(0);
            ui->horizontalSliderZ->setMaximum(0);
            ui->spinBoxZ->setMinimum(0);
            ui->spinBoxZ->setMaximum(0);
            ui->horizontalSliderZ->setEnabled(false);
            ui->spinBoxZ->setEnabled(true);
            ui->labelZ->setEnabled(false);
        }
        renderer->setSlice(ui->horizontalSliderZ->value());
        renderer->setImagePointer(fmaps[currentRow]);

        blockSignals(true);
        ui->spinBoxBright->setMaximum(fmaps[currentRow]->maxvalue);
        ui->spinBoxBright->setMinimum(fmaps[currentRow]->minvalue);
        ui->spinBoxDark->setMaximum(fmaps[currentRow]->maxvalue);
        ui->spinBoxDark->setMinimum(fmaps[currentRow]->minvalue);

        min = ui->spinBoxBright->minimum();
        span = ui->spinBoxBright->maximum() - min;
        ui->horizontalSliderMax->setValue((int)(1024*(ui->spinBoxBright->value()-min)/span));
        min = ui->spinBoxDark->minimum();
        span = ui->spinBoxDark->maximum() - min;
        ui->horizontalSliderMin->setValue((int)(1024*(ui->spinBoxDark->value()-min)/span));

        ui->spinBoxMax->setValue(fmaps[currentRow]->maxbrightness);
        ui->spinBoxMin->setValue(fmaps[currentRow]->minbrightness);

        ui->spinBoxBright->setValue(maxwindow);
        ui->spinBoxDark->setValue(minwindow);

        blockSignals(false);
    }
    else
    {
        renderer->setImagePointer(NULL);
    }
}


QImage MainWindow::toQImage(FloatImage* image)
{
    unsigned int size[FloatImage::Dimensions];
    image->GetSize(size);
    unsigned int strade = ((size[0]+3)>>2);

    if(image_data != NULL)
        delete[] image_data;
    image_data = new uint32_t[strade*size[1]];

    double min = image->minvalue;
    double mul;
    if(image->maxvalue > min)
        mul = (double)253/(image->maxvalue - min);
    else
        mul = 0;
    unsigned int coor[FloatImage::Dimensions];
    coor[0] = 0; coor[1] = 0;
    for(unsigned int d = 2; d < FloatImage::Dimensions; d++) coor[d] = size[d]/2;

    FloatImage::PixelType* ptrif32 = image->GetPixelPointer(coor);
    for(unsigned int y = 0; y < size[1]; y++)
    {
        uint8_t* ptro = (uint8_t*) (image_data+strade*y);
        for(unsigned int x = 0; x < size[0]; x++, ptro++, ptrif32++)
        {
            if(!(*ptrif32 == *ptrif32)) *ptro = 255;
            else *ptro = (uint8_t)((*ptrif32 - min) * mul + 1);

        }
    }
    QVector<QRgb> colorTable;
    for(int i = 0; i < 255; ++i) colorTable.append(qRgb(i, i, i));
    colorTable.append(qRgb(63, 0, 0));
    QImage qimage((uchar*)image_data, size[0], size[1], QImage::Format_Indexed8);
    qimage.setColorTable(colorTable);
    return qimage;
}

QPixmap MainWindow::toIcon(FloatImage* image)
{
    QPixmap scaledpixmap;
    if(image == NULL)
        return scaledpixmap;
    if(image->GetDataPointer() == NULL)
        return scaledpixmap;
    unsigned int size[FloatImage::Dimensions];
    image->GetSize(size);
    QSize icon_size = ui->mapNamesListWidget->iconSize(); //QSize(32,24);
    bool horiz = false;
    if(icon_size.height() <= 0) horiz = true;
    else if(icon_size.width() <= 0) horiz = false;
    else if(size[1]/icon_size.height() > size[0]/icon_size.width()) horiz = true;

    QSize iconsize(horiz ? icon_size.width() : icon_size.height()*size[0]/size[1], !horiz ? icon_size.height() : icon_size.width()*size[1]/size[0]);
    QImage qimage = toQImage(image);


//    static int count = 0;
//    count++;
//    char name[256];
//    sprintf(name, "/home/piotr/Program/bin/nn%i.jpg", count);
//    qimage.save(name);


    scaledpixmap = QPixmap(QPixmap::fromImage(qimage.scaled(iconsize, Qt::IgnoreAspectRatio, Qt::SmoothTransformation)));

//    sprintf(name, "/home/piotr/Program/bin/ss%i.jpg", count);
//    scaledpixmap.save(name);

    if(image_data != NULL)
        delete[] image_data;
    image_data = NULL;

    return scaledpixmap;
}

void MainWindow::assimilateImage(void)
{
    ui->mapNamesListWidget->clear();
    for(std::vector<FloatImage*>::iterator image = fmaps.begin(); image != fmaps.end(); ++image)
    {
        FPIRenderer::resetRanges(*image);
        QPixmap scaledpixmap =  toIcon(*image);

        if(scaledpixmap.isNull())
        {
            ui->mapNamesListWidget->addItem(QString::fromStdString((*image)->GetName()));
        }
        else
        {
            QListWidgetItem* item = new QListWidgetItem(QIcon(scaledpixmap), QString::fromStdString((*image)->GetName()));
            ui->mapNamesListWidget->addItem(item);
        }
    }

    if(fmaps.size() > 0)
    {
        blockSignals(true);
        ui->mapNamesListWidget->setCurrentRow(0);
        blockSignals(false);
        on_mapNamesListWidget_currentRowChanged(0);
    }
}

int MainWindow::loadImage(const char* filename)
{
    std::vector<FloatImage*> maps = MazdaMapIO <FloatImage>::Read(filename);
    if(maps.size() <= 0) return -1;
    for(unsigned int i = 0; i < fmaps.size(); i++) delete fmaps[i];
    fmaps.clear();
    fmaps = maps;
//    std::vector<MzMap*> maps = MazdaMapIO <MzMap>::Read(filename);
//    if(maps.size() <= 0) return -1;
//    for(unsigned int i = 0; i < fmaps.size(); i++) delete fmaps[i];
//    fmaps.clear();
//    for(unsigned int i = 0; i < maps.size(); i++)
//    {
//        unsigned int size[FloatImage::Dimensions];
//        double spacing[FloatImage::Dimensions];
//        maps[i]->GetSize(size);
//        maps[i]->GetSpacing(spacing);
//        FloatImage* image = new FloatImage(size, spacing, maps[i]->GetDataPointer());
//        maps[i]->DeallocateAtDestruction(false);
//        image->SetName(maps[i]->GetName());
//        fmaps.push_back(image);
//        delete maps[i];
//    }
    return fmaps.size();
}

bool MainWindow::loadImage(QString* fileName)
{
    if(loadImage(fileName->toStdString().c_str()) <= 0) return false;
    QString openFileName = QFileInfo(*fileName).fileName();
    this->setWindowTitle(QString("%1 - %2").arg(openFileName).arg(tr("MzMaps")));
    assimilateImage();
    return true;
}

void MainWindow::on_actionLoadImage_triggered()
{
    QString fileName = QFileDialog::getOpenFileName(this,
                                                    tr("Load feature maps"),
                                                    NULL,//domyslna nazwa pliku
                                                    tr("Floating point feature map (*.map *.tif *.tiff);;All Files (*)"));
    if (!fileName.isEmpty())
    {
        loadImage(&fileName);
    }
}

bool MainWindow::saveImage(const char* filename)
{
    return MazdaMapIO <FloatImage>::Write(filename, &fmaps);
}

void MainWindow::on_actionSaveImage_triggered()
{
    QString fileName = QFileDialog::getSaveFileName(this,
                                                    tr("Save feature maps"),
                                                    NULL,//domyslna nazwa pliku
                                                    tr("Floating point feature map (*.map *.tif *.tiff)"));
    if (!fileName.isEmpty())
    {
        saveImage(fileName.toStdString().c_str());
    }
}

bool MainWindow::exportImage(const char* filename)
{
    int currentRow = ui->mapNamesListWidget->currentRow();
    if(currentRow >= 0 && currentRow < (int)fmaps.size())
    {
        if(fmaps[currentRow] == NULL) return false;
        if(fmaps[currentRow]->GetDataPointer() == NULL)
            return false;
        if(! MazdaMapIO <FloatImage>::Export(filename, fmaps[currentRow]))
        {
            QPixmap qi = toIcon(fmaps[currentRow]);
            return qi.save(filename, nullptr, 100);
        }
    }
    return false;
}

void MainWindow::on_actionExport_image_triggered()
{
    QString fileName = QFileDialog::getSaveFileName(this,
                                                    tr("Export feature maps"),
                                                    NULL,//domyslna nazwa pliku
                                                    tr("Tiff files (*.tiff);;Nifti files (*.nii);;All Files (*)"));
    if (!fileName.isEmpty())
    {
        exportImage(fileName.toStdString().c_str());
    }
}


void MainWindow::on_spinBoxMin_valueChanged(int arg1)
{
    int currentRow = ui->mapNamesListWidget->currentRow();
    if(currentRow >= 0 && currentRow < (int)fmaps.size())
    {
        fmaps[currentRow]->minbrightness = arg1;
        renderer->setImagePointer(fmaps[currentRow]);
    }
}

void MainWindow::on_spinBoxMax_valueChanged(int arg1)
{
    int currentRow = ui->mapNamesListWidget->currentRow();
    if(currentRow >= 0 && currentRow < (int)fmaps.size())
    {
        fmaps[currentRow]->maxbrightness = arg1;
        renderer->setImagePointer(fmaps[currentRow]);
    }
}


void MainWindow::on_horizontalSliderMax_valueChanged(int value)
{
    double min = ui->spinBoxBright->minimum();
    double span = ui->spinBoxBright->maximum() - min;
    ui->spinBoxBright->setValue(min + span*value/1024);
}
void MainWindow::on_spinBoxBright_valueChanged(double arg1)
{
    blockSignals(true);
    double min = ui->spinBoxBright->minimum();
    double span = ui->spinBoxBright->maximum() - min;
    ui->horizontalSliderMax->setValue((int)(1024*(arg1-min)/span));
    blockSignals(false);

    int currentRow = ui->mapNamesListWidget->currentRow();
    if(currentRow >= 0 && currentRow < (int)fmaps.size())
    {
        fmaps[currentRow]->maxwindow = arg1;
        renderer->setImagePointer(fmaps[currentRow]);
    }
}

void MainWindow::on_horizontalSliderMin_valueChanged(int value)
{
    double min = ui->spinBoxDark->minimum();
    double span = ui->spinBoxDark->maximum() - min;
    ui->spinBoxDark->setValue(min + span*value/1024);
}
void MainWindow::on_spinBoxDark_valueChanged(double arg1)
{
    blockSignals(true);
    double min = ui->spinBoxDark->minimum();
    double span = ui->spinBoxDark->maximum() - min;
    ui->horizontalSliderMin->setValue((int)(1024*(arg1-min)/span));
    blockSignals(false);

    int currentRow = ui->mapNamesListWidget->currentRow();
    if(currentRow >= 0 && currentRow < (int)fmaps.size())
    {
        fmaps[currentRow]->minwindow = arg1;
        renderer->setImagePointer(fmaps[currentRow]);
    }
}

void MainWindow::adjustScrollBar(QScrollBar *scrollBar, double factor)
{
    scrollBar->setValue(int(factor * scrollBar->value() + ((factor - 1) * scrollBar->pageStep()/2)));
}

void MainWindow::on_actionZoom_in_triggered()
{
      zoomSlider.setValue(zoomSlider.value()+1);
}

void MainWindow::on_actionZoom_out_triggered()
{
    zoomSlider.setValue(zoomSlider.value()-1);
}

void MainWindow::on_actionZoom_1_triggered()
{
    zoomSlider.setValue(0);
}

//void MainWindow::on_zoomReset_clicked()
//{
//    ui->horizontalSliderZoom->setValue(0);
//}

//void MainWindow::on_horizontalSliderZoom_valueChanged(int value)
//{
//    if(setZoomRequested)
//        return;
//    double zoom = exp((double) value/64);
//    if(renderer != NULL)
//        renderer->SetZoom(zoom);

//    //renderer->setZoom(value);
//}

//void MainWindow::rendererSizeChanged(int zoomlevel)
//{
//    ui->horizontalSliderZoom->blockSignals(true);
//    ui->horizontalSliderZoom->setValue(zoomlevel);
//    ui->horizontalSliderZoom->blockSignals(false);
//}


void MainWindow::on_comboBox_currentIndexChanged(int index)
{
    renderer->setPseudoPalette(index);
}

void MainWindow::on_removeImageButton_clicked()
{
    QListWidgetItem *item = ui->mapNamesListWidget->currentItem();
    if(item == NULL) return;
    int row = ui->mapNamesListWidget->row(item);
    if(row < 0 || row >= (int)fmaps.size()) return;
    QString com("Delate map ");
    com += QString(fmaps[row]->GetName().c_str());
    com += QString("?");

    QMessageBox msgBox(QMessageBox::Question, "Delete", com, QMessageBox::Ok|QMessageBox::Cancel);

    if(msgBox.exec() == QMessageBox::Ok)
    {
        delete item;
        if(fmaps[row] != NULL) delete fmaps[row];
        fmaps[row] = NULL;
        fmaps.erase(fmaps.begin()+row);
        assimilateImage();
    }
}

void MainWindow::onMousePressed(QString txt)
{
    ui->statusBar->showMessage(txt);
}


void MainWindow::on_imageLevelsButton_clicked()
{
    ImageVisibilityMode vm = THREELEVEL;
    //ui->imageOffButton->setChecked(false);
    ui->imageLevelsButton->setChecked(true);
    ui->ImageGrayButton->setChecked(false);
    renderer->setImageVisibility(vm);
    //updateHistogram();
}

void MainWindow::on_ImageGrayButton_clicked()
{
    ImageVisibilityMode vm = NORMAL;
    //ui->imageOffButton->setChecked(false);
    ui->imageLevelsButton->setChecked(false);
    ui->ImageGrayButton->setChecked(true);
    renderer->setImageVisibility(vm);
    //updateHistogram();
}

void MainWindow::on_horizontalSliderZ_valueChanged(int value)
{
    renderer->setSlice(value);
}

void MainWindow::zoomResetClicked()
{
    if(renderer != NULL)
        renderer->SetZoom(1.0);
    zoomSlider.setValue(0);
}

void MainWindow::zoomSliderValueChanged(int value)
{
    double zoom = exp((double) value*0.021660849);
    if(renderer != NULL)
        renderer->SetZoom(zoom);
}

void MainWindow::setZoomRequest(double zoom)
{
    zoomSlider.blockSignals(true);
    zoomSlider.setValue(round(log(zoom)*46.166241));
    zoomSlider.blockSignals(false);
}

void MainWindow::on_actionSingle_thread_toggled(bool)
{

}

