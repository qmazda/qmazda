/*
 * qMaZda - Image Analysis and Pattern Recognition
 *
 * Copyright 2013-2014 Piotr M. Szczypiński <piotr.szczypinski@p.lodz.pl>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <QApplication>
#include <QScrollArea>
#include <QScrollBar>
#include <limits>
#include "fpirenderer.h"

const double mznan = std::numeric_limits<double>::quiet_NaN();

FPIRenderArea::FPIRenderArea(QWidget *parent) :
    QScrollArea(parent)
{
}

void FPIRenderArea::wheelEvent(QWheelEvent *ev)
{
    if(widget() == NULL)
        return;
    FPIRenderer* renderer = qobject_cast<FPIRenderer*>(widget());
    if(renderer != NULL)
    {
        if(QApplication::keyboardModifiers() & Qt::ControlModifier)
        {
            renderer->wheelEvent(ev);
        }
        else
        {
            QScrollArea::wheelEvent(ev);
        }
    }
}

void FPIRenderArea::wheelEventSuperclass(QWheelEvent *ev)
{
    QScrollArea::wheelEvent(ev);
}

FPIRenderer::FPIRenderer()
{
    image = NULL;
    slice = 0;
    zoomfactor = 1.0;
//    realupdate = true;
    image_data = NULL;
    visibility = NORMAL;
    setPseudoPalette(0);
}

FPIRenderer::~FPIRenderer()
{
//    if(image != NULL) delete[] image;
    if(image_data != NULL) delete[] image_data;
}

void FPIRenderer::setSlice(unsigned int s)
{
    slice = s;
    convertImage();
}
void FPIRenderer::setMinBrightness(double v)
{
    if(image!=NULL) image->minbrightness = v;
    convertImage();
}
void FPIRenderer::setMaxBrightness(double v)
{
    if(image!=NULL) image->maxbrightness = v;
    convertImage();
}
void FPIRenderer::setMinWindow(double v)
{
    if(image!=NULL) image->minwindow = v;
    convertImage();
}
void FPIRenderer::setMaxWindow(double v)
{
    if(image!=NULL) image->maxwindow = v;
    convertImage();
}
double FPIRenderer::getMaxValue(void)
{
    if(image==NULL) return 0.0;
    return image->maxvalue;
}
double FPIRenderer::getMinValue(void)
{
    if(image==NULL) return 0.0;
    return image->minvalue;
}

double FPIRenderer::getMinBrightness(void)
{
    if(image==NULL) return 0.0;
    return image->minbrightness;
}
double FPIRenderer::getMaxBrightness(void)
{
    if(image==NULL) return 0.0;
    return image->maxbrightness;
}
double FPIRenderer::getMinWindow(void)
{
    if(image==NULL) return 0.0;
    return image->minwindow;
}
double FPIRenderer::getMaxWindow(void)
{
    if(image==NULL) return 0.0;
    return image->maxwindow;
}

void FPIRenderer::setImagePointer(FloatImage* image)
{
    this->image = image;
    convertImage();
}

//double FPIRenderer::setZoom(int zoomlevel)
//{
//    if(pixmap.isNull()) return 0.0;
//    zoom_factor = pow(2.0, (double) zoomlevel/8.0);
//    realupdate = true;
//    resize(zoom_factor * pixmap.size());
//    return zoom_factor;
//}
//double FPIRenderer::setZoom(int z)
//{
//    if(screen_image.isNull())
//    {
//        zoomlevel = 0;
//        return 0.0;
//    }
//    zoomlevel = z;
//    if(zoomlevel > 256) zoomlevel = 256;
//    if(zoomlevel < -256) zoomlevel = -256;
//    zoomfactor = pow(2.0, (double) zoomlevel/64.0);
//    resize(zoomfactor * screen_image.size());
//    return zoomfactor;
//}
void FPIRenderer::SetZoom(double zoom)
{
    QWidget* papa = parentWidget();
    if(papa != NULL)
    {
        papa = papa->parentWidget();
        if(papa != NULL)
        {
            QScrollArea* pasa = qobject_cast<QScrollArea*>(papa);
            if(pasa != NULL)
            {
                int x, y;
                QScrollBar* sbh = pasa->horizontalScrollBar();
                QScrollBar* sbv = pasa->verticalScrollBar();
                if(sbh != NULL)
                {
                    int sw = sbh->value();
                    x = (sw + (width() - sbh->maximum())/2);
                    x = sw + (x/zoomfactor - x/zoom)*zoom;
                }
                if(sbh != NULL)
                {
                    int sh = sbv->value();
                    y = (sh + (height() - sbv->maximum())/2);
                    y = sh + (y/zoomfactor - y/zoom)*zoom;
                }
                resize(zoom * screen_image.size());
                if(sbh != NULL)
                    sbh->setValue(x);
                if(sbh != NULL)
                    sbv->setValue(y);
                zoomfactor = zoom;
            }
        }
    }
}

void FPIRenderer::setImageVisibility(ImageVisibilityMode ivisibility)
{
    visibility = ivisibility;
    convertImage();
}

void FPIRenderer::toByteBuffer(FloatImage* image, uint8_t** buffer, ImageVisibilityMode visibility, unsigned int slice)
{
    unsigned int size[FloatImage::Dimensions];
    image->GetSize(size);


    unsigned int strade = ((size[0]+3)>>2);
    uint32_t* image_data = new uint32_t[strade*size[1]];
    *buffer = (uint8_t*)image_data;
    double pix;

    double minbrightness = image->minbrightness;
    double maxbrightness = image->maxbrightness;
    double minwindow = image->minwindow;
    double maxwindow = image->maxwindow;

    double dif1 = (maxwindow - minwindow);
    double mul1 = dif1 != 0 ? (double)256/dif1 : 0;
    double dif2 = (maxbrightness - minbrightness);
    double med2 = (maxbrightness + minbrightness) / 2.0;
    double mul2 = dif2 != 0 ? (double)dif2/256 : 0;

    unsigned int coor[FloatImage::Dimensions];
    coor[0] = 0; coor[1] = 0;
    for(unsigned int d = 2; d < FloatImage::Dimensions; d++)
    {
        if(slice < size[d]) coor[d] = slice;
        else coor[d] = size[d]/2;
    }
    FloatImage::PixelType* ptrif32 = (FloatImage::PixelType*) image->GetPixelPointer(coor);
    switch(visibility)
    {
    case OFF: break;
    case NORMAL:

        for(unsigned int y = 0; y < size[1]; y++)
        {
            uint8_t* ptro = (uint8_t*) (image_data+strade*y);
            for(unsigned int x = 0; x < size[0]; x++, ptro++, ptrif32++)
            {
                pix = *ptrif32;
                if(!(pix == pix))
                {
                   *ptro = 255;
                }
                else
                {
                    if(dif1 != 0)
                    {
                        pix = (pix - minwindow) * mul1;
                        pix = (pix > 255 ? 255 :(pix < 0 ? 0 : pix));
                    }
                    else
                    {
                        pix = (pix > minwindow) ? 255 : 0;
                    }
                    pix *= mul2;
                    pix += minbrightness;
                    *ptro = (uint8_t)pix;
                }
            }
        }
        break;

    case THREELEVEL:
        for(unsigned int y = 0; y < size[1]; y++)
        {
            uint8_t* ptro = (uint8_t*) (image_data+strade*y);
            for(unsigned int x = 0; x < size[0]; x++, ptro++, ptrif32++)
            {
                pix = *ptrif32;
                if(!(pix == pix))
                {
                   *ptro = 255;
                }
                else
                {
                    if(dif1 != 0)
                    {
                        pix = (pix - minwindow) * mul1;
                        pix = (pix > 255 ? maxbrightness :(pix < 0 ? minbrightness : med2));
                    }
                    else
                    {
                        pix = (pix > minwindow) ? 255 : 0;
                    }
                    pix *= mul2;
                    pix += minbrightness;
                    *ptro = (uint8_t)pix;
                }
            }
        }
        break;
    }
}

void FPIRenderer::resetRanges(FloatImage *image)
{
    double min, max;
    FloatImage::PixelType* ptrif32 = (FloatImage::PixelType*) image->GetDataPointer();
    unsigned int size[FloatImage::Dimensions];
    image->GetSize(size);
    size_t count = 1;
    for(unsigned int d = 0; d < FloatImage::Dimensions; d++)
        count *= size[d];
    min = mznan;
    max = mznan;
    for(size_t x = 0; x < count; x++, ptrif32++)
    {
        if(!(min == min)) min = *ptrif32;
        if(!(max == max)) max = *ptrif32;
        if(min > *ptrif32) min = *ptrif32;
        if(max < *ptrif32) max = *ptrif32;
    }
    image->minvalue =  min;
    image->maxvalue = max;
    image->minbrightness = 0;
    image->maxbrightness = 255;
    image->minwindow = min;
    image->maxwindow = max;
}

void FPIRenderer::convertImage(void)
{
    if(image != NULL)
    {
        unsigned int size[FloatImage::Dimensions];
        image->GetSize(size);
        //unsigned int pixels = image->width * image->height;
        if(image_data != NULL) delete[] image_data;
        image_data = NULL;
        toByteBuffer(image, &image_data, visibility, slice);
        screen_image = QImage((uint8_t*)image_data, size[0], size[1], QImage::Format_Indexed8);
        screen_image.setColorTable(colorTable);
//        qimage.setColorTable(colorTable);
//        pixmap = QPixmap::fromImage(qimage);
        resize(zoomfactor * size[0], zoomfactor * size[1]);
//        delete[] data;
    }
    else
    {
        screen_image = QImage();
    }

    //realupdate = true;
    update();
}

void FPIRenderer::setPseudoPalette(int p)
{
    switch(p)
    {
    case 4: //pseudo steps
    {
        colorTable.clear();
        for(int i = 0; i < 32; ++i) colorTable.append(qRgb(0, 0, 0));
        for(int i = 0; i < 32; ++i) colorTable.append(qRgb(0, 0, 255));
        for(int i = 0; i < 32; ++i) colorTable.append(qRgb(255, 0, 0));
        for(int i = 0; i < 32; ++i) colorTable.append(qRgb(255, 0, 255));
        for(int i = 0; i < 32; ++i) colorTable.append(qRgb(0, 255, 0));
        for(int i = 0; i < 32; ++i) colorTable.append(qRgb(0, 255, 255));
        for(int i = 0; i < 32; ++i) colorTable.append(qRgb(255, 255, 0));
        for(int i = 0; i < 31; ++i) colorTable.append(qRgb(255, 255, 255));
        colorTable.append(qRgb(63, 0, 0));
    }break;

    case 3: //pseudo thermal inverted
    {
        colorTable.clear();
        for(int i = 41; i >= 0; --i) colorTable.append(qRgb(255, 5.93*i, 255));
        for(int i = 42; i >= 0; --i) colorTable.append(qRgb(255, 0, 5.93*i));
        for(int i = 42; i >= 0; --i) colorTable.append(qRgb(255, 255-6.07*i, 0));
        for(int i = 41; i >= 0; --i) colorTable.append(qRgb(5.93*i, 255, 255-5.93*i));
        for(int i = 42; i >= 0; --i) colorTable.append(qRgb(0, 5.93*i, 255));
        for(int i = 41; i >= 0; --i) colorTable.append(qRgb(0, 0, 6.07*i));
        colorTable.append(qRgb(63, 0, 0));
    }break;
    case 2: //pseudo thermal
    {
        colorTable.clear();
        for(int i = 0; i < 42; ++i) colorTable.append(qRgb(0, 0, 6.07*i));
        for(int i = 0; i < 43; ++i) colorTable.append(qRgb(0, 5.93*i, 255));
        for(int i = 0; i < 43; ++i) colorTable.append(qRgb(5.93*i, 255, 255-5.93*i));
        for(int i = 0; i < 42; ++i) colorTable.append(qRgb(255, 255-6.07*i, 0));
        for(int i = 0; i < 43; ++i) colorTable.append(qRgb(255, 0, 5.93*i));
        for(int i = 0; i < 42; ++i) colorTable.append(qRgb(255, 5.93*i, 255));
        colorTable.append(qRgb(63, 0, 0));
    }break;
    case 1: //grayscale negative
    {
        colorTable.clear();
        for(int i = 254; i >= 0; --i) colorTable.append(qRgb(i, i, i));
        colorTable.append(qRgb(63, 0, 0));
    }break;
    default: //grayscale
    {
        colorTable.clear();
        for(int i = 0; i < 255; ++i) colorTable.append(qRgb(i, i, i));
        colorTable.append(qRgb(63, 0, 0));
    }
    }
    if(!screen_image.isNull())
    {
        screen_image.setColorTable(colorTable);
        update();
    }
}

void FPIRenderer::mousePressEvent(QMouseEvent *ev)
{
    if(image == NULL) return;
    unsigned int x = (_ev_position_x / zoomfactor);
    unsigned int y = (_ev_position_y / zoomfactor);

    unsigned int size[FloatImage::Dimensions];
    image->GetSize(size);
    if(x >= size[0] || y >= size[1]) return;

    unsigned int coor[FloatImage::Dimensions];
    coor[0] = 0; coor[1] = 0;
    for(unsigned int d = 2; d < FloatImage::Dimensions; d++)
    {
        if(slice < size[d]) coor[d] = slice;
        else coor[d] = size[d]/2;
    }
    double v = *(((FloatImage::PixelType*) image->GetPixelPointer(coor))+y*size[0]+x);
    QString com;
    if(FloatImage::Dimensions > 2)
    {
        if(size[2] > 0) com = QString("%1(%2, %3, %4) = %5").arg(image->GetName().c_str()).arg(x).arg(y).arg(coor[2]).arg(v);
        else com = QString("%1(%2, %3) = %4").arg(image->GetName().c_str()).arg(x).arg(y).arg(v);
    }
    else
        com = QString("%1(%2, %3) = %4").arg(image->GetName().c_str()).arg(x).arg(y).arg(v);
    emit mousePressed(com);
    ev->accept();
}

void FPIRenderer::mouseMoveEvent(QMouseEvent *ev)
{
    if(ev->buttons() & Qt::LeftButton) mousePressEvent(ev);
}


//void FPIRenderer::wheelEvent(QWheelEvent *ev)
//{
//    if(QApplication::keyboardModifiers() & Qt::ControlModifier)
//    {
//        ev->accept();
//        setZoom(ev->delta() > 0 ? zoomlevel+2 : zoomlevel-2);
//        emit setZoomRequest(zoomlevel);
//    }
//    else ev->ignore();
//}


void FPIRenderer::zoomKeepLocation(int x, int y, double previous_zoom, double current_zoom)
{
    if(screen_image.isNull())
        return;
    QWidget* papa = parentWidget();
    if(papa == NULL)
        return;
    papa = papa->parentWidget();
    if(papa == NULL)
        return;
    QScrollArea* pasa = qobject_cast<QScrollArea*>(papa);
    if(pasa == NULL)
        return;
    QScrollBar* sb = pasa->horizontalScrollBar();
    if(sb != NULL)
        sb->setValue(sb->value() + (x/previous_zoom - x/current_zoom)*current_zoom);
    sb = pasa->verticalScrollBar();
    if(sb != NULL)
        sb->setValue(sb->value() + (y/previous_zoom - y/current_zoom)*current_zoom);
}

void FPIRenderer::wheelEvent(QWheelEvent *ev)
{
    if(QApplication::keyboardModifiers() & Qt::ControlModifier)
    {
        double previous_zoom = zoomfactor;
        ev->accept();
        if(ev->angleDelta().y() > 0)
        {
            if(zoomfactor < 13.34)
            {
                zoomfactor *= 1.2;
                if(zoomfactor > 0.9 && zoomfactor < 1.1)
                    zoomfactor = 1.0;
                if(! screen_image.isNull())
                {
                    resize(zoomfactor * screen_image.size());
                    zoomKeepLocation(ev->position().x(), ev->position().y(), previous_zoom, zoomfactor);
                }
                emit setZoomRequest(zoomfactor);
                //update();
            }
        }
        else if(ev->angleDelta().y() < 0)
        {
            if(zoomfactor > 0.075)
            {
                zoomfactor /= 1.2;
                if(zoomfactor > 0.9 && zoomfactor < 1.1)
                    zoomfactor = 1.0;
                if(! screen_image.isNull())
                {
                    zoomKeepLocation(ev->position().x(), ev->position().y(), previous_zoom, zoomfactor);
                    resize(zoomfactor * screen_image.size());
                }
                emit setZoomRequest(zoomfactor);
                //update();
            }
        }
    }
    else
    {
        QWidget* papa = parentWidget();
        if(papa == NULL)
            return;
        papa = papa->parentWidget();
        if(papa == NULL)
            return;
        FPIRenderArea* pasa = qobject_cast<FPIRenderArea*>(papa);
        if(pasa == NULL)
            return;
        pasa->wheelEventSuperclass(ev);
    }
}



void FPIRenderer::paintEvent(QPaintEvent *e)
{
    QPainter painter(this);
    if(zoomfactor == 1.0)
    {
        painter.drawImage(screen_image.rect(), screen_image);
    }
    else
    {
        painter.drawImage(contentsRect(), screen_image);
    }
    e->accept();
}

//For reference https://qt.gitorious.org/qt/qt-gberg/source/8af3500125a03a54dddb8c46ee709b7b8d257f27:src/gui/widgets/qlabel.cpp
//    QStyle *style = QWidget::style();
//    QPainter painter(this);
//    QRect cr = contentsRect();
//    int align = QStyle::visualAlignment(layoutDirection(), QFlag(align));

//    if (!pixmap.isNull())
//    {
//        if(realupdate || scaledpixmap.isNull())
//        {
//            QImage image = QImage(pixmap.toImage());
//            scaledpixmap = QPixmap(QPixmap::fromImage(image.scaled(cr.size(),Qt::IgnoreAspectRatio,Qt::SmoothTransformation)));
//            realupdate = false;
//        }
//        style->drawItemPixmap(&painter, cr, align, scaledpixmap);
//    }

/*
    QPainter painter(this);
    QRect viewport = geometry();
    painter.drawPixmap(viewport, pixmap);
*/

