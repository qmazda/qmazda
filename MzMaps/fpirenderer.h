/*
 * qMaZda - Image Analysis and Pattern Recognition
 *
 * Copyright 2013-2014 Piotr M. Szczypiński <piotr.szczypinski@p.lodz.pl>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef FPIRENDERER_H
#define FPIRENDERER_H

#include <QObject>
#include <QWidget>
#include <tiffio.h>
#include <string.h>
#include <QMouseEvent>
#include <QPainter>
#include <QPaintEvent>
#include <QStyle>
#include <QStyleOption>
#include <QScrollArea>

#include <math.h>
//#include "../MzShared/mzselectioninterface.h"
#include "../MzShared/dataforconsole.h"
//#include "../MzShared/mzmapio.h"
#include "../SharedImage/mazdaimage.h"
#include "../MzShared/qt5qt6compatibility.h"

//typedef enum {UINT8, INT8, UINT16, INT16, FLOAT32, FLOAT64} ImageType;
typedef enum {NORMAL, THREELEVEL, OFF} ImageVisibilityMode;

typedef MazdaImage<MazdaMapPixelType, 3> MzMap;

class FloatImage : public MzMap
{
//    void* imageptr;
//    ImageType type;
public:
    FloatImage(const unsigned int size[MzMap::Dimensions], const double spacing[MzMap::Dimensions]):MzMap(size, spacing){}
    FloatImage(const unsigned int size[MzMap::Dimensions], const double spacing[MzMap::Dimensions], MzMap::PixelType* data):MzMap(size, spacing, data){}

//    ~FloatImage():~MzMap(){}

//    unsigned int width(){return GetSizeD(0);}
//    unsigned int height(){return GetSizeD(1);}
//    unsigned int depth(){return GetSizeD(2);}
    double minvalue;
    double maxvalue;
    double minbrightness;
    double maxbrightness;
    double minwindow;
    double maxwindow;
//    std::string name;
//    ImageVisibilityMode visibility;
//    IMAGE_VISIBILITY visibility;
//    IMAGE_PALETTE pallette;
};

class FPIRenderArea : public QScrollArea
{
    Q_OBJECT
public:
    explicit FPIRenderArea(QWidget *parent = 0);
    void wheelEvent(QWheelEvent *ev);
    void wheelEventSuperclass(QWheelEvent *ev);
signals:
public slots:
};


class FPIRenderer : public QWidget
{
    Q_OBJECT
public:
    FPIRenderer();
    ~FPIRenderer();
    void wheelEvent(QWheelEvent *ev);
    void setSlice(unsigned int s);
    void setMinBrightness(double v);
    void setMaxBrightness(double v);
    void setMinWindow(double v);
    void setMaxWindow(double v);
    double getMinBrightness(void);
    double getMaxBrightness(void);
    double getMinWindow(void);
    double getMaxWindow(void);
    double getMinValue(void);
    double getMaxValue(void);
    void setImagePointer(FloatImage *image);
    //double setZoom(int z);
    void SetZoom(double zoom);
    void setPseudoPalette(int p);
    void setImageVisibility(ImageVisibilityMode ivisibility);

    static void resetRanges(FloatImage *image);
    static void toByteBuffer(FloatImage* image, uint8_t **buffer, ImageVisibilityMode visibility, unsigned int slice);
    QVector<QRgb> colorTable;

signals:
    void setZoomRequest(double);
    void mousePressed(QString);

private:
    unsigned int slice;
    ImageVisibilityMode visibility;
    void convertImage(void);
    void mouseMoveEvent(QMouseEvent *ev);
    void mousePressEvent(QMouseEvent *ev);
    void paintEvent(QPaintEvent *e);
    void zoomKeepLocation(int x, int y, double previous_zoom, double current_zoom);
//    int pallette;
    //int zoomlevel;
    double zoomfactor;
    FloatImage* image;
//    QPixmap pixmap;
//    QPixmap scaledpixmap;
    uint8_t* image_data;
    QImage screen_image;
   // bool realupdate;
};

#endif // FPIRENDERER_H
