/*
 * qMaZda - Image Analysis and Pattern Recognition
 *
 * Copyright 2013-2014 Piotr M. Szczypiński <piotr.szczypinski@p.lodz.pl>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QLineEdit>
#include <QList>
#include <QDropEvent>
#include <QMimeData>
#include <QScrollArea>
#include <QFileDialog>
#include <QScrollBar>
#include <QPluginLoader>
#include <QAction>
#include <QToolButton>

#include "fpirenderer.h"
#include "../MzShared/mzselectioninterface.h"
//#include "../MzShared/dataforconsole.h"
#include "../MzShared/progress.h"

namespace Ui {
class MainWindow;
}

//#include "../MzShared/PlugWorker.h"

class MzAction : public QAction
{
    Q_OBJECT
public:
    MzAction(const QString &text, QObject *parent) :
        QAction(text, parent) {}

    MzSelectionPluginInterface* interfaceMzSelectionPluginInterface;
    unsigned int indexToOnActionCall;
};


class PlugWorker : public QObject
{
    Q_OBJECT
public:
    explicit PlugWorker(void* plugin_object,
                        MzThreadStaticPluginFunction threaded_plugin) :
        QObject(NULL)
    {
        this->plugin_object = plugin_object;
        this->threaded_plugin = threaded_plugin;
    }
    ~PlugWorker()
    {}
    static void static_notify_step(void* object_this)
    {
        emit ((PlugWorker*)object_this)->notify_progress();
    }
    static void static_notify_text(void* object_this, std::string text)
    {
        emit ((PlugWorker*)object_this)->notify_progress(QString::fromStdString(text));
    }
public slots:
    void process()
    {
        threaded_plugin(plugin_object, this, static_notify_step, static_notify_text);
        emit finished(true);
        emit finished();
    }
signals:
    void finished();
    void finished(bool);
    void notify_progress();
    void notify_progress(QString text);

private:
    void* plugin_object;
    MzThreadStaticPluginFunction threaded_plugin;
};



class MainWindow : public QMainWindow, MzPullDataInterface, MzGuiRelatedInterface
{
    Q_OBJECT
//    Q_INTERFACES(MzPullDataInterface)
    bool getData(DataForSegmentation* data);
    bool getData(std::vector<std::string>* featureNames, DataForSelection* data);
    bool getData(DataForSelection* data);
    bool setSelection(SelectedFeatures* selected_features);
    bool setData(DataForSelection* data, SelectedFeatures* selected_features);

//    Q_INTERFACES(MzGuiRelatedInterface)
    void InitiateAnalysisActions(void *plugin_object,
                                 MzBeforeStaticPluginFunction before_plugin,
                                 MzThreadStaticPluginFunction threaded_plugin,
                                 MzAfterStaticPluginFunction after_plugin);

    bool selectClassifiers(std::vector<std::string>* classifierNames, std::vector<bool>* selectionResult, string title);
    bool openProgressDialog(int maxTime, mz_uint64 maxSteps, MzSelectionPluginInterface* object, string title);
    void closeProgressDialog(void);
    bool openOptionsDialog(std::string title);
    void addGroupOptionsDialog(std::string property, std::string tip);
    void addPropertyOptionsDialog(std::string property, std::string value, std::string tip);
    void getValueOptionsDialog(std::string property, std::string* value);
    void closeOptionsDialog(void);
    void showTestResults(std::string* confusionTable, string title);
    void showAbout(std::string title, std::string text);
    void showMessage(std::string title, std::string text, unsigned int iconType);
    bool getOpenFile(std::string* fileName);
    bool getSaveFile(std::string* fileName, unsigned int* filter);
    void* addMenuAction(const char *name, const char *tip, const unsigned int index);
    void menuEnable(void* menu, bool enable);

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();
    bool setData(DataForSegmentation* data);

protected:
    void dragEnterEvent(QDragEnterEvent* event)
    {
        event->acceptProposedAction();
    }

    void dragMoveEvent(QDragMoveEvent* event)
    {
        event->acceptProposedAction();
    }

    void dragLeaveEvent(QDragLeaveEvent* event)
    {
        event->accept();
    }

    void dropEvent(QDropEvent* event);

public slots:
    //void rendererSizeChanged(int);
    void setZoomRequest(double);
    void onMousePressed(QString txt);

private slots:
    void zoomSliderValueChanged(int value);
    void zoomResetClicked();

    void on_actionLoadImage_triggered();

    void on_mapNamesListWidget_currentRowChanged(int currentRow);
    void on_spinBoxMin_valueChanged(int arg1);

    void on_spinBoxMax_valueChanged(int arg1);

    void on_spinBoxDark_valueChanged(double arg1);

    void on_horizontalSliderMax_valueChanged(int value);

    void on_spinBoxBright_valueChanged(double arg1);

    void on_horizontalSliderMin_valueChanged(int value);

    void on_actionZoom_in_triggered();

    void on_actionZoom_out_triggered();

    void on_actionZoom_1_triggered();

    //void on_zoomReset_clicked();

    //void on_horizontalSliderZoom_valueChanged(int value);

    void on_comboBox_currentIndexChanged(int index);

    void on_removeImageButton_clicked();

    void on_ImageGrayButton_clicked();

    void on_imageLevelsButton_clicked();

    void on_actionSaveImage_triggered();

    void on_actionContents_triggered();

    void on_actionAbout_triggered();


    void on_finished(bool success);
    void notifyProgressSlot();
    void notifyProgressSlot(QString text);

    void on_actionExport_image_triggered();

    void on_horizontalSliderZ_valueChanged(int value);

    void pluginMenuTriggered();

    void on_actionSingle_thread_toggled(bool);

private:
    Progress* progressdialog;
    //bool setZoomRequested;
    int zoom_level;
    QSlider zoomSlider;
    QToolButton zoomReset;
    uint32_t* image_data;
    int loadImage(const char *filename);
    bool loadImage(QString* fileName);
    bool saveImage(const char* filename);
    bool exportImage(const char* filename);
    QImage toQImage(FloatImage* image);
    QPixmap toIcon(FloatImage* image);
    void ClearData(void);

    void adjustScrollBar(QScrollBar *scrollBar, double factor);
    void assimilateImage(void);
    std::vector<FloatImage*> fmaps;
    FPIRenderArea *scrollArea;
    FPIRenderer* renderer;
    void loadPlugins(void);
    bool initiatePlugins(void);
    QList<MzSelectionPluginInterface*> selectionplugins;
    MzSelectionPluginInterface* selectionplugin;
    Ui::MainWindow *ui;
    void* plugin_object;
    MzAfterStaticPluginFunction after_plugin;
    DataForSegmentation dataCopy;
    QMenu* plugin_menu;
//    QString getExecutableWithPath(const char* name);
};

#endif // MAINWINDOW_H
