/*
 * qMaZda - Image Analysis and Pattern Recognition
 * 
 * Copyright 2013  Piotr M. Szczypiński <piotr.szczypinski@p.lodz.pl>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <limits>
#include <sstream>

#include "ldaselection.h"

LdaSelectionReduction::LdaSelectionReduction()
{
    featureReindexLut = NULL;
    outClassIndices = NULL;
    classFireRequired = NULL;
    normal_plus = NULL;
    normal_multi = NULL;
    computeclassifier = false;
    classifier = NULL;
    data = NULL;
//    classtodistinguish = -1;
    report = NULL;
}
LdaSelectionReduction::~LdaSelectionReduction()
{
    if(featureReindexLut != NULL) delete[] featureReindexLut;
    featureReindexLut = NULL;
    if(outClassIndices != NULL) delete[] outClassIndices;
    outClassIndices = NULL;
    if(classFireRequired != NULL) delete[] classFireRequired;
    classFireRequired = NULL;

    if(normal_plus != NULL) free(normal_plus);
    normal_plus = NULL;
    if(normal_multi != NULL) free(normal_multi);
    normal_multi = NULL;
}

//bool LdaSelectionReduction::SetOption(const char* property, const char* value)
//{
//    std::string sproperty = property;
//    std::string svalue = value;
//    char* end;
//    if(sproperty == "Time")
//        maxtime = strtol(svalue.c_str(), &end, 10);
//    else if(sproperty == "Dimesvalueionality")
//        dimensions = strtol(svalue.c_str(), &end, 10);
//    else if(sproperty == "Normalization")
//    {
//        if(svalue == "none") normalizeoption = NORMNONE;
//        else if(svalue == "minmax") normalizeoption = NORMALIZE;
//        else normalizeoption = STANDARDIZE;
//    }
//    else if(sproperty == "Threshold")
//    {
//        if(svalue == "accuracy") threshold_algorithm = THRESHOLD_FROM_ACCURACY;
//        else if(svalue == "balanced") threshold_algorithm = THRESHOLD_FROM_BALANCED_ACCURACY;
//        else threshold_algorithm = THRESHOLD_FROM_STANDARD_DEV;
//    }
//    else if(sproperty == "Direction")
//    {
//        if(svalue == "means") direction_algorithm = DIRECTION_FROM_MEANS;
//        else direction_algorithm = DIRECTION_FROM_LDA;
//    }
//    else
//        return false;
//    return true;
//}
//bool LdaSelectionReduction::saveClassifierToFile(const char* filename)
//{
//    return classifier->saveClassifier(filename, false);
//}


void LdaSelectionReduction::NotifyProgressText(std::string)
{
}
void LdaSelectionReduction::NotifyProgressStep(void)
{
}
void LdaSelectionReduction::NotifyProgressStage(const int, const int, const double, const unsigned int*)
{
}

void LdaSelectionReduction::setReportStream(ofstream* report)
{
    this->report = report;
}
void LdaSelectionReduction::setParameters(int dimensions, int maxtime)
{
    this->dimensions = dimensions;
    this->maxtime = maxtime;
}
void LdaSelectionReduction::setClassifier(Classifiers *classifier)//, int classtodistinguish)
{
    this->classifier = classifier;
//    this->classtodistinguish = classtodistinguish;
}
void LdaSelectionReduction::setInputData(DataForSelection* data)
{
    this->data = data;
    switch(normalizeoption)
    {
    case STANDARDIZE: Standardize(); break;
    case NORMALIZE: NormalizeMinMax(); break;
    }
}
//void LdaSelectionReduction::setMapBuffer(MapsForSegmentation* input, MapsForSegmentation* output)
//{
//    this->inmaps = input;
//    this->outmaps = output;
//}
void LdaSelectionReduction::setOutputBuffers(double* Qtable, unsigned int* Qsorted)
{
    this->Qtable = Qtable;
    this->Qsorted = Qsorted;
}


//------------------------------------------------------------------------------
void LdaSelectionReduction::NormalizeMinMax(void)
{
    double min, max, d;
    int f, v;

    if(normal_plus) free(normal_plus);
    if(normal_multi) free(normal_multi);
    normal_plus = (double*)malloc(sizeof(double) * data->featurenumber);
    normal_multi = (double*)malloc(sizeof(double) * data->featurenumber);

    for(f = 0; f < data->featurenumber; f++)
    {
        v = 0;
        min = max = data->values[data->featurenumber * v + f];
        for(v = 1; v < data->vectornumber; v++)
        {
            d = data->values[data->featurenumber * v + f];

            if(d > max) max = d;
            if(d < min) min = d;
        }
        normal_plus[f] = -(max+min)/2.0;
        max -= min;
        if(max > machineepsilon) max = 2.0/max;
        else max = 1.0;
        normal_multi[f] = max;
        min = normal_plus[f];

        for(v = 0; v < data->vectornumber; v++)
        {
            d = data->values[data->featurenumber * v + f];
            d += min; d *= max;
            data->values[data->featurenumber * v + f] = d;
        }
    }
}

//------------------------------------------------------------------------------
void LdaSelectionReduction::Standardize(void)
{
    double mean, stdd, d;
    int f, v;

    if(normal_plus) free(normal_plus);
    if(normal_multi) free(normal_multi);
    normal_plus = (double*)malloc(sizeof(double) * data->featurenumber);
    normal_multi = (double*)malloc(sizeof(double) * data->featurenumber);

    for(f = 0; f < data->featurenumber; f++)
    {
        mean = 0;
        stdd = 0;
        for(v = 0; v < data->vectornumber; v++)
        {
            d = data->values[data->featurenumber * v + f];
            mean += d;
            stdd += (d * d);
        }
        mean /= data->vectornumber;
        stdd /= data->vectornumber;
        stdd -= (mean * mean);
        stdd = sqrt(stdd);

        normal_plus[f] = -mean;
        if(stdd > machineepsilon) stdd = 1.0/stdd;
        else stdd = 1.0;
        normal_multi[f] = stdd;

        for(v = 0; v < data->vectornumber; v++)
        {
            d = data->values[data->featurenumber * v + f];
            d -= mean; d *= stdd;
            data->values[data->featurenumber * v + f] = d;
        }
    }
}
/*
//------------------------------------------------------------------------------
inline double LdaSelectionReduction::GoalFunction1(unsigned int dim, unsigned int* picked)
{
    unsigned int v, f;
    int c;
    ae_int_t info;

    double discriminant;
    double miu[2];
    double std[2];
    real_2d_array xy;
    real_1d_array w;
    real_1d_array mdf;
    double miuall = 0.0;
    double varall = 0.0;
    double varcla = 0.0;
    double miuloc = 0.0;
    double varloc = 0.0;
    unsigned int vs, vm;

    if(dim == 1)
    {
        mdf.setlength(data->vectornumber);
        unsigned int vm = data->vectornumber;
        for(v = 0; v < vm; v++)
        {
            mdf(v) = data->values[data->featurenumber * v + picked[0]];
        }
    }
    else
    {
        xy.setlength(data->vectornumber, dim+1);
        w.setlength(dim);
        mdf.setlength(data->vectornumber);

        vs = 0;
        for(c = 0; c < data->classnumber; c++)
        {
            vm = data->classendvectorindex[c];
            for(v = vs; v < vm; v++)
            {
                for(f = 0; f < dim; f++)
                {
                    xy(v, f) = data->values[data->featurenumber * v + picked[f]];
                }
                xy(v, dim) = (c == classtodistinguish ? 0 : 1);
            }
            vs = vm;
        }

        fisherlda(xy, data->vectornumber, dim, data->classnumber, info, w);
        if(info < 0) return -1.0;

        rmatrixmv(data->vectornumber, dim, xy, 0, 0, 0, w, 0, mdf, 0);
    }

    vm = data->vectornumber;
    for(v = 0; v < vm; v++)
    {
        double f = mdf(v);
        miuall += f;
        varall += (f*f);
    }
    miuall /= vm;
    varall /= vm;
    varall -= (miuall * miuall);

    c = classtodistinguish;
    vs = c > 0 ? data->classendvectorindex[c-1] : 0;
    vm = data->classendvectorindex[c];
    for(v = vs; v < vm; v++)
    {
        double f = mdf(v);
        miuloc += f;
        varloc += (f*f);
    }
    miuloc /= (vm-vs);
    varloc /= (vm-vs);
    varloc -= (miuloc * miuloc);

    miu[0] = miuloc;
    std[0] = varloc;

    varcla += varloc;

    int div = data->vectornumber-vm+vs;
    miuloc = 0.0;
    varloc = 0.0;
    vs = 0;
    for(c = 0; c < data->classnumber; c++)
    {
        vm = data->classendvectorindex[c];
        if(c != classtodistinguish)
        {
            for(v = vs; v < vm; v++)
            {
                double f = mdf(v);
                miuloc += f;
                varloc += (f*f);
            }
        }
        vs = vm;
    }
    miuloc /= div;
    varloc /= div;
    varloc -= (miuloc * miuloc);

    miu[1] = miuloc;
    std[1] = varloc;

    varcla += varloc;

    discriminant = (2 * varall + machineepsilon) / (varcla + machineepsilon);

    if(computeclassifier)
    {
        std[0] = sqrt(std[0]);
        std[1] = sqrt(std[1]);
        if(dim == 1) StoreClassifier(dim, miu, std, picked, discriminant, NULL);
        else StoreClassifier(dim, miu, std, picked, discriminant, &w);
    }
    return discriminant;
}
*/





/*


//------------------------------------------------------------------------------
inline double LdaSelectionReduction::GoalFunctionNN(unsigned int dim, unsigned int* picked)
{
    unsigned int v, f;
    double discriminant;
    ae_int_t info;
    unsigned int vs, vm;
    unsigned int divider = 0;
    real_2d_array xy;
    real_1d_array w;
    real_1d_array mdf;

    double miu[2];
    double std[2];

    if(dim == 1)
    {
        double miuall = 0.0;
        double varall = 0.0;
        double varcla = 0.0;
        double miuloc = 0.0;
        double varloc = 0.0;

        vs = classtodistinguish1 > 0 ? data->classendvectorindex[classtodistinguish1-1] : 0;
        vm = data->classendvectorindex[classtodistinguish1];
        for(v = vs; v < vm; v++)
        {
            double f = data->values[data->featurenumber * v + picked[0]];//mdf(v);
            miuloc += f;
            varloc += (f*f);

            miuall += f;
            varall += (f*f);
        }
        miuloc /= (vm-vs);
        varloc /= (vm-vs);
        varloc -= (miuloc * miuloc);
        varcla += varloc;
        divider += (vm-vs);

        miu[1] = miuloc;
        std[1] = varloc;

        miuloc = 0.0;
        varloc = 0.0;
        vs = classtodistinguish2 > 0 ? data->classendvectorindex[classtodistinguish2-1] : 0;
        vm = data->classendvectorindex[classtodistinguish2];
        for(v = vs; v < vm; v++)
        {
            double f = data->values[data->featurenumber * v + picked[0]];//mdf(v);
            miuloc += f;
            varloc += (f*f);

            miuall += f;
            varall += (f*f);
        }
        miuloc /= (vm-vs);
        varloc /= (vm-vs);
        varloc -= (miuloc * miuloc);
        varcla += varloc;
        divider += (vm-vs);

        miu[0] = miuloc;
        std[0] = varloc;

        miuall /= divider;
        varall /= divider;
        varall -= (miuall * miuall);

        discriminant = (2 * varall + machineepsilon) / (varcla + machineepsilon);

        if(computeclassifier)
        {
            std[0] = sqrt(std[0]);
            std[1] = sqrt(std[1]);
            StoreClassifier(dim, miu, std, picked, discriminant, NULL);
        }

        return discriminant;
    }

    vs = classtodistinguish1 > 0 ? data->classendvectorindex[classtodistinguish1-1] : 0;
    vm = data->classendvectorindex[classtodistinguish1];
    divider += (vm-vs);
    vs = classtodistinguish2 > 0 ? data->classendvectorindex[classtodistinguish2-1] : 0;
    vm = data->classendvectorindex[classtodistinguish2];
    divider += (vm-vs);

    xy.setlength(divider, dim+1);
    w.setlength(dim);
    mdf.setlength(divider);

    int vv = 0;

    for(v = vs; v < vm; v++, vv++)
    {
        for(f = 0; f < dim; f++)
        {
            xy(vv, f) = data->values[data->featurenumber * v + picked[f]];
        }
        xy(vv, dim) = 1;
    }
    vs = classtodistinguish1 > 0 ? data->classendvectorindex[classtodistinguish1-1] : 0;
    vm = data->classendvectorindex[classtodistinguish1];
    for(v = vs; v < vm; v++, vv++)
    {
        for(f = 0; f < dim; f++)
        {
            xy(vv, f) = data->values[data->featurenumber * v + picked[f]];
        }
        xy(vv, dim) = 0;
    }

    fisherlda(xy, divider, dim, data->classnumber, info, w);
    if(info < 0) return -1.0;

    rmatrixmv(divider, dim, xy, 0, 0, 0, w, 0, mdf, 0);

    double miuall = 0.0;
    double varall = 0.0;
    double varcla = 0.0;
    double miuloc = 0.0;
    double varloc = 0.0;

    vv = 0;
    vs = classtodistinguish1 > 0 ? data->classendvectorindex[classtodistinguish1-1] : 0;
    vm = data->classendvectorindex[classtodistinguish1];
    for(v = vs; v < vm; v++, vv++)
    {
        double f = mdf(vv);
        miuloc += f;
        varloc += (f*f);

        miuall += f;
        varall += (f*f);
    }
    miuloc /= (vm-vs);
    varloc /= (vm-vs);
    varloc -= (miuloc * miuloc);
    varcla += varloc;

    miu[0] = miuloc;
    std[0] = varloc;

    miuloc = 0.0;
    varloc = 0.0;
    vs = classtodistinguish2 > 0 ? data->classendvectorindex[classtodistinguish2-1] : 0;
    vm = data->classendvectorindex[classtodistinguish2];
    for(v = vs; v < vm; v++, vv++)
    {
        double f = mdf(vv);
        miuloc += f;
        varloc += (f*f);

        miuall += f;
        varall += (f*f);
    }
    miuloc /= (vm-vs);
    varloc /= (vm-vs);
    varloc -= (miuloc * miuloc);
    varcla += varloc;

    miu[1] = miuloc;
    std[1] = varloc;

    miuall /= divider;
    varall /= divider;
    varall -= (miuall * miuall);
    discriminant = (2 * varall + machineepsilon) / (varcla + machineepsilon);

    if(computeclassifier)
    {
        std[0] = sqrt(std[0]);
        std[1] = sqrt(std[1]);
        StoreClassifier(dim, miu, std, picked, discriminant, &w);
    }
    return discriminant;
}


*/

/*
//------------------------------------------------------------------------------
inline double LdaSelectionReduction::GoalFunctionM(unsigned int dim, unsigned int *picked)
{
    unsigned int v, f;
    int c;
    double discriminant;
    ae_int_t info;
    ae_int_t ae_vectornumber = data->vectornumber;
    ae_int_t ae_dim = dim;
    ae_int_t ae_classnumber = data->classnumber;
    unsigned int vs;//, vm;

//    if(dim == 1)
//    {
//        double miuall = 0.0;
//        double varall = 0.0;
//        double varcla = 0.0;
//        unsigned int vm = data->vectornumber;
//        for(v = 0; v < vm; v++)
//        {
//            double f = data->values[data->featurenumber * v + picked[0]];//mdf(v);
//            miuall += f;
//            varall += (f*f);
//        }
//        miuall /= vm;
//        varall /= vm;
//        varall -= (miuall * miuall);

//        unsigned int vs = 0;
//        for(c = 0; c < data->classnumber; c++)
//        {
//            double miuloc = 0.0;
//            double varloc = 0.0;
//            unsigned int vm = data->classendvectorindex[c];
//            for(v = vs; v < vm; v++)
//            {
//                double f = data->values[data->featurenumber * v + picked[0]];//mdf(v);
//                miuloc += f;
//                varloc += (f*f);
//            }
//            miuloc /= (vm-vs);
//            varloc /= (vm-vs);
//            varloc -= (miuloc * miuloc);
//            varcla += varloc;
//            vs = vm;
//        }

//        discriminant = (data->classnumber * varall + machineepsilon) / (varcla + machineepsilon);

//        if(computeclassifier)
//        {
//            StoreClassifier(dim, NULL, NULL, picked, discriminant, NULL);
//        }
//        return discriminant;
//    }

    real_2d_array xy;
    real_2d_array w;
    //real_1d_array mdf;

//    if(dim == 1)
//    {
//        xy.setlength(data->vectornumber, dim+1);
//        //mdf.setlength(data->vectornumber);
//        w.setlength(dim, dim);
//        w(0, 0) = 1.0;
//        unsigned int vm = data->vectornumber;
//        for(v = 0; v < vm; v++)
//        {
//            mdf(v) = data->values[data->featurenumber * v + picked[0]];
//        }
//    }
//    else
    {
        xy.setlength(data->vectornumber, dim+1);
        w.setlength(dim, dim);
        //mdf.setlength(data->vectornumber);

        vs = 0;
        for(c = 0; c < data->classnumber; c++)
        {
            unsigned int vm = data->classendvectorindex[c];
            for(v = vs; v < vm; v++)
            {
                for(f = 0; f < dim; f++)
                {
                    xy(v, f) = data->values[data->featurenumber * v + picked[f]];
                }
                xy(v, dim) = c;
            }
            vs = vm;
        }

        fisherldan(xy, ae_vectornumber, ae_dim, ae_classnumber, info, w);
        if(info < 0) return -1.0;
        //rmatrixmv(ae_vectornumber, ae_dim, xy, 0, 0, 0, w, 0, mdf, 0);
    }

//    double miuall = 0.0;
//    double varall = 0.0;
//    double varcla = 0.0;
//    vs = 0;
//    for(c = 0; c < data->classnumber; c++)
//    {
//        double miuloc = 0.0;
//        double varloc = 0.0;
//        vm = data->classendvectorindex[c];
//        for(v = vs; v < vm; v++)
//        {
//            double f = mdf(v);
//            miuloc += f;
//            miuall += f;
//            f *= f;
//            varloc += f;
//            varall += f;
//        }
//        miuloc /= (vm-vs);
//        varloc /= (vm-vs);
//        varloc -= (miuloc * miuloc);
//        varcla += varloc;
//        vs = vm;
//    }
//    vm = data->vectornumber;
//    miuall /= vm;
//    varall /= vm;
//    varall -= (miuall * miuall);

//    discriminant = (data->classnumber * varall + machineepsilon)/ (varcla + machineepsilon);
    discriminant = 1.0;
    if(computeclassifier)
    {
        StoreProjectionMatrix(dim, picked, &xy, &w);
    }
    return discriminant;
}
*/




////------------------------------------------------------------------------------
//double LdaSelectionReduction::GoalFunction(unsigned int dim, unsigned int* picked)
//{
////    if(classtodistinguish < -1)
//    return GoalFunctionNN(dim, picked);
////    else if(classtodistinguish < 0) return GoalFunctionM(dim, picked);
////    else return GoalFunction1(dim, picked);
//}


//------------------------------------------------------------------------------
void LdaSelectionReduction::StoreProjectionMatrix(unsigned int dim, unsigned int* picked, real_2d_array* xy, real_2d_array* projection)
{
    unsigned int vs, vm, v;
    int c;
    double discriminant;
    real_1d_array w;
    real_1d_array mdf;
    mdf.setlength(data->vectornumber);
    w.setlength(dim);

    //ae_int_t info;
    ae_int_t ae_vectornumber = data->vectornumber;
    ae_int_t ae_dim = dim;
    //ae_int_t ae_classnumber = data->classnumber;

    for(int j = 0; j < ((int)dim < data->classnumber ? (int)dim : (int)data->classnumber); j++)
    {
        for(int i = 0; i < (int)dim; i++)  w(i) = (*projection)(i, j);

        rmatrixmv(ae_vectornumber, ae_dim, *xy, 0, 0, 0, w, 0, mdf, 0);

        double miuall = 0.0;
        double varall = 0.0;
        double varcla = 0.0;
        vs = 0;
        for(c = 0; c < data->classnumber; c++)
        {
            double miuloc = 0.0;
            double varloc = 0.0;
            vm = data->classendvectorindex[c];
            for(v = vs; v < vm; v++)
            {
                double f = mdf(v);
                miuloc += f;
                miuall += f;
                f *= f;
                varloc += f;
                varall += f;
            }
            miuloc /= (vm-vs);
            varloc /= (vm-vs);
            varloc -= (miuloc * miuloc);
            varcla += varloc;
            vs = vm;
        }
        vm = data->vectornumber;
        miuall /= vm;
        varall /= vm;
        varall -= (miuall * miuall);

        discriminant = (data->classnumber * varall + machineepsilon)/ (varcla + machineepsilon);
        StoreClassifier(dim, 0, false, picked, discriminant, &w);
    }
}


//------------------------------------------------------------------------------

void LdaSelectionReduction::sortValCla(double* val, bool* cla, real_1d_array* mdf, unsigned int br, unsigned int end)
{

    for(unsigned int v = 0; v < end; v++)
    {
        double dd = (*mdf)(v);
        unsigned int vv;
        for(vv = 0; vv < v; vv++)
        {
            if(dd < val[vv]) break;
        }
        for(unsigned int vvv = v; vvv > vv; vvv--)
        {
            val[vvv] = val[vvv-1];
            cla[vvv] = cla[vvv-1];
        }
        val[vv] = dd;
        cla[vv] = (v >= br);
    }
}

void LdaSelectionReduction::threshValCla(double* val, bool* cla, double* th, unsigned int br, unsigned int length, bool balanced)
{
    unsigned int negatives = br;
    unsigned int positives = length - br;

    int countpositives = negatives;
    int countnegatives = 0;
    double minacc = (double)length*length*length;
    double bayesdist = -1.0;
    unsigned int vbest = 0;

    for(unsigned int v = 0; v < length-1; v++)
    {
        double acc;
        if(cla[v]) countpositives--;
        else countnegatives++;

        if(balanced) acc = (double)countpositives*positives + (double)countnegatives*negatives;
        else acc = countpositives + countnegatives;

        if(acc < minacc)
        {
            bayesdist = fabs(val[v]-*th);
            minacc = acc;
            vbest = v;
        }
        else if(acc == minacc)
        {
            if(bayesdist > fabs(val[v]-*th))
            {
                bayesdist = fabs(val[v]-*th);
                minacc = acc;
                vbest = v;
            }
        }
    }

    *th = (val[vbest]+val[vbest+1])/2.0;

}

void LdaSelectionReduction::computeThreshold(double* threshold, bool* rev, double miu[2], double std[2], real_1d_array* mdf, unsigned int br)
{
//    double addthr = *threshold;


    *rev = false;
    *threshold = 0;
    if(miu == NULL || std == NULL)
    {
        *threshold = std::numeric_limits<double>::quiet_NaN();
    }
    else
    {
        *threshold = std[1]+std[0];
        if(*threshold != 0)
            *threshold = miu[0] + std[0]*(miu[1]-miu[0])/ *threshold;
        else
            *threshold = (miu[0] + miu[1])/2.0;
        *rev = (miu[0] < miu[1]);

        if(threshold_algorithm == THRESHOLD_FROM_ACCURACY || threshold_algorithm == THRESHOLD_FROM_BALANCED_ACCURACY)
        {
            unsigned int length = mdf->length();
            if(br >= length) return;
            double* val = new double[length];
            bool* cla = new bool[length];
            sortValCla(val, cla, mdf, br, length);







//            printf("==================================\n");
//            for(int k = 0; k < length; k++)
//            {
//                printf("%.3i) %i  %f\t%f\n", k, cla[k], val[k], val[k]+addthr);
//            }
//            fflush(stdout);










            threshValCla(val, cla, threshold, br, length, (threshold_algorithm == THRESHOLD_FROM_BALANCED_ACCURACY));
            delete[] cla;
            delete[] val;
        }
    }
}


void LdaSelectionReduction::StoreClassifier(unsigned int dim, double threshold, bool rev, unsigned int* picked, double discriminant, real_1d_array* projection)
{
    bool nor = (normal_plus != NULL && normal_multi != NULL);
    unsigned int f;

    vector<double> vprojection;
    vprojection.resize(dim);
    if(rev) threshold = -threshold;
    for(f = 0; f < dim; f++)
    {
        double ww = 1.0;
        if(projection != NULL) ww = (*projection)(f);
        if(rev) ww = -ww;
        if(nor)
        {
            ww *= normal_multi[picked[f]];
            threshold -= (ww * normal_plus[picked[f]]);
        }
        vprojection[f] = ww;
    }

    vector<double> vthreshold;
    vthreshold.resize(2);
    vthreshold[0] = threshold;
    vthreshold[1] = discriminant;

    vector< vector<double> > vvalues;
    vvalues.resize(2);
    vvalues[0] = vprojection;
    vvalues[1] = vthreshold;

    vector<string> vclassnames;
//    if(classtodistinguish >= 0)
//    {
//        vclassnames.resize(1);
//        vclassnames[0] = data->classnames[classtodistinguish];
//    }
//    else
//    {
    if(classtodistinguish1 != classtodistinguish2)
    {
        vclassnames.resize(2);
        vclassnames[0] = data->classnames[classtodistinguish1];
        vclassnames[1] = data->classnames[classtodistinguish2];
    }
    else
    {
        vclassnames.resize(1);
        stringstream ss;
        ss << "Mdf" << classifier->classifiers.size();
        vclassnames[0] = ss.str();
    }
//    }
    vector<string> vfeaturenames;
    vfeaturenames.resize(dim);
    for(f = 0; f < dim; f++)
    {
        vfeaturenames[f] = data->featurenames[picked[f]];
    }

    Classifier vclassifier;
    vclassifier.classnames = vclassnames;
    vclassifier.featurenames = vfeaturenames;
    vclassifier.values = vvalues;

    classifier->classifiers.push_back(vclassifier);
}

//------------------------------------------------------------------------------


bool LdaSelectionReduction::meantomean(const real_2d_array &xy, const ae_int_t npoints, const ae_int_t dims, real_1d_array &w)
{
    real_1d_array mean;
    mean.setlength(dims);
    unsigned int n0, n1;
    double dist;
    n0 = 0;
    n1 = 0;
    for(unsigned int d = 0; d < dims; d++)
    {
        w(d) = 0;
        mean(d) = 0;
    }
    for(unsigned int v = 0; v < npoints; v++)
    {
        if(xy(v, dims) == 0)
        {
            for(unsigned int d = 0; d < dims; d++)
            {
                w(d) += xy(v, d);
            }
            n0++;
        }
        else
        {
            for(unsigned int d = 0; d < dims; d++)
            {
                mean(d) += xy(v, d);
            }
            n1++;
        }
    }
    dist = 0;
    for(unsigned int d = 0; d < dims; d++)
    {
        w(d) = mean(d)/n1 - w(d)/n0;
        dist += (w(d)*w(d));
    }
    dist = sqrt(dist);
    if(dist > machineepsilon)
    {
        for(unsigned int d = 0; d < dims; d++)
        {
            w(d) = w(d)/dist;
        }
        return true;
    }
    return false;
}

inline double LdaSelectionReduction::GoalFunction(unsigned int dim, unsigned int* picked)
{
    unsigned int v, f;
    double discriminant;
    ae_int_t info;
    unsigned int vs1, vm1, vs2, vm2, vv;
    unsigned int nvectors = 0;
    real_2d_array xy;
    real_1d_array w;
    real_1d_array mdf;

    double miu[2];
    double std[2];

    vs1 = classtodistinguish1 > 0 ? data->classendvectorindex[classtodistinguish1-1] : 0;
    vm1 = data->classendvectorindex[classtodistinguish1];
    nvectors += (vm1-vs1);
    vs2 = classtodistinguish2 > 0 ? data->classendvectorindex[classtodistinguish2-1] : 0;
    vm2 = data->classendvectorindex[classtodistinguish2];
    nvectors += (vm2-vs2);

    if(dim == 1)
    {
        mdf.setlength(nvectors);

        vv = 0;
        for(v = vs2; v < vm2; v++, vv++)
        {
            mdf(vv) = data->values[data->featurenumber * v + picked[0]];
        }
        for(v = vs1; v < vm1; v++, vv++)
        {
            mdf(vv) = data->values[data->featurenumber * v + picked[0]];
        }
    }
    else
    {
        xy.setlength(nvectors, dim+1);
        w.setlength(dim);
        mdf.setlength(nvectors);

        vv = 0;
        for(v = vs2; v < vm2; v++, vv++)
        {
            for(f = 0; f < dim; f++)
            {
                xy(vv, f) = data->values[data->featurenumber * v + picked[f]];
            }
            xy(vv, dim) = 1;
        }
        for(v = vs1; v < vm1; v++, vv++)
        {
            for(f = 0; f < dim; f++)
            {
                xy(vv, f) = data->values[data->featurenumber * v + picked[f]];
            }
            xy(vv, dim) = 0;
        }

        if(direction_algorithm == DIRECTION_FROM_MEANS)
        {
            if(! meantomean(xy, nvectors, dim, w))
                 return -1.0;
        }
        else
        {
            fisherlda(xy, nvectors, dim, 2 /*data->classnumber*/, info, w);
            if(info < 0) return -1.0;
        }


//        for(unsigned int d = 0; d < dim; d++)
//            printf("%i %f  ", picked[d], w(d));
//        printf(" >>>>>>>>>>>\n");
//        fflush(stdout);

        //////////////////////////////////////////////////////////
        //rmatrixmv(divider, dim, xy, 0, 0, 0, w, 0, mdf, 0);

        for(v = 0; v < nvectors; v++)
        {
            mdf(v) = 0;
            for(unsigned int d = 0; d < dim; d++)
            {
                mdf(v) += w(d)*xy(v, d);
            }
        }
    }

    double miuloc;
    double varloc;
    double miuall = 0.0;
    double varall = 0.0;
    double varcla = 0.0;

    vv = 0;

    miuloc = 0.0;
    varloc = 0.0;
    for(v = vs2; v < vm2; v++, vv++)
    {
        double f = mdf(vv);
        miuloc += f;
        varloc += (f*f);

        miuall += f;
        varall += (f*f);
    }
    miuloc /= (vm2-vs2);
    varloc /= (vm2-vs2);
    varloc -= (miuloc * miuloc);
    varcla += varloc;
    miu[1] = miuloc;
    std[1] = varloc;

    miuloc = 0.0;
    varloc = 0.0;
    for(v = vs1; v < vm1; v++, vv++)
    {
        double f = mdf(vv);
        miuloc += f;
        varloc += (f*f);

        miuall += f;
        varall += (f*f);
    }
    miuloc /= (vm1-vs1);
    varloc /= (vm1-vs1);
    varloc -= (miuloc * miuloc);
    varcla += varloc;
    miu[0] = miuloc;
    std[0] = varloc;

    miuall /= nvectors;
    varall /= nvectors;
    varall -= (miuall * miuall);
    discriminant = (2 * varall + machineepsilon) / (varcla + machineepsilon);

    if(computeclassifier)
    {
        std[0] = sqrt(std[0]);
        std[1] = sqrt(std[1]);

        double threshold = 0.0;
        bool reverse = false;








//        if(dim == 1)
//        {
//            threshold -= (normal_multi[picked[0]] * normal_plus[picked[0]]);
//        }
//        else
//        {
//            for(unsigned int f = 0; f < dim; f++)
//            {
//                threshold -= (normal_multi[picked[f]] * w(f) * normal_plus[picked[f]]);
//            }
//        }









        computeThreshold(&threshold, &reverse, miu, std, &mdf, vm2-vs2);

        if(dim == 1) StoreClassifier(dim, threshold, reverse, picked, discriminant, NULL);
        else StoreClassifier(dim, threshold, reverse, picked, discriminant, &w);
    }
    return discriminant;
}



//------------------------------------------------------------------------------
double LdaSelectionReduction::ClassifierTraining(unsigned int dimensions, unsigned int* selected)
{
    double ret;
    computeclassifier = true;

//    if(classtodistinguish < -1)
    ret = GoalFunction(dimensions, selected);
//    else if(classtodistinguish < 0) ret = GoalFunctionM(dimensions, selected);
//    else ret = GoalFunction1(dimensions, selected);

    computeclassifier = false;
    return ret;
}







//------------------------------------------------------------------------------
//bool LdaSelectionReduction::featureReindex(int* featurereindex, vector<string> featurenames)
//{
//    int c = 0;
//    for(vector<string>::iterator fn = featurenames.begin(); fn != featurenames.end(); ++fn, c++)
//    {
//        featurereindex[c] = -1;
//        for(int i = 0; i < data->featurenumber; i++)
//        {
//            if(data->featurenames[i] == *fn) featurereindex[c] = i;
//        }
//        if(featurereindex[c] < 0) return false;
//    }
///*
//    for(int c = 0; c < classifier->featurenumber; c++)
//    {
//        featurereindex[c] = -1;
//        for(int i = 0; i < data->featurenumber; i++)
//        {
//            if(data->featurenames[i] == classifier->featurenames[c]) featurereindex[c] = i;
//        }
//        if(featurereindex[c] < 0) return false;
//    }
//*/
//    return true;
//}



//bool LdaSelectionReduction::mapReindex(int* featurereindex, vector<string> featurenames)
//{
//    int c = 0;
//    for(vector<string>::iterator fn = featurenames.begin(); fn != featurenames.end(); ++fn, c++)
//    {
//        featurereindex[c] = -1;
//        for(unsigned int i = 0; i < (*inmaps).size(); i++)
//        {
//            if((*inmaps)[i]->name == *fn) featurereindex[c] = i;
//        }
//        if(featurereindex[c] < 0) return false;
//    }
//    return true;
//}

//bool LdaSelectionReduction::computeMapmdfs(bool segment)
//{
//    breakanalysis = false;
//    int cc = 0;
//    for(vector<Classifier>::iterator c = classifier->classifiers.begin(); c != classifier->classifiers.end(); ++c, cc++)
//    {
//        if(c->classnames.size() <= 0) continue;
//        if(c->featurenames.size() <= 0) continue;
//        if(c->values.size() <= 0) continue;

//        stringstream ss;
//        ss << c->getName() << "\n";
//        NotifyProgressText(ss.str());

//        vector< vector<double> > projection = c->values;

//        int* featurereindex = new int[c->featurenames.size()];
//        unsigned int yxmax = (*outmaps)[cc]->size[0] * (*outmaps)[cc]->size[1];
//        double* bufer = new double[yxmax];
//        memset(bufer, 0, yxmax*sizeof(double));

//        if(mapReindex(featurereindex, c->featurenames))
//        {
//            int fff = c->values.size() - 1;
//            for(int ff = 0; ff < fff; ff++)
//            {
//                unsigned int featurenumber = projection[ff].size();
//                if(featurenumber != c->featurenames.size()) continue;

//                for(unsigned int f = 0; f < featurenumber; f++)
//                {
//                    switch((*inmaps)[featurereindex[f]]->pixel)
//                    {
//                    case MZPIXELF64:
//                    {
//                        double pr = projection[ff][f];
//                        double* im = (double*) ((*inmaps)[featurereindex[f]]->data);
//                        for(unsigned int yx = 0; yx < yxmax; yx++)
//                        {
//                            bufer[yx] += (im[yx] * pr);
//                        }
//                    }break;
//                    case MZPIXELF32:
//                    {
//                        double pr = projection[ff][f];
//                        float* im = (float*) ((*inmaps)[featurereindex[f]]->data);
//                        for(unsigned int yx = 0; yx < yxmax; yx++)
//                        {
//                            bufer[yx] += (im[yx] * pr);
//                        }
//                    }break;
//                    }
//                }
//            }
//        }

//        double threshold = projection[projection.size()-1][0];
//        switch((*outmaps)[cc]->pixel)
//        {
//            case MZPIXELF64:
//            {

//                double* im = (double*) ((*outmaps)[cc]->data);
//                if(segment)
//                {
//                    for(unsigned int yx = 0; yx < yxmax; yx++)
//                    {
//                        im[yx] = bufer[yx]-threshold > 0.0 ?1.0 :0;
//                    }
//                }
//                else
//                {
//                    for(unsigned int yx = 0; yx < yxmax; yx++)
//                    {
//                        im[yx] = bufer[yx]-threshold;
//                    }
//                }
//            }break;
//            case MZPIXELF32:
//            {
//                float* im = (float*) ((*outmaps)[cc]->data);
//                if(segment)
//                {
//                    for(unsigned int yx = 0; yx < yxmax; yx++)
//                    {
//                        im[yx] = bufer[yx]-threshold > 0.0 ?1.0 :0;
//                    }
//                }
//                else
//                {
//                    for(unsigned int yx = 0; yx < yxmax; yx++)
//                    {
//                        im[yx] = bufer[yx]-threshold;
//                    }
//                }
//            }break;
//        }
//        delete[] bufer;
//        delete[] featurereindex;
//        NotifyProgressStep();
//    }
//    return true;
//}








//int LdaSelectionReduction::classify()
//{
//    for(vector<Classifier>::iterator c = classifier->classifiers.begin(); c != classifier->classifiers.end(); ++c)
//    {
//        unsigned int mc = c->classnames.size();
//        unsigned int i;
//        if(mc > 2) mc = 2;
//        for(i = 0; i < mc; i++)
//        {
//            classindexes[cc] = outclassnamesnu-1;
//            for(unsigned int ii = 0; ii < outclassnamesnu; ii++)
//            {
//                if(outclassnames[ii] == c->classnames[i])
//                {
//                    classmaxfired[ii]++;
//                    classindexes[cc] = ii;
//                }
//            }
//            cc++;
//        }
//        for(; i < 2; i++)
//        {
//            classindexes[cc] = outclassnamesnu-1;
//            cc++;
//        }
//        int* featurereindex = new int[c->featurenames.size()];
//        if(!featureReindex(featurereindex, c->featurenames))
//        {
//            delete[] featurereindex;
//            //NotifyProgressStage(FAILED, featurenumber, 0.0, NULL);
//            continue;
//        }
//        featurereindextab[ccc] = featurereindex;
//    }
//}

bool LdaSelectionReduction::computeSelect(void)
{
    int d;
    bool ret = true;
    breakanalysis = false;
    classtodistinguish1 = 0;
    classtodistinguish2 = 0;

//    if(classtodistinguish == -2)
    {
        int ile = (data->classnumber*data->classnumber-data->classnumber)/2;
        int* tempsort = new int[ile * dimensions];
        int ilesek = (maxtime+ile-1)/ile;

        int ctd = 0;
        int c = 0;
        for(d = 0; d < data->featurenumber; d++) Qtable[d] = 0.0;
        for(classtodistinguish2 = 1; classtodistinguish2 < data->classnumber; classtodistinguish2++)
        {
            for(classtodistinguish1 = 0; classtodistinguish1 < classtodistinguish2; classtodistinguish1++)
            {
                bool okl = FullSearch(data->featurenumber, Qtable, Qsorted, dimensions, ilesek, true);
                if(okl)
                {
                    for(d = 0; d < (int)dimensions; d++)
                        tempsort[ctd * dimensions + d] = Qsorted[d];
                }
                else
                {
                    for(d = 0; d < (int)dimensions; d++)
                        tempsort[ctd * dimensions + d] = -1;
                }
                ret = ret && okl;
                ctd++;

                if(breakanalysis)
                    break;
            }
            if(breakanalysis)
                break;
        }
        ile *= dimensions;
        for(d = 0; d < ile && c < data->featurenumber; d++)
        {
            //bool found = false;
            int dd;
            for(dd = 0; dd < c; dd++)
            {
                if((int)Qsorted[dd] == tempsort[d])
                {
                    Qtable[Qsorted[dd]] += 1.0;
                    //found = true;
                    break;
                }
            }
            if(dd >= c)
            {
                Qsorted[c] =  tempsort[d];
                Qtable[Qsorted[c]] = 1.0;
                c++;
            }
        }
        delete[] tempsort;
        for(; c < data->featurenumber; c++) Qsorted[c] = (unsigned int)(-1);
    }
//    else if(classtodistinguish == -3)
//    {
//        classtodistinguish = -1;
//        ret = FullSearch(data->featurenumber, Qtable, Qsorted, dimensions, maxtime, true);
//        classtodistinguish = -3;
//    }
//    else if(classtodistinguish == -1)
//    {
//        int ile = (data->classnumber);
//        int* tempsort = new int[ile * dimensions];
//        ile = (maxtime+ile-1)/ile;
//        for(classtodistinguish = 0; classtodistinguish < data->classnumber; classtodistinguish++)
//        {
//            bool okl = FullSearch(data->featurenumber, Qtable, Qsorted, dimensions, ile, true);
//            if(okl)
//            {
//                for(d = 0; d < (int)dimensions; d++)
//                    tempsort[classtodistinguish * dimensions + d] = Qsorted[d];
//            }
//            else
//            {
//                for(d = 0; d < (int)dimensions; d++)
//                    tempsort[classtodistinguish * dimensions + d] = -1;
//            }
//            ret = ret && okl;
//        }
//        int c = 0;
//        for(d = 0; d < data->featurenumber; d++) Qtable[d] = 0.0;
//        for(d = 0; d < (int)dimensions*data->classnumber && c < data->featurenumber; d++)
//        {
//            bool found = false;
//            for(int dd = 0; dd < c; dd++)
//            {
//                if((int)Qsorted[dd] == tempsort[d])
//                {
//                    Qtable[Qsorted[dd]] += 1.0;
//                    found = true;
//                    break;
//                }
//            }
//            if(!found)
//            {
//                Qsorted[c] =  tempsort[d];
//                Qtable[Qsorted[c]] = 1.0;
//                c++;
//            }
//        }
//        delete[] tempsort;
//        for(; c < data->featurenumber; c++) Qsorted[c] = (unsigned int)(-1);
//        classtodistinguish = -1;
//    }
//    else if(classtodistinguish >= 0)
//    {
//        ret = FullSearch(data->featurenumber, Qtable, Qsorted, dimensions, maxtime, true);
//    }

    return ret;
}


bool LdaSelectionReduction::computeProject(void)
{
    bool ret = true;
    breakanalysis = false;
    classtodistinguish1 = 0;
    classtodistinguish2 = 0;

    unsigned int* Qsorted = new unsigned int[data->featurenumber];
    for(int f = 0; f < data->featurenumber; f++) Qsorted[f] = f;

    NotifyProgressStage(MULTI_SELECTION_BEGINS, data->featurenumber, 0.0, NULL);

    for(classtodistinguish2 = 1; classtodistinguish2 < data->classnumber; classtodistinguish2++)
    {
        for(classtodistinguish1 = 0; classtodistinguish1 < classtodistinguish2; classtodistinguish1++)
        {
            ret = ret && (ClassifierTraining(data->featurenumber, Qsorted) > 0.0);//FullSearch(data->featurenumber, Qtable, Qsorted, dimensions, ile, true);
            NotifyProgressStep();
            if(breakanalysis)
                break;

        }
        if(breakanalysis)
            break;
    }

//    else if(classtodistinguish == -3)
//    {
//        classtodistinguish = -1;
//        ret = (ClassifierTraining(data->featurenumber, Qsorted) > 0.0);//FullSearch(data->featurenumber, Qtable, Qsorted, dimensions, maxtime, true);
//        NotifyProgressStep();
//        classtodistinguish = -3;
//    }
//    else if(classtodistinguish == -1)
//    {
//        for(classtodistinguish = 0; classtodistinguish < data->classnumber; classtodistinguish++)
//        {
//            ret = ret && (ClassifierTraining(data->featurenumber, Qsorted) > 0.0);//FullSearch(data->featurenumber, Qtable, Qsorted, dimensions, ile, true);
//            NotifyProgressStep();
//        }
//        classtodistinguish = -1;
//    }
//    else if(classtodistinguish >= 0)
//    {
//        ret = (ClassifierTraining(data->featurenumber, Qsorted) > 0.0);//FullSearch(data->featurenumber, Qtable, Qsorted, dimensions, maxtime, true);
//        NotifyProgressStep();
//    }

    delete[] Qsorted;
    return ret;
}




bool LdaSelectionReduction::computeMdf(void)
{
    int classifnumber = classifier->classifiers.size();
    int vectornumber = data->vectornumber;
    if(classifnumber <= 0 || vectornumber <= 0)
    {
        return false;
    }

    int mdfnumber = 0;
    for(vector<Classifier>::iterator c = classifier->classifiers.begin(); c != classifier->classifiers.end(); ++c)
    {
        int l = c->values.size() - 1;
        if(l > 0) mdfnumber += l;
    }
    if(mdfnumber <= 0)
        return false;

    double* Qtable = new double[vectornumber*mdfnumber];

    for(int y = 0; y < vectornumber*mdfnumber; y++) Qtable[y] = std::numeric_limits<double>::quiet_NaN();
    bool res = ComputeMdfs(Qtable, mdfnumber);
    if(res)
    {
        delete[] data->values;
        delete[] data->featurenames;

        data->featurenumber = classifnumber;
        data->featurenames = new string[classifnumber];
        data->values = Qtable;

        int cc = 0;
        for(vector<Classifier>::iterator c = classifier->classifiers.begin(); c != classifier->classifiers.end(); ++c)
        {
            if(c->classnames.size() <= 0) continue;
            if(c->featurenames.size() <= 0) continue;
            if(c->values.size() <= 0) continue;

            string ss;
            ss = c->getName();

            int l = c->values.size() - 1;
            if(l == 1)
            {
                data->featurenames[cc] = ss;
                cc++;
            }
            else if(l > 0)
            {
                for(int ll = 0; ll < l; ll++)
                {
                    stringstream sss;
                    sss << ss;
                    sss << ll;
                    data->featurenames[cc] = sss.str();
                    cc++;
                }
            }
        }
    }
    else delete[] Qtable;
    return res;
}

//------------------------------------------------------------------------------
bool LdaSelectionReduction::ComputeMdfs(double* table, unsigned int mdfsnumber)
{
    breakanalysis = false;
    vector<string> featurenames;
    for(int f = 0; f < data->featurenumber; f++)
    {
        featurenames.push_back(data->featurenames[f]);
    }
    if(! configureForClassification(&featurenames)) return false;
    for(int ccc = 0; ccc < data->classnumber; ccc++)
    {
        int vs = ccc > 0 ? data->classendvectorindex[ccc-1] : 0;
        int vm = data->classendvectorindex[ccc];

        for(int v = vs; v < vm; v++)
        {
            double* ptri = data->values + (data->featurenumber * v);
            double* mdfs = table + mdfsnumber*v;
            mdfFromFeatureVector(ptri, mdfs);
            if(breakanalysis)
            {
                if(featureReindexLut != NULL) delete[] featureReindexLut;
                featureReindexLut = NULL;
                if(outClassIndices != NULL) delete[] outClassIndices;
                outClassIndices = NULL;
                if(classFireRequired != NULL) delete[] classFireRequired;
                classFireRequired = NULL;
                return false;
            }
            NotifyProgressStep();
        }
    }
    if(featureReindexLut != NULL) delete[] featureReindexLut;
    featureReindexLut = NULL;
    if(outClassIndices != NULL) delete[] outClassIndices;
    outClassIndices = NULL;
    if(classFireRequired != NULL) delete[] classFireRequired;
    classFireRequired = NULL;
    return true;
}

void LdaSelectionReduction::segmentImage(unsigned int vectornumber, MazdaMapPixelType** values, MazdaMapPixelType* result)
{
    breakanalysis = false;
    unsigned int classifiers_size = classifier->classifiers.size();
    unsigned int classname_size = outClassNames.size();
    unsigned int* class_fired = new unsigned int[classname_size];

    unsigned int featureReindexLutSize = 0;
    unsigned int featureReindexLutPosition = 0;
    for(unsigned int c = 0; c < classifiers_size; c++)
    {
        featureReindexLutSize += classifier->classifiers[c].featurenames.size();
    }
    MazdaMapPixelType** reindexed_map_pointers = new MazdaMapPixelType*[featureReindexLutSize];
    for(unsigned int c = 0; c < classifiers_size; c++)
    {
        unsigned int values_first_size = classifier->classifiers[c].values[0].size();
        unsigned int features_number = classifier->classifiers[c].featurenames.size();

        for(unsigned int f = 0; f < values_first_size; f++)
        {
            reindexed_map_pointers[featureReindexLutPosition+f] = values[featureReindexLut[featureReindexLutPosition+f]];
        }
        featureReindexLutPosition += features_number;
    }

    for(unsigned int v = 0; v < vectornumber; v++)
    {
        //unsigned int
        featureReindexLutPosition = 0;
        unsigned int classIndicesPosition = 0;
        for(unsigned int i = 0; i < classname_size; i++) class_fired[i] = 0;
        for(unsigned int c = 0; c < classifiers_size; c++)
        {
            unsigned int class_number = classifier->classifiers[c].classnames.size();
            unsigned int values_size = classifier->classifiers[c].values.size();
            unsigned int values_first_size = classifier->classifiers[c].values[0].size();
            unsigned int features_number = classifier->classifiers[c].featurenames.size();
            double d = -classifier->classifiers[c].values[values_size-1][0];
            for(unsigned int f = 0; f < values_first_size; f++)
            {
                //d += (dataMap.values[featureReindexLut[featureReindexLutPosition+f]][v] * classifier->classifiers[c].values[0][f]);
                d += (reindexed_map_pointers[featureReindexLutPosition+f][v] * classifier->classifiers[c].values[0][f]);
            }
            if(d > 0)
            {
                class_fired[outClassIndices[classIndicesPosition]]++;
            }
            else
            {
                if(class_number > 1) class_fired[outClassIndices[classIndicesPosition+1]]++;
                else class_fired[0]++;
            }
            featureReindexLutPosition += features_number;
            classIndicesPosition += class_number;
        }
        result[v] = 0;
        for(unsigned int c = classname_size-1; c < classname_size; c--)
        {
            if(class_fired[c] >= classFireRequired[c])
            {
                result[v] = c;
                break;
            }
        }
        NotifyProgressStep();
        if(breakanalysis)
            break;
    }
    delete[] reindexed_map_pointers;
    delete[] class_fired;
}

bool LdaSelectionReduction::computeSegmentation(void)
{
    if(! configureForClassification(&dataMap.featurenames))
        return false;
    segmentImage(dataMap.vectornumber, dataMap.values, dataMap.result[0]);
    if(featureReindexLut != NULL) delete[] featureReindexLut;
    featureReindexLut = NULL;
    if(outClassIndices != NULL) delete[] outClassIndices;
    outClassIndices = NULL;
    if(classFireRequired != NULL) delete[] classFireRequired;
    classFireRequired = NULL;
    return true;
}

bool LdaSelectionReduction::computeMdfMaps(void)
{
    breakanalysis = false;
    if(! configureForClassification(&dataMap.featurenames))
        return false;
    unsigned int classifiers_size = classifier->classifiers.size();
    for(unsigned int v = 0; v < dataMap.vectornumber; v++)
    {
        unsigned int featureReindexLutPosition = 0;
        for(unsigned int c = 0; c < classifiers_size; c++)
        {
            unsigned int values_size = classifier->classifiers[c].values.size();
            unsigned int values_first_size = classifier->classifiers[c].values[0].size();
            unsigned int features_number = classifier->classifiers[c].featurenames.size();
            double d = -classifier->classifiers[c].values[values_size-1][0];
            for(unsigned int f = 0; f < values_first_size; f++)
            {
                //d += (dataMap.values[featureReindexLut[featureReindexLutPosition+f]][v] * classifier->classifiers[c].values[0][f]);
                d += (dataMap.values[featureReindexLutPosition+f][v] * classifier->classifiers[c].values[0][f]);
            }
            dataMap.result[c][v] = d;
            featureReindexLutPosition += features_number;
        }
        NotifyProgressStep();
    }
    if(featureReindexLut != NULL) delete[] featureReindexLut;
    featureReindexLut = NULL;
    if(outClassIndices != NULL) delete[] outClassIndices;
    outClassIndices = NULL;
    if(classFireRequired != NULL) delete[] classFireRequired;
    classFireRequired = NULL;
    return true;
}


bool LdaSelectionReduction::computeTest(void)
{
    breakanalysis = false;
    vector<string> featurenames;
    for(int f = 0; f < data->featurenumber; f++)
    {
        featurenames.push_back(data->featurenames[f]);
    }
    if(! configureForClassification(&featurenames))
        return false;

    unsigned int columns = outClassNames.size();
    unsigned int rows = data->classnumber;

    unsigned int* confusionTable = new unsigned int[columns*rows];
    for(unsigned int f = 0; f < columns*rows; f++) confusionTable[f] = 0;

    for(int ccc = 0; ccc < data->classnumber; ccc++)
    {
        int vs = ccc > 0 ? data->classendvectorindex[ccc-1] : 0;
        int vm = data->classendvectorindex[ccc];

        for(int v = vs; v < vm; v++)
        {
            double* ptri = data->values + (data->featurenumber * v);
            unsigned int cls = classifyFeatureVector(ptri);
            //printf("I%i O%i\n", ccc, cls);
            if(cls >= columns || breakanalysis)
            {
                delete[] confusionTable;
                return false;
            }
            confusionTable[ccc*columns + cls]++;
        }
    }
    std::stringstream ss;
    for(unsigned int c = 1; c < columns; c++) ss << "\t" << outClassNames[c];
    ss << "\t" << outClassNames[0] << std::endl;
    for(int r = 0; r < data->classnumber; r++)
    {
        ss << data->classnames[r];
        for(unsigned int c = 1; c < columns; c++) ss << "\t" << confusionTable[r*columns + c];
        ss << "\t" << confusionTable[r*columns] << std::endl;
    }
    *confusionTableText = ss.str();

    delete[] confusionTable;
    NotifyProgressStep();
    return true;
}


int LdaSelectionReduction::enumerateClasses(vector<string>* outClassNames)
{
    unsigned int classifiers_size = classifier->classifiers.size();
    unsigned int classIndicesLutSize = 0;
    outClassNames->clear();
    if(classifiers_size <= 0)
        return false;
    outClassNames->push_back("!");
    for(unsigned int c = 0; c < classifiers_size; c++)
    {
        unsigned int class_number = classifier->classifiers[c].classnames.size();
        classIndicesLutSize += class_number;
        for(unsigned int k = 0; k < class_number; k++)
        {
            bool on_list = false;
            for(unsigned int kk = 0; kk < outClassNames->size(); kk++)
            {
                if((*outClassNames)[kk] == classifier->classifiers[c].classnames[k])
                {
                    on_list = true;
                    break;
                }
            }
            if(!on_list) outClassNames->push_back(classifier->classifiers[c].classnames[k]);
        }
    }
    return classIndicesLutSize;
}

bool LdaSelectionReduction::configureForClassification(vector<string> *featurenames)
{
    if(featureReindexLut != NULL) delete[] featureReindexLut;
    featureReindexLut = NULL;
    if(outClassIndices != NULL) delete[] outClassIndices;
    outClassIndices = NULL;
    if(classFireRequired != NULL) delete[] classFireRequired;
    classFireRequired = NULL;

    unsigned int classifiers_size = classifier->classifiers.size();
    if(classifiers_size <= 0)
        return false;
//    this->classifiers = classifiers;
    unsigned int classIndicesLutSize = enumerateClasses(&outClassNames);
    unsigned int featureReindexLutSize = 0;
    for(unsigned int c = 0; c < classifiers_size; c++)
    {
        featureReindexLutSize += classifier->classifiers[c].featurenames.size();
    }
    if(outClassNames.size() <= 1 || featureReindexLutSize <= 0 || classIndicesLutSize <= 0) return false;

    outClassIndices = new unsigned int[classIndicesLutSize];
    featureReindexLut = new unsigned int[featureReindexLutSize];
    classFireRequired = new unsigned int[classIndicesLutSize+1];

    for(unsigned int i = 0; i <= classIndicesLutSize; i++) classFireRequired[i] = 0;

    unsigned int featureReindexLutPosition = 0;
    unsigned int classIndicesPosition = 0;
    for(unsigned int c = 0; c < classifiers_size; c++)
    {
        unsigned int features_number = classifier->classifiers[c].featurenames.size();
        unsigned int class_number = classifier->classifiers[c].classnames.size();

        for(unsigned int k = 0; k < features_number; k++)
        {
            bool found = false;
            unsigned int fns = featurenames->size();
            for(unsigned int kk = 0; kk < fns; kk++)
            {
                if((*featurenames)[kk] == classifier->classifiers[c].featurenames[k])
                {
                    found = true;
                    featureReindexLut[featureReindexLutPosition+k] = kk;
                    break;
                }
            }
            if(!found)
                return false;
        }

        for(unsigned int k = 0; k < class_number; k++)
        {
            bool found = false;
            for(unsigned int kk = 0; kk < outClassNames.size(); kk++)
            {
                if(outClassNames[kk] == classifier->classifiers[c].classnames[k])
                {
                    found = true;
                    classFireRequired[kk]++;
                    outClassIndices[classIndicesPosition+k] = kk;
                    break;
                }
            }
            if(!found)
                return false;
        }

        featureReindexLutPosition += features_number;
        classIndicesPosition += class_number;
    }

    return true;
}

unsigned int LdaSelectionReduction::classifyFeatureVector(double* featureVector)
{
//    printf("classifyFeatureVector\n");

    unsigned int featureReindexLutPosition = 0;
    unsigned int classIndicesPosition = 0;
    unsigned int classifiers_size = classifier->classifiers.size();
    unsigned int classname_size = outClassNames.size();
    unsigned int* class_fired = new unsigned int[classname_size];
    for(unsigned int i = 0; i < classname_size; i++) class_fired[i] = 0;

    for(unsigned int c = 0; c < classifiers_size; c++)
    {
        unsigned int class_number = classifier->classifiers[c].classnames.size();
        unsigned int values_size = classifier->classifiers[c].values.size();
        unsigned int values_first_size = classifier->classifiers[c].values[0].size();
        unsigned int features_number = classifier->classifiers[c].featurenames.size();
        double threshold = classifier->classifiers[c].values[values_size-1][0];

        double d = 0.0;
        for(unsigned int f = 0; f < values_first_size; f++)
        {
            d += (featureVector[featureReindexLut[featureReindexLutPosition+f]] * classifier->classifiers[c].values[0][f]);
        }
        d -= threshold;
#ifdef MZ_DEBUG_PRINT_PROBABILITY
        double probd = fabs(d);
        probd = probd/(probd + 1);
        printf("Probab = %f\n", (float)probd);
#endif
        if(d > 0)
        {
            class_fired[outClassIndices[classIndicesPosition]]++;
        }
        else
        {
            if(class_number > 1) class_fired[outClassIndices[classIndicesPosition+1]]++;
            else class_fired[0]++;
        }
        featureReindexLutPosition += features_number;
        classIndicesPosition += class_number;
        //classIndicesPosition += class_number;
    }
    for(unsigned int c = classname_size-1; c < classname_size; c--)
    {
        if(class_fired[c] >= classFireRequired[c])
        {
            delete[] class_fired;
            return c;
        }
    }
    delete[] class_fired;
    return 0;
}



void LdaSelectionReduction::mdfFromFeatureVector(double* featureVector, double* mdfs)
{
    unsigned int featureReindexLutPosition = 0;
    unsigned int classIndicesPosition = 0;
    unsigned int classifiers_size = classifier->classifiers.size();

    for(unsigned int c = 0; c < classifiers_size; c++)
    {
        unsigned int class_number = classifier->classifiers[c].classnames.size();
        unsigned int values_size = classifier->classifiers[c].values.size();
        unsigned int values_first_size = classifier->classifiers[c].values[0].size();
        unsigned int features_number = classifier->classifiers[c].featurenames.size();
        double threshold = classifier->classifiers[c].values[values_size-1][0];

        double d = 0.0;
        for(unsigned int f = 0; f < values_first_size; f++)
        {
            d += (featureVector[featureReindexLut[featureReindexLutPosition+f]] * classifier->classifiers[c].values[0][f]);
        }
        d -= threshold;
        mdfs[c] = d;
        featureReindexLutPosition += features_number;
        classIndicesPosition += class_number;
    }
}

bool LdaSelectionReduction::loadClassifierFromFile(const char *filename)
{
    Classifiers* tmpClassifier = new Classifiers(LdaSelectionReductionClassifierName);
    if(tmpClassifier->loadClassifier(filename))
    {
        if(classifier != NULL) delete classifier;
        classifier = tmpClassifier;
        return true;
    }
    else
        delete tmpClassifier;
    return false;
}

std::vector<std::string> LdaSelectionReduction::getClassNames(void)
{
    vector<string> class_names;
    enumerateClasses(&class_names);
    return class_names;
}

std::vector<std::string> LdaSelectionReduction::getFeatureNames(void)
{
    std::vector<std::string> names;
    if(classifier == NULL) return names;

    unsigned int nc = classifier->classifiers.size();
    for(unsigned int c = 0; c < nc; c++)
    {
        std::vector <std::string>* cl;
        cl = & classifier->classifiers[c].featurenames;
        //cl = & classifier->classifiers[c].classnames;
        unsigned int nn = cl->size();
        for(unsigned int n = 0; n < nn; n++)
        {
            unsigned int nm = names.size();
            unsigned int m;
            for(m = 0; m < nm; m++)
            {
                if((*cl)[n] == names[m]) break;
            }
            if(m >= nm) names.push_back((*cl)[n]);
        }
    }
    return names;
}
