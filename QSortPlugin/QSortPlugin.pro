#-------------------------------------------------
#
# Project created by QtCreator 2013-08-31T08:44:48
#
#-------------------------------------------------

#QT       += core

#! [0]
CONFIG -= app_bundle
CONFIG -= qt

TEMPLATE        = lib
CONFIG         += shared
CONFIG         += plugin

SOURCES += \
    ../MzShared/dataforconsole.cpp \
    ../MzShared/csvio.cpp \
    qsortselection.cpp \
    qsortplugin.cpp


HEADERS +=\
    ../MzShared/dataforconsole.h \
    ../MzShared/mzselectioninterface.h \
    qsortplugin.h \
    qsortselection.h

TARGET          = $$qtLibraryTarget(QSortPlugin)
DESTDIR         = ../../Executables

include(../Pri/config.pri)
include(../Pri/alglib.pri)
