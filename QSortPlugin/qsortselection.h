/*
 * qMaZda - Image Analysis and Pattern Recognition
 * 
 * Copyright 2013  Piotr M. Szczypiński <piotr.szczypinski@p.lodz.pl>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef QSORTSELECTION_H
#define QSORTSELECTION_H
#include "../MzShared/mzselectioninterface.h"
#include "../MzShared/dataforconsole.h"

#define STANDARDIZE 0
#define NORMALIZE 1

class QSortSelectionReduction
{
public:
    QSortSelectionReduction();
    ~QSortSelectionReduction();
    DataForSelection* data;
    virtual void NotifyProgress(int notification, int dimensionality){}// = 0;
    bool SelectFisher(double* Qtable, unsigned int* Qsorted);
    bool SelectMutualInformation(double* Qtable, unsigned int* Qsorted);

private:
    double* normal_plus;
    double* normal_multi;
//    void NormalizeMinMax(void);
    void Standardize(void);
    bool BubleSortMaxMin(int size, double* Qtable, unsigned int* Qsorted);
    bool BubleSortMinMax(int size, double* Qtable, unsigned int* Qsorted);

};

#endif // QSORTSELECTION_H
