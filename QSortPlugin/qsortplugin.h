/*
 * qMaZda - Image Analysis and Pattern Recognition
 * 
 * Copyright 2013  Piotr M. Szczypiński <piotr.szczypinski@p.lodz.pl>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef QSORTPLUGIN_H
#define QSORTPLUGIN_H

//#include <QtGlobal>
//#include <QObject>
#include "../MzShared/mzselectioninterface.h"
#include "qsortselection.h"

class QSortPlugin : public MzSelectionPluginInterface, QSortSelectionReduction
{

//    Q_OBJECT
public:
//    Q_INTERFACES(MzSelectionPluginInterface)
    void NotifyProgress(int notification, int dimensionality);

    QSortPlugin();
    ~QSortPlugin();
    const char* getName(void);
    bool initiateTablePlugin(MzPullDataInterface *pull_data, MzGuiRelatedInterface *gui_tools);
    bool initiateMapsPlugin(MzPullDataInterface *pull_data, MzGuiRelatedInterface *gui_tools);
    void callBack(const unsigned int index, const bool);
    bool openFile(std::string *filename);
    void cancelAnalysis(void);

    bool before_selection_this(void);
    static bool before_selection(void* object);
    void thread_selection_this(void);
    static void thread_selection(void* object,
                                 void *notifier_object,
                                 MzStepNotificationFunction step_notifier,
                                 MzTextNotificationFunction text_notifier);
    void after_selection_this(void);
    static void after_selection(void* object);

    void on_menuAbout_triggered();
    void on_menuSelectFisher_triggered();
    void on_menuSelectMutual_triggered();

private:
    int method;
    bool success;
    double* Qtable;
    unsigned int* Qsorted;

    typedef void(QSortPlugin::*OnActionFunctionPointer)(void);
    MzPullDataInterface* pull_data;
    MzGuiRelatedInterface* gui_tools;
    std::vector <OnActionFunctionPointer> onActionTable;
    void* connectMenuAction(const char* name, const char* tip, OnActionFunctionPointer function);
};

extern "C"
{
    MZ_DECL_EXPORT void* MzNewPluginObject(void);
    MZ_DECL_EXPORT void MzDeletePluginObject(void* object);
}

#endif // QSORTPLUGIN_H
