/*
 * qMaZda - Image Analysis and Pattern Recognition
 * 
 * Copyright 2013  Piotr M. Szczypiński <piotr.szczypinski@p.lodz.pl>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef QSORTWORKER_H
#define QSORTWORKER_H

#include <QObject>
#include "../MzShared/dataforconsole.h"
#include "qsortselection.h"
#include "../MzShared/progress.h"


class PluginWorker : public QObject, QSortSelectionReduction
{
    Q_OBJECT
public:
    explicit PluginWorker(int method_in, double* Qtable, unsigned int* Qsorted,
                       DataForSelection* data,
                       Progress* progressdialog_in,
                       QObject *parent = 0);
    ~PluginWorker();
    //void initLdaWorker(DataForSelection* data, Progress* progressdialog, int dimensions, int maxtime);
    //void notifyProgress(int notification, int dimensionality);

    void breakAnalysis(void);
    void NotifyProgress(int notification, int dimensionality);

public slots:
    void process_sel();

signals:
    void finished(bool success);
    void finished();
    void notifyProgressSignal(int notification, int dimensionality);

private:
    int method;
    double* Qtable;
    unsigned int* Qsorted;

    Progress* progressdialog;
};

#endif // QSORTWORKER_H
