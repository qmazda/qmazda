/*
 * qMaZda - Image Analysis and Pattern Recognition
 * 
 * Copyright 2013  Piotr M. Szczypiński <piotr.szczypinski@p.lodz.pl>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "../MzShared/mzdefines.h"

//#include <QtGlobal>
#include "svmplugin.h"
//#include "../MzShared/multidimselection.h"
#include <string.h>
#include <string>
#include <sstream>
#include <stdlib.h>
#include <stdio.h>
#include <limits>
#include <time.h>
#include "../MzShared/filenameremap.h"

void* MzNewPluginObject(void)
{
    return new SvmPlugin();
}

void MzDeletePluginObject(void* object)
{
    delete static_cast<SvmPlugin *> (object);
}

void SvmPlugin::NotifyProgressStep(void)
{
    (*step_notifier)(notifier_object);
}
void SvmPlugin::NotifyProgressText(std::string text)
{
    (*text_notifier)(notifier_object, text);
}
void SvmPlugin::NotifyProgressStage(const int notification, const int dimensionality, const double value, const unsigned int* selected)
{
    time_t currenttime = time(NULL);
    char* ct = ctime(&currenttime);
    strtok(ct, "\n\r");
    char text[256];

    switch(notification)
    {
    case MULTI_SELECTION_BEGINS: sprintf(text, "Begins %iD at %s\n", dimensionality, ct); (*text_notifier)(notifier_object, text); break;
    case MULTI_SELECTION_STOPPED: sprintf(text, "Stopped %.2f%%\n", (float)(value*100.0)); (*text_notifier)(notifier_object, text); break;
    case MULTI_SELECTION_CANCELED: sprintf(text, "Canceled\n"); (*text_notifier)(notifier_object, text); break;
    case MULTI_SELECTION_COMPLETED: sprintf(text, "Completed %iD\n", dimensionality); (*text_notifier)(notifier_object, text); break;
    case MULTI_SELECTION_FAILED: sprintf(text,"Failed %iD\n", dimensionality); (*text_notifier)(notifier_object, text); break;
    case MULTI_SELECTION_SUCCESS:
        sprintf(text,"D = %i Q = %f\n", dimensionality, (float)value);
        (*text_notifier)(notifier_object, text);
        if(data->featurenames != NULL && selected != NULL)
        {
            for(int d = 0; d < dimensionality; d++)
            {
                sprintf(text," %s %i\n", data->featurenames[selected[d]].c_str(), selected[d]);
                //emit notifyProgressText(text);
                (*text_notifier)(notifier_object, text);
            }
        }
        break;
    }
}


bool SvmPlugin::before_training_this(void)
{
    if(! SetMachineLearningOptions()) return false; //User canceled
    if(! startThreadIn()) return false;
    setInputData(data, normalizeoption);
    gui_tools->openProgressDialog(0, 0, this, "SVM training");
    return true;
}
void SvmPlugin::thread_training_this()
{
    breakanalysis = false;
    success = computeTraining();
}
void SvmPlugin::after_training_this(void)
{
    stopThreadIn();
    if(svmClassifier == NULL || mzClassifier == NULL)
    {
        gui_tools->showMessage("Error", "Training failed", 3);
    }
    stopThreadOut();
}
bool SvmPlugin::before_training(void* object)
{
    return ((SvmPlugin*)object)->before_training_this();
}
void SvmPlugin::thread_training(void* object, void* notifier_object,
                                 MzStepNotificationFunction step_notifier,
                                 MzTextNotificationFunction text_notifier)
{

    ((SvmPlugin*)object)->notifier_object = notifier_object;
    ((SvmPlugin*)object)->step_notifier = step_notifier;
    ((SvmPlugin*)object)->text_notifier = text_notifier;
    ((SvmPlugin*)object)->thread_training_this();
}
void SvmPlugin::after_training(void* object)
{
    ((SvmPlugin*)object)->after_training_this();
}
void SvmPlugin::on_menuTrain_triggered()
{
    gui_tools->InitiateAnalysisActions(this, &before_training, &thread_training, &after_training);
}

bool SvmPlugin::before_test_this(void)
{
    if(svmClassifier == NULL || mzClassifier == NULL)
        return false;
    if(mzClassifier->classifiers.size() != 1)
        return false;
    if((mzClassifier->classifiers)[0].classnames.size() < 2)
        return false;
    if((mzClassifier->classifiers)[0].featurenames.size() < 1)
        return false;
    if(!startThreadIn(&((mzClassifier->classifiers)[0].featurenames)))
        return false;
    if(data->vectornumber < 1)
        return false;
    gui_tools->openProgressDialog(0, data->vectornumber, this, "SVM test");
    return true;
}

void SvmPlugin::thread_test_this()
{
    breakanalysis = false;
    success = computeTest();
}
void SvmPlugin::after_test_this(void)
{
    stopThreadIn();
    if(success)
    {
        gui_tools->showTestResults(&confusionMatrixText, "Test results");
    }
    else
    {
        gui_tools->showMessage("Error", "Test failed. Features required by the classifier may be missing.", 3);
    }
    stopThreadOut();
}
bool SvmPlugin::before_test(void* object)
{
    return ((SvmPlugin*)object)->before_test_this();
}
void SvmPlugin::thread_test(void* object, void* notifier_object,
                                 MzStepNotificationFunction step_notifier,
                                 MzTextNotificationFunction text_notifier)
{

    ((SvmPlugin*)object)->notifier_object = notifier_object;
    ((SvmPlugin*)object)->step_notifier = step_notifier;
    ((SvmPlugin*)object)->text_notifier = text_notifier;
    ((SvmPlugin*)object)->thread_test_this();
}
void SvmPlugin::after_test(void* object)
{
    ((SvmPlugin*)object)->after_test_this();
}
void SvmPlugin::on_menuTest_triggered()
{
    gui_tools->InitiateAnalysisActions(this, &before_test, &thread_test, &after_test);
}

bool SvmPlugin::before_segmentation_this(void)
{
    if(svmClassifier == NULL || mzClassifier == NULL)
    {
        gui_tools->showMessage("Error", "Invalid classifier", 3);
        return false;
    }
    if(mzClassifier->classifiers.size() != 1)
    {
        gui_tools->showMessage("Error", "Invalid classifier: count", 3);
        return false;
    }
    if((mzClassifier->classifiers)[0].classnames.size() < 2)
    {
        gui_tools->showMessage("Error", "Invalid classifier: class names", 3);
        return false;
    }
    if((mzClassifier->classifiers)[0].featurenames.size() < 1)
    {
        gui_tools->showMessage("Error", "Invalid classifier: feature names", 3);
        return false;
    }

    dataMap.result = NULL;
    dataMap.values = NULL;
    dataMap.featurenames.clear();
    dataMap.resultnames.clear();
    unsigned int features_number = (mzClassifier->classifiers)[0].featurenames.size();
    for(unsigned int k = 0; k < features_number; k++)
    {
        dataMap.featurenames.push_back((mzClassifier->classifiers)[0].featurenames[k]);
    }
    dataMap.resultnames.push_back("SVM");

    if(! pull_data->getData(&dataMap))
    {
        gui_tools->showMessage("Error", "Incomplete data or invalid parameters", 3);
        return false;
    }
    gui_tools->openProgressDialog(0, dataMap.vectornumber, this, "SVM segmentation");
    return true;
}
void SvmPlugin::thread_segmentation_this()
{
    breakanalysis = false;
    success = computeSegmentation();
}
void SvmPlugin::after_segmentation_this(void)
{
    stopThreadIn();
    stopThreadOut();
}
bool SvmPlugin::before_segmentation(void* object)
{
    return ((SvmPlugin*)object)->before_segmentation_this();
}
void SvmPlugin::thread_segmentation(void* object, void* notifier_object,
                                 MzStepNotificationFunction step_notifier,
                                 MzTextNotificationFunction text_notifier)
{

    ((SvmPlugin*)object)->notifier_object = notifier_object;
    ((SvmPlugin*)object)->step_notifier = step_notifier;
    ((SvmPlugin*)object)->text_notifier = text_notifier;
    ((SvmPlugin*)object)->thread_segmentation_this();
}
void SvmPlugin::after_segmentation(void* object)
{
    ((SvmPlugin*)object)->after_segmentation_this();
}
void SvmPlugin::on_menuSegment_triggered()
{
    gui_tools->InitiateAnalysisActions(this, &before_segmentation, &thread_segmentation, &after_segmentation);
}

void SvmPlugin::cancelAnalysis(void)
{
    breakanalysis = true;
}

SvmPlugin::SvmPlugin()
{
    normalizeoption = STANDARDIZE;
    pull_data = NULL;
    gui_tools = NULL;
    use_multithreading = true;
}

SvmPlugin::~SvmPlugin()
{
    if(data != NULL) delete data;
}

const char *SvmPlugin::getName(void)
{
    return "Support vector machines";
}

bool SvmPlugin::startThreadIn(void)
{
    if(data != NULL) delete data;
    data = NULL;

    data = new DataForSelection();
    pull_data->getData(data);
    if(data->featurenumber < 1)
    {
        delete data;
        data = NULL;
        gui_tools->showMessage("Error", "Incomplete data or invalid parameters", 3);
        return false;
    }
    return true;
}

bool SvmPlugin::startThreadIn(std::vector<std::string>* featureNames)
{
    if(data != NULL) delete data;
    data = NULL;

    data = new DataForSelection();
    pull_data->getData(featureNames, data);
    if(data->featurenumber < 1)
    {
        delete data;
        data = NULL;
        gui_tools->showMessage("Error", "Incomplete data or invalid parameters", 3);
        return false;
    }
    return true;
}

void SvmPlugin::stopThreadIn(void)
{
    gui_tools->closeProgressDialog();
}

void SvmPlugin::stopThreadOut(void)
{
    dataMap.result = NULL;
    dataMap.values = NULL;
    dataMap.featurenames.clear();
    dataMap.resultnames.clear();
    if(data != NULL) delete data;
    data = NULL;

    if(svmClassifier == NULL || mzClassifier == NULL)
    {
        gui_tools->menuEnable(plugin_save, false);
        gui_tools->menuEnable(plugin_test, false);
    }
    else
    {
        gui_tools->menuEnable(plugin_save, true);
        gui_tools->menuEnable(plugin_test, true);
    }
}



void* SvmPlugin::connectMenuAction(const char* name, const char* tip, OnActionFunctionPointer function)
{
    void* ret = gui_tools->addMenuAction(name, tip, onActionTable.size());
    if(name != NULL && function != NULL)
        onActionTable.push_back(function);
    return ret;
}
void SvmPlugin::callBack(const unsigned int index, const bool use_multithreading)
{
    this->use_multithreading = use_multithreading;
    if(index < onActionTable.size())
        if(onActionTable[index] != NULL)
            (this->*(onActionTable[index]))();
}


bool SvmPlugin::initiateTablePlugin(MzPullDataInterface* pull_data, MzGuiRelatedInterface* gui_tools)
{
    this->pull_data = pull_data;
    this->gui_tools = gui_tools;
    connectMenuAction("Load classifier...", "Load rules to classify data", &SvmPlugin::on_menuLoad_triggered);
    plugin_save = connectMenuAction("Save classifier...", "Save rules to classify data", &SvmPlugin::on_menuSave_triggered);
    connectMenuAction(NULL, NULL, NULL);
    connectMenuAction("Training...", "Training of the support vector machines classifier", &SvmPlugin::on_menuTrain_triggered);
    plugin_test = connectMenuAction("Test classifier...", "Test classifier performance", &SvmPlugin::on_menuTest_triggered);
    connectMenuAction(NULL, NULL, NULL);
    connectMenuAction("About...", "Info about this plugin", &SvmPlugin::on_menuAbout_triggered);

    stopThreadOut();
    return true;
}

bool SvmPlugin::initiateMapsPlugin(MzPullDataInterface* pull_data, MzGuiRelatedInterface* gui_tools)
{
    this->pull_data = pull_data;
    this->gui_tools = gui_tools;
    connectMenuAction("Load classifier...", "Load rules to classify data", &SvmPlugin::on_menuLoad_triggered);
    plugin_save = connectMenuAction("Save classifier...", "Save rules to classify data", &SvmPlugin::on_menuSave_triggered);
    connectMenuAction(NULL, NULL, NULL);
    plugin_test = connectMenuAction("Segment image...", "Image segmentation by the support vector machines classifier", &SvmPlugin::on_menuSegment_triggered);
    connectMenuAction(NULL, NULL, NULL);
    connectMenuAction("About...", "Info about this plugin", &SvmPlugin::on_menuAbout_triggered);

    stopThreadOut();
    return true;
}


void SvmPlugin::on_menuAbout_triggered()
{
    std::stringstream ss;
    ss << "<h2>qmazda SvmPlugin</h2> " << std::endl;
    ss << QMAZDA_VERSION << "<br>" << std::endl;
    ss << "Copyright 2013-2020 by Piotr M. Szczypiński & Artur Klepaczko" << "<br>" << std::endl;
    ss << "Built on " << __DATE__ << " at " << __TIME__ << "<br> <br>" << std::endl;
    ss << "The program is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING THE WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.<br><br>" << std::endl;
    ss << "Home: <a href=\"http://www.eletel.p.lodz.pl/pms/SoftwareQmazda.html\"> http://www.eletel.p.lodz.pl/pms/SoftwareQmazda.html</a><br>" << std::endl;
    ss << "Sources: <a href=\"https://gitlab.com/qmazda/qmazda\"> https://gitlab.com/qmazda/qmazda </a> <br>" << std::endl;
    ss << "Built with:<br>" << std::endl;
    ss << "- libsvm" << " <a href=\"https://www.csie.ntu.edu.tw/~cjlin/libsvm/\">https://www.csie.ntu.edu.tw/~cjlin/libsvm</a> <br>" << std::endl;
    gui_tools->showAbout("About SvmPlugin", ss.str().c_str());
}

bool SvmPlugin::SetMachineLearningOptions(void)
{
    std::stringstream ss;
    ss << "t;";
    switch(normalizeoption)
    {
    case NORMNONE: ss << "none"; break;
    case NORMALIZE: ss << "minmax"; break;
    default: ss << "standardize"; break;
    }
    ss << ";none;standardize;minmax";
    gui_tools->addPropertyOptionsDialog("Normalization", ss.str(), "Feature values normalization");
    ss.str(std::string());
    ss << "t;";
    switch(parameters.svm_type)
    {
    case C_SVC: ss << "C_SVC"; break;
    case NU_SVC: ss << "NU_SVC"; break;
    case ONE_CLASS: ss << "ONE_CLASS"; break;
    case EPSILON_SVR: ss << "EPSILON_SVR"; break;
    case NU_SVR: ss << "NU_SVR"; break;
    default: ss << "C_SVC"; break;
    }
    ss << ";C_SVC;NU_SVC;ONE_CLASS;EPSILON_SVR;NU_SVR";
    gui_tools->addPropertyOptionsDialog("Type", ss.str(), "Svm type");

    ss.str(std::string());
    ss << "t;";
    switch(parameters.kernel_type)
    {
    case LINEAR: ss << "LINEAR"; break;
    case POLY: ss << "POLY"; break;
    case RBF: ss << "RBF"; break;
    case SIGMOID: ss << "SIGMOID"; break;
    case PRECOMPUTED: ss << "PRECOMPUTED"; break;
    default: ss << "LINEAR"; break;
    }
    ss << ";LINEAR;POLY;RBF;SIGMOID;PRECOMPUTED";
    gui_tools->addPropertyOptionsDialog("Kernel", ss.str(), "Svm kernel type");
    ss.str(std::string());
    ss << "i;" << parameters.degree;
    gui_tools->addPropertyOptionsDialog("Degree", ss.str(), "Degree for polynomial");
    ss.str(std::string());
    ss << "f;" << parameters.gamma;
    gui_tools->addPropertyOptionsDialog("Gamma", ss.str(), "For poly/rbf/sigmoid");
    ss.str(std::string());
    ss << "f;" << parameters.coef0;
    gui_tools->addPropertyOptionsDialog("Coef0", ss.str(), "For poly/sigmoid");
    ss.str(std::string());
    ss << "f;" << parameters.cache_size;
    gui_tools->addPropertyOptionsDialog("Cache", ss.str(), "Cache size in MB");
    ss.str(std::string());
    ss << "f;" << parameters.eps;
    gui_tools->addPropertyOptionsDialog("Eps", ss.str(), "Stopping criteria");
    ss.str(std::string());
    ss << "f;" << parameters.C;
    gui_tools->addPropertyOptionsDialog("C", ss.str(), "For C_SVC, EPSILON_SVR and NU_SVR");

    ss.str(std::string());
    ss << "f;" << parameters.nu;
    gui_tools->addPropertyOptionsDialog("Nu", ss.str(), "For NU_SVC, ONE_CLASS, and NU_SVR");
    ss.str(std::string());
    ss << "f;" << parameters.p;
    gui_tools->addPropertyOptionsDialog("P", ss.str(), "For EPSILON_SVR");


    if(gui_tools->openOptionsDialog("SVM classifier training"))
    {
        char* end;
        std::string ns;
        gui_tools->getValueOptionsDialog("Normalization", &ns);
        if(ns == "none") normalizeoption = NORMNONE;
        else if(ns == "minmax") normalizeoption = NORMALIZE;
        else normalizeoption = STANDARDIZE;

        gui_tools->getValueOptionsDialog("Type", &ns);
        if(ns == "NU_SVC") parameters.svm_type = NU_SVC;
        else if(ns == "ONE_CLASS") parameters.svm_type = ONE_CLASS;
        else if(ns == "EPSILON_SVR") parameters.svm_type = EPSILON_SVR;
        else if(ns == "NU_SVR") parameters.svm_type = NU_SVR;
        else parameters.svm_type = C_SVC;

        gui_tools->getValueOptionsDialog("Kernel", &ns);
        if(ns == "POLY") parameters.kernel_type = POLY;
        else if(ns == "RBF") parameters.kernel_type = RBF;
        else if(ns == "SIGMOID") parameters.kernel_type = SIGMOID;
        else if(ns == "PRECOMPUTED") parameters.kernel_type = PRECOMPUTED;
        else parameters.kernel_type = LINEAR;

        gui_tools->getValueOptionsDialog("Degree", &ns);
        parameters.degree = strtol(ns.c_str(), &end, 10);
        gui_tools->getValueOptionsDialog("Gamma", &ns);
        parameters.gamma = atof(ns.c_str());
        gui_tools->getValueOptionsDialog("Coef0", &ns);
        parameters.coef0 = atof(ns.c_str());
        gui_tools->getValueOptionsDialog("Cache", &ns);
        parameters.cache_size = atof(ns.c_str());
        gui_tools->getValueOptionsDialog("Eps", &ns);
        parameters.eps = atof(ns.c_str());
        gui_tools->getValueOptionsDialog("C", &ns);
        parameters.C = atof(ns.c_str());
        gui_tools->getValueOptionsDialog("Nu", &ns);
        parameters.nu = atof(ns.c_str());
        gui_tools->getValueOptionsDialog("P", &ns);
        parameters.p = atof(ns.c_str());
        gui_tools->closeOptionsDialog();
        return true;
    }
    gui_tools->closeOptionsDialog();
    return false;
}

void SvmPlugin::on_menuSave_triggered()
{
    if(svmClassifier != NULL && mzClassifier != NULL)
    {
        std::string fileName;
        unsigned int filter = 0;
        if(gui_tools->getSaveFile(&fileName, &filter))
        {
            bool isok;
            isok = mzClassifier->saveClassifier(fileName.c_str(), (filter==1));
            if(isok)
            {
#if defined _MZ_UTF8_TO_U16
                FILE *fp = _wfopen(_mz_utf8_to_u16(fileName.c_str()).c_str(), L"a");
#else
                FILE *fp = fopen(fileName.c_str(),"a");
#endif
                if(fp != NULL)
                {
                    fprintf(fp, "\n@LibSvmClassifier\n");
                    isok = (svm_save_model(fp, svmClassifier) == 0);
                }
                else isok = false;
            }
            if(!isok)
            {
                gui_tools->showMessage("Error", "Failed to save classifier", 3);
            }
        }
    }
    else
    {
        gui_tools->showMessage("Warning", "No classifier to save.", 2);
    }
}

bool SvmPlugin::openFile(string *filename)
{
    bool r = loadClassifierFromFile(filename->c_str());
    stopThreadOut();
    return r;
}

void SvmPlugin::on_menuLoad_triggered()
{
    std::string fileName;
    if(gui_tools->getOpenFile(&fileName))
    {
        if(! openFile(&fileName))
        {
            gui_tools->showMessage("Error", "Failed to load classifier", 3);
        }
    }
}
