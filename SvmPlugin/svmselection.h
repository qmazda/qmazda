/*
 * qMaZda - Image Analysis and Pattern Recognition
 * 
 * Copyright 2013  Piotr M. Szczypiński <piotr.szczypinski@p.lodz.pl>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef SVMSELECTION_H
#define SVMSELECTION_H
#include "../MzShared/multidimselection.h"
#include "../MzShared/dataforconsole.h"


#include "./libsvm/svm.h"

#include "../MzShared/classifierio.h"

#define NORMNONE -1
#define STANDARDIZE 0
#define NORMALIZE 1

const char SvmSelectionReductionClassifierName[] = "SvmMazdaClassifiers2017";

class SvmSelectionReduction : public MultiDimensionalSelection, public ClassifierAccessInterface
{
public:
    virtual void NotifyProgressText(std::string text);
    virtual void NotifyProgressStep(void);
    virtual void NotifyProgressStage(const int notification, const int dimensionality, const double value, const unsigned int* selected);

    double GoalFunction(unsigned int dimensions, unsigned int *picked_features);
    double ClassifierTraining(unsigned int dimensions, unsigned int* selected);
    SvmSelectionReduction();
    ~SvmSelectionReduction();
    void setInputData(DataForSelection* data, int normalize = STANDARDIZE);

    bool computeTraining(void);
    bool computeTest(void);
    bool computeSegmentation(void);

    const char* getClassifierMagic(void){return SvmSelectionReductionClassifierName;}
    bool configureForClassification(vector<string>* featurenames);
    unsigned int classifyFeatureVector(double* featureVector);
    void segmentImage(unsigned int vectornumber, MazdaMapPixelType **values, MazdaMapPixelType *result);
    virtual bool loadClassifierFromFile(const char *filename);

    std::vector<std::string> getFeatureNames(void);
    std::vector<std::string> getClassNames(void);

    void StoreClassifier(void);

protected:
    DataForSegmentation dataMap;
    DataForSelection* data;
    std::string confusionMatrixText;
    Classifiers* mzClassifier;
    svm_model* svmClassifier;
    svm_parameter parameters;

private:
    svm_problem inputTable;
    void freeInputTable(void);

    unsigned int* featureReindexLut;
    svm_node* featureBuffer;

//    vector<string> outClassNames;
    double* normal_plus;
    double* normal_multi;
    void NormalizeMinMax(void);
    void Standardize(void);
};

#endif // SVMSELECTION_H
