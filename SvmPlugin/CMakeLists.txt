cmake_minimum_required(VERSION 3.1)

project(SvmPlugin LANGUAGES CXX)

set(CMAKE_INCLUDE_CURRENT_DIR ON)
set(CMAKE_SHARED_LIBRARY_PREFIX "mzp")

add_library(SvmPlugin SHARED
    ../MzShared/dataforconsole.cpp
    ../MzShared/csvio.cpp
    ../MzShared/multidimselection.cpp
    svmplugin.cpp
    ../MzShared/classifierio.cpp
    svmselection.cpp
    libsvm/svm.cpp
    ../MzShared/filenameremap.cpp
)

find_package(TIFF REQUIRED)
target_link_libraries(SvmPlugin PRIVATE TIFF::TIFF)

install(TARGETS SvmPlugin DESTINATION bin)
