/*
 * qMaZda - Image Analysis and Pattern Recognition
 * 
 * Copyright 2013  Piotr M. Szczypiński <piotr.szczypinski@p.lodz.pl>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <limits>
#include <sstream>

#include <math.h>
#include "svmselection.h"
#include "../MzShared/filenameremap.h"

const double machineepsilon = 1e-15;

SvmSelectionReduction::SvmSelectionReduction()
{
    featureReindexLut = NULL;
    featureBuffer = NULL;
    normal_plus = NULL;
    normal_multi = NULL;
    data = NULL;
    mzClassifier = NULL;
    svmClassifier = NULL;
    inputTable.l = 0;
    inputTable.y = NULL;
    inputTable.x = NULL;
    memset(&parameters, 0, sizeof(parameters));

    parameters.svm_type = C_SVC;	// SVM variant, C_SVC – classifier with C regularization paramets
    parameters.kernel_type = RBF; // RBF/POLY To use POLY, consider data normalization and experiment with C value
    parameters.degree = 5;		// For interpretation see the LibSVM docs
    parameters.gamma = 0.008;	//if 0 & RBF ==> 1/num_features
    parameters.coef0 = 1;
    parameters.nu = 0.5;
    parameters.cache_size = 100;
    parameters.C = 1.41;
    parameters.eps = 1e-3;
    parameters.p = 0.1;
    parameters.shrinking = 1;
    parameters.probability = 1;
    parameters.nr_weight = 0;
    parameters.weight_label = NULL;
    parameters.weight = NULL;

}
SvmSelectionReduction::~SvmSelectionReduction()
{
    if(mzClassifier != NULL) delete mzClassifier;
    mzClassifier = NULL;
    if(svmClassifier != NULL) svm_free_and_destroy_model(&svmClassifier);
    svmClassifier = NULL;
    freeInputTable();
    if(featureReindexLut != NULL) delete[] featureReindexLut;
    featureReindexLut = NULL;
    if(featureBuffer != NULL) delete[] featureBuffer;
    featureBuffer = NULL;
    if(normal_plus != NULL) free(normal_plus);
    normal_plus = NULL;
    if(normal_multi != NULL) free(normal_multi);
    normal_multi = NULL;
}
void SvmSelectionReduction::freeInputTable(void)
{
    inputTable.l = 0;
    if(inputTable.y != NULL) free(inputTable.y);
    inputTable.y = NULL;
    if(inputTable.x != NULL)
    {
        free(inputTable.x[0]);
        free(inputTable.x);
    }
    inputTable.x = NULL;
}

void SvmSelectionReduction::NotifyProgressText(std::string)
{
}
void SvmSelectionReduction::NotifyProgressStep(void)
{
}
void SvmSelectionReduction::NotifyProgressStage(const int, const int, const double, const unsigned int*)
{
}

void SvmSelectionReduction::setInputData(DataForSelection* data, int normalize)
{
    this->data = data;
    switch(normalize)
    {
    case STANDARDIZE: Standardize(); break;
    case NORMALIZE: NormalizeMinMax(); break;
    }
}


//------------------------------------------------------------------------------
void SvmSelectionReduction::NormalizeMinMax(void)
{
    double min, max, d;
    int f, v;

    if(normal_plus) free(normal_plus);
    if(normal_multi) free(normal_multi);
    normal_plus = (double*)malloc(sizeof(double) * data->featurenumber);
    normal_multi = (double*)malloc(sizeof(double) * data->featurenumber);

    for(f = 0; f < data->featurenumber; f++)
    {
        v = 0;
        min = max = data->values[data->featurenumber * v + f];
        for(v = 1; v < data->vectornumber; v++)
        {
            d = data->values[data->featurenumber * v + f];

            if(d > max) max = d;
            if(d < min) min = d;
        }
        normal_plus[f] = -(max+min)/2.0;
        max -= min;
        if(max >= machineepsilon) max = 2.0/max;
        else max = 1.0;
        normal_multi[f] = max;
        min = normal_plus[f];

        for(v = 0; v < data->vectornumber; v++)
        {
            d = data->values[data->featurenumber * v + f];
            d += min; d *= max;
            data->values[data->featurenumber * v + f] = d;
        }
    }
}

//------------------------------------------------------------------------------
void SvmSelectionReduction::Standardize(void)
{
    double mean, stdd, d;
    int f, v;

    if(normal_plus) free(normal_plus);
    if(normal_multi) free(normal_multi);
    normal_plus = (double*)malloc(sizeof(double) * data->featurenumber);
    normal_multi = (double*)malloc(sizeof(double) * data->featurenumber);

    for(f = 0; f < data->featurenumber; f++)
    {
        mean = 0;
        stdd = 0;
        for(v = 0; v < data->vectornumber; v++)
        {
            d = data->values[data->featurenumber * v + f];
            mean += d;
            stdd += (d * d);
        }
        mean /= data->vectornumber;
        stdd /= data->vectornumber;
        stdd -= (mean * mean);
        stdd = sqrt(stdd);

        normal_plus[f] = -mean;
        if(stdd >= machineepsilon) stdd = 1.0/stdd;
        else stdd = 1.0;
        normal_multi[f] = stdd;

        for(v = 0; v < data->vectornumber; v++)
        {
            d = data->values[data->featurenumber * v + f];
            d -= mean; d *= stdd;
            data->values[data->featurenumber * v + f] = d;
        }
    }
}


//------------------------------------------------------------------------------
double SvmSelectionReduction::GoalFunction(unsigned int /*dim*/, unsigned int* /*picked*/)
{
    return 0;
}

//------------------------------------------------------------------------------
void SvmSelectionReduction::StoreClassifier(void)
{
}

//------------------------------------------------------------------------------
double SvmSelectionReduction::ClassifierTraining(unsigned int /*dimensions*/, unsigned int* /*selected*/)
{
    return 0;
}

bool SvmSelectionReduction::computeTraining(void)
{
    breakanalysis = false;
    svm_node *node;
    double* categoryTable;
    int vs, vm;
    int idx = 0;

    if(mzClassifier != NULL) delete mzClassifier;
    mzClassifier = NULL;
    if(svmClassifier != NULL) svm_free_and_destroy_model(&svmClassifier);
    svmClassifier = NULL;
    freeInputTable();

    int tableSize = data->vectornumber * (data->featurenumber + 1);
    inputTable.x = (svm_node **)malloc(data->vectornumber * sizeof(svm_node*));
    node = (svm_node*) malloc(tableSize*sizeof(svm_node));
    categoryTable = (double*) malloc(data->vectornumber * sizeof(double));
    inputTable.l = data->vectornumber;
    inputTable.y = categoryTable;

    vs = 0;
    for(int c = 0; c < data->classnumber; c++)
    {
        vm = data->classendvectorindex[c];

        for(int v = vs; v < vm; v++)
        {
            categoryTable[v] = c+1;
            inputTable.x[v] = &node[idx];
            for(int f=0; f < data->featurenumber; ++f)
            {
                //if (data->values[data->featurenumber * v + f] != 0.0)
                {
                    node[idx].value = data->values[data->featurenumber * v + f];
                    node[idx].index = f+1;
                    ++idx;
                }
            }
            node[idx++].index = -1;
        }
        vs = vm;
    }

    // Czy zwraca NULL gdy nieudane? Jak stwierdzic czy sukces?
    const char* errtxt = svm_check_parameter(&inputTable, &parameters);
    if(errtxt != NULL)
    {
        printf("Svm training: %s\n", errtxt);
        freeInputTable();
        return false;
    }
    svmClassifier = svm_train(&inputTable, &parameters);

// Jeśli zwolnię te zasoby to zapisuje smiecie do pliku?
//    free(categoryTable);
//    free(inputTable.x);
//    free(node);
/*
    *NOTE* Because svm_model contains pointers to svm_problem, you can
    not free the memory used by svm_problem if you are still using the
    svm_model produced by svm_train().

    *NOTE* To avoid wrong parameters, svm_check_parameter() should be
    called before svm_train().
*/

    //Do zrobienie usuwanie klasyfikatora, zwalnianie pamieci, spr. wyplyw pamieci


//    for(int v = 0; v < data->vectornumber; v++)
//    {
//        svm_node* nnn = &node[v * (data->featurenumber + 1)];
//        double cn = svm_predict(svmClassifier, nnn);
//        printf("Class %f %i : ", (float) cn, (int)cn);
//        for(unsigned int f = 0; f < data->featurenumber; f++) printf(" %i:%f", nnn[f].index , (float) nnn[f].value);
//        printf("\n");
//        fflush(stdout);
//    }

    if(svmClassifier != NULL)
    {
        Classifier classifier;
        for(int c = 0; c < data->classnumber; c++)
        {
            classifier.classnames.push_back(data->classnames[c]);
        }
        std::vector< double >  nplus;
        std::vector< double >  nmult;
        for(int f = 0; f < data->featurenumber; f++)
        {
            if(normal_multi != NULL && normal_plus != NULL)
            {
                nmult.push_back(normal_multi[f]);
                nplus.push_back(normal_plus[f]);
            }
            else
            {
                nmult.push_back(1.0);
                nplus.push_back(0.0);
            }
            classifier.featurenames.push_back(data->featurenames[f]);
        }
        classifier.values.push_back(nmult);
        classifier.values.push_back(nplus);

        mzClassifier = new Classifiers(SvmSelectionReductionClassifierName);
        mzClassifier->classifiers.push_back(classifier);
        return true;
    }
    else
    {
        return false;
    }
}

bool SvmSelectionReduction::computeSegmentation(void)
{
    breakanalysis = false;
    if(! configureForClassification(&dataMap.featurenames))
        return false;
    segmentImage(dataMap.vectornumber, dataMap.values, dataMap.result[0]);
    return true;
}


bool SvmSelectionReduction::computeTest(void)
{
    breakanalysis = false;
    vector<string> featurenames;
    for(int f = 0; f < data->featurenumber; f++)
    {
        featurenames.push_back(data->featurenames[f]);
    }
    if(!configureForClassification(&featurenames))
        return false;
    int classnumber = mzClassifier->classifiers[0].classnames.size();

    unsigned int* fire_table = new unsigned int[data->classnumber*(classnumber+1)];
    memset(fire_table, 0, sizeof(unsigned int)*(data->classnumber*(classnumber+1)));

    for(int ccc = 0; ccc < data->classnumber; ccc++)
    {
        int vs = ccc > 0 ? data->classendvectorindex[ccc-1] : 0;
        int vm = data->classendvectorindex[ccc];
        for(int v = vs; v < vm; v++)
        {
            double* ptri = data->values + (data->featurenumber * v);
            unsigned int cc = classifyFeatureVector(ptri);

            fire_table[cc + (classnumber+1) * ccc]++;

            NotifyProgressStep();
            if(breakanalysis)
            {
                delete[] fire_table;
                if(featureReindexLut != NULL) delete[] featureReindexLut;
                featureReindexLut = NULL;
                if(featureBuffer != NULL) delete[] featureBuffer;
                featureBuffer = NULL;
                return false;
            }
        }
    }

    std::stringstream ss;
    for(int cc = 0; cc < classnumber; cc++)
        ss << "\t" << mzClassifier->classifiers[0].classnames[cc];
    ss << "\t!" << std::endl;

    for(int ccc = 0; ccc < data->classnumber; ccc++)
    {
        ss << data->classnames[ccc];
        for(int cc = 1; cc <= classnumber; cc++)
        {
            ss << "\t" << fire_table[cc + (classnumber+1) * ccc];
        }
        ss << "\t" << fire_table[0 + (classnumber+1) * ccc];
        ss << std::endl;
    }
    confusionMatrixText = ss.str();
    delete[] fire_table;
    if(featureReindexLut != NULL) delete[] featureReindexLut;
    featureReindexLut = NULL;
    if(featureBuffer != NULL) delete[] featureBuffer;
    featureBuffer = NULL;
    return true;
}

bool SvmSelectionReduction::loadClassifierFromFile(const char* filename)
{
    Classifiers* mzClassifierTemp = new Classifiers(SvmSelectionReductionClassifierName);
    svm_model* svmClassifierTemp;
    if(!mzClassifierTemp->loadClassifier(filename))
    {
        delete mzClassifierTemp;
        return false;
    }
    else
    {
#if defined _MZ_UTF8_TO_U16
        FILE *fp = _wfopen(_mz_utf8_to_u16(filename).c_str(), L"r");
#else
        FILE *fp = fopen(filename,"r");
#endif
        if(fp != NULL)
        {
            char strbufer[64];
            do
            {
                if(fscanf(fp, "%63s", strbufer) != 1)
                {
                    delete mzClassifierTemp;
                    return false;
                }
            }
            while(strcmp(strbufer,"@LibSvmClassifier") != 0);

            svmClassifierTemp = svm_load_model(fp);
            if(svmClassifierTemp == NULL)
            {
                delete mzClassifierTemp;
                return false;
            }
        }
        else
        {
            delete mzClassifierTemp;
            return false;
        }
        if(mzClassifier != NULL) delete mzClassifier;
        mzClassifier = mzClassifierTemp;
        if(svmClassifier != NULL) svm_free_and_destroy_model(&svmClassifier);
        svmClassifier = svmClassifierTemp;
        freeInputTable();

        return true;
    }
}

bool SvmSelectionReduction::configureForClassification(vector<string> *featurenames)
{
    if(featureReindexLut != NULL) delete[] featureReindexLut;
    featureReindexLut = NULL;
    if(featureBuffer != NULL) delete[] featureBuffer;
    featureBuffer = NULL;
    if(mzClassifier == NULL || svmClassifier == NULL)
        return false;
    unsigned int classifiers_size = mzClassifier->classifiers.size();
    if(classifiers_size != 1)
        return false;

    unsigned int featureReindexLutSize = mzClassifier->classifiers[0].featurenames.size();
    if(featureReindexLutSize <= 0)
        return false;

    if(mzClassifier->classifiers[0].values[0].size() < featureReindexLutSize)
        return false;
    if(mzClassifier->classifiers[0].values[1].size() < featureReindexLutSize)
        return false;
    featureReindexLut = new unsigned int[featureReindexLutSize];
    featureBuffer = new svm_node[featureReindexLutSize+1];
    featureBuffer[featureReindexLutSize].index = -1;
    featureBuffer[featureReindexLutSize].value = 0.0;

    for(unsigned int k = 0; k < featureReindexLutSize; k++)
    {
        featureBuffer[k].index = k + 1;
        bool found = false;
        unsigned int fns = featurenames->size();
        for(unsigned int kk = 0; kk < fns; kk++)
        {
            if((*featurenames)[kk] == mzClassifier->classifiers[0].featurenames[k])
            {
                found = true;
                featureReindexLut[k] = kk;
                break;
            }
        }
        if(!found)
        {
            if(featureReindexLut != NULL) delete[] featureReindexLut;
            featureReindexLut = NULL;
            if(featureBuffer != NULL) delete[] featureBuffer;
            featureBuffer = NULL;
            return false;
        }
    }
    return true;
}

unsigned int SvmSelectionReduction::classifyFeatureVector(double* featureVector)
{
    if(featureReindexLut == NULL || featureBuffer == NULL || mzClassifier == NULL || svmClassifier == NULL)
        return 0;
    if(mzClassifier->classifiers[0].values.size() < 2)
        return 0;
    unsigned int featureReindexLutSize = mzClassifier->classifiers[0].featurenames.size();
    if(mzClassifier->classifiers[0].values[0].size() < featureReindexLutSize)
        return 0;
    if(mzClassifier->classifiers[0].values[1].size() < featureReindexLutSize)
        return 0;
    for(unsigned int f = 0; f < featureReindexLutSize; f++)
    {
        unsigned int ff = featureReindexLut[f];
        double value = featureVector[ff];
        featureBuffer[f].value = (value + mzClassifier->classifiers[0].values[1][f]) * mzClassifier->classifiers[0].values[0][f];
    }
    double cn = svm_predict(svmClassifier, featureBuffer);

#ifdef MZ_DEBUG_PRINT_PROBABILITY
    if(svm_check_probability_model(svmClassifier) > 0)
    {
        unsigned int nclass = mzClassifier->classifiers[0].classnames.size();
        double* probtab = new double[nclass];
        memset(probtab, nclass, sizeof(double));
        svm_predict_probability(svmClassifier, featureBuffer, probtab);
        double maxprobtab = 0.0;
        for(unsigned int i = 0; i < nclass; i++)
        {
            if(maxprobtab < probtab[i])
                maxprobtab = probtab[i];
        }
        printf("Probability = %f\n", (float)maxprobtab);
    }
#endif

    unsigned int icn = cn;
//    printf("Class %f %i : ", (float) cn, icn);
//    for(unsigned int f = 0; f < featureReindexLutSize; f++) printf(" %i:%f", featureBuffer[f].index , (float) featureBuffer[f].value);
//    printf("\n");
//    fflush(stdout);
    return icn;
}

void SvmSelectionReduction::segmentImage(unsigned int vectornumber, MazdaMapPixelType **values, MazdaMapPixelType *result)
{
    breakanalysis = false;
    unsigned int featureReindexLutSize = mzClassifier->classifiers[0].featurenames.size();
    for(unsigned int v = 0; v < vectornumber; v++)
    {
        result[v] = 0;
        for(unsigned int f = 0; f < featureReindexLutSize; f++)
        {
            unsigned int ff = featureReindexLut[f];
            double value = values[ff][v];
            featureBuffer[f].value = (value + mzClassifier->classifiers[0].values[1][f]) * mzClassifier->classifiers[0].values[0][f];
        }
        double cn = svm_predict(svmClassifier, featureBuffer);
        result[v] = cn;
        if(breakanalysis) break;
        NotifyProgressStep();
    }
}


std::vector<std::string> SvmSelectionReduction::getClassNames(void)
{
    std::vector<std::string> ret = mzClassifier->classifiers[0].classnames;
    ret.insert(ret.begin(), std::string("!"));
    //ret.push_back(std::string("!"));
    return ret;
}

std::vector<std::string> SvmSelectionReduction::getFeatureNames(void)
{
    std::vector<std::string> names;
    if(mzClassifier == NULL) return names;

    unsigned int nc = mzClassifier->classifiers.size();
    for(unsigned int c = 0; c < nc; c++)
    {
        std::vector <std::string>* cl;
        cl = & mzClassifier->classifiers[c].featurenames;
        //cl = & classifier->classifiers[c].classnames;
        unsigned int nn = cl->size();
        for(unsigned int n = 0; n < nn; n++)
        {
            unsigned int nm = names.size();
            unsigned int m;
            for(m = 0; m < nm; m++)
            {
                if((*cl)[n] == names[m]) break;
            }
            if(m >= nm) names.push_back((*cl)[n]);
        }
    }
    return names;
}

