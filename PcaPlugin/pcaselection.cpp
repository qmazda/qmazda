/*
 * qMaZda - Image Analysis and Pattern Recognition
 * 
 * Copyright 2023  Piotr M. Szczypiński <piotr.szczypinski@p.lodz.pl>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <limits>
#include <sstream>

#include "pcaselection.h"


// Do zastanowienia zmiana implementacji z Alglib na Eigen library ref:
// https://blog.demofox.org/2022/07/12/calculating-svd-and-pca-in-c/
// https://stackoverflow.com/questions/33531505/principal-component-analysis-with-eigen-library

PcaReduction::PcaReduction()
{
    featureReindexLut = NULL;
    normal_plus = NULL;
    normal_multi = NULL;
    classifier = NULL;
    data = NULL;
}
PcaReduction::~PcaReduction()
{
    if(featureReindexLut != NULL) delete[] featureReindexLut;
    featureReindexLut = NULL;
    if(normal_plus != NULL) free(normal_plus);
    normal_plus = NULL;
    if(normal_multi != NULL) free(normal_multi);
    normal_multi = NULL;
}

void PcaReduction::NotifyProgressText(std::string)
{
}

void PcaReduction::setClassifier(Classifiers *classifier)//, int classtodistinguish)
{
    this->classifier = classifier;
}

void PcaReduction::setInputData(DataForSelection* data)
{
    this->data = data;
    switch(normalizeoption)
    {
    case STANDARDIZE: Standardize(); break;
    case NORMALIZE: NormalizeMinMax(); break;
    }
}

//------------------------------------------------------------------------------
void PcaReduction::NormalizeMinMax(void)
{
    double min, max, d;
    int f, v;

    if(normal_plus) free(normal_plus);
    if(normal_multi) free(normal_multi);
    normal_plus = (double*)malloc(sizeof(double) * data->featurenumber);
    normal_multi = (double*)malloc(sizeof(double) * data->featurenumber);

    for(f = 0; f < data->featurenumber; f++)
    {
        v = 0;
        min = max = data->values[data->featurenumber * v + f];
        for(v = 1; v < data->vectornumber; v++)
        {
            d = data->values[data->featurenumber * v + f];

            if(d > max) max = d;
            if(d < min) min = d;
        }
        normal_plus[f] = -(max+min)/2.0;
        max -= min;
        if(max >= machineepsilon) max = 2.0/max;
        else max = 1.0;
        normal_multi[f] = max;
        min = normal_plus[f];

        for(v = 0; v < data->vectornumber; v++)
        {
            d = data->values[data->featurenumber * v + f];
            d += min; d *= max;
            data->values[data->featurenumber * v + f] = d;
        }
    }
}

//------------------------------------------------------------------------------
void PcaReduction::Standardize(void)
{
    double mean, stdd, d;
    int f, v;

    if(normal_plus) free(normal_plus);
    if(normal_multi) free(normal_multi);
    normal_plus = (double*)malloc(sizeof(double) * data->featurenumber);
    normal_multi = (double*)malloc(sizeof(double) * data->featurenumber);

    for(f = 0; f < data->featurenumber; f++)
    {
        mean = 0;
        stdd = 0;
        for(v = 0; v < data->vectornumber; v++)
        {
            d = data->values[data->featurenumber * v + f];
            mean += d;
            stdd += (d * d);
        }
        mean /= data->vectornumber;
        stdd /= data->vectornumber;
        stdd -= (mean * mean);
        stdd = sqrt(stdd);

        normal_plus[f] = -mean;
        if(stdd >= machineepsilon) stdd = 1.0/stdd;
        else stdd = 1.0;
        normal_multi[f] = stdd;

        for(v = 0; v < data->vectornumber; v++)
        {
            d = data->values[data->featurenumber * v + f];
            d -= mean; d *= stdd;
            data->values[data->featurenumber * v + f] = d;
        }
    }
}

void PcaReduction::StoreClassifier(unsigned int dim, real_2d_array& rotation_matrix, real_1d_array& variances)
{
    bool nor = (normal_plus != NULL && normal_multi != NULL);
    vector< vector<double> > vvalues;

    unsigned int diml = 0;
    float variance = 0;
    float varl = 0;
    for(unsigned int ff = 0; ff < dim; ff++)
    {
        variance += variances(ff);
        //printf("%f\n", variances(ff));
    }
    //fflush(stdout);

    for(diml = 0; diml < dim; diml++)
    {
        varl += variances(diml);
        if(varl/variance >= collected_var && diml+1 >= dimensions)
        {
            diml++;
            break;
        }
    }

//    if(rotation_algorithm == ROTATION_PCA)
//    {
//        for(diml = 0; diml < dim; diml++)
//        {
//            varl += variances(diml);
//            if(varl/variance >= collected_var && diml >= dimensions)
//            {
//                diml++;
//                break;
//            }
//        }
//    }
//    else
//    {
//        diml = (dimensions > data->classnumber-1 ? dimensions : data->classnumber-1);
//    }

    for(unsigned int ff = 0; ff < diml; ff++)
    {
        vector<double> vprojection;
        vprojection.resize(dim + 1);
        for(unsigned int f = 0; f < dim; f++)
        {
            double ww = 1.0;
            ww = rotation_matrix(f, ff);
            if(nor)
            {
                ww *= normal_multi[f];
            }
            vprojection[f] = ww;
        }
        vprojection[dim] = variances(ff);
        vvalues.push_back(vprojection);
    }

    vector<string> vclassnames;
    vclassnames.resize(1);
    stringstream ss;

    if(rotation_algorithm == ROTATION_PCA)
        ss << "Pc";
    else
        ss << "Mdf";

    vclassnames[0] = ss.str();

    vector<string> vfeaturenames;
    vfeaturenames.resize(dim);
    for(unsigned int f = 0; f < dim; f++)
    {
        vfeaturenames[f] = data->featurenames[f];
    }

    Classifier vclassifier;
    vclassifier.classnames = vclassnames;
    vclassifier.featurenames = vfeaturenames;
    vclassifier.values = vvalues;

    if(classifier != NULL) delete classifier;
    classifier = new Classifiers(PcaSelectionReductionClassifierName);
    classifier->classifiers.push_back(vclassifier);
}

bool PcaReduction::computeRotation(void)
{
//    bool ret = true;
    int dim = data->featurenumber;
    if(dim <= 1)
        return false;

    unsigned int nvectors = data->vectornumber;
    unsigned int nclasses = data->classnumber;

    ae_int_t info;
    real_2d_array xy;
    real_1d_array variances;
    real_2d_array rotation_matrix;
    real_1d_array mdf;

    xy.setlength(nvectors, dim+1);
    variances.setlength(dim);
    rotation_matrix.setlength(dim, dim);
    mdf.setlength(nvectors);

    for(int cls = 0; cls < nclasses; cls++)
    {
        unsigned int vs = cls > 0 ? data->classendvectorindex[cls-1] : 0;
        unsigned int ve = data->classendvectorindex[cls];
        for(unsigned int v = vs; v < ve; v++)
        {
            for(unsigned int f = 0; f < dim; f++)
            {
                xy(v, f) = data->values[data->featurenumber * v + f];
            }
            xy(v, dim) = cls;
        }
    }
    if(rotation_algorithm == ROTATION_LDA)
    {
        fisherldan(xy, nvectors, dim, nclasses, info, rotation_matrix);

        if(info > 0)
        {
            for(unsigned int ff = 0; ff < dim; ff++)
            {
                double mc = 0;
                double vbc = 0;
                double vic = 0;

                for(int cls = 0; cls < nclasses; cls++)
                {
                    unsigned int vs = cls > 0 ? data->classendvectorindex[cls-1] : 0;
                    unsigned int ve = data->classendvectorindex[cls];

                    double mt = 0;
                    double vt = 0;
                    for(unsigned int v = vs; v < ve; v++)
                    {
                        double mdf = 0;
                        for(unsigned int f = 0; f < dim; f++)
                        {
                            mdf += data->values[data->featurenumber * v + f] * rotation_matrix(f, ff) ;
                        }
                        mt += mdf;
                        vt += (mdf*mdf);
                    }
                    mt /= fabs(ve-vs);
                    vt /= fabs(ve-vs);
                    vt = vt - mt*mt;
                    mc += mt;
                    vbc += (mt*mt);
                    vic += vt;
                }
                mc /= nclasses;
                vic /= nclasses;
                vbc /= nclasses;
                vbc = vbc-mc*mc;
                variances(ff) = vbc/vic;
            }
        }
    }
    else
        pcabuildbasis(xy, nvectors, dim, info, variances, rotation_matrix);

    if(info > 0)
    {
        StoreClassifier(dim, rotation_matrix, variances);
        return true;
    }
    return false;
}

bool PcaReduction::configureForClassification(vector<string> *featurenames)
{
    if(featureReindexLut != NULL) delete[] featureReindexLut;
    featureReindexLut = NULL;

    if(classifier == nullptr)
        return false;
    if (classifier->classifiers.size() < 1)
        return false;
    if(classifier->classifiers[0].classnames.size() < 1)
        return false;
    if(classifier->classifiers[0].values.size() < 1)
        return false;

    unsigned int featureReindexLutSize = classifier->classifiers[0].featurenames.size();
    featureReindexLut = new unsigned int[featureReindexLutSize];

    unsigned int featureReindexLutPosition = 0;
    unsigned int classIndicesPosition = 0;

    unsigned int features_number = classifier->classifiers[0].featurenames.size();
    for(unsigned int k = 0; k < features_number; k++)
    {
        bool found = false;
        unsigned int fns = featurenames->size();
        for(unsigned int kk = 0; kk < fns; kk++)
        {
            if((*featurenames)[kk] == classifier->classifiers[0].featurenames[k])
            {
                found = true;
                featureReindexLut[featureReindexLutPosition+k] = kk;
                break;
            }
        }
        if(!found)
            return false;
    }
    return true;
}

unsigned int PcaReduction::classifyFeatureVector(double* featureVector)
{
    return 0;
}
void PcaReduction::segmentImage(unsigned int vectornumber, MazdaMapPixelType** values, MazdaMapPixelType* result)
{
}

//------------------------------------------------------------------------------
bool PcaReduction::computeMdf(void)
{
    vector<string> featurenames;
    for(int f = 0; f < data->featurenumber; f++)
        featurenames.push_back(data->featurenames[f]);
    if(! configureForClassification(&featurenames))
        return false;
    if(data->vectornumber <= 0)
        return false;
    int vectornumber = data->vectornumber;
    int mdfnumber = classifier->classifiers[0].values.size();

    string ss = classifier->classifiers[0].classnames[0];
    double* mdf_table = new double[vectornumber*mdfnumber];
    for(int y = 0; y < vectornumber*mdfnumber; y++)
        mdf_table[y] = std::numeric_limits<double>::quiet_NaN();

//    for(int ccc = 0; ccc < data->classnumber; ccc++) ????
    {
        int vm = data->vectornumber;
        for(int v = 0; v < vm; v++)
        {
            double* featureVector = data->values + (data->featurenumber * v);
            double* mdfs = mdf_table + mdfnumber*v;
            std::vector< std::vector<double> > * values = &(classifier->classifiers[0].values);
//            unsigned int featureReindexLutPosition = 0;
            int mdfnumber = values->size();
            for(unsigned int m = 0; m < mdfnumber; m++)
            {
                double mdf = 0;
                int weights_number = (*values)[m].size() - 1;
                for(int w = 0; w < weights_number; w++)
                {
                    mdf += (*values)[m][w] * (featureVector[featureReindexLut[w]]);
                }
                mdfs[m] = mdf;
            }
        }
    }
    if(featureReindexLut != NULL) delete[] featureReindexLut;
    featureReindexLut = NULL;

    delete[] data->values;
    delete[] data->featurenames;
    data->featurenumber = mdfnumber;
    data->featurenames = new string[mdfnumber];
    data->values = mdf_table;
    for(unsigned int m = 0; m < mdfnumber; m++)
    {
        stringstream sss;
        sss << ss;
        sss << (m+1);
        data->featurenames[m] = sss.str();
    }
    return true;
}

bool PcaReduction::computeMdfMaps(void)
{
    if(! configureForClassification(&dataMap.featurenames))
        return false;
    int mdfnumber = classifier->classifiers[0].values.size();
    if(dataMap.resultnames.size() != mdfnumber)
        return false;

    for(unsigned int v = 0; v < dataMap.vectornumber; v++)
    {
        for(unsigned int m = 0; m < mdfnumber; m++)
        {
            unsigned int weights_number = classifier->classifiers[0].values[m].size() - 1;
            double mdf = 0;
            for(unsigned int f = 0; f < weights_number; f++)
            {
                mdf += (dataMap.values[f][v] * classifier->classifiers[0].values[m][f]);
            }
            dataMap.result[m][v] = mdf;
        }
        //NotifyProgressStep();
    }
    if(featureReindexLut != NULL) delete[] featureReindexLut;
    featureReindexLut = NULL;
    return true;
}

bool PcaReduction::loadClassifierFromFile(const char *filename)
{
    Classifiers* tmpClassifier = new Classifiers(PcaSelectionReductionClassifierName);
    if(tmpClassifier->loadClassifier(filename))
    {
        if(classifier != NULL) delete classifier;
        classifier = tmpClassifier;
        return true;
    }
    else
        delete tmpClassifier;
    return false;
}

std::vector<std::string> PcaReduction::getClassNames(void)
{
    vector<string> class_names;
    return class_names;
}

std::vector<std::string> PcaReduction::getFeatureNames(void)
{
    std::vector<std::string> names;
    if(classifier == NULL) return names;

    unsigned int nc = classifier->classifiers.size();
    for(unsigned int c = 0; c < nc; c++)
    {
        std::vector <std::string>* cl;
        cl = & classifier->classifiers[c].featurenames;
        //cl = & classifier->classifiers[c].classnames;
        unsigned int nn = cl->size();
        for(unsigned int n = 0; n < nn; n++)
        {
            unsigned int nm = names.size();
            unsigned int m;
            for(m = 0; m < nm; m++)
            {
                if((*cl)[n] == names[m]) break;
            }
            if(m >= nm) names.push_back((*cl)[n]);
        }
    }
    return names;
}
