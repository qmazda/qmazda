/*
 * qMaZda - Image Analysis and Pattern Recognition
 * 
 * Copyright 2023  Piotr M. Szczypiński <piotr.szczypinski@p.lodz.pl>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "../MzShared/mzdefines.h"

//#include <QtGlobal>
#include "pcaplugin.h"
#include "../MzShared/multidimselection.h"

#include <string>
#include <sstream>
#include <stdlib.h>
#include <limits>
#include <time.h>

void* MzNewPluginObject(void)
{
    return new PcaPlugin();
}

void MzDeletePluginObject(void* object)
{
    delete static_cast<PcaPlugin *> (object);
}

void PcaPlugin::NotifyProgressStep(void)
{
    (*step_notifier)(notifier_object);
}
void PcaPlugin::NotifyProgressText(std::string text)
{
    (*text_notifier)(notifier_object, text);
}
void PcaPlugin::NotifyProgressStage(const int notification, const int dimensionality, const double value, const unsigned int* selected)
{
    time_t currenttime = time(NULL);
    char* ct = ctime(&currenttime);
    strtok(ct, "\n\r");
    char text[256];

    switch(notification)
    {
    case MULTI_SELECTION_BEGINS: sprintf(text, "Begins %iD at %s\n", dimensionality, ct); (*text_notifier)(notifier_object, text); break;
    case MULTI_SELECTION_STOPPED: sprintf(text, "Stopped %.2f%%\n", (float)(value*100.0)); (*text_notifier)(notifier_object, text); break;
    case MULTI_SELECTION_CANCELED: sprintf(text, "Canceled\n"); (*text_notifier)(notifier_object, text); break;
    case MULTI_SELECTION_COMPLETED: sprintf(text, "Completed %iD\n", dimensionality); (*text_notifier)(notifier_object, text); break;
    case MULTI_SELECTION_FAILED: sprintf(text,"Failed %iD\n", dimensionality); (*text_notifier)(notifier_object, text); break;
    case MULTI_SELECTION_SUCCESS:
        sprintf(text,"D = %i Q = %f\n", dimensionality, (float)value);
        (*text_notifier)(notifier_object, text);
        if(data->featurenames != NULL && selected != NULL)
        {
            for(int d = 0; d < dimensionality; d++)
            {
                sprintf(text," %s %i\n", data->featurenames[selected[d]].c_str(), selected[d]);
                //emit notifyProgressText(text);
                (*text_notifier)(notifier_object, text);
            }
        }
        break;
    }
}

void PcaPlugin::thread_mdfs_this()
{
    success = computeMdf();
}
void PcaPlugin::after_mdfs_this(void)
{
    stopThreadIn();
//    releaseTempClassifier();
    if(success)
    {
        unsigned int* sorted = new unsigned int[data->featurenumber];
        for(int c = 0; c < data->featurenumber; c++) sorted[c] = c;
        {
            int count = data->featurenumber;
            if(count > 0)
            {
                SelectedFeatures ret;
                ret.featurenumber = count;
                ret.featureoriginalindex = new int[count];
                for(int c = 0; c < data->featurenumber; c++)
                {
                    ret.featureoriginalindex[c] = sorted[c];
                }
                pull_data->setData(data, &ret);
            }
        }
        delete[] sorted;
//        Qtable = NULL;
    }
    else
    {
        gui_tools->showMessage("Error", "Computation failed. Features required by the classifier may be missing.", 3);
    }
    stopThreadOut();
}
void PcaPlugin::thread_mdfs(void* object, void* notifier_object,
                                 MzStepNotificationFunction step_notifier,
                                 MzTextNotificationFunction text_notifier)
{

    ((PcaPlugin*)object)->notifier_object = notifier_object;
    ((PcaPlugin*)object)->step_notifier = step_notifier;
    ((PcaPlugin*)object)->text_notifier = text_notifier;
    ((PcaPlugin*)object)->thread_mdfs_this();
}
void PcaPlugin::after_mdfs(void* object)
{
    ((PcaPlugin*)object)->after_mdfs_this();
}

bool PcaPlugin::before_mdfs_this(void)
{
    std::vector<std::string> featureNames;
    unsigned int classifiers_size = classifier->classifiers.size();
    for(unsigned int c = 0; c < classifiers_size; c++)
    {
        unsigned int features_number = (classifier->classifiers)[c].featurenames.size();
        for(unsigned int k = 0; k < features_number; k++)
        {
            featureNames.push_back((classifier->classifiers)[c].featurenames[k]);
        }
    }
    if(!startThreadIn(&featureNames)) return false;
    featureNames.clear();

    mz_uint64 maxSteps = data->vectornumber;
    if(classifier == NULL)
    {
        gui_tools->showMessage("Error", "Empty rotation matrix", 3);
        return false;
    }
    int oldn = normalizeoption;
    normalizeoption = NORMNONE;
    setInputData(data);
    normalizeoption = oldn;
    setClassifier(classifier);//, classtodistinguish);
    gui_tools->openProgressDialog(0, maxSteps, this, "Compute reduced space features");
    return true;
}
bool PcaPlugin::before_mdfs(void* object)
{
    return ((PcaPlugin*)object)->before_mdfs_this();
}




void PcaPlugin::on_menuMdfs_triggered()
{
    gui_tools->InitiateAnalysisActions(this, &before_mdfs, &thread_mdfs, &after_mdfs);
}

bool PcaPlugin::before_projection_this(void)
{
    if(! startThreadIn()) return false;
    unsigned int maxf = data->featurenumber*data->vectornumber;
    for(unsigned int f = 0; f < maxf; f++)
    {
        double fff = data->values[f];
        if(!(fff == fff))
        {
            gui_tools->showMessage("Error", "Remove NaNs, input data cannot contain NaNs.", 3);
            return false;
        }
    }

    mz_uint64 maxSteps = 1;
//    int classtodistinguish = -3;
    setInputData(data);
    setClassifier(classifier);//, classtodistinguish);
//    setOutputBuffers(Qtable, Qsorted);
    gui_tools->openProgressDialog(0, maxSteps, this, "Space reduction analysis");
    return true;
}

void PcaPlugin::thread_projection_this()
{
//    breakanalysis = false;
    success = computeRotation();
}
void PcaPlugin::after_projection_this(void)
{
    stopThreadIn();
    if(success)
    {
        gui_tools->showMessage("Information", "Projection complete", 1);
    }
    else
    {
        if(rotation_algorithm == ROTATION_LDA)
            gui_tools->showMessage("Error", "Projection failed. LDA requires at least two classes.", 3);
        else
            gui_tools->showMessage("Error", "Projection failed", 3);
    }
    stopThreadOut();
}
bool PcaPlugin::before_projection(void* object)
{
    return ((PcaPlugin*)object)->before_projection_this();
}
void PcaPlugin::thread_projection(void* object, void* notifier_object,
                                 MzStepNotificationFunction step_notifier,
                                 MzTextNotificationFunction text_notifier)
{

    ((PcaPlugin*)object)->notifier_object = notifier_object;
    ((PcaPlugin*)object)->step_notifier = step_notifier;
    ((PcaPlugin*)object)->text_notifier = text_notifier;
    ((PcaPlugin*)object)->thread_projection_this();
}
void PcaPlugin::after_projection(void* object)
{
    ((PcaPlugin*)object)->after_projection_this();
}

void PcaPlugin::on_menuPca_triggered()
{
    rotation_algorithm = ROTATION_PCA;
    gui_tools->InitiateAnalysisActions(this, &before_projection, &thread_projection, &after_projection);
}

void PcaPlugin::on_menuLda_triggered()
{
    rotation_algorithm = ROTATION_LDA;
    gui_tools->InitiateAnalysisActions(this, &before_projection, &thread_projection, &after_projection);
}

bool PcaPlugin::before_mdfmaps_this(void)
{
    if(classifier->classifiers.size() != 1)
    {
        gui_tools->showMessage("Error", "Load valid rotation matrix first", 3);
        return false;
    }
    if(classifier->classifiers[0].classnames.size() != 1)
    {
        gui_tools->showMessage("Error", "Load valid rotation matrix first", 3);
        return false;
    }

    dataMap.result = NULL;
    dataMap.values = NULL;
    dataMap.featurenames.clear();
    dataMap.resultnames.clear();

    unsigned int features_number = (classifier->classifiers)[0].featurenames.size();
    for(unsigned int k = 0; k < features_number; k++)
    {
        dataMap.featurenames.push_back((classifier->classifiers)[0].featurenames[k]);
    }
    string ss = classifier->classifiers[0].classnames[0];

    unsigned int mmax = classifier->classifiers[0].values.size();
    for(unsigned int m = 0; m < mmax; m++)
    {
        stringstream sss;
        sss << ss;
        sss << (m+1);
        dataMap.resultnames.push_back(sss.str());
    }
    if(! pull_data->getData(&dataMap))
    {
        gui_tools->showMessage("Error", "Incomplete data or invalid parameters", 3);
        return false;
    }
    if(classifier == NULL)
    {
        gui_tools->showMessage("Error", "Invalid classifier", 3);
        return false;
    }
    gui_tools->openProgressDialog(0, dataMap.vectornumber, this, "Feature maps computation");
    return true;
}
void PcaPlugin::thread_mdfmaps_this()
{
//    breakanalysis = false;
    success = computeMdfMaps();
}
void PcaPlugin::after_mdfmaps_this(void)
{
    stopThreadIn();
    stopThreadOut();
}
bool PcaPlugin::before_mdfmaps(void* object)
{
    return ((PcaPlugin*)object)->before_mdfmaps_this();
}
void PcaPlugin::thread_mdfmaps(void* object, void* notifier_object,
                                 MzStepNotificationFunction step_notifier,
                                 MzTextNotificationFunction text_notifier)
{

    ((PcaPlugin*)object)->notifier_object = notifier_object;
    ((PcaPlugin*)object)->step_notifier = step_notifier;
    ((PcaPlugin*)object)->text_notifier = text_notifier;
    ((PcaPlugin*)object)->thread_mdfmaps_this();
}
void PcaPlugin::after_mdfmaps(void* object)
{
    ((PcaPlugin*)object)->after_mdfmaps_this();
}
void PcaPlugin::on_menuMdfMaps_triggered()
{
    gui_tools->InitiateAnalysisActions(this, &before_mdfmaps, &thread_mdfmaps, &after_mdfmaps);
}

void PcaPlugin::cancelAnalysis(void)
{
//    breakanalysis = true;
}

PcaPlugin::PcaPlugin()
{
    data = NULL;
    classifier = NULL;
    normalizeoption = STANDARDIZE;
    dimensions = 3;
    collected_var = 0.99;
    rotation_algorithm = ROTATION_PCA;
    pull_data = NULL;
    gui_tools = NULL;
    plugin_save = NULL;
    plugin_mdfs = NULL;
//    plugin_test = NULL;
}

PcaPlugin::~PcaPlugin()
{
    if(data != NULL) delete data;
}

const char* PcaPlugin::getName(void)
{
    return "Space reduction";
}


bool PcaPlugin::startThreadIn(void)
{
    if(data != NULL) delete data;
    data = new DataForSelection();
    pull_data->getData(data);
    if(data->featurenumber < 1)
    {
        delete data;
        data = NULL;
        gui_tools->showMessage("Error", "Incomplete data or invalid parameters", 3);
        return false;
    }
    return true;
}

bool PcaPlugin::startThreadIn(std::vector<std::string>* featureNames)
{
    if(data != NULL) delete data;
    data = new DataForSelection();
    pull_data->getData(featureNames, data);
    if(data->featurenumber < 1)
    {
        delete data;
        data = NULL;
        gui_tools->showMessage("Error", "Incomplete data or invalid parameters", 3);
        return false;
    }
    return true;
}

void PcaPlugin::stopThreadIn(void)
{
    gui_tools->closeProgressDialog();
}

void PcaPlugin::stopThreadOut(void)
{
    dataMap.result = NULL;
    dataMap.values = NULL;
    dataMap.featurenames.clear();
    dataMap.resultnames.clear();
    if(data != NULL) delete data;
    data = NULL;
    if(classifier == NULL)
    {
        gui_tools->menuEnable(plugin_save, false);
        gui_tools->menuEnable(plugin_mdfs, false);
//        gui_tools->menuEnable(plugin_test, false);
    }
    else
    {
        gui_tools->menuEnable(plugin_save, true);
        gui_tools->menuEnable(plugin_mdfs, true);
//        gui_tools->menuEnable(plugin_test, true);
    }
}

void* PcaPlugin::connectMenuAction(const char* name, const char* tip, OnActionFunctionPointer function)
{
    void* ret = gui_tools->addMenuAction(name, tip, onActionTable.size());
    if(name != NULL && function != NULL)
        onActionTable.push_back(function);
    return ret;
}

bool PcaPlugin::initiateTablePlugin(MzPullDataInterface* pull_data, MzGuiRelatedInterface* gui_tools)
{
    this->pull_data = pull_data;
    this->gui_tools = gui_tools;

    connectMenuAction("Load matrix...", "Load rotation matrix to compute principal components or the most discriminative features", &PcaPlugin::on_menuLoad_triggered);
    plugin_save = connectMenuAction("Save matrix...", "Save rotation matrix to compute principal components or the most discriminative features", &PcaPlugin::on_menuSave_triggered);
    connectMenuAction(NULL, NULL, NULL);
//    connectMenuAction("Selection and training...", "Selection of features and training of the classifier", &PcaPlugin::on_menuSelect_triggered);

    connectMenuAction("Principal component analysis...", "Computes rotation matrix for the principal components space", &PcaPlugin::on_menuPca_triggered);
    connectMenuAction("Linear discriminant analysis...", "Computes rotation matrix for the most discriminative features space", &PcaPlugin::on_menuLda_triggered);

    plugin_mdfs = connectMenuAction("Compute new features...", "Computes the most discriminating features or principal components", &PcaPlugin::on_menuMdfs_triggered);
//    plugin_test = connectMenuAction("Test classifier...", "Test classifier performance", &PcaPlugin::on_menuTest_triggered);
    connectMenuAction(NULL, NULL, NULL);
    connectMenuAction("Options...", "Set options for the analysis", &PcaPlugin::on_menuOptions_triggered);
    connectMenuAction("About...", "Info about this plugin", &PcaPlugin::on_menuAbout_triggered);
    stopThreadOut();
    return true;
}

bool PcaPlugin::initiateMapsPlugin(MzPullDataInterface* pull_data, MzGuiRelatedInterface* gui_tools)
{
    this->pull_data = pull_data;
    this->gui_tools = gui_tools;
    connectMenuAction("Load classifier...", "Load rules to classify data", &PcaPlugin::on_menuLoad_triggered);
    plugin_save = connectMenuAction("Save classifier...", "Save rules to classify data", &PcaPlugin::on_menuSave_triggered);
    connectMenuAction(NULL, NULL, NULL);
    plugin_mdfs = connectMenuAction("Compute new maps...", "Computes maps of the most discriminating features or principal components", &PcaPlugin::on_menuMdfMaps_triggered);
//    plugin_test = connectMenuAction("Segment image...", "Image segmentation by the linear classifier", &PcaPlugin::on_menuSegment_triggered);
    connectMenuAction(NULL, NULL, NULL);
    connectMenuAction("About...", "Info about this plugin", &PcaPlugin::on_menuAbout_triggered);
    stopThreadOut();
    return true;
}

void PcaPlugin::callBack(const unsigned int index, const bool)
{
//    typedef void (LdaPlugin::*ActionFunction)(void);
//        union u_ptm_cast {
//            ActionFunction f;
//            void* v;
//        };
//        u_ptm_cast ftov;
//        u_ptm_cast vtof;
//        ActionFunction aa = & LdaPlugin::on_menuAbout_triggered;
//        ftov.f = aa;
//        void* a1 = ftov.v;
//        vtof.v = a1;
//        ActionFunction bb = vtof.f;
//        (this->*bb)();  //I tak nie dziala

//    this->use_multithreading = use_multithreading;
    if(index < onActionTable.size())
        if(onActionTable[index] != NULL)
            (this->*(onActionTable[index]))();
}


void PcaPlugin::on_menuAbout_triggered()
{
    std::stringstream ss;
    ss << "<h2>qmazda PcaPlugin</h2> " << std::endl;
    ss << QMAZDA_VERSION << "<br>" << std::endl;
    ss << QMAZDA_COPYRIGHT << "<br>" << std::endl;
    ss << "Built on " << __DATE__ << " at " << __TIME__ << "<br> <br>" << std::endl;
    ss << "The program is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING THE WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE." << std::endl;
    ss << "Home: <a href=\"http://www.eletel.p.lodz.pl/pms/SoftwareQmazda.html\"> http://www.eletel.p.lodz.pl/pms/SoftwareQmazda.html </a> <br>" << std::endl;
    ss << "Sources: <a href=\"https://gitlab.com/qmazda/qmazda\"> https://gitlab.com/qmazda/qmazda </a> <br>" << std::endl;
    ss << "<br>Built with:<br>" << std::endl;
    ss << "- alglib" << " <a href=\"http://www.alglib.net/\">http://www.alglib.net</a> <br>" << std::endl;
    gui_tools->showAbout("About LdaPlugin", ss.str().c_str());
}

void PcaPlugin::on_menuOptions_triggered()
{
    SetMachineLearningOptions();
}

bool PcaPlugin::SetMachineLearningOptions(void)
{
    std::stringstream ss;
    ss << "t;";
    switch(normalizeoption)
    {
    case NORMNONE: ss << "none"; break;
    case NORMALIZE: ss << "minmax"; break;
    default: ss << "standardize"; break;
    }
    ss << ";none;standardize;minmax";
    gui_tools->addPropertyOptionsDialog("Normalization", ss.str(), "Feature values normalization");

    ss.str(std::string());
    ss << "i;" << dimensions << ";1;32";
    gui_tools->addPropertyOptionsDialog("Dimensionality", ss.str(), "Minimum number of new features");

    ss.str(std::string());
    ss << "f;" << collected_var;
    gui_tools->addPropertyOptionsDialog("Preserved variance", ss.str(), "The number of new features will be set to preserve this variance of the original data set");

    if(gui_tools->openOptionsDialog("Linear classifier training"))
    {
        std::string ns;
        char* end;
        gui_tools->getValueOptionsDialog("Normalization", &ns);
        if(ns == "none") normalizeoption = NORMNONE;
        else if(ns == "minmax") normalizeoption = NORMALIZE;
        else normalizeoption = STANDARDIZE;

        gui_tools->getValueOptionsDialog("Dimensionality", &ns);
        dimensions = strtol(ns.c_str(), &end, 10);

        gui_tools->getValueOptionsDialog("Preserved variance", &ns);
        collected_var = atof(ns.c_str());
        if(collected_var > 1.0) collected_var = 1.0;
        if(collected_var < 0.0) collected_var = 0.0;

        gui_tools->closeOptionsDialog();
        return true;
    }
    gui_tools->closeOptionsDialog();
    return false;
}

void PcaPlugin::on_menuSave_triggered()
{
    if(classifier != NULL)
    {
        std::string fileName;
        unsigned int filter = 0;
        if(gui_tools->getSaveFile(&fileName, &filter))
        {
            if(!classifier->saveClassifier(fileName.c_str(), (filter==1)))
            {
                gui_tools->showMessage("Error", "Failed to save", 3);
            }
        }
    }
    else
    {
        gui_tools->showMessage("Warning", "No rotation matrix to save, run analysis first.", 2);
    }
}

bool PcaPlugin::openFile(string *filename)
{
    Classifiers* tempclassifier = new Classifiers(PcaSelectionReductionClassifierName);
    if(tempclassifier->loadClassifier(filename->c_str()))
    {
        if(classifier != NULL) delete classifier;
        classifier = tempclassifier;
        stopThreadOut();
        return true;
    }
    else
    {
        if(tempclassifier != NULL) delete tempclassifier;
        tempclassifier = NULL;
        return false;
    }
}

void PcaPlugin::on_menuLoad_triggered()
{
    std::string fileName;
    if(gui_tools->getOpenFile(&fileName))
    {
        openFile(&fileName);
    }
    else
        stopThreadOut();
}
