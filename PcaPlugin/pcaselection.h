/*
 * qMaZda - Image Analysis and Pattern Recognition
 * 
 * Copyright 2023  Piotr M. Szczypiński <piotr.szczypinski@p.lodz.pl>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef LDASELECTION_H
#define LDASELECTION_H
#include "../MzShared/multidimselection.h"
#include "../MzShared/dataforconsole.h"

#include <ap.h>
//#ifdef alglib
#include <linalg.h>
#include <dataanalysis.h>
//#else
//#include <lda.h>
//#endif

//#ifdef alglib
using namespace alglib;
//using namespace alglib_impl;

//#else
//using namespace ap;
//#endif

#include "../MzShared/classifierio.h"

#define NORMNONE -1
#define STANDARDIZE 0
#define NORMALIZE 1

//#define THRESHOLD_FROM_STANDARD_DEV 0
//#define THRESHOLD_FROM_ACCURACY 1
//#define THRESHOLD_FROM_BALANCED_ACCURACY 2

#define ROTATION_PCA 0
#define ROTATION_LDA 1

const char PcaSelectionReductionClassifierName[] = "MzPcaLdaRotation2023";

class PcaReduction : /*public MultiDimensionalSelection,*/ public ClassifierAccessInterface
{
public:
    virtual void NotifyProgressText(std::string text);
//    virtual void NotifyProgressStep(void);
//    virtual void NotifyProgressStage(const int notification, const int dimensionality, const double value, const unsigned int* selected);

    //double GoalFunction(unsigned int dimensions, unsigned int* picked_features);
    //double ClassifierTraining(unsigned int dim, unsigned int* picked);

//    bool saveClassifierToFile(const char* filename);

    PcaReduction();
    ~PcaReduction();

//    void setParameters(int dimensions, int maxtime);
    void setClassifier(Classifiers *classifier);//, int classtodistinguish = -1);
    void setInputData(DataForSelection* data);
//    void setOutputBuffers(double* Qtable, unsigned int* Qsorted);
    //void setMapBuffer(MapsForSegmentation* input, MapsForSegmentation* output);
    //void setReportStream(ofstream* report);

    bool computeMdfMaps(void);
//    bool computeSegmentation(void);
//    bool computeTest(void);
//    bool computeSelect(void);
    bool computeRotation(void);
    bool computeMdf(void);
//    bool computeMapmdfs(bool segment = false);

    const char* getClassifierMagic(void){return PcaSelectionReductionClassifierName;}
    bool loadClassifierFromFile(const char* filename);
    bool configureForClassification(vector<string>* featurenames);
    unsigned int classifyFeatureVector(double* featureVector);
    void segmentImage(unsigned int vectornumber, MazdaMapPixelType **values, MazdaMapPixelType *result);

    std::vector<std::string> getFeatureNames(void);
    std::vector<std::string> getClassNames(void);

//    void mdfFromFeatureVector(const double* featureVector, const std::vector< std::vector<double> >& values, double* mdfs);

protected:
    DataForSegmentation dataMap;
    DataForSelection* data;
//    ofstream *report;
//    string* confusionTableText;
//    double* Qtable;
//    unsigned int* Qsorted;
    int normalizeoption;
    int dimensions;
    float collected_var;
//    unsigned int dimensions;
//    int maxtime;
    int rotation_algorithm;
//    int direction_algorithm;
    Classifiers* classifier;


private:
//    int enumerateClasses(vector<string> *outClassNames);
    unsigned int* featureReindexLut;
    //vector<string> outClassNames;
    //unsigned int* outClassIndices;
    //vector<Classifier> *classifiers;
//    unsigned int* classFireRequired;
// Funkcje do zmodyfikowania
//    bool ComputeMdfs(double* table, unsigned int mdfsnumber);
    void StoreClassifier(unsigned int dim, real_2d_array& rotation_matrix, real_1d_array& variances);

//    void sortValCla(double* val, bool* cla, real_1d_array* mdf, unsigned int br, unsigned int end);
//    void threshValCla(double* val, bool* cla, double* th, unsigned int br, unsigned int length, bool balanced);
//    bool meantomean(const real_2d_array &xy, const ae_int_t npoints, const ae_int_t dims, real_1d_array &w);
//    void computeThreshold(double* threshold, bool* rev, double miu[2], double std[2], real_1d_array *mdf, unsigned int br);
//    void StoreProjectionMatrix(unsigned int dim, unsigned int* picked, real_2d_array* xy, real_2d_array* projection);
//    int classtodistinguish1;
//    int classtodistinguish2;
    vector<string> outclassnames;
//    bool computeclassifier;
    double* normal_plus;
    double* normal_multi;
    void NormalizeMinMax(void);
    void Standardize(void);
};

#endif // LDASELECTION_H
