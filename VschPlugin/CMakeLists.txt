cmake_minimum_required(VERSION 3.1)

project(VschPlugin LANGUAGES CXX)

set(CMAKE_INCLUDE_CURRENT_DIR ON)
set(CMAKE_SHARED_LIBRARY_PREFIX "mzp")

add_library(VschPlugin SHARED
    ../MzShared/dataforconsole.cpp
    ../MzShared/csvio.cpp
    ../MzShared/multidimselection.cpp
    vschselection.cpp
    vschplugin.cpp
    ../MzShared/classifierio.cpp
    ../MzShared/filenameremap.cpp
)

find_package(Qhull REQUIRED)
target_link_libraries(VschPlugin PRIVATE Qhull::qhullstatic_r)

find_package(TIFF REQUIRED)
target_link_libraries(VschPlugin PRIVATE TIFF::TIFF)

#target_link_libraries(VschPlugin ${QHULL_LIB})
#target_include_directories(VschPlugin PRIVATE ${QHULL_INCLUDE_DIRS})

install(TARGETS VschPlugin DESTINATION bin)
