#ifndef IMAGEDEFS_H
#define IMAGEDEFS_H

#ifdef MAZDA_IMAGE_8BIT_PIXEL
typedef unsigned char MazdaImagePixelType;
#else
typedef unsigned short int MazdaImagePixelType;
#endif

typedef float MazdaMapPixelType;
typedef unsigned int MazdaRoiBlockType;


#endif // IMAGEDEFS_H
