/*
 * QMAZDA project: Image Analysis and Pattern Recognition
 *
 * Copyright 2013-2016 Piotr M. Szczypiński <piotr.szczypinski@p.lodz.pl>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

//TO DO: loading of time series
//TO DO: loading series of 2D images
//TO DO: image writers
//TO DO: load and write floating point maps

#ifndef MAZDAIMAGEIO_H
#define MAZDAIMAGEIO_H
#include "mazdaimage.h"
#include <memory>
#include <string>
#include <vector>
#include <limits>
#include <math.h>
#include <tiffio.h>
#include <itkImageIOFactory.h>
#include <itkImageFileReader.h>
#include <itkImageFileWriter.h>
#include <itkGDCMImageIO.h>
#include <itkMetaDataObject.h>
#include "itkImageIOFactoryRegisterManager.h"
#include "itkTransformIOFactoryRegisterManager.h"
//#include <opencv2/imgcodecs.hpp>
#include "../MzShared/filenameremap.h"

//#if (defined(WIN32) || defined(_WIN32) || defined(WIN64) || defined(_WIN64)) && defined(__MINGW32__) //Windows with MINGW
//#include "../MzShared/filenameremap.h"
//#endif

#ifdef MAZDA_CONVERT_BGR_IMAGE
const int Rch = 2;
const int Gch = 1;
const int Bch = 0;
#else
const int Rch = 0;
const int Gch = 1;
const int Bch = 2;
#endif

template <typename TChannel, typename MzRGBImage, typename MzImage>
/**
 * @brief colorConvert
 * @param channel
 * @param rgbImage
 * @param grayImage
 */
void MzImageColorConvert(TChannel channel, MzRGBImage* rgbImage, MzImage* grayImage)
{
    const int WhiteValue = std::numeric_limits<typename MzImage::PixelType>::max();
    MazdaImageIterator<MzImage> outi = MazdaImageIterator<MzImage>(grayImage);
    MazdaImageIterator<MzRGBImage> ini = MazdaImageIterator<MzRGBImage>(rgbImage);
    switch(channel)
    {
    case 'Y':
    {
        while(! ini.IsBehind())
        {
            typename MzRGBImage::PixelType* rgb = ini.GetPixelPointer();
            typename MzImage::PixelType r = rgb->channel[Rch];
            typename MzImage::PixelType g = rgb->channel[Gch];
            typename MzImage::PixelType b = rgb->channel[Bch];
            typename MzImage::PixelType out = (typename MzImage::PixelType)(((int)114*b+(int)587*g+(int)299*r)/1000);
            outi.SetPixel(out);
            ++ini;
            ++outi;
        }
    } break;
    case 'R':
    {
        while(! ini.IsBehind())
        {
            typename MzRGBImage::PixelType* rgb = ini.GetPixelPointer();
            typename MzImage::PixelType r = rgb->channel[Rch];
//            typename MzImage::PixelType g = rgb->channel[Gch];
//            typename MzImage::PixelType b = rgb->channel[Bch];
            typename MzImage::PixelType out = r;
            outi.SetPixel(out);
            ++ini;
            ++outi;
        }
    } break;
    case 'G':
    {
        while(! ini.IsBehind())
        {
            typename MzRGBImage::PixelType* rgb = ini.GetPixelPointer();
//            typename MzImage::PixelType r = rgb->channel[Rch];
            typename MzImage::PixelType g = rgb->channel[Gch];
//            typename MzImage::PixelType b = rgb->channel[Bch];
            typename MzImage::PixelType out = g;
            outi.SetPixel(out);
            ++ini;
            ++outi;
        }
    } break;
    case 'B':
    {
        while(! ini.IsBehind())
        {
            typename MzRGBImage::PixelType* rgb = ini.GetPixelPointer();
//            typename MzImage::PixelType r = rgb->channel[Rch];
//            typename MzImage::PixelType g = rgb->channel[Gch];
            typename MzImage::PixelType b = rgb->channel[Bch];
            typename MzImage::PixelType out = b;
            outi.SetPixel(out);
            ++ini;
            ++outi;
        }
    } break;

    case 'U':
    {
        while(! ini.IsBehind())
        {
            typename MzRGBImage::PixelType* rgb = ini.GetPixelPointer();
            typename MzImage::PixelType r = rgb->channel[Rch];
            typename MzImage::PixelType g = rgb->channel[Gch];
            typename MzImage::PixelType b = rgb->channel[Bch];
            typename MzImage::PixelType out = (((int)886*b-(int)587*g-(int)299*r+886*WhiteValue)/1772);
            outi.SetPixel(out);
            ++ini;
            ++outi;
        }
    } break;
    case 'V':
    {
        while(! ini.IsBehind())
        {
            typename MzRGBImage::PixelType* rgb = ini.GetPixelPointer();
            typename MzImage::PixelType r = rgb->channel[Rch];
            typename MzImage::PixelType g = rgb->channel[Gch];
            typename MzImage::PixelType b = rgb->channel[Bch];
            typename MzImage::PixelType out = ((-(int)114*b-(int)587*g+(int)701*r+701*WhiteValue)/1402);
            outi.SetPixel(out);
            ++ini;
            ++outi;
        }
    } break;
    case 'I':
    {
        while(! ini.IsBehind())
        {
            typename MzRGBImage::PixelType* rgb = ini.GetPixelPointer();
            typename MzImage::PixelType r = rgb->channel[Rch];
            typename MzImage::PixelType g = rgb->channel[Gch];
            typename MzImage::PixelType b = rgb->channel[Bch];
            typename MzImage::PixelType out = ((-(int)3213*b-(int)2744*g+(int)5957*r+5958*WhiteValue)/(11916));
            outi.SetPixel(out);
            ++ini;
            ++outi;
        }
    } break;
    case 'Q':
    {
        while(! ini.IsBehind())
        {
            typename MzRGBImage::PixelType* rgb = ini.GetPixelPointer();
            typename MzImage::PixelType r = rgb->channel[Rch];
            typename MzImage::PixelType g = rgb->channel[Gch];
            typename MzImage::PixelType b = rgb->channel[Bch];
            typename MzImage::PixelType out = (((int)3111*b-(int)5226*g+(int)2115*r+5226*WhiteValue)/(10452));
            outi.SetPixel(out);
            ++ini;
            ++outi;
        }
    } break;
    case 'H':
    {
        while(! ini.IsBehind())
        {
            typename MzRGBImage::PixelType* rgb = ini.GetPixelPointer();
            typename MzImage::PixelType r = rgb->channel[Rch];
            typename MzImage::PixelType g = rgb->channel[Gch];
            typename MzImage::PixelType b = rgb->channel[Bch];
            double u = ((886.0*(double)b-587.0*(double)g-299.0*(double)r)/886.0);
            double v = ((-114.0*(double)b-587.0*(double)g+701.0*(double)r)/701.0);
            typename MzImage::PixelType out;
            if(u == 0 && v == 0) out = 0;
            else
            {
                double h = atan2(v, u);
                if (h<0) h+=(2*M_PI);
                out = (typename MzImage::PixelType)(h*((double)(WhiteValue+1)/(2*M_PI)));//10430.378);
            }
            outi.SetPixel(out);
            ++ini;
            ++outi;
        }
    } break;
    case 'S':
    {
        while(! ini.IsBehind())
        {
            typename MzRGBImage::PixelType* rgb = ini.GetPixelPointer();
            typename MzImage::PixelType r = rgb->channel[Rch];
            typename MzImage::PixelType g = rgb->channel[Gch];
            typename MzImage::PixelType b = rgb->channel[Bch];
            double u = ((886.0*(double)b-587.0*(double)g-299.0*(double)r)/886.0);
            double v = ((-114.0*(double)b-587.0*(double)g+701.0*(double)r)/701.0);
            typename MzImage::PixelType out =  (typename MzImage::PixelType)(0.93652797*sqrt(u*u+v*v));
            outi.SetPixel(out);
            ++ini;
            ++outi;
        }
    } break;

    case 'u':
    {
        while(! ini.IsBehind())
        {
            typename MzRGBImage::PixelType* rgb = ini.GetPixelPointer();
            typename MzImage::PixelType r = rgb->channel[Rch];
            typename MzImage::PixelType g = rgb->channel[Gch];
            typename MzImage::PixelType b = rgb->channel[Bch];
            double u = (886.0*(double)b-587.0*(double)g-299.0*(double)r);
            double bb = (114.0*(double)b+587.0*(double)g+299.0*(double)r+1.0);
            typename MzImage::PixelType out = (typename MzImage::PixelType)((u/bb+1.0)*WhiteValue*0.114);
            outi.SetPixel(out);
            ++ini;
            ++outi;
        }
    } break;
    case 'v':
    {
        while(! ini.IsBehind())
        {
            typename MzRGBImage::PixelType* rgb = ini.GetPixelPointer();
            typename MzImage::PixelType r = rgb->channel[Rch];
            typename MzImage::PixelType g = rgb->channel[Gch];
            typename MzImage::PixelType b = rgb->channel[Bch];
            double v = (-114.0*(double)b-587.0*(double)g+701.0*(double)r);
            double bb = (114.0*(double)b+587.0*(double)g+299.0*(double)r+1.0);
            typename MzImage::PixelType out = (typename MzImage::PixelType)((v/bb+1.0)*WhiteValue*0.299);
            outi.SetPixel(out);
            ++ini;
            ++outi;
        }
    } break;
    case 'i':
    {
        while(! ini.IsBehind())
        {
            typename MzRGBImage::PixelType* rgb = ini.GetPixelPointer();
            typename MzImage::PixelType r = rgb->channel[Rch];
            typename MzImage::PixelType g = rgb->channel[Gch];
            typename MzImage::PixelType b = rgb->channel[Bch];
            double i = (-321.3*(double)b-274.4*(double)g+595.7*(double)r);
            double bb = (114.0*(double)b+587.0*(double)g+299.0*(double)r+1.0);
            typename MzImage::PixelType out = (typename MzImage::PixelType)((i/bb+2.8185)*WhiteValue*0.20786);
            outi.SetPixel(out);
            ++ini;
            ++outi;
        }
    } break;
    case 'q':
    {
        while(! ini.IsBehind())
        {
            typename MzRGBImage::PixelType* rgb = ini.GetPixelPointer();
            typename MzImage::PixelType r = rgb->channel[Rch];
            typename MzImage::PixelType g = rgb->channel[Gch];
            typename MzImage::PixelType b = rgb->channel[Bch];
            double q = (311.1*(double)b-522.6*(double)g+211.5*(double)r);
            double bb = (114.0*(double)b+587.0*(double)g+299.0*(double)r+1.0);
            typename MzImage::PixelType out = (typename MzImage::PixelType)((q/bb+0.8903)*WhiteValue*0.26817);
            outi.SetPixel(out);
            ++ini;
            ++outi;
        }
    } break;
    case 'h':
    {
        while(! ini.IsBehind())
        {
            typename MzRGBImage::PixelType* rgb = ini.GetPixelPointer();
            typename MzImage::PixelType r = rgb->channel[Rch];
            typename MzImage::PixelType g = rgb->channel[Gch];
            typename MzImage::PixelType b = rgb->channel[Bch];

            double ii = ((-(int)3213*b-(int)2744*g+(int)5957*r)/(11916));
            double qq = (((int)3111*b-(int)5226*g+(int)2115*r)/(10452));

            //    double u = ((886.0*(double)b-587.0*(double)g-299.0*(double)r)/886.0);
            //    double v = ((-114.0*(double)b-587.0*(double)g+701.0*(double)r)/701.0);
            //    if(u == 0 && v == 0) typename MzImage::PixelType out = 0;
            typename MzImage::PixelType out;
            if(ii == 0 && qq == 0) out = 0;
            else
            {
                double h = atan2(qq, ii);
                if (h<0) h+=(2*M_PI);
                out = (typename MzImage::PixelType)(h*((double)(WhiteValue+1)/(2*M_PI)));//10430.378);
            }
            outi.SetPixel(out);
            ++ini;
            ++outi;
        }
    } break;
    case 'x':
    {
        while(! ini.IsBehind())
        {
            typename MzRGBImage::PixelType* rgb = ini.GetPixelPointer();
            typename MzImage::PixelType r = rgb->channel[Rch];
            typename MzImage::PixelType g = rgb->channel[Gch];
            typename MzImage::PixelType b = rgb->channel[Bch];
            double rr = ((double)r)/WhiteValue;
            if (rr <= 0.04045) rr = rr / 12.92;
            else rr = pow(((rr + 0.055) / 1.055), 2.4);
            double gg = ((double)g)/WhiteValue;
            if (gg <= 0.04045) gg = gg / 12.92;
            else gg = pow(((gg + 0.055) / 1.055), 2.4);
            double bb = ((double)b)/WhiteValue;
            if (bb <= 0.04045) bb = bb / 12.92;
            else bb = pow(((bb + 0.055) / 1.055), 2.4);
            typename MzImage::PixelType out = (typename MzImage::PixelType)((105.162*rr+91.188*gg+46.0275*bb)*(WhiteValue/255));
            outi.SetPixel(out);
            ++ini;
            ++outi;
        }
    } break;
    case 'y':
    {
        while(! ini.IsBehind())
        {
            typename MzRGBImage::PixelType* rgb = ini.GetPixelPointer();
            typename MzImage::PixelType r = rgb->channel[Rch];
            typename MzImage::PixelType g = rgb->channel[Gch];
            typename MzImage::PixelType b = rgb->channel[Bch];
            double rr = ((double)r)/WhiteValue;
            if (rr <= 0.04045) rr = rr / 12.92;
            else rr = pow(((rr + 0.055) / 1.055), 2.4);
            double gg = ((double)g)/WhiteValue;
            if (gg <= 0.04045) gg = gg / 12.92;
            else gg = pow(((gg + 0.055) / 1.055), 2.4);
            double bb = ((double)b)/WhiteValue;
            if (bb <= 0.04045) bb = bb / 12.92;
            else bb = pow(((bb + 0.055) / 1.055), 2.4);
            typename MzImage::PixelType out = (typename MzImage::PixelType)((54.213*rr+182.376*gg+18.411*bb)*(WhiteValue/255));
            outi.SetPixel(out);
            ++ini;
            ++outi;
        }
    } break;
    case 'z':
    {
        while(! ini.IsBehind())
        {
            typename MzRGBImage::PixelType* rgb = ini.GetPixelPointer();
            typename MzImage::PixelType r = rgb->channel[Rch];
            typename MzImage::PixelType g = rgb->channel[Gch];
            typename MzImage::PixelType b = rgb->channel[Bch];
            double rr = ((double)r)/WhiteValue;
            if (rr <= 0.04045) rr = rr / 12.92;
            else rr = pow(((rr + 0.055) / 1.055), 2.4);
            double gg = ((double)g)/WhiteValue;
            if (gg <= 0.04045) gg = gg / 12.92;
            else gg = pow(((gg + 0.055) / 1.055), 2.4);
            double bb = ((double)b)/WhiteValue;
            if (bb <= 0.04045) bb = bb / 12.92;
            else bb = pow(((bb + 0.055) / 1.055), 2.4);
            typename MzImage::PixelType out = (typename MzImage::PixelType)((4.5162*rr+27.8928*gg+222.417*bb)*(WhiteValue/255));
            outi.SetPixel(out);
            ++ini;
            ++outi;
        }
    } break;
    case 'L':
    {
        while(! ini.IsBehind())
        {
            typename MzRGBImage::PixelType* rgb = ini.GetPixelPointer();
            typename MzImage::PixelType r = rgb->channel[Rch];
            typename MzImage::PixelType g = rgb->channel[Gch];
            typename MzImage::PixelType b = rgb->channel[Bch];
            double rr = ((double)r)/WhiteValue;
            if (rr <= 0.04045) rr = rr / 12.92;
            else rr = pow(((rr + 0.055) / 1.055), 2.4);
            double gg = ((double)g)/WhiteValue;
            if (gg <= 0.04045) gg = gg / 12.92;
            else gg = pow(((gg + 0.055) / 1.055), 2.4);
            double bb = ((double)b)/WhiteValue;
            if (bb <= 0.04045) bb = bb / 12.92;
            else bb = pow(((bb + 0.055) / 1.055), 2.4);
            double yy = 0.2126*rr+0.7152*gg+0.0722*bb;
            yy = pow(yy, 1.0 / 3.0);
            typename MzImage::PixelType out = (typename MzImage::PixelType)(WhiteValue*yy);
            outi.SetPixel(out);
            ++ini;
            ++outi;
        }
    } break;
    case 'a':
    {
        while(! ini.IsBehind())
        {
            typename MzRGBImage::PixelType* rgb = ini.GetPixelPointer();
            typename MzImage::PixelType r = rgb->channel[Rch];
            typename MzImage::PixelType g = rgb->channel[Gch];
            typename MzImage::PixelType b = rgb->channel[Bch];
            double rr = ((double)r)/WhiteValue;
            if (rr <= 0.04045) rr = rr / 12.92;
            else rr = pow(((rr + 0.055) / 1.055), 2.4);
            double gg = ((double)g)/WhiteValue;
            if (gg <= 0.04045) gg = gg / 12.92;
            else gg = pow(((gg + 0.055) / 1.055), 2.4);
            double bb = ((double)b)/WhiteValue;
            if (bb <= 0.04045) bb = bb / 12.92;
            else bb = pow(((bb + 0.055) / 1.055), 2.4);
            double xx = 0.43387*rr+0.37622*gg+0.18990*bb;
            double yy = 0.2126*rr+0.7152*gg+0.0722*bb;
            xx = pow(xx, 1.0 / 3.0);
            yy = pow(yy, 1.0 / 3.0);
            typename MzImage::PixelType out = (typename MzImage::PixelType)(376.128385*(xx-yy+0.33898))*(WhiteValue/255);
            outi.SetPixel(out);
            ++ini;
            ++outi;
        }
    } break;
    case 'b':
    {
        while(! ini.IsBehind())
        {
            typename MzRGBImage::PixelType* rgb = ini.GetPixelPointer();
            typename MzImage::PixelType r = rgb->channel[Rch];
            typename MzImage::PixelType g = rgb->channel[Gch];
            typename MzImage::PixelType b = rgb->channel[Bch];
            double rr = ((double)r)/WhiteValue;
            if (rr <= 0.04045) rr = rr / 12.92;
            else rr = pow(((rr + 0.055) / 1.055), 2.4);
            double gg = ((double)g)/WhiteValue;
            if (gg <= 0.04045) gg = gg / 12.92;
            else gg = pow(((gg + 0.055) / 1.055), 2.4);
            double bb = ((double)b)/WhiteValue;
            if (bb <= 0.04045) bb = bb / 12.92;
            else bb = pow(((bb + 0.055) / 1.055), 2.4);
            double yy = 0.2126*rr+0.7152*gg+0.0722*bb;
            double zz = 0.01772*rr+0.109458*gg+0.872819*bb;
            yy = pow(yy, 1.0 / 3.0);
            zz = pow(zz, 1.0 / 3.0);
            typename MzImage::PixelType out = (typename MzImage::PixelType)(159.235668*(yy-zz+0.8007))*(WhiteValue/255);
            outi.SetPixel(out);
            ++ini;
            ++outi;
        }
    } break;
    }

//#ifdef USE_DEBUG_LOG
//    if(debugfile.is_open())
//    {
//        outi.GoToBegin();
//        ini.GoToBegin();
//        unsigned int size[MzRGBImage::Dimensions];
//        rgbImage->GetSize(size);
//        int logi = 0;

//        while(! ini.IsBehind())
//        {
//            typename MzRGBImage::PixelType* rgb = ini.GetPixelPointer();
//            typename MzImage::PixelType r = rgb->channel[Rch];
//            typename MzImage::PixelType g = rgb->channel[Gch];
//            typename MzImage::PixelType b = rgb->channel[Bch];
//            typename MzImage::PixelType* outg = outi.GetPixelPointer();
//            debugfile << "\t" << (int)r << ","<< (int)g << ","<< (int)b << "/" << (int)(*outg);
//            logi++;
//            if(logi%size[0] == 0)
//            {
//                debugfile << std::endl;
//            }
//            ++ini;
//            ++outi;
//        }
//    }
//#endif
}

template< typename TItkType, typename TMzType, typename TChannelType>
/**
 * @brief The RoiAddOperation class defines function to erase the roi data and is used in MazdaRoiRegionCopier class template
 */
class ItkImageToMazdaImageConverter
{
public:
    typedef typename TMzType::PixelType MzPixelType;
    typedef typename TItkType::PixelType ItkPixelType;

    inline static TMzType* ToMazdaImageMinMax(std::string filename, unsigned int frame = 0)
    {
        typedef typename itk::ImageFileReader<TItkType> ReaderType;
        typename ReaderType::Pointer reader = ReaderType::New();
        reader->SetFileName(filename.c_str());
        try
        {
            reader->Update();
        }
        catch (itk::ExceptionObject &ex)
        {
            std::cout << ex << std::endl;
            return NULL;
        }
        typename TItkType::Pointer itk_image = reader->GetOutput();
        unsigned int size[TMzType::Dimensions];
        double spacing[TMzType::Dimensions];
        unsigned int begin[TMzType::Dimensions];
        unsigned int end[TMzType::Dimensions];
        for(int d = 0; d < TMzType::Dimensions; d++)
        {
            begin[d] = 0;
            size[d] = itk_image->GetLargestPossibleRegion().GetSize()[d];
            end[d] = size[d] - 1;
            spacing[d] = itk_image->GetSpacing()[d];
        }
        TMzType* mz_image = new TMzType(size, spacing);

        MazdaImageRegionReversiveIterator<TMzType> mz_iterator(mz_image, begin, end);
        itk::ImageRegionIterator<TItkType> itk_terator(itk_image, itk_image->GetRequestedRegion());
        double min = 0;
        double max = 0;
        if(!itk_terator.IsAtEnd())
        {
            double val = itk_terator.Get();
            min = val;
            max = val;
            ++itk_terator;
        }
        while(!itk_terator.IsAtEnd())
        {
            double val = itk_terator.Get();
            if(min>val) min = val;
            if(max<val) max = val;
            ++itk_terator;
        }

        max = std::numeric_limits<typename TMzType::PixelType>::max()/(max - min);
        itk_terator.GoToBegin();
        while(!itk_terator.IsAtEnd() && !mz_iterator.IsBehind())
        {
            mz_iterator.SetPixel(max*((double)itk_terator.Get() - min));
            ++itk_terator;
            ++mz_iterator;
        }
        return mz_image;
    }


    inline static bool ToMazdaImageSlice(TMzType* mz_image, std::string filename, unsigned int slice)
    {
        unsigned int d;
        typedef typename itk::ImageFileReader<TItkType> ReaderType;
        typename ReaderType::Pointer reader = ReaderType::New();
        reader->SetFileName(filename.c_str());
        try
        {
            reader->Update();
        }
        catch (itk::ExceptionObject &ex)
        {
            std::cout << ex << std::endl;
            return NULL;
        }
        typename TItkType::Pointer itk_image = reader->GetOutput();

        typename TItkType::RegionType isize = itk_image->GetRequestedRegion();
        unsigned int size[TMzType::Dimensions];
        mz_image->GetSize(size);

        unsigned int begin[TMzType::Dimensions];
        unsigned int end[TMzType::Dimensions];

        for(d = 0; d < TItkType::ImageDimension; d++)
        {
            begin[d] = 0;
            if(isize.GetSize(d)< size[d])
            {
                end[d] = isize.GetSize(d)-1;
            }
            else
            {
                end[d] = size[d]-1;
                isize.SetSize(d, size[d]-1);
            }
        }
        for(; d < TMzType::Dimensions; d++)
        {
            begin[d] = slice;
            end[d] = slice;
        }
        MazdaImageRegionReversiveIterator<TMzType> mz_iterator(mz_image, begin, end);
        itk::ImageRegionIterator<TItkType> itk_terator(itk_image, itk_image->GetRequestedRegion());
        while(!itk_terator.IsAtEnd() && !mz_iterator.IsBehind())
        {
            mz_iterator.SetPixel(ConvertPixel(itk_terator.Get()));
            ++itk_terator;
            ++mz_iterator;
        }
        return mz_image;
    }

#if defined _MZ_UTF8_TO_SHORT_WORKAROUND
    inline static TMzType* ToMazdaImage(std::string filename8, unsigned int frame = 0)
    {
        std::string filename = _mz_utf8_to_short_for_reading(filename8.c_str());
#else
    inline static TMzType* ToMazdaImage(std::string filename, unsigned int frame = 0)
    {
#endif
        typedef typename itk::ImageFileReader<TItkType> ReaderType;
        typename ReaderType::Pointer reader = ReaderType::New();
        reader->SetFileName(filename.c_str());
        try
        {
            reader->Update();
        }
        catch (itk::ExceptionObject &ex)
        {
            std::cout << ex << std::endl;
            return NULL;
        }
        typename TItkType::Pointer itk_image = reader->GetOutput();
        unsigned int size[TMzType::Dimensions];
        double spacing[TMzType::Dimensions];
        unsigned int begin[TMzType::Dimensions];
        unsigned int end[TMzType::Dimensions];
        for(int d = 0; d < TMzType::Dimensions; d++)
        {
            begin[d] = 0;
            size[d] = itk_image->GetLargestPossibleRegion().GetSize()[d];
            end[d] = size[d] - 1;
            spacing[d] = itk_image->GetSpacing()[d];
        }
        TMzType* mz_image = new TMzType(size, spacing);
        MazdaImageRegionReversiveIterator<TMzType> mz_iterator(mz_image, begin, end);
        itk::ImageRegionIterator<TItkType> itk_terator(itk_image, itk_image->GetRequestedRegion());
        while(!itk_terator.IsAtEnd() && !mz_iterator.IsBehind())
        {
            mz_iterator.SetPixel(ConvertPixel(itk_terator.Get()));
            ++itk_terator;
            ++mz_iterator;
        }
        return mz_image;
    }

#if defined _MZ_UTF8_TO_SHORT_WORKAROUND
    inline static TMzType* ToMazdaImageRGB(std::string filename8, unsigned int frame = 0)
    {
        std::string filename = _mz_utf8_to_short_for_reading(filename8.c_str());
#else
    inline static TMzType* ToMazdaImageRGB(std::string filename, unsigned int frame = 0)
    {
#endif
        typedef typename itk::ImageFileReader<TItkType> ReaderType;
        typename ReaderType::Pointer reader = ReaderType::New();
        reader->SetFileName(filename.c_str());
        try
        {
            reader->Update();
        }
        catch (itk::ExceptionObject &ex)
        {
            std::cout << ex << std::endl;
            return NULL;
        }
        typename TItkType::Pointer itk_image = reader->GetOutput();
        unsigned int size[TMzType::Dimensions];
        double spacing[TMzType::Dimensions];
        unsigned int begin[TMzType::Dimensions];
        unsigned int end[TMzType::Dimensions];
        for(int d = 0; d < TMzType::Dimensions; d++)
        {
            begin[d] = 0;
            size[d] = itk_image->GetLargestPossibleRegion().GetSize()[d];
            end[d] = size[d] - 1;
            spacing[d] = itk_image->GetSpacing()[d];
        }
        TMzType* mz_image = new TMzType(size, spacing);
        MazdaImageRegionReversiveIterator<TMzType> mz_iterator(mz_image, begin, end);
        itk::ImageRegionIterator<TItkType> itk_terator(itk_image, itk_image->GetRequestedRegion());
        while(!itk_terator.IsAtEnd() && !mz_iterator.IsBehind())
        {
            typename TItkType::PixelType pixel = itk_terator.Get();
            typename TMzType::PixelType* mzp = mz_iterator.GetPixelPointer();
            mzp->channel[0] = ConvertPixel(pixel[0]);
            mzp->channel[1] = ConvertPixel(pixel[1]);
            mzp->channel[2] = ConvertPixel(pixel[2]);
            ++itk_terator;
            ++mz_iterator;
        }
        return mz_image;
    }
private:
    static TChannelType ConvertPixel(short int i)
    {
        return i+0x8000;
    }
    static TChannelType ConvertPixel(unsigned short int i)
    {
        return i;
    }
    static TChannelType ConvertPixel(char i)
    {
        return 0x101*(TChannelType)(i+0x80);
    }
    static TChannelType ConvertPixel(unsigned char i)
    {
        return 0x101*(TChannelType)i;
    }
    static TChannelType ConvertPixel(float i)
    {
        return i;
    }
};


template < typename TMazdaImage, typename TChannelType >
/**
 * @brief The MazdaImageIO class implements reading and writing methods for MazdaImage
 */
class MazdaImageIO
{
public:
    static TMazdaImage* Read(std::vector < std::string > filenames)
    {
        try
        {
            return UseItkReader(filenames);
        }
        catch(...)
        {
            return NULL;
        }
        //std::cout << "---- NOT IMPLEMENTED ---- MazdaDummy* Read(std::vector < std::string > filenames, unsigned int* frame = NULL)"  << std::endl;
        return NULL;
    }
    static MazdaDummy* Read(std::string filename, unsigned int* frame = NULL)
    {
        try
        {

#if defined _MZ_UTF8_TO_SHORT_WORKAROUND
            return UseItkReader(_mz_utf8_to_short_for_reading(filename.c_str()), frame);
#else
            return UseItkReader(filename, frame);
#endif


        }
        catch(...)
        {
            return NULL;
        }
    }
    static bool Write(std::string filename, TMazdaImage* image)
    {
        try
        {
            if(UseItkWriter(filename, image)) return true;
        }
        catch(...) {        }
        return false;
    }
    /**
     * @brief WriteSlices writes image slices to separate numbered files
     * @param filename file name string with number formatting eg. "~/directory/filename%.4d.jpg"
     * @param image should be 3D image (VDimensions = 3)
     * @return zero on success
     */
    static bool WriteSlices(std::string filename, TMazdaImage* image)
    {
        std::cout << "---- NOT IMPLEMENTED ---- int WriteSlices(std::string filename, TMazdaImage* image)"  << std::endl;

        return false;
    }
private:
    static bool UseItkWriter(std::string filename, TMazdaImage* image)
    {
        //std::cout << "PixelSize: " << sizeof(typename TMazdaImage::PixelType) << std::endl;

        if(sizeof(typename TMazdaImage::PixelType) % 3) return UseItkWriterGray(filename,  image);
        else return UseItkWriterRGB(filename,  image);
        //return false;
    }

    static bool UseItkWriterGray(std::string filename, TMazdaImage* image)
    {
//        if(image->IsEmpty())
//            return false;
        typedef itk::Image< typename TMazdaImage::PixelType, TMazdaImage::Dimensions > ImageType;
        typename ImageType::Pointer output = ImageType::New();
        typename ImageType::RegionType region;
        typename ImageType::RegionType::IndexType start;
        typename ImageType::RegionType::SizeType size;
        double ispacing[TMazdaImage::Dimensions];
        image->GetSpacing(ispacing);
        unsigned int isize[TMazdaImage::Dimensions];
        image->GetSize(isize);
        double spacing[3], orgin[3];
        for(int i = 0; i < TMazdaImage::Dimensions; i++)
        {
            start[i] = 0;
            size[i]  = isize[i];
            spacing[i] = ispacing[i];
            orgin[i] = 0;
        }
        region.SetIndex( start );
        region.SetSize( size );
        output->SetRegions( region );
        output->SetOrigin(orgin);
        output->SetSpacing(spacing);
        size_t number_of_pixels = output->GetLargestPossibleRegion().GetNumberOfPixels();
        //true - dealocate data on delete, false - no date dealocation
        output->GetPixelContainer()->SetImportPointer(image->GetDataPointer(), number_of_pixels, false);
        typename itk::ImageFileWriter< ImageType >::Pointer writer = itk::ImageFileWriter< ImageType >::New();
#if defined _MZ_UTF8_TO_SHORT_WORKAROUND
        writer->SetFileName(_mz_utf8_to_short_for_writing(filename.c_str()));
#else
        writer->SetFileName(filename);
#endif
        writer->SetInput(output);
        writer->Update();
        return true;
    }

    static bool UseItkWriterRGB(std::string filename, TMazdaImage* image)
    {
//        typedef typename TMazdaImage::PixelType PixelStructure;
        typedef itk::Image< itk::RGBPixel < TChannelType >, TMazdaImage::Dimensions > ImageType;
        typename ImageType::Pointer output = ImageType::New();
        typename ImageType::RegionType region;
        typename ImageType::RegionType::IndexType start;
        typename ImageType::RegionType::SizeType size;
        double ispacing[TMazdaImage::Dimensions];
        image->GetSpacing(ispacing);
        unsigned int isize[TMazdaImage::Dimensions];
        image->GetSize(isize);
        double spacing[3], orgin[3];
        for(int i = 0; i < TMazdaImage::Dimensions; i++)
        {
            start[i] = 0;
            size[i]  = isize[i];
            spacing[i] = ispacing[i];
            orgin[i] = 0;
        }
        region.SetIndex( start );
        region.SetSize( size );
        output->SetRegions( region );
        output->SetOrigin(orgin);
        output->SetSpacing(spacing);
        size_t number_of_pixels = output->GetLargestPossibleRegion().GetNumberOfPixels();
        //true - dealocate data on delete, false - no date dealocation
        output->GetPixelContainer()->SetImportPointer((itk::RGBPixel < TChannelType >*) image->GetDataPointer(), number_of_pixels, false);
        typename itk::ImageFileWriter< ImageType >::Pointer writer = itk::ImageFileWriter< ImageType >::New();
#if defined _MZ_UTF8_TO_SHORT_WORKAROUND
        writer->SetFileName(_mz_utf8_to_short_for_writing(filename.c_str()));
#else
        writer->SetFileName(filename);
#endif
        writer->SetInput(output);
        writer->Update();
        return true;
    }





//    static MazdaDummy* UseOpenCVReader(std::string filename)
//    {
//    cv::Mat src = cv::imread(filename, cv::IMREAD_ANYDEPTH | cv::IMREAD_ANYCOLOR );
//    if(src.data == nullptr)
//        return nullptr;
//    unsigned int size[4];
//    double spacing[4];
//    for(unsigned int i = 0; i < src.dims && i < 3; i++)
//    {
//        size[i] = src.size[i];
//        spacing[i] = 1.0;
//    }
//    TMazdaImage* mz_image = new TMazdaImage(size, spacing, true);
//    cv::flip(src, src, 0);
//    CzImage* image = czCreateImage(src.rows, src.cols, src.channels());
//    unsigned int lmax = image->height;
//    for(unsigned int l = 0; l < lmax; l++)
//    {
//        unsigned char* d = image->data+l*image->stride;
//        unsigned char* s = (unsigned char*)(src.data + l*src.step[0]);
//        memcpy(d, s, image->width*image->channels);
//    }
//    return image;
//    }








    static TMazdaImage* UseItkReader(std::vector < std::string > filenames)
    {
        unsigned int size[3];
        double spacing[3];
        size[2] = filenames.size();
        if(size[2] <= 1) return NULL;
        itk::ImageIOBase::IOComponentType componentType;
        //itk::ImageIOBase::IOPixelType pixelType;
        size_t numDimensions;

#if defined _MZ_UTF8_TO_SHORT_WORKAROUND
       itk::ImageIOBase::Pointer imageIO =
               itk::ImageIOFactory::CreateImageIO(_mz_utf8_to_short_for_reading(filenames[0].c_str()).c_str(), itk::ImageIOFactory::ReadMode);
#else
       itk::ImageIOBase::Pointer imageIO = itk::ImageIOFactory::CreateImageIO(filenames[0].c_str(), itk::ImageIOFactory::ReadMode);
#endif
        if( !imageIO ) return NULL;
        imageIO->SetFileName(filenames[0]);
        imageIO->ReadImageInformation();
        numDimensions =  imageIO->GetNumberOfDimensions();
        //pixelType = imageIO->GetPixelType();
        componentType = imageIO->GetComponentType();

        //if(pixelType == itk::ImageIOBase::RGB || pixelType == itk::ImageIOBase::RGBA) return NULL;
        if(numDimensions < 2) return NULL;

        size[1] = imageIO->GetDimensions(1);
        size[0] = imageIO->GetDimensions(0);
        spacing[0] = imageIO->GetSpacing(0);
        spacing[1] = imageIO->GetSpacing(1);

        if(numDimensions > 2) spacing[2] = imageIO->GetSpacing(2);
        else spacing[2] = (spacing[0]+spacing[1])/2.0;

        std::stringstream ss;
        ss << "Loaded from file stack."<< std::endl;
        ss << "Size = " << size[0] << " "<< size[1] << " "<< size[2]<<std::endl;
        ss << "Spacing = " << spacing[0] << " "<< spacing[1] << " "<< spacing[2]<<std::endl;

        //typedef MazdaImage< TChannelType, 3 > MIType;
        TMazdaImage* mz_image = new TMazdaImage(size, spacing, true);

        if(componentType == itk::ImageIOBase::UCHAR)
        {
            typedef itk::Image< unsigned char, 2 > ITKType;
            for(unsigned int slice = 0; slice < size[2]; slice++)
                ItkImageToMazdaImageConverter<ITKType, TMazdaImage, TChannelType>::ToMazdaImageSlice(mz_image, filenames[slice], slice);
        }
        else if(componentType == itk::ImageIOBase::CHAR)
        {
            typedef itk::Image< char, 2 > ITKType;
            for(unsigned int slice = 0; slice < size[2]; slice++)
                ItkImageToMazdaImageConverter<ITKType, TMazdaImage, TChannelType>::ToMazdaImageSlice(mz_image, filenames[slice], slice);
        }
        else if(componentType == itk::ImageIOBase::USHORT)
        {
            typedef itk::Image< unsigned short int, 2 > ITKType;
            for(unsigned int slice = 0; slice < size[2]; slice++)
                ItkImageToMazdaImageConverter<ITKType, TMazdaImage, TChannelType>::ToMazdaImageSlice(mz_image, filenames[slice], slice);
        }
        else if(componentType == itk::ImageIOBase::SHORT)
        {
            typedef itk::Image< short int, 2 > ITKType;
            for(unsigned int slice = 0; slice < size[2]; slice++)
                ItkImageToMazdaImageConverter<ITKType, TMazdaImage, TChannelType>::ToMazdaImageSlice(mz_image, filenames[slice], slice);
        }
        else
        {
            typedef itk::Image< float, 2 > ITKType;
            for(unsigned int slice = 0; slice < size[2]; slice++)
                ItkImageToMazdaImageConverter<ITKType, TMazdaImage, TChannelType>::ToMazdaImageSlice(mz_image, filenames[slice], slice);
        }
        return mz_image;
    }






#if defined _MZ_UTF8_TO_SHORT_WORKAROUND
    static MazdaDummy* UseItkReader(std::string filename8, unsigned int* frame = NULL)
    {
        std::string filename = _mz_utf8_to_short_for_reading(filename8.c_str());
#else
    static MazdaDummy* UseItkReader(std::string filename, unsigned int* frame = NULL)
    {
#endif
        MazdaDummy* dummy = NULL;
        std::stringstream ss;
        itk::ImageIOBase::IOComponentType componentType;
        itk::ImageIOBase::IOPixelType pixelType;
        size_t numDimensions;
        itk::ImageIOBase::Pointer imageIO = itk::ImageIOFactory::CreateImageIO(filename.c_str(), itk::ImageIOFactory::ReadMode);
        if( !imageIO ) return NULL;
        imageIO->SetFileName(filename);
        imageIO->ReadImageInformation();
        numDimensions =  imageIO->GetNumberOfDimensions();
        pixelType = imageIO->GetPixelType();
        componentType = imageIO->GetComponentType();

        if(numDimensions > 2)
            if(imageIO->GetDimensions(2) < 2)
                numDimensions = 2;

//        ss << "File name: " << filename << std::endl;
//        ss << "Dimensions: " << numDimensions << std::endl;
//        ss << "Type: " << itk::ImageIOBase::GetComponentTypeAsString(componentType) <<" "<< itk::ImageIOBase::GetPixelTypeAsString(pixelType)<< std::endl;
//        ss << "Dimensions: " << numDimensions  << std::endl;
//        ss << "Size: " << imageIO->GetDimensions(0);
//        for(int d = 1; d < numDimensions; d++) ss << " x " << imageIO->GetDimensions(d);
//        ss << std::endl;
//        ss << "Spacing: " << imageIO->GetSpacing(0);
//        for(int d = 1; d < numDimensions; d++) ss << " x " << imageIO->GetSpacing(d);
//        ss << std::endl;
        //Try to read image information
        try
        {
            ss << "Loaded with Insight Toolkit IO."<< std::endl;
            imageIO.Print(ss);
        }
        catch(...)
        {
        }

        if(pixelType == itk::ImageIOBase::RGB || pixelType == itk::ImageIOBase::RGBA)
        {
//            typedef MultiChannelPixel <MazdaPixelType, 3> PixelRGBType;
//            typedef MazdaImage<MazdaPixelType, 2> MIType2D;
//            typedef MazdaImage<MultiChannelPixel <MazdaPixelType, 3> , 2> MITypeRGB;

            if(componentType == itk::ImageIOBase::UCHAR && numDimensions == 2)
            {
                typedef itk::Image< itk::RGBPixel< unsigned char >, 2 > ITKType;
                typedef MazdaImage<MultiChannelPixel <TChannelType, 3> , 2> MIType;
                MIType* mz_image = ItkImageToMazdaImageConverter<ITKType, MIType, TChannelType>::ToMazdaImageRGB(filename);
                if(mz_image != NULL)
                {
                    mz_image->SetName(ss.str());
                    dummy = mz_image;
                }
            }
            else if(componentType == itk::ImageIOBase::UCHAR && numDimensions == 3)
            {
                typedef itk::Image< itk::RGBPixel< unsigned char >, 3 > ITKType;
                typedef MazdaImage<MultiChannelPixel <TChannelType, 3> , 3> MIType;
                MIType* mz_image = ItkImageToMazdaImageConverter<ITKType, MIType, TChannelType>::ToMazdaImageRGB(filename);
                if(mz_image != NULL)
                {
                    mz_image->SetName(ss.str());
                    dummy = mz_image;
                }
            }
            else if(componentType == itk::ImageIOBase::USHORT && numDimensions == 2)
            {
                typedef itk::Image< itk::RGBPixel< unsigned short int >, 2 > ITKType;
                typedef MazdaImage<MultiChannelPixel <TChannelType, 3> , 2> MIType;
                MIType* mz_image = ItkImageToMazdaImageConverter<ITKType, MIType, TChannelType>::ToMazdaImageRGB(filename);
                if(mz_image != NULL)
                {
                    mz_image->SetName(ss.str());
                    dummy = mz_image;
                }
            }
            else if(componentType == itk::ImageIOBase::USHORT && numDimensions == 3)
            {
                typedef itk::Image< itk::RGBPixel< unsigned short int >, 3 > ITKType;
                typedef MazdaImage<MultiChannelPixel <TChannelType, 3> , 3> MIType;
                MIType* mz_image = ItkImageToMazdaImageConverter<ITKType, MIType, TChannelType>::ToMazdaImageRGB(filename);
                if(mz_image != NULL)
                {
                    mz_image->SetName(ss.str());
                    dummy = mz_image;
                }
            }
        }
        else
        {
            if(componentType == itk::ImageIOBase::UCHAR && numDimensions == 2)
            {
                typedef itk::Image< unsigned char, 2 > ITKType;
                typedef MazdaImage< TChannelType, 2 > MIType;
                MIType* mz_image = ItkImageToMazdaImageConverter<ITKType, MIType, TChannelType>::ToMazdaImage(filename);
                if(mz_image != NULL)
                {
                    mz_image->SetName(ss.str());
                    dummy = mz_image;
                }
            }
            else if(componentType == itk::ImageIOBase::UCHAR && numDimensions == 3)
            {
                typedef itk::Image< unsigned char, 3 > ITKType;
                typedef MazdaImage< TChannelType, 3 > MIType;
                MIType* mz_image = ItkImageToMazdaImageConverter<ITKType, MIType, TChannelType>::ToMazdaImage(filename);
                if(mz_image != NULL)
                {
                    mz_image->SetName(ss.str());
                    dummy = mz_image;
                }
            }
            else if(componentType == itk::ImageIOBase::CHAR && numDimensions == 2)
            {
                typedef itk::Image< char, 2 > ITKType;
                typedef MazdaImage< TChannelType, 2 > MIType;
                MIType* mz_image = ItkImageToMazdaImageConverter<ITKType, MIType, TChannelType>::ToMazdaImage(filename);
                if(mz_image != NULL)
                {
                    mz_image->SetName(ss.str());
                    dummy = mz_image;
                }
            }
            else if(componentType == itk::ImageIOBase::CHAR && numDimensions == 3)
            {
                typedef itk::Image< char, 3 > ITKType;
                typedef MazdaImage< TChannelType, 3 > MIType;
                MIType* mz_image = ItkImageToMazdaImageConverter<ITKType, MIType, TChannelType>::ToMazdaImage(filename);
                if(mz_image != NULL)
                {
                    mz_image->SetName(ss.str());
                    dummy = mz_image;
                }
            }
            else if(componentType == itk::ImageIOBase::USHORT && numDimensions == 2)
            {
                typedef itk::Image< unsigned short int, 2 > ITKType;
                typedef MazdaImage< TChannelType, 2 > MIType;
                MIType* mz_image = ItkImageToMazdaImageConverter<ITKType, MIType, TChannelType>::ToMazdaImage(filename);
                if(mz_image != NULL)
                {
                    mz_image->SetName(ss.str());
                    dummy = mz_image;
                }
            }
            else if(componentType == itk::ImageIOBase::USHORT && numDimensions == 3)
            {
                typedef itk::Image< unsigned short int, 3 > ITKType;
                typedef MazdaImage< TChannelType, 3 > MIType;
                MIType* mz_image = ItkImageToMazdaImageConverter<ITKType, MIType, TChannelType>::ToMazdaImage(filename);
                if(mz_image != NULL)
                {
                    mz_image->SetName(ss.str());
                    dummy = mz_image;
                }
            }
            else if(componentType == itk::ImageIOBase::SHORT && numDimensions == 2)
            {
                typedef itk::Image< short int, 2 > ITKType;
                typedef MazdaImage< TChannelType, 2 > MIType;
                MIType* mz_image = ItkImageToMazdaImageConverter<ITKType, MIType, TChannelType>::ToMazdaImage(filename);
                if(mz_image != NULL)
                {
                    mz_image->SetName(ss.str());
                    dummy = mz_image;
                }
            }
            else if(componentType == itk::ImageIOBase::SHORT && numDimensions == 3)
            {
                typedef itk::Image< short int, 3 > ITKType;
                typedef MazdaImage< TChannelType, 3 > MIType;
                MIType* mz_image = ItkImageToMazdaImageConverter<ITKType, MIType, TChannelType>::ToMazdaImage(filename);
                if(mz_image != NULL)
                {
                    mz_image->SetName(ss.str());
                    dummy = mz_image;
                }
            }
            else if(numDimensions == 2)
            {
                typedef itk::Image< float, 2 > ITKType;
                typedef MazdaImage< TChannelType, 2 > MIType;
                MIType* mz_image = ItkImageToMazdaImageConverter<ITKType, MIType, TChannelType>::ToMazdaImageMinMax(filename);
                if(mz_image != NULL)
                {
                    mz_image->SetName(ss.str());
                    dummy = mz_image;
                }
            }
            else if(numDimensions == 3)
            {
                typedef itk::Image< float, 3 > ITKType;
                typedef MazdaImage< TChannelType, 3 > MIType;
                MIType* mz_image = ItkImageToMazdaImageConverter<ITKType, MIType, TChannelType>::ToMazdaImageMinMax(filename);
                if(mz_image != NULL)
                {
                    mz_image->SetName(ss.str());
                    dummy = mz_image;
                }
            }
        }
        return dummy;
    }
};

template < typename TMazdaImage >
/**
 * @brief The MazdaMapIO class implements reading and writing methods for MazdaImage used as map container
 */
class MazdaMapIO
{
public:
    static std::vector < TMazdaImage* > Read(std::string filename)
    {
        std::vector < TMazdaImage* > r;
        try
        {
            r = PagedTiffReader(filename.c_str());
            if(r.size() > 0) return r;
        }
        catch(...)
        {
        }
        try
        {
            TMazdaImage* i = UseItkReader(filename);
            if(i != NULL) r.push_back(i);
            if(r.size() > 0) return r;
        }
        catch(...)
        {
        }
        return r;
    }

    static bool Write(std::string filename, std::vector < TMazdaImage* >* maps)
    {
        try
        {
            if(PagedTiffWriter(filename.c_str(), maps)) return true;
        }
        catch(...) {}
        return false;
    }
    static bool Export(std::string filename, TMazdaImage* map)
    {
        try
        {
            if(UseItkWriter(filename, map)) return true;
        }
        catch(...) {}
        return false;
    }
private:
    static std::vector <TMazdaImage*> PagedTiffReader(const char *filename)
    {
        std::vector <TMazdaImage*> r;
#if defined _MZ_UTF8_TO_U16
        TIFF* tiff = TIFFOpenW(_mz_utf8_to_u16(filename).c_str(), "r");
#else
        TIFF* tiff = TIFFOpen(filename, "r");
#endif
        if(!tiff) return r;
        do{
            TMazdaImage* map = NULL;
            if(!TiffReader(tiff, &map)) break;
            if(map == NULL) break;
            r.push_back(map);
            if(!TIFFReadDirectory(tiff)) break;
        }while(true);
        TIFFClose(tiff);
        return r;
    }

    static bool PagedTiffWriter(const char *filename, std::vector <TMazdaImage*>* maps)
    {
        unsigned int page;
#if defined _MZ_UTF8_TO_U16
        TIFF* tiff = TIFFOpenW(_mz_utf8_to_u16(filename).c_str(), "w");
#else
        TIFF* tiff = TIFFOpen(filename, "w");
#endif
        if (!tiff) return false;
        for(page = 0; page < maps->size(); page++)
        {
            TiffWriter(tiff, (*maps)[page], page, maps->size());
            TIFFWriteDirectory(tiff);
        }
        TIFFClose(tiff);
        return true;
    }

    static bool TiffReader(TIFF* tiff, TMazdaImage** map)
    {
        uint32_t temp32;
        uint16_t temp16, bps, format;
        char* pname;
        unsigned int size[TMazdaImage::Dimensions];
        double spacing[TMazdaImage::Dimensions];
        unsigned int y = 0;
        unsigned int begin[TMazdaImage::Dimensions];
        unsigned int end[TMazdaImage::Dimensions];

        TIFFGetFieldDefaulted(tiff, TIFFTAG_IMAGEWIDTH, &temp32); size[0] = temp32;
        TIFFGetFieldDefaulted(tiff, TIFFTAG_IMAGELENGTH, &temp32); size[1] = temp32;
        if(TMazdaImage::Dimensions > 2)
        {
            temp32 = 1;
            TIFFGetFieldDefaulted(tiff, TIFFTAG_IMAGEDEPTH, &temp32);
            if(temp32 > 1)
            {
                size[2] = temp32;
                size[1] /= size[2];
            }
            else
            {
                size[2] = 1;
            }
            for(int i = 3; i < TMazdaImage::Dimensions; i++) size[i] = 1;
        }
        for(int i = 0; i < TMazdaImage::Dimensions; i++) spacing[i] = 1.0;

        TIFFGetFieldDefaulted(tiff, TIFFTAG_BITSPERSAMPLE, &bps);
        TIFFGetFieldDefaulted(tiff, TIFFTAG_SAMPLESPERPIXEL, &temp16); if(temp16 != 1)  return false;
        TIFFGetFieldDefaulted(tiff, TIFFTAG_SAMPLEFORMAT, &format);
        if (!TIFFGetField(tiff, TIFFTAG_IMAGEDESCRIPTION, &pname))
        {
            if (!TIFFGetField(tiff, TIFFTAG_DOCUMENTNAME, &pname)) pname = NULL;
        }
        *map = new TMazdaImage(size, spacing);
        if(pname == NULL) (*map)->SetName("");
        else (*map)->SetName(pname);
        begin[0] = 0;
        end[0] = 0;
        for(y = 1; y < TMazdaImage::Dimensions; y++)
        {
            begin[y] = 0;
            end[y] = size[y] - 1;
        }
        MazdaImageRegionIterator< TMazdaImage > iterator(*map, begin, end);

// Przed C++11        
//        if((bps == 32 && format == SAMPLEFORMAT_IEEEFP && std::tr1::is_same< typename TMazdaImage::PixelType, float >::value) ||
//           (bps == 64 && format == SAMPLEFORMAT_IEEEFP && std::tr1::is_same< typename TMazdaImage::PixelType, double >::value))
// C++11
//        if((bps == 32 && format == SAMPLEFORMAT_IEEEFP && std::is_same< typename TMazdaImage::PixelType, float >::value) ||
//           (bps == 64 && format == SAMPLEFORMAT_IEEEFP && std::is_same< typename TMazdaImage::PixelType, double >::value))

        if((bps == 32 && format == SAMPLEFORMAT_IEEEFP && sizeof(typename TMazdaImage::PixelType) == sizeof(float)) ||
           (bps == 64 && format == SAMPLEFORMAT_IEEEFP && sizeof(typename TMazdaImage::PixelType) == sizeof(double)))
        {
            y = 0;
            while(! iterator.IsBehind())
            {
                TIFFReadScanline(tiff, (void*)iterator.GetPixelPointer(), y);
                ++iterator; y++;
            }
        }
        else if(bps == 32 && format == SAMPLEFORMAT_IEEEFP)
        {
            float* line = new float[size[0]];
            y = 0;
            while(! iterator.IsBehind())
            {
                TIFFReadScanline(tiff, line, y);
                typename TMazdaImage::PixelType* pp = iterator.GetPixelPointer();
                for(unsigned int i = 0; i < size[0]; i++) pp[i] = line[i];
                ++iterator; y++;
            }
            delete[] line;
        }
        else if(bps == 64 && format == SAMPLEFORMAT_IEEEFP)
        {
            double* line = new double[size[0]];
            y = 0;
            while(! iterator.IsBehind())
            {
                TIFFReadScanline(tiff, line, y);
                typename TMazdaImage::PixelType* pp = iterator.GetPixelPointer();
                for(unsigned int i = 0; i < size[0]; i++) pp[i] = line[i];
                ++iterator; y++;
            }
            delete[] line;
        }
        else
        {
            delete *map;
            *map = NULL;
            return false;
        }
        return true;
    }

    static bool TiffWriter(TIFF* tiff, TMazdaImage* map, unsigned int page, unsigned int pages)
    {
        unsigned int y = 0;
//        unsigned int zero = 0;
//        TMazdaImage* dest;
        if(map == NULL) return false;
//        if(source->IsEmpty()) return false;
        unsigned int size[TMazdaImage::Dimensions];
        map->GetSize(size);
        TIFFSetField(tiff, TIFFTAG_IMAGEWIDTH, size[0]);
        TIFFSetField(tiff, TIFFTAG_IMAGELENGTH, size[1]);
        if(TMazdaImage::Dimensions > 2)
        {
            if(size[2] > 1)
                TIFFSetField(tiff, TIFFTAG_IMAGEDEPTH, size[2]);
        }
        TIFFSetField(tiff, TIFFTAG_BITSPERSAMPLE, sizeof(typename TMazdaImage::PixelType)*8);
        TIFFSetField(tiff, TIFFTAG_SAMPLESPERPIXEL, 1);
        TIFFSetField(tiff, TIFFTAG_SAMPLEFORMAT, SAMPLEFORMAT_IEEEFP);
        TIFFSetField(tiff, TIFFTAG_PLANARCONFIG, PLANARCONFIG_CONTIG);
        TIFFSetField(tiff, TIFFTAG_ORIENTATION, ORIENTATION_TOPLEFT);
        TIFFSetField(tiff, TIFFTAG_XRESOLUTION, (float)100);
        TIFFSetField(tiff, TIFFTAG_YRESOLUTION, (float)100);
        TIFFSetField(tiff, TIFFTAG_RESOLUTIONUNIT, RESUNIT_INCH);
        std::string name = map->GetName();
        if(! name.empty())
        {
            TIFFSetField(tiff, TIFFTAG_IMAGEDESCRIPTION, name.c_str());
            TIFFSetField(tiff, TIFFTAG_DOCUMENTNAME, name.c_str());
        }
        if(page <= pages)
        {
            TIFFSetField(tiff, TIFFTAG_SUBFILETYPE, FILETYPE_PAGE);
            TIFFSetField(tiff, TIFFTAG_PAGENUMBER, page, pages);
        }

        unsigned int lines = size[1];
        if(TMazdaImage::Dimensions > 2)
        {
            lines *= size[2];
        }
        unsigned int begin[TMazdaImage::Dimensions];
        unsigned int end[TMazdaImage::Dimensions];

        begin[0] = 0;
        end[0] = 0;

        for(y = 1; y < TMazdaImage::Dimensions; y++)
        {
            begin[y] = 0;
            end[y] = size[y] - 1;
        }

        MazdaImageRegionIterator< TMazdaImage > iterator(map, begin, end);
//        if(std::tr1::is_same< typename TMazdaImage::PixelType, float >::value)
//        {
            for(y = 0; y < lines; y++)
            {
                TIFFWriteScanline(tiff, (void*) iterator.GetPixelPointer(), y, 0);
                ++iterator;
            }
//        }
//        else
//        {
//            float* line = new float[size[0]];
//            for(y = 0; y < lines; y++)
//            {
//                typename TMazdaImage::PixelType* pp = iterator.GetPixelPointer();
//                typename TMazdaImage::PixelType* ppmax = pp + size[0];
//                float* l = line;
//                for(; pp < ppmax; pp++, l++) *l = *pp;
//                TIFFWriteScanline(tiff, line, y, 0);
//                ++iterator;
//            }
//            delete[] line;
//        }
        return true;
    }


#if defined _MZ_UTF8_TO_SHORT_WORKAROUND
    static bool UseItkWriter(std::string filename8, TMazdaImage* image)
    {
        std::string filename = _mz_utf8_to_short_for_writing(filename8.c_str());
#else
    static bool UseItkWriter(std::string filename, TMazdaImage* image)
    {
#endif
        typedef itk::Image< typename TMazdaImage::PixelType, TMazdaImage::Dimensions > ImageType;
        typedef itk::Image< typename TMazdaImage::PixelType, 2 > ImageType2;

        double ispacing[TMazdaImage::Dimensions];
        image->GetSpacing(ispacing);
        unsigned int isize[TMazdaImage::Dimensions];
        image->GetSize(isize);
        unsigned int cisize = 1;
        for(int i = 2; i < TMazdaImage::Dimensions; i++)
        {
            cisize *= isize[i];
        }

        if(cisize > 1)
        {
            typename ImageType::Pointer output = ImageType::New();
            typename ImageType::RegionType region;
            typename ImageType::RegionType::IndexType start;
            typename ImageType::RegionType::SizeType size;

            double spacing[4], orgin[4];
            for(int i = 0; i < TMazdaImage::Dimensions; i++)
            {
                start[i] = 0;
                size[i]  = isize[i];
                spacing[i] = ispacing[i];
                orgin[i] = 0;
            }
            region.SetIndex( start );
            region.SetSize( size );
            output->SetRegions( region );
            output->SetOrigin(orgin);
            output->SetSpacing(spacing);
            size_t number_of_pixels = output->GetLargestPossibleRegion().GetNumberOfPixels();
            //true - dealocate data on delete, false - no date dealocation
            output->GetPixelContainer()->SetImportPointer(image->GetDataPointer(), number_of_pixels, false);
            typename itk::ImageFileWriter< ImageType >::Pointer writer = itk::ImageFileWriter< ImageType >::New();
            writer->SetFileName(filename);
            writer->SetInput(output);
            writer->Update();
        }
        else
        {
            typename ImageType2::Pointer output = ImageType2::New();
            typename ImageType2::RegionType region;
            typename ImageType2::RegionType::IndexType start;
            typename ImageType2::RegionType::SizeType size;

            double spacing[2], orgin[2];
            for(int i = 0; i < 2; i++)
            {
                start[i] = 0;
                size[i]  = isize[i];
                spacing[i] = ispacing[i];
                orgin[i] = 0;
            }
            region.SetIndex( start );
            region.SetSize( size );
            output->SetRegions( region );
            output->SetOrigin(orgin);
            output->SetSpacing(spacing);
            size_t number_of_pixels = output->GetLargestPossibleRegion().GetNumberOfPixels();
            //true - dealocate data on delete, false - no date dealocation
            output->GetPixelContainer()->SetImportPointer(image->GetDataPointer(), number_of_pixels, false);
            typename itk::ImageFileWriter< ImageType2 >::Pointer writer = itk::ImageFileWriter< ImageType2 >::New();
            writer->SetFileName(filename);
            writer->SetInput(output);
            writer->Update();
        }
        return true;
    }

    static TMazdaImage* UseItkReader(std::string filename)
    {
        typedef itk::Image< typename TMazdaImage::PixelType, TMazdaImage::Dimensions > ITKType;
        return ItkImageToMazdaImageConverter<ITKType, TMazdaImage, typename TMazdaImage::PixelType>::ToMazdaImage(filename);

//        MazdaDummy* dummy = NULL;
//        //itk::ImageIOBase::IOComponentType componentType;
//        //itk::ImageIOBase::IOPixelType pixelType;
//        size_t numDimensions;
//        itk::ImageIOBase::Pointer imageIO = itk::ImageIOFactory::CreateImageIO(filename.c_str(), itk::ImageIOFactory::ReadMode);
//        if( !imageIO ) return NULL;
//        imageIO->SetFileName(filename);
//        imageIO->ReadImageInformation();
//        numDimensions =  imageIO->GetNumberOfDimensions();
//        //pixelType = imageIO->GetPixelType();
//        //componentType = imageIO->GetComponentType();
//        typedef typename TMazdaImage::PixelType PixelType;
//        if(numDimensions == 2)
//        {
//            typedef itk::Image< float, 2 > ITKType;
//            typedef MazdaImage< PixelType, 2 > MIType;
//            MIType* mz_image = ItkImageToMazdaImageConverter<ITKType, MIType, PixelType>::ToMazdaImage(filename);
//            mz_image->SetName(filename);
//            dummy = mz_image;
//        }
//        else if(numDimensions >= 3)
//        {
//            typedef itk::Image< float, 3 > ITKType;
//            typedef MazdaImage< PixelType, 3 > MIType;
//            MIType* mz_image = ItkImageToMazdaImageConverter<ITKType, MIType, PixelType>::ToMazdaImage(filename);
//            mz_image->SetName(filename);
//            dummy = mz_image;
//        }
//        return dummy;
    }
};




/* Refs:
 * https://itk.org/Doxygen/html/classitk_1_1GDCMSeriesFileNames.html
 * https://itk.org/Doxygen/html/Examples_2IO_2DicomImageReadPrintTags_8cxx-example.html
 * https://itk.org/Doxygen/html/classitk_1_1GDCMImageIO.html
 * https://itk.org/ITKSoftwareGuide/html/Book1/ITKSoftwareGuide-Book1ch7.html
 * https://itk.org/Doxygen/html/classitk_1_1CastImageFilter.html
 * https://en.wikipedia.org/wiki/Smart_pointer
 */


#endif // MAZDAIMAGEIO_H
