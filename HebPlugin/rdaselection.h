#ifndef RDASELECTION_H
#define RDASELECTION_H
#include "../MzShared/multidimselection.h"

#include "../MzShared/dataforconsole.h"
#include "../MzShared/classifierio.h"

#include <ap.h>
#include "linalg.h"
#include "dataanalysis.h"
using namespace alglib;

struct ACC_CLASS_DATA
{
    int piacc;
    int iacc;
    int acc;
    unsigned int cl;
};

class RdaSelectionReduction : public MultiDimensionalSelection
{
public:
    virtual void NotifyProgressText(std::string text)()// = 0;

    double GoalFunction(unsigned int dimensions, unsigned int *picked_features);
    double ClassifierTraining(unsigned int dim, unsigned int* picked);
    void StoreClassifier(unsigned int dim, double Q, double th, unsigned int* picked, real_2d_array* evctrs, real_1d_array* mean);

    RdaSelectionReduction(DataForSelection* data_in, Classifiers* classifier_in, int goal_in, int classtodistinguish_in);
    ~RdaSelectionReduction();
    bool ComputeMdfs(double* table);
    //bool Select(double* Qtable, unsigned int* Qsorted);
    //bool Select(void);
    //bool Training(double* Qtable);
    bool Test(unsigned int* Qsorted, std::ofstream *file);

    DataForSelection* data;
    Classifiers* classifier;


protected:
    int classtodistinguish;

private:
//    int dimensions;
//    int maxtime;
//    bool computeclassifier;
    int parameter_goal;
    bool featureReindex(int* featurereindex, vector<string> featurenames);

    bool GoalFunctionStepTransform(unsigned int dim, unsigned int *picked, real_2d_array& evctrs, real_1d_array& mean);
    void GoalFunctionStepFishDyscriminant(double* Q, double* th, bool logar);
    void GoalFunctionStepFishDyscriminantMin(double* Q, double* th, int* index, bool logar);
    void GoalFunctionSort(void);
    void GoalFunctionStepAccDyscriminant(double *Q, double *th, bool balanced);
    void GoalFunctionStepAccDyscriminantMin(double* Q, double* th, int* index, bool balanced);

    ACC_CLASS_DATA* acd;
    double* features;
    double* fsorted;
    int* csorted;

};

#endif // RDASELECTION_H
