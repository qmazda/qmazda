/*
 * qMaZda - Image Analysis and Pattern Recognition
 *
 * Copyright 2013-2024  Piotr M. Szczypiński <piotr.szczypinski@p.lodz.pl>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef MZ_MAIN_REMAP_H
#define MZ_MAIN_REMAP_H

#if defined(WIN32) || defined(_WIN32) || defined(WIN64) || defined(_WIN64)

#include <windows.h>
#include <shellapi.h>
#include <string>
#include <vector>
#include <locale>
#include <codecvt>

#define _mz_main_console                                                        \
int main(int mz_argc, char* mz_argv[]) {                                        \
    int argc;                                                                   \
    LPWSTR* mz_szArglist = CommandLineToArgvW(GetCommandLineW(), &argc);        \
    std::wstring_convert<std::codecvt_utf8<wchar_t>> converter;                 \
    std::vector<std::string> mz_argv_strings;                                   \
    char** argv = new char*[argc];                                              \
    for(int i = 0; i < argc; i++) {                                             \
        mz_argv_strings.push_back(converter.to_bytes(mz_szArglist[i]));         \
        argv[i] = (char*)(mz_argv_strings[i].c_str());                          \
    }                                                                           \
    LocalFree(mz_szArglist);

#define _mz_main_window                                                         \
int APIENTRY WinMain(HINSTANCE, HINSTANCE, PSTR, int) {                         \
    int argc;                                                                   \
    LPWSTR* mz_szArglist = CommandLineToArgvW(GetCommandLineW(), &argc);        \
    std::wstring_convert<std::codecvt_utf8<wchar_t>> converter;                 \
    std::vector<std::string> mz_argv_strings;                                   \
    char** argv = new char*[argc];                                              \
    for(int i = 0; i < argc; i++) {                                             \
        mz_argv_strings.push_back(converter.to_bytes(mz_szArglist[i]));         \
        argv[i] = (char*)(mz_argv_strings[i].c_str());                          \
    }                                                                           \
    LocalFree(mz_szArglist);

#else

#define _mz_main_console                                                        \
int main(int argc, char* argv[]) {

#define _mz_main_window                                                         \
int main(int argc, char* argv[]) {

#endif

#endif // MZ_MAIN_REMAP_H
