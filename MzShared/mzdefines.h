#ifndef MZDEFINES_H
#define MZDEFINES_H
/*
#if defined(__APPLE__) && defined(MAZDA_IN_MACOS_BUNDLE)
const char generator_name[] = "/MzGengui.app/Contents/MacOS/MzGengui";
const char report_viewer_name[] = "/MzReport.app/Contents/MacOS/MzReport";
const char map_viewer_name[] = "/MzMaps.app/Contents/MacOS/MzMaps";*/
#if defined(WIN32) || defined(_WIN32) || defined(WIN64) || defined(_WIN64)
const char generator_name[] = "/MzGengui.exe";
const char query_name[] = "/MzGenerator.exe";
const char report_viewer_name[] = "/MzReport.exe";
const char map_viewer_name[] = "/MzMaps.exe";
#else
const char generator_name[] = "/MzGengui";
const char query_name[] = "/MzGengui";
const char report_viewer_name[] = "/MzReport";
const char map_viewer_name[] = "/MzMaps";
#endif

const char QMAZDA_VERSION[] = "Version 25.01";
#if defined(WIN32) || defined(_WIN32) || defined(WIN64) || defined(_WIN64)
const char QMAZDA_COPYRIGHT[] = "Copyright 2013-2025 by Piotr M. Szczypinski";
#else
const char QMAZDA_COPYRIGHT[] = "Copyright 2013-2025 by Piotr M. Szczypiński";
#endif

#endif // MZDEFINES_H
