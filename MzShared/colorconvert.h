/*
 * qMaZda - Image Analysis and Pattern Recognition
 *
 * Copyright 2013-2014 Piotr M. Szczypiński <piotr.szczypinski@p.lodz.pl>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef COLORCONVERT_H
#define COLORCONVERT_H

#include "imageio.h"
#include <vector>
#include <string>

extern void colorConvert(const char *chan, MzImage* image, MzImage* newimage);
extern void colorConvertNames(std::vector<std::string>* names);
inline uint16_t GetPixelMem(const unsigned int x, const unsigned int y, const MzImage* input)
{
    return *(((uint16_t*)(input->data+y*input->linesize))+x);
}

inline uint16_t GetPixelNorm(const unsigned int x, const unsigned int y, const MzImage* input, const MzNormalization* norm)
{
    if(norm->min == 0 && norm->max == 0xffff)
    {
        return GetPixelMem(x, y, input)>>(16-norm->bpp);
    }
    else
    {
        int p = GetPixelMem(x, y, input);
        return (p>norm->max ?norm->max-norm->min :(p<norm->min ?0 :p-norm->min)) * (((int)1<<(norm->bpp))-1) / (norm->max-norm->min);
    }
}


inline uint16_t GetPixelMem(const unsigned int x, const unsigned char* l)
{
    return *(((uint16_t*)l)+x);
}

inline uint16_t GetPixelNorm(const unsigned int x, const unsigned char* l, const MzNormalization* norm)
{
    if(norm->min == 0 && norm->max == 0xffff)
    {
        return GetPixelMem(x, l)>>(16-norm->bpp);
    }
    else
    {
        int divider = norm->max-norm->min;
        if(divider == 0)
            return (((int)1<<(norm->bpp))-1)/2;
        int p = GetPixelMem(x, l);
//        int pp =(p>norm->max ?norm->max-norm->min :(p<norm->min ?0 :p-norm->min)) * (((int)1<<16)-1) / (divider);
        return (p>norm->max ?norm->max-norm->min :(p<norm->min ?0 :p-norm->min)) * (((int)1<<(norm->bpp))-1) / (divider);
    }
}



inline uint16_t GetPixelMem(unsigned int x[MZDIMENSIONS], MzImage* input)
{
    unsigned char* l = input->line(x);
    return *(((uint16_t*)l)+x[0]);
}

inline uint16_t GetPixelNorm(unsigned int x[MZDIMENSIONS], const MzNormalization* norm, MzImage* input)
{
    if(norm->min == 0 && norm->max == 0xffff)
    {
        return GetPixelMem(x, input)>>(16-norm->bpp);
    }
    else
    {
        int p = GetPixelMem(x, input);
        return (p>norm->max ?norm->max-norm->min :(p<norm->min ?0 :p-norm->min)) * (((int)1<<(norm->bpp))-1) / (norm->max-norm->min);
    }
}


#endif // COLORCONVERT_H
