#include "../MzShared/propertyvaluetree.h"
#include "ui_propertyvaluetree.h"

PropertyValueTree::PropertyValueTree(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::PropertyValueTree)
{
    ui->setupUi(this);
    tree = ui->treeWidget;
    item = NULL;
}

PropertyValueTree::~PropertyValueTree()
{
    delete ui;
}

QString PropertyValueTree::getValue(QString name)
{
    return tree->getValue(name);
}

QString PropertyValueTree::getValue(unsigned int index)
{
    return tree->getValue(index);
}

void PropertyValueTree::CreateGroup(const std::string name, const std::string tip)
{
    item = tree->createMethod(QString::fromStdString(name), QString::fromStdString(tip));
}
void PropertyValueTree::CreateOption(const std::string name, const std::string value, const std::string tip)
{
    tree->createProperty(item, QString::fromStdString(name), QString::fromStdString(value), QString::fromStdString(tip));
}
void PropertyValueTree::CreateEnd(void)
{
    tree->expandAll();
    tree->setItemDelegateForColumn(0, new NoEditDelegate(this));
    tree->setItemDelegateForColumn(1, new EditDelegate(this, tree, 0));
}
