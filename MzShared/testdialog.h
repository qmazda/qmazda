/*
 * qMaZda - Image Analysis and Pattern Recognition
 * 
 * Copyright 2013  Piotr M. Szczypiński <piotr.szczypinski@p.lodz.pl>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef TESTDIALOG_H
#define TESTDIALOG_H

#include <QDialog>
#include <string>

namespace Ui {
class testdialog;
}

class TestDialog : public QDialog
{
    Q_OBJECT
    
public:
    explicit TestDialog(QWidget *parent = 0);
    ~TestDialog();
//    void setTableData(int nc, int nr, std::string *cn, std::string *rn, unsigned int* data);
//    void setTableData(int nr, std::vector<std::string> cn, std::string* rn, unsigned int *data);
    void setTableData(std::string* data);
    
private slots:
    void on_pushButton_clicked();

    void on_copyPushButton_clicked();

private:
    Ui::testdialog *ui;
};

#endif // TESTDIALOG_H
