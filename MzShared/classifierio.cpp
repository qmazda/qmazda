#include <sstream>
#include <string.h>
#include <locale.h>
#include <stdlib.h>
#include "classifierio.h"
#include "csvio.h"
#include "filenameremap.h"


//=============================================================================
string Classifier::getName()
{
    stringstream ss;
    for(vector<string>::iterator cn = classnames.begin(); cn != classnames.end(); ++cn)
    {
        ss << *cn << "#";
    }
    ss << featurenames.size() << "D";
    return ss.str();
}

//=============================================================================
// Save classifier function

bool Classifiers::loadClassifier(ifstream *file, bool appended)
{
    string inputstring;
    int classifiersnumber;

    char *prevloc = setlocale(LC_ALL, NULL);
    if(prevloc) prevloc = strdup(prevloc);
    setlocale(LC_ALL, "C");

    if(appended)
    {
        bool notok = true;
        while(!file->eof())
        {
            *file>>inputstring;
            if(inputstring != identifier)
            {
                notok = false;
                break;
            }
        }
        if(notok)
        {
            setlocale(LC_ALL, prevloc);
            free(prevloc);
            return false;
        }
    }
    else
    {
        *file>>inputstring;
        if(inputstring != identifier)
        {
            setlocale(LC_ALL, prevloc);
            free(prevloc);
            return false;
        }
    }
    *file>>inputstring;
    if(inputstring != "@Classifiers")
    {
        setlocale(LC_ALL, prevloc);
        free(prevloc);
        return false;
    }
    *file>>classifiersnumber;

    classifiers.resize(classifiersnumber);

    for(int c = 0; c < classifiersnumber; c++)
    {
        int classnumber;
        *file>>inputstring;
        if(inputstring != "@ClassNames")
        {
            setlocale(LC_ALL, prevloc);
            free(prevloc);
            return false;
        }
        *file>>classnumber;
        vector<string> vclassnames;
        vclassnames.resize(classnumber);
        for(int cl = 0; cl < classnumber; cl++) *file>>vclassnames[cl];

        int featurenumber;
        *file>>inputstring;
        if(inputstring != "@FeatureNames")
        {
            setlocale(LC_ALL, prevloc);
            free(prevloc);
            return false;
        }
        *file>>featurenumber;
        vector<string> vfeaturenames;
        vfeaturenames.resize(featurenumber);
        for(int fn = 0; fn < featurenumber; fn++) *file>>vfeaturenames[fn];

        int valuelines;
        *file>>inputstring;
        if(inputstring != "@Values")
        {
            setlocale(LC_ALL, prevloc);
            free(prevloc);
            return false;
        }
        *file>>valuelines;
        vector< vector<double> > vvalues;
        vvalues.resize(valuelines);

        for(int vl = 0; vl < valuelines; vl++)
        {
            int valuesnumber;
            *file >> valuesnumber;
            vector<double> vvvalues;
            vvvalues.resize(valuesnumber);
            for(int va = 0; va < valuesnumber; va++) *file>>vvvalues[va];
            vvalues[vl] = vvvalues;
        }
        if(file->eof())
        {
            setlocale(LC_ALL, prevloc);
            free(prevloc);
            return false;
        }

        Classifier vclassifier;
        vclassifier.classnames = vclassnames;
        vclassifier.featurenames = vfeaturenames;
        vclassifier.values = vvalues;
        classifiers[c] = vclassifier;
    }
    setlocale(LC_ALL, prevloc);
    free(prevloc);
    return true;
}


bool Classifiers::loadClassifier(const char* fileName, bool appended)
{
    ifstream file;
#if defined _MZ_UTF8_TO_U16
        file.open(_mz_utf8_to_u16(fileName).c_str());
#else
        file.open(fileName);
#endif
    if (!file.is_open())
        return false;
    if (!file.good())
        return false;

    bool ret = loadClassifier(&file, appended);

    file.close();
    return ret;
}

//=============================================================================
// Save classifier function

bool Classifiers::saveClassifier(ofstream* file, bool hexa)
{
    char *prevloc = setlocale(LC_ALL, NULL);
    if(prevloc) prevloc = strdup(prevloc);
    setlocale(LC_ALL, "C");

    *file<<identifier<<std::endl;
    *file << "@Classifiers " << classifiers.size() << std::endl;
    for(vector<Classifier>::iterator c = classifiers.begin(); c != classifiers.end(); ++c)
    {
/*
        for(vector<ParameterLine>::iterator p = c->parameters.begin(); p != c->parameters.end(); ++p)
        {
            *file << p->name << " " << p->value << std::endl;
        }
*/
        *file << "@ClassNames " << c->classnames.size();
        for(vector<string>::iterator cn = c->classnames.begin(); cn != c->classnames.end(); ++cn)
        {
            *file << " " << *cn;
        }
        *file << std::endl;

        *file << "@FeatureNames " << c->featurenames.size();
        for(vector<string>::iterator fn = c->featurenames.begin(); fn != c->featurenames.end(); ++fn)
        {
            *file << " " << *fn;
        }
        *file << std::endl;

        *file << "@Values " << c->values.size() << std::endl;
        for(vector< vector<double> >::iterator dd = c->values.begin(); dd != c->values.end(); ++dd)
        {
            *file << dd->size();
            for(vector<double>::iterator d = dd->begin(); d != dd->end(); ++d)
            {
                if(hexa) *file << " " << CsvIo::doubleToHex(*d);
                else *file << " " << *d;
            }
            *file << std::endl;
        }
    }
    setlocale(LC_ALL, prevloc);
    free(prevloc);
    return true;
}


bool Classifiers::saveClassifier(const char* fileName, bool hexa, bool append)
{
    ofstream file;
    if(append)
    {
#if defined _MZ_UTF8_TO_U16
        file.open(_mz_utf8_to_u16(fileName).c_str(), std::ofstream::out | std::ofstream::app);
#else
        file.open(fileName, std::ofstream::out | std::ofstream::app);
#endif
        file << std::endl << std::endl;
    }
    else
#if defined _MZ_UTF8_TO_U16
        file.open(_mz_utf8_to_u16(fileName).c_str());
#else
        file.open(fileName);
#endif
    if (!file.is_open()) return false;
    if (!file.good()) return false;

    bool ret = saveClassifier(&file, hexa);

    file.close();
    return ret;
}
