/*
 * qMaZda - Image Analysis and Pattern Recognition
 * 
 * Copyright 2013  Piotr M. Szczypiński <piotr.szczypinski@p.lodz.pl>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


#include "selectionlist.h"
#include "ui_selectionlist.h"
#include "../MzShared/dataforconsole.h"


SelectionList::SelectionList(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::SelectionList)
{
    ui->setupUi(this);
    checkedFeatures = NULL;
}

SelectionList::~SelectionList()
{
    delete ui;
    if(checkedFeatures != NULL) delete[] checkedFeatures;
}

void SelectionList::setList(DataForSelection* data, double* Qtable, unsigned int* Qsorted, int number_checked)
{
    features = data->featurenumber;

    if(Qtable != NULL && Qsorted!= NULL)
    {
        for(int c = 0; c < features; c++)
        {
            if((int)Qsorted[c] < features)
            {
                QListWidgetItem *item = new QListWidgetItem(ui->listWidget);
                item->setText(QString("%1\t%2").arg(Qtable[Qsorted[c]], 10, 'f', 4, ' ').arg(QString::fromStdString(data->featurenames[Qsorted[c]])));
                item->setFlags(Qt::ItemIsSelectable | Qt::ItemIsEnabled | Qt::ItemIsUserCheckable);
                if(c < number_checked) item->setCheckState(Qt::Checked);
                else item->setCheckState(Qt::Unchecked);
            }
            else break;
        }
    }
    else
    {
        for(int c = 0; c < features; c++)
        {
            QListWidgetItem *item = new QListWidgetItem(ui->listWidget);
            item->setText(QString("%1").arg(QString::fromStdString(data->featurenames[c])));
            item->setFlags(Qt::ItemIsSelectable | Qt::ItemIsEnabled | Qt::ItemIsUserCheckable);
            if(c < number_checked) item->setCheckState(Qt::Checked);
            else item->setCheckState(Qt::Unchecked);
        }

    }
}

void SelectionList::on_acceptButton_clicked()
{
    checkedFeatures = new bool[features]; //[ui->listWidget->count()];
    for(int c = 0; c < features; c++)
    {
        QListWidgetItem* item = ui->listWidget->item(c);
        if(item != NULL) checkedFeatures[c] = (item->checkState() == Qt::Checked);
        else checkedFeatures[c] = false;
        //checkedFeatures[c] = (ui->listWidget->item(c)->checkState() == Qt::Checked);
    }
    done(QDialog::Accepted);
}

void SelectionList::on_cancelButton_clicked()
{
    done(QDialog::Rejected);
}
