
#ifndef QT5QT6COMPATIBILITY_H
#define QT5QT6COMPATIBILITY_H

#if QT_VERSION >= QT_VERSION_CHECK(6, 0, 0)
    #define _ev_position_x ev->position().x()
    #define _ev_position_y ev->position().y()

    #define _regularexpression_from_wildecard(txt) QRegularExpression rx = QRegularExpression::fromWildcard(txt);
    #define _if_rx_match(txt) if(rx.match(txt).hasMatch())

    #define _table_type typeId
#else
    #define _ev_position_x ev->x()
    #define _ev_position_y ev->y()

    #define _regularexpression_from_wildecard(txt) QRegExp rx(txt); rx.setPatternSyntax(QRegExp::Wildcard);
    #define _if_rx_match(txt) rx.setPatternSyntax(QRegExp::Wildcard); if(rx.exactMatch(txt))

    #define _table_type type
#endif


#endif // QT5QT6COMPATIBILITY_H
