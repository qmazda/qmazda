/*
 * qMaZda - Image Analysis and Pattern Recognition
 * 
 * Copyright 2013  Piotr M. Szczypiński <piotr.szczypinski@p.lodz.pl>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


#include "testdialog.h"
#include "ui_testdialog.h"
#include <QClipboard>
#include <sstream>
#include <stdio.h>

TestDialog::TestDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::testdialog)
{
    ui->setupUi(this);
}

TestDialog::~TestDialog()
{
    delete ui;
}

//void TestDialog::setTableData(int nc, int nr, std::string* cn, std::string* rn, unsigned int *data)
//{
//    ui->tableWidget->setColumnCount(nc);
//    ui->tableWidget->setRowCount(nr);

//    for(int c = 0; c < nc; c++)
//    {
//        QTableWidgetItem *newItem = new QTableWidgetItem(QString::fromStdString(cn[c]));
//        ui->tableWidget->setHorizontalHeaderItem(c, newItem);
//    }

//    for(int r = 0; r < nr; r++)
//    {
//        QTableWidgetItem *newItem = new QTableWidgetItem(QString::fromStdString(rn[r]));
//        ui->tableWidget->setVerticalHeaderItem(r, newItem);
//        for(int c = 0; c < nc; c++)
//        {
//            QTableWidgetItem *newItem = new QTableWidgetItem(tr("%1").arg(data[r*nc + c]));
//            if(cn[c].compare(0, rn[r].size(), rn[r]) == 0) newItem->setBackgroundColor(QColor(Qt::green).lighter());
//            else if(cn[c] == "#") newItem->setBackgroundColor(QColor(Qt::yellow).lighter());
//            ui->tableWidget->setItem(r, c, newItem);
//        }
//    }
//}

//void TestDialog::setTableData(int nr, std::vector<std::string> cn, std::string* rn, unsigned int *data)
//{
//    int nc = cn.size();
//    ui->tableWidget->setColumnCount(nc);
//    ui->tableWidget->setRowCount(nr);

//    for(int c = 0; c < nc; c++)
//    {
//        QTableWidgetItem *newItem = new QTableWidgetItem(QString::fromStdString(cn[c]));
//        ui->tableWidget->setHorizontalHeaderItem(c, newItem);
//    }

//    for(int r = 0; r < nr; r++)
//    {
//        QTableWidgetItem *newItem = new QTableWidgetItem(QString::fromStdString(rn[r]));
//        ui->tableWidget->setVerticalHeaderItem(r, newItem);
//        for(int c = 0; c < nc; c++)
//        {
//            QTableWidgetItem *newItem = new QTableWidgetItem(tr("%1").arg(data[r*nc + c]));
//            if(cn[c].compare(0, cn[c].size(), rn[r]) == 0) newItem->setBackgroundColor(QColor(Qt::green).lighter());
//            else if(cn[c] == "#" || cn[c] == "!") newItem->setBackgroundColor(QColor(Qt::yellow).lighter());
//            ui->tableWidget->setItem(r, c, newItem);
//        }
//    }
//}


//void TestDialog::setTableData(std::string* data)
//{
//    std::stringstream ss(*data);
////    printf("%s\n", (*data).c_str());
////    fflush(stdout);
//    std::vector<std::string> vertHead;
//    std::vector<std::string> horzHead;
//    char* separator = (char*)"\t";
//    unsigned int row = 0;
//    do
//    {
//        std::string line;
//        std::getline(ss, line);
//        if(line.length() <= 1) break;
//        unsigned int column = 0;
//        size_t lastfound = 0;
//        size_t found = line.find_first_of(separator);
//        bool dowhile = true;
//        while(dowhile)
//        {
//            if(found == std::string::npos)
//            {
//                found = line.length();
//                dowhile = false;
//            }
//            if(row == 0)
//            {
//                if(column > 0)
//                {
//                    horzHead.push_back(line.substr(lastfound, found-lastfound));
//                }
//            }
//            else
//            {
//                if(column == 0)
//                {
//                    vertHead.push_back(line.substr(lastfound, found-lastfound));
//                }
//            }
//            column++;
//            lastfound = found+1;
//            found = line.find_first_of(separator, found+1);
//        }
//        row++;
//    }
//    while(!ss.eof());

//    ui->tableWidget->setColumnCount(horzHead.size());
//    ui->tableWidget->setRowCount(vertHead.size());

////    bool color_set = false;
////    for(unsigned int h = 0; h < horzHead.size(); h++)
////    {
////        if(horzHead[h] == "#") color_set = true;
////        for(unsigned int v = 0; v < vertHead.size(); v++)
////        {
////            if(horzHead[h] == vertHead[v]) color_set = true;
////        }
////    }

//    std::stringstream ss2(*data);
//    row = 0;
//    do
//    {
//        std::string line;
//        std::getline(ss2, line);
//        if(line.length() <= 1) break;
//        unsigned int column = 0;
//        size_t lastfound = 0;
//        size_t found = line.find_first_of(separator);
//        bool dowhile = true;
//        while(dowhile)
//        {
//            if(found == std::string::npos)
//            {
//                found = line.length();
//                dowhile = false;
//            }
//            if(row == 0)
//            {
//                if(column > 0)
//                {
//                    QTableWidgetItem *newItem = new QTableWidgetItem(QString::fromStdString(line.substr(lastfound, found-lastfound)));
//                    ui->tableWidget->setHorizontalHeaderItem(column-1, newItem);
//                }
//            }
//            else
//            {
//                if(column == 0)
//                {
//                    QTableWidgetItem *newItem = new QTableWidgetItem(QString::fromStdString(line.substr(lastfound, found-lastfound)));
//                    ui->tableWidget->setVerticalHeaderItem(row-1, newItem);
//                }
//                else
//                {
//                    QTableWidgetItem *newItem = new QTableWidgetItem(QString::fromStdString(line.substr(lastfound, found-lastfound)));
//                    //if(horzHead[column-1] == vertHead[row-1])
//                    if(horzHead[column-1].compare(0, vertHead[row-1].size(), vertHead[row-1]) == 0)
//                    {
//                        newItem->setBackgroundColor(QColor(Qt::green).lighter());
//                    }
//                    else if(horzHead[column-1] == "#" || horzHead[column-1] == "$" )
//                    {
//                        newItem->setBackgroundColor(QColor(Qt::blue).lighter());
//                    }
//                    else if(horzHead[column-1] == "!" || horzHead[column-1] == "~" )
//                    {
//                        newItem->setBackgroundColor(QColor(Qt::yellow).lighter());
//                    }
////                    if(!color_set)
////                    {
////                        if((column-1)%vertHead.size() == row-1) newItem->setBackgroundColor(QColor(Qt::green).lighter());
////                    }
//                    ui->tableWidget->setItem(row-1, column-1, newItem);
//                }
//            }
//            column++;
////            if(column > horzHead.size())
////                return;
//            lastfound = found+1;
//            found = line.find_first_of(separator, found+1);
//        }
//        row++;
////        if(row > vertHead.size())
////            return;
//    }
//    while(!ss2.eof());
//}



void TestDialog::setTableData(std::string* data)
{
    std::stringstream ss(*data);
//    printf("%s\n", (*data).c_str());
//    fflush(stdout);
    std::vector<std::string> vertHead;
    std::vector<std::string> horzHead;
    char* separator = (char*)"\t";
    unsigned int row = 0;

    QColor background = this->palette().color(QPalette::Window);
    bool dark = false;
    if(background.red() + background.green() + background.blue() <= 384)
        dark = true;


    do
    {
        std::string line;
        std::getline(ss, line);
        if(line.length() <= 1) break;
        unsigned int column = 0;
        size_t lastfound = 0;
        size_t found = line.find_first_of(separator);
        bool dowhile = true;
        while(dowhile)
        {
            if(found == std::string::npos)
            {
                found = line.length();
                dowhile = false;
            }
            if(row == 0)
            {
                if(column > 0)
                {
                    horzHead.push_back(line.substr(lastfound, found-lastfound));
                }
            }
            else
            {
                if(column == 0)
                {
                    vertHead.push_back(line.substr(lastfound, found-lastfound));
                }
            }
            column++;
            lastfound = found+1;
            found = line.find_first_of(separator, found+1);
        }
        row++;
    }
    while(!ss.eof());

    ui->tableWidget->setColumnCount(vertHead.size());
    ui->tableWidget->setRowCount(horzHead.size());
    std::stringstream ss2(*data);
    row = 0;
    do
    {
        std::string line;
        std::getline(ss2, line);
        if(line.length() <= 1) break;
        unsigned int column = 0;
        size_t lastfound = 0;
        size_t found = line.find_first_of(separator);
        bool dowhile = true;
        while(dowhile)
        {
            if(found == std::string::npos)
            {
                found = line.length();
                dowhile = false;
            }
            if(row == 0)
            {
                if(column > 0)
                {
                    QTableWidgetItem *newItem = new QTableWidgetItem(QString::fromStdString(line.substr(lastfound, found-lastfound)));
                    ui->tableWidget->setVerticalHeaderItem(column-1, newItem);
                }
            }
            else
            {
                if(column == 0)
                {
                    QTableWidgetItem *newItem = new QTableWidgetItem(QString::fromStdString(line.substr(lastfound, found-lastfound)));
                    ui->tableWidget->setHorizontalHeaderItem(row-1, newItem);
                }
                else
                {
                    QTableWidgetItem *newItem = new QTableWidgetItem(QString::fromStdString(line.substr(lastfound, found-lastfound)));
                    if(dark)
                    {
                        if(horzHead[column-1].compare(0, vertHead[row-1].size(), vertHead[row-1]) == 0)
                        {
                            newItem->setBackground(QBrush(QColor(Qt::green).darker()));
                        }
                        else if(horzHead[column-1] == "#" || horzHead[column-1] == "$" )
                        {
                            newItem->setBackground(QBrush(QColor(Qt::blue).darker()));
                        }
                        else if(horzHead[column-1] == "!" || horzHead[column-1] == "~" )
                        {
                            newItem->setBackground(QBrush(QColor(Qt::yellow).darker()));
                        }
                    }
                    else
                    {
                        if(horzHead[column-1].compare(0, vertHead[row-1].size(), vertHead[row-1]) == 0)
                        {
                            //newItem->setBackgroundColor(QColor(Qt::green).lighter());
                            newItem->setBackground(QBrush(QColor(Qt::green).lighter()));
                        }
                        else if(horzHead[column-1] == "#" || horzHead[column-1] == "$" )
                        {
                            //newItem->setBackgroundColor(QColor(Qt::blue).lighter());
                            newItem->setBackground(QBrush(QColor(Qt::blue).lighter()));
                        }
                        else if(horzHead[column-1] == "!" || horzHead[column-1] == "~" )
                        {
                            //newItem->setBackgroundColor(QColor(Qt::yellow).lighter());
                            newItem->setBackground(QBrush(QColor(Qt::yellow).lighter()));
                        }
                    }
                    ui->tableWidget->setItem(column-1, row-1, newItem);
                }
            }
            column++;
            lastfound = found+1;
            found = line.find_first_of(separator, found+1);
        }
        row++;
    }
    while(!ss2.eof());
}

void TestDialog::on_pushButton_clicked()
{
    done(QDialog::Accepted);
}

void TestDialog::on_copyPushButton_clicked()
{
    QString selected_text;
    int cm = ui->tableWidget->columnCount();
    int rm = ui->tableWidget->rowCount();
    selected_text.append(' ');
    for(int c = 0; c < cm; c++)
    {
        selected_text.append('\t');
        selected_text.append(ui->tableWidget->horizontalHeaderItem(c)->text());
    }
    selected_text.append('\n');
    for(int r = 0; r < rm; r++)
    {
        selected_text.append(ui->tableWidget->verticalHeaderItem(r)->text());
        for(int c = 0; c < cm; c++)
        {
            selected_text.append('\t');
            QTableWidgetItem *item = ui->tableWidget->item(r, c);
            selected_text.append(item->text());
        }
        selected_text.append('\n');
    }
    QApplication::clipboard()->setText(selected_text);
}
