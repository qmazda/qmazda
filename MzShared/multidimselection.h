/*
 * qMaZda - Image Analysis and Pattern Recognition
 * 
 * Copyright 2013  Piotr M. Szczypiński <piotr.szczypinski@p.lodz.pl>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


#ifndef MULTIDIMSELECTION_H
#define MULTIDIMSELECTION_H

#define MULTI_SELECTION_FAILED 0
#define MULTI_SELECTION_SUCCESS 1
#define MULTI_SELECTION_BEGINS 2
#define MULTI_SELECTION_STOPPED 3
#define MULTI_SELECTION_COMPLETED 4
#define MULTI_SELECTION_STEP 5
#define MULTI_SELECTION_CANCELED 6

#include <vector>
#include <string>

#define MZ_USE_THREADS

#ifdef MZ_USE_THREADS
    #include <thread>
    #include <mutex>
#endif

//extern bool breakanalysis;

class MultiDimensionalSelection
{
public:
    virtual void NotifyProgressStep(void) = 0;
    virtual void NotifyProgressStage(const int notification, const int dimensionality, const double value, const unsigned int* selected) = 0;

    virtual double GoalFunction(unsigned int dimensions, unsigned int* picked_features) = 0;
    virtual double ClassifierTraining(unsigned int dimensions, unsigned int* selected) = 0;

    virtual ~MultiDimensionalSelection(){}
    void cancelFullSearch(void);
    int GenerateNextCombination(int dim, unsigned int *feature);
    int FullSearchThread(const unsigned int *Qsorted, const int dims, const int parameter_seconds, const time_t endtime,
            double *Qtable, double* Qextr, unsigned int* feature, unsigned int* best, bool parameter_maximize);
    bool FullSearch(int nooffeatures, double* Qtable, unsigned int* Qsorted,
                    int parameter_dimensions, int parameter_seconds, bool parameter_maximize);
    bool breakanalysis;
    bool use_multithreading;

private:
    void QualitySorter(int nooffeatures, double* Qtable, unsigned int* Qsorted);
    void PenaltySorter(int nooffeatures, double* Qtable, unsigned int* Qsorted);

#ifdef MZ_USE_THREADS
private:
    std::mutex m_mutex;
    std::mutex m_mutex1;
    std::mutex m_mutex2;
#endif
};

#endif //MULTIDIMSELECTION_H
