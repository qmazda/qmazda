#include "csvio.h"
#include <limits>
#include <fstream>
#include <stdlib.h>
#include <stdio.h>
#include <iostream>
#include <string.h>
#include <string>
#include <vector>


char* CsvIo::doubleToHex(const double value)
{
    static char text[sizeof(double)*2+4];
    int test_endian;
    unsigned char* byte;
    int v;
    char* ptr;

    test_endian = 1;
    if(((unsigned char*)&test_endian)[0] == 0) test_endian = 0;

    ptr = text;
    byte = (unsigned char*)&value;
    sprintf(ptr, "$"); ptr++;//ptr+=2;
    if(test_endian) for(v = sizeof(double)-1; v >= 0; v--)
    {
        sprintf(ptr, "%.2X", (int)byte[v]); ptr+=2;
    }
    else for(v = 0; v < (int) sizeof(double); v++)
    {
        sprintf(ptr, "%.2X", (int)byte[v]); ptr+=2;
    }
    return text;
}

#if defined(WIN32) || defined(_WIN32) || defined(WIN64) || defined(_WIN64)
    #define strtouq _strtoui64
#endif

double CsvIo::hexToDouble(const char* text)
{
    if(text[0] == '$')
    {
        unsigned long long longint;
        char* ptr;
        longint = strtouq(text+1, &ptr, 16);
        if(ptr == text) return std::numeric_limits<double>::quiet_NaN();
        return *((double*)&longint);
    }
    else
    {
        char* endptr = NULL;
        double value = strtod(text, &endptr);
        if (*endptr) return std::numeric_limits<double>::quiet_NaN();
        return value;
    }
}

void CsvIo::saveCSVStreamHeader(std::ostream* file, std::vector<std::string> *feature_names, bool savecategory, bool savecomment)
{
    prevloc = setlocale(LC_ALL, NULL);
    if(prevloc) prevloc = strdup(prevloc);
    setlocale(LC_ALL, "C");
    number_of_features = feature_names->size();
    if(number_of_features > 0)
    {
        *file << (*feature_names)[0];
        for(int r = 1; r < number_of_features; r++)
        {
           *file << "," << (*feature_names)[r];
        }
    }
    if(savecategory)
        *file << ",Category";
    if(savecomment)
        *file << ",Comment";

    *file << std::endl;
}

void CsvIo::saveCSVStreamDataLine(std::ostream* file, double* feature_values, bool hexa, std::string* category, std::string* comment)
{
    if(number_of_features > 0)
    {
        if(hexa) *file << doubleToHex(feature_values[0]);
        else *file << feature_values[0];
        for(int r = 1; r < number_of_features; r++)
        {
            if(hexa) *file << "," << doubleToHex(feature_values[r]);
            else *file << "," << feature_values[r];
        }
    }
    if(category != NULL)
        *file << "," << *category;
    if(comment != NULL)
        *file << "," << *comment;
    *file << std::endl;
}

void CsvIo::saveCSVStreamClose()
{
    setlocale(LC_ALL, prevloc);
    free(prevloc);
}

bool CsvIo::loadCSVStreamHeader(std::istream* file, std::vector<std::string> *feature_names)
{
    number_of_features = 0;
    if(! file->good())
        return false;
    std::string line;
    prevloc = setlocale(LC_ALL, NULL);
    if(prevloc) prevloc = strdup(prevloc);
    setlocale(LC_ALL, "C");
    getline(*file, line);

    size_t found = line.find_first_of(toremove);
    while (found != std::string::npos)
    {
        line.erase(line.begin()+found);
        found=line.find_first_of(toremove, found);
    }
    number_of_features = 1;
    found = line.find_first_of(separator);
    while (found != std::string::npos)
    {
        number_of_features++;
        found=line.find_first_of(separator, found+1);
    }
    if(number_of_features < 2)
    {
        return false;
    }
    category_index = -1;
    comment_index = -1;
    size_t lastfound = 0;
    found = 0;
    for(int r = 0; r < number_of_features; r++)
    {
        found = line.find_first_of(separator, found+1);
        if(found == std::string::npos) found = line.length();

        if(line.substr(lastfound, found-lastfound) == "Category")
        {
            category_index = r;
        }
        else if(line.substr(lastfound, found-lastfound) == "Comment")
        {
            comment_index = r;
        }
        else
        {
            feature_names->push_back(line.substr(lastfound, found-lastfound));
        }
        lastfound = found+1;
    }
    return true;
}

bool CsvIo::loadCSVStreamDataLine(std::istream* file, double* feature_values, std::string* category, std::string* comment)
{
    //number_of_features = feature_names.size();
    if(! file->good())
        return false;
    std::string line;
    getline(*file, line);
    size_t found = line.find_first_of(toremove);
    while (found != std::string::npos)
    {
        line.erase(found, found);
        found=line.find_first_of(toremove, found+1);
    }
    int number_of = 1;
    found = line.find_first_of(separator);
    while (found != std::string::npos)
    {
        number_of++;
        found=line.find_first_of(separator, found+1);
    }

    int r;
    if(number_of == number_of_features)
    {
        size_t lastfound = 0;
        found = 0;
        int rr = 0;
        size_t line_length = line.length();
        for(r = 0; r < number_of_features && found < line_length; r++)
        {
            found = line.find_first_of(separator, found+1);
            if(found == std::string::npos) found = line_length;

            if(category_index == r)
            {
                *category = line.substr(lastfound, found-lastfound);
            }
            else if(comment_index == r)
            {
                *comment = line.substr(lastfound, found-lastfound);
            }
            else
            {
                double vv = hexToDouble(line.substr(lastfound, found-lastfound).c_str());
                feature_values[rr] = vv;
                rr++;
            }
            lastfound = found+1;
        }
        if(r != number_of_features)
            return false;

        return true;
    }
    return false;
}

void CsvIo::loadCSVStreamClose()
{
    setlocale(LC_ALL, prevloc);
    free(prevloc);
}
