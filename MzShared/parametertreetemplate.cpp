/*
 * QMAZDA project: Image Analysis and Pattern Recognition
 *
 * Copyright 2013-2016 Piotr M. Szczypiński <piotr.szczypinski@p.lodz.pl>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "parametertreetemplate.h"

#include <limits>
#include <string>
#include <iostream>



/*
Example text file to code parameter tree template:

t;?;Mor/Morphological features
.n;X
.n;Y
.n;Area
.n;Spol/Something
.n;RectSxL
.n;RectSoL
.n;RectS
.n;RectL
t;?;Y;R;G;B;U;V;H;S;I;Q;u;v;i;q;h;x;y;z;L;a;b/Color channels
.t;?;D;N;S/Grayscale normalization
..i;5;1;16/Bits per pixel
...t;?;Gab/Select algorithm
....i;4;1;32/Standard deviation of the Gaussian envelope
.....t;?;O;H;N;V;Z;Hn;Nv;Vz;Zh/Direction
......i;4;1;32/Frequency
.......n;Mag
...t;?;Grlm/h;Gray level run length matrix
....t;?;H;V;N;Z/Direction
.....n;RLNonUni
.....n;GLevNonUn
.....n;LngREmph
.....n;Fraction
.....n;MRLNU
.....n;MGLNU
...t;?;Glcm;Glch/Gray level coocurrence matrix
....t;?;H;V;N;Z/Direction
.....i;1;1;9/Offset
......n;Area
......n;AngScMom
......n;Contrast
......n;Correlat
......n;SumOfSqs
......n;InvDfMom
......n;SumAverg
......n;SumVarnc
......n;SumEntrp
......n;Entropy
......n;DifVarnc
......n;DifEntrp
...t;?;Arm
....n;Teta1
....n;Teta2
....n;Teta3
....n;Teta4
....n;Sigma
...t;?;Grad/Image gradient magnitude
....n;Area
....n;Mean
....n;Variance
....n;Skewness
....n;Kurtosis
....n;NonZeros
...t;?;Hist/Image histogram
....n;Area
....n;Mean
....n;Variance
....n;Skewness
....n;Kurtosis
....n;Perc01
....n;Perc10
....n;Perc50
....n;Perc90
....n;Perc99
....n;Maxm01
....n;Domn01
....n;Maxm10
....n;Domn10
*/

/**
 * @brief NameStub::toString serializes the object to text string
 * @return
 */
std::string NameStub::toString(void)
{
    std::stringstream ss;
    switch(type)
    {
    case NSTInt:
        ss << "i;" << iprefered;
        if(min != std::numeric_limits<int>::min() || max != std::numeric_limits<int>::max())
            ss << ";" << min;
        if(max != std::numeric_limits<int>::max())
            ss << ";" << max;
        break;
    case NSTText:
        ss << "t;" << tprefered;
        for(unsigned int i = 0; i < list.size(); i++)
            ss << ";" << list[i];
        break;
    case NSTName:
        ss << "n";// << tprefered;
        for(unsigned int i = 0; i < list.size(); i++)
            ss << ";" << list[i];
        break;

    case NSTRange:
        ss << "r;" << iprefered;
        if(min != std::numeric_limits<int>::min() || max != std::numeric_limits<int>::max())
            ss << ";" << min;
        if(max != std::numeric_limits<int>::max())
            ss << ";" << max;
        break;

    default:break;
    }
    if(tip.length() > 0)
        ss << "/" << tip;
    return ss.str();
}

/**
 * @brief NameStub::fromString
 * @param str
 */
void NameStub::fromString(std::string str)
{
    std::string::size_type length = str.length();
    while(length > 0)
    {
        char lst = str.at(length-1);
        if(lst == '\r' || lst == '\n' || lst == '\t')
        {
            length --;
            str.resize(length);
        }
        else break;
    }
    if(length <= 0) return;

    std::string::size_type slash = str.find_first_of('/');
    std::string code;
    if(slash < length - 1)
    {
        tip = str.substr(slash + 1);
        code = str.substr(0, slash);
    }
    else
    {
        tip = "";
        code = str;
    }
    std::vector<std::string> listlocal;
    std::string::size_type prev = 0, curr = 0;
    while((curr = code.find(";", curr)) != std::string::npos)
    {
        listlocal.push_back(code.substr(prev, curr-prev));
        prev = ++curr;
    }
    listlocal.push_back(code.substr(prev, curr-prev));
    if(listlocal[0] == "i")
    {
        type = NSTInt;
        min = std::numeric_limits<int>::min();
        max = std::numeric_limits<int>::max();
        if(listlocal.size() > 1) iprefered = atoi(listlocal[1].c_str());
        if(listlocal.size() > 2) min = atoi(listlocal[2].c_str());
        if(listlocal.size() > 3) max = atoi(listlocal[3].c_str());
    }
    else if(listlocal[0] == "r")
    {
        type = NSTRange;
        min = std::numeric_limits<int>::min();
        max = std::numeric_limits<int>::max();
        if(listlocal.size() > 1) iprefered = atoi(listlocal[1].c_str());
        if(listlocal.size() > 2) min = atoi(listlocal[2].c_str());
        if(listlocal.size() > 3) max = atoi(listlocal[3].c_str());
    }
    else if(listlocal[0] == "n")
    {
        type = NSTName;
        for(unsigned int i = 1; i < listlocal.size(); i++) list.push_back(listlocal[i]);
    }
    else if(listlocal[0] == "t")
    {
        type = NSTText;
        if(listlocal.size() > 1) tprefered = listlocal[1];
        for(unsigned int i = 2; i < listlocal.size(); i++) list.push_back(listlocal[i]);
    }
    else
    {
        type = NSTUnknown;
    }
}


/**
 * @brief NameStubTree::doesItMatch checks if the name or a part of a name matches the template tree. It ignores white spaces between name stubs.
 * @param name is a string to be matched/verified
 * @param pitem pointer to tree item to match the incomplete name stub. Should be null to match the name from the first stub
 * @param left pointer to pointer to the unmatched characters of the name string
 * @param depth maximum number of stubs to be matched
 * @return 0 - the full name matches, >0 incomplete name matches, <0 does not match -- name is invalid
 * @example
 *   NameStubTree tree;
 *   tree.load("./parameters.txt", '.');
 *   ParameterTreeItem<NameStub>* matched; int n;
 *   matched = NULL; n = tree.doesItMatch("BlaBla", &matched); //returns -1
 *   matched = NULL; n = tree.doesItMatch("MorSpol", &matched); //returns 0
 *   matched = NULL; n = tree.doesItMatch(" Mor  Spol ", &matched); //returns 0
 *   matched = NULL; n = tree.doesItMatch("Mor", &matched); //returns 1
 *   n = tree.doesItMatch("Spol", &matched); //returns 0
 */
int NameStubTree::doesItMatch(const char* name, ParameterTreeItem<NameStub>** pitem, char** left, int depth)
{
    char* nameleft = (char*) name;
    unsigned int length = strlen(nameleft);
    if(*pitem == NULL) *pitem = getRoot();
    int ret = 0;

    while(length > 0 && *pitem != NULL && ret < depth)
    {
        ret++;
        while(length > 0)
        {
            if(*nameleft == ' ' || *nameleft == '\t')
            {
                nameleft++;
                length--;
            }
            else break;
        }
        char* nextname = NULL;
        int parameter = strtol(nameleft, &nextname, 10);
        bool match = false;
        if(nextname != NULL && nextname != nameleft)
        {
            unsigned int cmax = (*pitem)->children.size();
            for(unsigned int c = 0; c < cmax && !match; c++)
            {
                ParameterTreeItem<NameStub>* citem = (*pitem)->children[c];
                if(citem->data.type == NSTInt ||
                   citem->data.type == NSTRange)
                {
                    if(parameter <= citem->data.max && parameter >= citem->data.min)
                    {
                        if(citem->children.size() == 0)
                        {
                            ret = 0;
                        }
                        match = true;
                        length -= (nextname-nameleft);
                        nameleft = nextname;
                        *pitem = citem;
                    }
                }
            }
        }
        else
        {
            //Text/combo parameter
            unsigned int cmax = (*pitem)->children.size();
            for(unsigned int c = 0; c < cmax && !match; c++)
            {
                ParameterTreeItem<NameStub>* citem = (*pitem)->children[c];
                for(unsigned int i = 0; i < citem->data.list.size() && !match; i++)
                {
                    if(citem->data.type == NSTText ||
                       citem->data.type == NSTName)
                    {
                        unsigned int l = citem->data.list[i].length();
                        if(citem->data.list[i].compare(0, length, nameleft, l) == 0)
                        {
                            if(citem->children.size() == 0)
                                ret = 0;
                            if(length < l) l = length;
                            nameleft += l;
                            length -= l;
                            match = true;
                            *pitem = citem;
                        }
                    }
                }
            }
        }
        if(!match)
        {
            if(left != NULL) *left = nameleft;
            return -ret;
        }
    }
    if(left != NULL) *left = nameleft;
    return ret;
}

/**
 * @brief NameStubTree::doesItMatch
 * @param name
 * @return
 */
int NameStubTree::doesItMatch(const char* name)
{
    ParameterTreeItem<NameStub>* pitem = NULL;
    doesItMatch(name, &pitem);
    return 0;
}


///**
// * @brief NameStubTree::isLeaf checks for leaf item -- item that has no children
// * @param pitem pointer to the item
// * @return true if leaf item, false otherwise
// */
//bool NameStubTree::isLeaf(ParameterTreeItem<NameStub>* pitem)
//{
//    if(pitem == NULL) return false;
//    return (pitem->children.size() == 0);
//}


/**
 * @brief NameStubTree::getOption gets the contained data in text string format
 * @param pitem pointer to the item
 * @return
 */
std::string NameStubTree::getOption(ParameterTreeItem<NameStub>* pitem)
{
    return pitem->data.toString();
}

/**
 * @brief NameStubTree::getChildrenOption
 * @param pitem
 * @param option
 */
std::vector<std::string> NameStubTree::getChildrenOption(const ParameterTreeItem<NameStub>* pitem)
{
    if(pitem == NULL) pitem = getRoot();
    unsigned int cmax = pitem->children.size();
    NameStub stub;
    stub.type = NSTUnknown;
    std::vector<std::string> option;
    for(unsigned int c = 0; c < cmax; c++)
    {
        ParameterTreeItem<NameStub>* citem = pitem->children[c];
        if(citem->data.type == NSTInt && (stub.type == NSTUnknown || stub.type == NSTInt))
        {
            if(stub.type == NSTUnknown)
            {
                stub.tip = citem->data.tip;
                stub.iprefered = citem->data.iprefered;
                stub.min = citem->data.min;
                stub.max = citem->data.max;
                stub.type = NSTInt;
            }
            else
            {
                stub.tip = "";
                if(stub.min > citem->data.min) stub.min = citem->data.min;
                if(stub.max < citem->data.max) stub.max = citem->data.max;
            }
        }
        else if(citem->data.type == NSTRange && (stub.type == NSTUnknown || stub.type == NSTRange))
        {
            if(stub.type == NSTUnknown)
            {
                stub.tip = citem->data.tip;
                stub.iprefered = citem->data.iprefered;
                stub.min = citem->data.min;
                stub.max = citem->data.max;
                stub.type = NSTRange;
            }
            else
            {
                stub.tip = "";
                if(stub.min > citem->data.min) stub.min = citem->data.min;
                if(stub.max < citem->data.max) stub.max = citem->data.max;
            }
        }
        else if(citem->data.type == NSTText && (stub.type == NSTUnknown || stub.type == NSTText))
        {
            if(stub.type == NSTUnknown)
            {
                stub.tip = citem->data.tip;
                stub.tprefered = citem->data.tprefered;
                for(unsigned int c = 0; c < citem->data.list.size(); c++)
                {
                    stub.list.push_back(citem->data.list[c]);
                }
                stub.type = NSTText;
            }
            else
            {
                stub.tip = "";
                for(unsigned int c = 0; c < citem->data.list.size(); c++)
                {
                    stub.list.push_back(citem->data.list[c]);
                }
            }
        }
        else if(citem->data.type == NSTName && (stub.type == NSTUnknown || stub.type == NSTName))
        {
            option.push_back(citem->data.toString());
            stub.type = NSTName;
        }
    }
    if(stub.type == NSTInt || stub.type == NSTText || stub.type == NSTRange)
    {
        option.push_back(stub.toString());
    }
    return option;
}
