/*
 * qMaZda - Image Analysis and Pattern Recognition
 * 
 * Copyright 2013-2019 Piotr M. Szczypiński <piotr.szczypinski@p.lodz.pl>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


#ifndef MZSELECTIONINTERFACE_H
#define MZSELECTIONINTERFACE_H


//#if defined(Q_OS_WIN) && !defined(Q_CC_GNU)
//typedef unsigned __int64 mzuint64;  /* 64 bit unsigned */
//#else
//typedef unsigned long long mzuint64; /* 64 bit unsigned */
//#endif

//#include <qglobal.h>
//#include <QString>
//#include <QMenu>
//#include <QObject>
//#include <QMessageBox>
#include <string>
#include <vector>
#include "dataforconsole.h"
//#include "../SharedImage/mazdadummy.h"

#if defined(WIN32) || defined(_WIN32) || defined(WIN64) || defined(_WIN64)
#    define MZ_DECL_EXPORT     __declspec(dllexport)
typedef unsigned __int64 mz_uint64;
#else
#    define MZ_DECL_EXPORT     __attribute__((visibility("default")))
typedef unsigned long long mz_uint64;
#endif

typedef void* (*MzNewPluginFunction)(void);
typedef void (*MzStepNotificationFunction)(void*);
typedef void (*MzTextNotificationFunction)(void*, std::string);
typedef bool (*MzBeforeStaticPluginFunction)(void*);
typedef void (*MzThreadStaticPluginFunction)(void*, void*, MzStepNotificationFunction, MzTextNotificationFunction);
typedef void (*MzAfterStaticPluginFunction)(void*);
typedef void (*MzOnActionFunction)(void);


//typedef void (*MzNotificationFunction)(unsigned int, std::string);

//class MzPullMapsInterface
//{
//public:
//    virtual bool getImages(MapsForSegmentation* maps) = 0;
//    virtual bool setImages(MapsForSegmentation* maps) = 0;
//};

class MzPullDataInterface
{
public:
    virtual bool getData(DataForSegmentation* data) = 0;
    virtual bool getData(std::vector<std::string>* featureNames, DataForSelection* data) = 0;
    virtual bool getData(DataForSelection* data) = 0;
    virtual bool setSelection(SelectedFeatures* selected_features) = 0;
    virtual bool setData(DataForSelection* data, SelectedFeatures* selected_features) = 0;
};

class MzSelectionPluginInterface;

class MzGuiRelatedInterface
{
public:
    virtual void InitiateAnalysisActions(void *plugin_object,
                                 MzBeforeStaticPluginFunction before_plugin,
                                 MzThreadStaticPluginFunction threaded_plugin,
                                 MzAfterStaticPluginFunction after_plugin) = 0;
//    virtual bool mzpconnect(const QObject *sender, const char *signal, const QObject *receiver, const char *member) = 0;
//    virtual void mzpconnectall(QObject *sender, QObject *receiver, const char *analysisSlot, const char *finishSlot,
//                                    quint64 maxSteps, int timerestriction, const char *title) = 0;
    virtual bool selectClassifiers(std::vector<std::string>* classifierNames, std::vector<bool>* selectionResult, std::string title) = 0;

    virtual bool openProgressDialog(int maxTime, mz_uint64 maxSteps, MzSelectionPluginInterface* object, std::string title) = 0;
//    virtual void notifyProgressDialog(void) = 0;
//    virtual void notifyProgressDialog(std::string message) = 0;
    virtual void closeProgressDialog(void) = 0;

    virtual bool openOptionsDialog(std::string title) = 0;
    virtual void addGroupOptionsDialog(std::string property, std::string tip) = 0;
    virtual void addPropertyOptionsDialog(std::string property, std::string value, std::string tip) = 0;
    virtual void getValueOptionsDialog(std::string property, std::string* value) = 0;
    virtual void closeOptionsDialog(void) = 0;

    virtual void showTestResults(std::string* confusionTable, std::string title) = 0;
    virtual void showAbout(std::string title, std::string text) = 0;
    virtual void showMessage(std::string title, std::string text, unsigned int iconType) = 0;
    virtual bool getOpenFile(std::string* filename) = 0;
    virtual bool getSaveFile(std::string* filename, unsigned int* filter) = 0;

    virtual void* addMenuAction(const char* name, const char* tip, const unsigned int index) = 0;
    virtual void menuEnable(void* menu, bool enable) = 0;
};

class MzSelectionPluginInterface
{
public:
    virtual ~MzSelectionPluginInterface(){}
    virtual const char* getName(void) = 0;
    virtual bool initiateTablePlugin(MzPullDataInterface *pull_data, MzGuiRelatedInterface *gui_tools) = 0;
    virtual bool initiateMapsPlugin(MzPullDataInterface *pull_data, MzGuiRelatedInterface *gui_tools) = 0;
//    virtual void onActionCall(void* action) = 0;
    virtual bool openFile(std::string* filename) = 0;
    virtual void cancelAnalysis(void) = 0;
    virtual void callBack(const unsigned int index, const bool use_multithreading) = 0;
};



//QT_BEGIN_NAMESPACE
////Q_DECLARE_INTERFACE(MzPullMapsInterface, "MzPullMapsInterface")
//Q_DECLARE_INTERFACE(MzPullDataInterface, "MzPullDataInterface")
//Q_DECLARE_INTERFACE(MzSelectionPluginInterface, "MzSelectionPluginInterface")
//Q_DECLARE_INTERFACE(MzGuiRelatedInterface, "MzGuiRelatedInterface")
//QT_END_NAMESPACE

#endif // MZSELECTIONINTERFACE_H
