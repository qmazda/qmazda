/*
 * qMaZda - Image Analysis and Pattern Recognition
 * 
 * Copyright 2013  Piotr M. Szczypiński <piotr.szczypinski@p.lodz.pl>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "addnoise.h"
#include "ui_addnoise.h"

AddNoise::AddNoise(int distribution, double snr, QWidget *parent) :
    QDialog(parent),
    ui(new Ui::AddNoise)
{
    this->distribution = distribution;
    this->snr = snr;
    ui->setupUi(this);
    ui->snrDoubleSpinBox->setValue(snr);
    ui->distributionComboBox->setCurrentIndex(distribution);
}

AddNoise::~AddNoise()
{
    delete ui;
}

void AddNoise::on_okPushButton_clicked()
{
    snr = ui->snrDoubleSpinBox->value();
    distribution = ui->distributionComboBox->currentIndex();
    done(QDialog::Accepted);
}

void AddNoise::on_cancelPushButton_clicked()
{
    done(QDialog::Rejected);
}
