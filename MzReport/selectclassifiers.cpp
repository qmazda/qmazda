#include "selectclassifiers.h"
#include "ui_selectclassifiers.h"
#include "../MzShared/qt5qt6compatibility.h"

SelectClassifiers::SelectClassifiers(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::SelectClassifiers)
{
    ui->setupUi(this);
    use = NULL;
}

SelectClassifiers::~SelectClassifiers()
{
    delete ui;
}

void SelectClassifiers::setOptions(std::vector<std::string>* classifiers, std::vector<bool>* use)
{
   this->use = use;
   unsigned int maxc = classifiers->size();
   unsigned int maxu = use->size();
   for(unsigned int i = 0; i < maxc; i++)
   {
       ui->classifierListWidget->addItem(QString::fromStdString((*classifiers)[i]));
   }
   for(unsigned int i = 0; i < maxc && i < maxu; i++)
   {
       if(use == NULL) ui->classifierListWidget->item(i)->setCheckState(Qt::Checked);
       else if((*use)[i]) ui->classifierListWidget->item(i)->setCheckState(Qt::Checked);
       else ui->classifierListWidget->item(i)->setCheckState(Qt::Unchecked);
   }
}

void SelectClassifiers::on_checkPushButton_clicked()
{
    _regularexpression_from_wildecard(ui->wildLineEdit->text());

    int rr = ui->classifierListWidget->count();
    for(int r = 0; r < rr; r++)
    {
        _if_rx_match(ui->classifierListWidget->item(r)->text())
        {
            ui->classifierListWidget->item(r)->setCheckState(Qt::Checked);
        };
    }
}

void SelectClassifiers::on_uncheckPushButton_clicked()
{
    _regularexpression_from_wildecard(ui->wildLineEdit->text());

    int rr = ui->classifierListWidget->count();
    for(int r = 0; r < rr; r++)
    {
        _if_rx_match(ui->classifierListWidget->item(r)->text())
        {
            ui->classifierListWidget->item(r)->setCheckState(Qt::Unchecked);
        };
    }
}

void SelectClassifiers::on_buttonBox_accepted()
{
    if(use != NULL)
    {
        use->clear();
        int rr = ui->classifierListWidget->count();
        for(int r = 0; r < rr; r++)
        {

            if(ui->classifierListWidget->item(r)->checkState() == Qt::Checked)
                use->push_back(true);
            else
                use->push_back(false);
        }
    }
    done(QDialog::Accepted);
}

void SelectClassifiers::on_buttonBox_rejected()
{
    done(QDialog::Rejected);
}
