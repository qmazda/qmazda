#ifndef SELECTCLASSIFIERS_H
#define SELECTCLASSIFIERS_H

#include <QDialog>
#include <QStringList>
#include "../MzShared/dataforconsole.h"

namespace Ui {
class SelectClassifiers;
}

class SelectClassifiers : public QDialog
{
    Q_OBJECT

public:
    explicit SelectClassifiers(QWidget *parent = 0);
    ~SelectClassifiers();
    void setOptions(std::vector<std::string>* classifiers, std::vector<bool>* use);

private slots:
    void on_checkPushButton_clicked();
    void on_uncheckPushButton_clicked();
    void on_buttonBox_accepted();
    void on_buttonBox_rejected();

private:
    Ui::SelectClassifiers *ui;
    std::vector<bool>* use;
};

#endif // SELECTCLASSIFIERS_H
