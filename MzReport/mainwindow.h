/*
 * qMaZda - Image Analysis and Pattern Recognition
 * 
 * Copyright 2013  Piotr M. Szczypiński <piotr.szczypinski@p.lodz.pl>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef MAINWINDOW_H
#define MAINWINDOW_H


#include <QMainWindow>
#include <QLineEdit>
#include <QList>
#include <QDropEvent>
#include <QMimeData>
#include <QAction>

#include "../MzShared/mzselectioninterface.h"

#include "mztablemodel.h"
#include "../MzShared/progress.h"
#include "../MzShared/propertyvaluetree.h"

namespace Ui {
class MainWindow;
}

#include "../MzShared/PlugWorker.h"

class MzAction : public QAction
{
    Q_OBJECT
public:
    MzAction(const QString &text, QObject *parent) :
        QAction(text, parent) {}

    MzSelectionPluginInterface* interfaceMzSelectionPluginInterface;
    unsigned int indexToOnActionCall;
};

class MainWindow : public QMainWindow, MzPullDataInterface, MzGuiRelatedInterface
{
    Q_OBJECT
    //Q_INTERFACES(MzPullDataInterface)
    bool getData(DataForSegmentation* data);
    bool getData(std::vector<std::string>* featureNames, DataForSelection* data);
    bool getData(DataForSelection* data);
    bool setSelection(SelectedFeatures* selected_features);
    bool setData(DataForSelection* data, SelectedFeatures* selected_features);

    //Q_INTERFACES(MzGuiRelatedInterface)
    void InitiateAnalysisActions(void *plugin_object,
                                 MzBeforeStaticPluginFunction before_plugin,
                                 MzThreadStaticPluginFunction threaded_plugin,
                                 MzAfterStaticPluginFunction after_plugin);

//    bool mzpconnect(const QObject *sender, const char *signal, const QObject *receiver, const char *member);
//    void mzpconnectall(QObject *sender, QObject *receiver, const char *analysisSlot, const char *finishSlot,
//                                    quint64 maxSteps, int timerestriction, const char *title );
    bool selectClassifiers(std::vector<std::string>* classifierNames, std::vector<bool>* selectionResult, string title);
    bool openProgressDialog(int maxTime, mz_uint64 maxSteps, MzSelectionPluginInterface *object, string title);
//    void notifyProgressDialog(void);
//    void notifyProgressDialog(std::string message);
    void closeProgressDialog(void);

    bool openOptionsDialog(std::string title);
    void addGroupOptionsDialog(std::string property, std::string tip);
    void addPropertyOptionsDialog(std::string property, std::string value, std::string tip);
    void getValueOptionsDialog(std::string property, std::string* value);
    void closeOptionsDialog(void);

    void showTestResults(std::string* confusionTable, string title);
    void showAbout(std::string title, std::string text);
    void showMessage(std::string title, std::string text, unsigned int iconType);
    bool getOpenFile(std::string* fileName);
    bool getSaveFile(std::string* fileName, unsigned int* filter);
    void* addMenuAction(const char *name, const char *tip, const unsigned int index);
    void menuEnable(void* menu, bool enable);
    
public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

    bool setDataHere(DataForSelection* data);

protected:
    void dragEnterEvent(QDragEnterEvent* event);
    void dragMoveEvent(QDragMoveEvent* event);
    void dragLeaveEvent(QDragLeaveEvent* event);
    void dropEvent(QDropEvent* event);

private slots:
    void headerDoubleClicked(int index);
    void verticalDoubleClicked(int index);
    void temporaryEditingFinished(void);

    void on_actionScatter_plot_triggered();

    void on_actionOpen_report_triggered();

    void on_actionSelect_all_triggered();

    void on_actionDeselect_all_triggered();

    void on_actionInvert_selection_triggered();

    void on_actionSelect_all_vectors_triggered();

    void on_actionDeselect_all_vectors_triggered();

    void on_actionInvert_selection_vectors_triggered();

    void on_actionReneame_classes_triggered();

    void on_actionRandom_selection_triggered();

    void on_actionCopy_selected_triggered();

    void on_actionHighlight_all_triggered();

    void on_actionExit_triggered();

    void on_actionSave_triggered();

    void on_actionSave_selected_triggered();

    void on_actionNew_window_triggered();

    void on_actionVectorsFromFolds_triggered();

    void on_actionNames_from_file_triggered();

//    void on_actionUse_checked_toggled(bool arg1);

//    void on_actionUse_unchecked_toggled(bool arg1);

//    void on_actionAppend_columns_triggered();

    void on_actionAppend_rows_triggered();

    void on_actionAdd_noise_triggered();

    void on_actionContents_triggered();

    void on_actionAbout_triggered();

    void on_actionClose_report_triggered();

    void on_actionNames_from_clipboard_triggered();

    void on_actionCopy_checked_names_triggered();
    void on_finished(bool success);
    void notifyProgressSlot();
    void notifyProgressSlot(QString text);
    void on_actionUncheck_Nan_Row_triggered();
    void on_actionUncheck_Nan_Col_triggered();

    void pluginMenuTriggered();

    void on_actionSingle_thread_toggled(bool);

private:
    //static bool plugins_loaded;
    Progress* progressdialog;
    PropertyValueTree* optionsdialog;
    MzTableModel* previous_model;
    int edited_section;
    Ui::MainWindow *ui;
    QLineEdit* temporary_edit;
    void loadPlugins(void);
    bool initiatePlugins(void);
    bool openReport(QString fileName);
    bool appendReport(QString fileName);
    QString openFileName;

    MzSelectionPluginInterface* selectionplugin;
    QMenu* plugin_menu;
    QList<MzSelectionPluginInterface*> selectionplugins;
    void* plugin_object;
    MzAfterStaticPluginFunction after_plugin;
//    QString getExecutableWithPath(const char* name);
};

#endif // MAINWINDOW_H
