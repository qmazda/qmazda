/*
 * qMaZda - Image Analysis and Pattern Recognition
 * 
 * Copyright 2013  Piotr M. Szczypiński <piotr.szczypinski@p.lodz.pl>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "plot3dwidget.h"
#define _USE_MATH_DEFINES
#include <math.h>
#include <stdio.h>
#include "../MzShared/qt5qt6compatibility.h"


double Macierz::podwyznacznik(double M[2][2], int w, int k)
{
    int w0;
    int k0;

    w0=(w+1)%2;
    k0=(k+1)%2;
    if((w+k)%2) return -M[w0][k0];
    return M[w0][k0];
}

/*------------------------------------------------------------------------*/
double Macierz::podwyznacznik(double M[3][3], int w, int k)
{
    int w0, w1;
    int k0, k1;

    w0=0; if(w0==w) w0++;
    w1=w0+1; if(w1==w) w1++;
    k0=0; if(k0==k) k0++;
    k1=k0+1; if(k1==k) k1++;

    double r=
        M[w0][k0]*M[w1][k1]-M[w0][k1]*M[w1][k0];

    if((w+k)%2) return -r;
    return r;
}

/*------------------------------------------------------------------------*/
double Macierz::podwyznacznik(double M[4][4], int w, int k)
{
    int w0, w1, w2;
    int k0, k1, k2;

    w0=0; if(w0==w) w0++;
    w1=w0+1; if(w1==w) w1++;
    w2=w1+1; if(w2==w) w2++;
    k0=0; if(k0==k) k0++;
    k1=k0+1; if(k1==k) k1++;
    k2=k1+1; if(k2==k) k2++;

    double r=
        M[w0][k0]*(M[w1][k1]*M[w2][k2]-M[w1][k2]*M[w2][k1])
        -M[w0][k1]*(M[w1][k0]*M[w2][k2]-M[w1][k2]*M[w2][k0])
        +M[w0][k2]*(M[w1][k0]*M[w2][k1]-M[w1][k1]*M[w2][k0]);

    if((w+k)%2) return -r;
    return r;
}

/*------------------------------------------------------------------------*/
void Macierz::transpozycja(double MM[4][4], double M[4][4])
{
    for(int k=1; k<4; k++)
        for(int w=0; w<k; w++)
        {
            MM[w][k]=M[k][w];
            MM[k][w]=M[w][k];
        }
    for(int w=0; w<4; w++)
        MM[w][w]=M[w][w];
}

/*------------------------------------------------------------------------*/
void Macierz::transpozycja(double MM[4][4])
{
    for(int k=1; k<4; k++)
        for(int w=0; w<k; w++)
        {
            double d =MM[w][k];
            MM[w][k]=MM[k][w];
            MM[k][w]=d;
        }
}
/*------------------------------------------------------------------------*/
void Macierz::transpozycja(double MM[3][3], double M[3][3])
{
    for(int k=1; k<3; k++)
        for(int w=0; w<k; w++)
        {
            MM[w][k]=M[k][w];
            MM[k][w]=M[w][k];
        }
    for(int w=0; w<3; w++)
        MM[w][w]=M[w][w];
}

/*------------------------------------------------------------------------*/
void Macierz::transpozycja(double MM[3][3])
{
    for(int k=1; k<3; k++)
        for(int w=0; w<k; w++)
        {
            double d =MM[w][k];
            MM[w][k]=MM[k][w];
            MM[k][w]=d;
        }
}

/*------------------------------------------------------------------------*/
void Macierz::transpozycja(double MM[2][2], double M[2][2])
{
    MM[0][1]=M[1][0];
    MM[1][0]=M[0][1];
    MM[0][0]=M[0][0];
    MM[1][1]=M[1][1];
}

/*------------------------------------------------------------------------*/
void Macierz::transpozycja(double MM[2][2])
{
    double d =MM[0][1];
    MM[0][1]=MM[1][0];
    MM[1][0]=d;
}


/*------------------------------------------------------------------------*/
double Macierz::wyznacznik(double M[4][4])
{
    return
        M[0][0]*
        (
        M[1][1]*(M[2][2]*M[3][3]-M[2][3]*M[3][2])
        -M[1][2]*(M[2][1]*M[3][3]-M[2][3]*M[3][1])
        +M[1][3]*(M[2][1]*M[3][2]-M[2][2]*M[3][1])
        )
        -M[0][1]*
        (
        M[1][0]*(M[2][2]*M[3][3]-M[2][3]*M[3][2])
        -M[1][2]*(M[2][0]*M[3][3]-M[2][3]*M[3][0])
        +M[1][3]*(M[2][0]*M[3][2]-M[2][2]*M[3][0])
        )
        +M[0][2]*
        (
        M[1][0]*(M[2][1]*M[3][3]-M[2][3]*M[3][1])
        -M[1][1]*(M[2][0]*M[3][3]-M[2][3]*M[3][0])
        +M[1][3]*(M[2][0]*M[3][1]-M[2][1]*M[3][0])
        )
        -M[0][3]*
        (
        M[1][0]*(M[2][1]*M[3][2]-M[2][2]*M[3][1])
        -M[1][1]*(M[2][0]*M[3][2]-M[2][2]*M[3][0])
        +M[1][2]*(M[2][0]*M[3][1]-M[2][1]*M[3][0])
        );
}

/*------------------------------------------------------------------------*/
double Macierz::wyznacznik(double M[3][3])
{
    return
        M[0][0]*(M[1][1]*M[2][2]-M[1][2]*M[2][1])
        -M[0][1]*(M[1][0]*M[2][2]-M[1][2]*M[2][0])
        +M[0][2]*(M[1][0]*M[2][1]-M[1][1]*M[2][0]);
}

/*------------------------------------------------------------------------*/
double Macierz::wyznacznik(double M[2][2])
{
    return
        M[0][0]*M[1][1]-M[0][1]*M[1][0];
}

/*------------------------------------------------------------------------*/
double Macierz::inwersja(double MM[4][4], double M[4][4])
{
    double det = wyznacznik(M);
    if(det==0.0) return det;

    for(int k=0; k<4; k++)
        for(int w=0; w<4; w++)
        {
            MM[w][k] = podwyznacznik(M, w, k)/det;
        }
    transpozycja(MM);
    return det;
}

/*------------------------------------------------------------------------*/
double Macierz::inwersja(double MM[3][3], double M[3][3])
{
    double det = wyznacznik(M);
    if(det==0.0) return det;

    for(int k=0; k<3; k++)
        for(int w=0; w<3; w++)
        {
            MM[w][k] = podwyznacznik(M, w, k)/det;
        }
    transpozycja(MM);
    return det;
}

/*------------------------------------------------------------------------*/
bool Macierz::inwersja(double MM[2][2], double M[2][2])
{
    double det = wyznacznik(M);
    if(fabs(det)<PRAWIE_ZERO) return false;
    for(int k=0; k<2; k++)
        for(int w=0; w<2; w++)
        {
            MM[w][k] = podwyznacznik(M, w, k)/det;
        }
    transpozycja(MM);
    return true;
}

//---------------------------------------------------------------------------
Plot3dWidget::Plot3dWidget(QWidget *parent) :
    QWidget(parent)
{
    W[0][0] = 1; W[0][1] = 0; W[0][2] = 0;
    W[1][0] = 0; W[1][1] = 1; W[1][2] = 0;
    W[2][0] = 0; W[2][1] = 0; W[2][2] = 1;

    //inwersja(WI, W);
    transpozycja(WI, W);

    dataforselection = NULL;
    sorted = NULL;
    classmembership = NULL;
    colors = NULL;

    plottimer = new QTimer(this);
    connect(plottimer, SIGNAL(timeout()), this, SLOT(animatePlot()));
    plottimer->setInterval(62);
}

//---------------------------------------------------------------------------
Plot3dWidget::~Plot3dWidget()
{
    if(plottimer != NULL) plottimer->stop();
    delete plottimer;
    if(sorted != NULL) delete[] sorted;
    if(classmembership != NULL) delete[] classmembership;

}

//---------------------------------------------------------------------------

void Plot3dWidget::Rotate(int rot, int l1, int l2)
{
  double a, b;
  double sina = sin((double)rot/100);
  double cosa = cos((double)rot/100);

    a = cosa * W[0][l1] + sina * W[0][l2];
    b = -sina * W[0][l1] + cosa * W[0][l2];
    W[0][l1] = a;
    W[0][l2] = b;
    a = cosa * W[1][l1] + sina * W[1][l2];
    b = -sina * W[1][l1] + cosa * W[1][l2];
    W[1][l1] = a;
    W[1][l2] = b;
    a = cosa * W[2][l1] + sina * W[2][l2];
    b = -sina * W[2][l1] + cosa * W[2][l2];
    W[2][l1] = a;
    W[2][l2] = b;
  inwersja(WI, W);
}

//---------------------------------------------------------------------------
void Plot3dWidget::Project(double i[3], double o[2])
{
  o[0] = WI[0][0]*i[0] + WI[0][1]*i[1] + WI[0][2]*i[2];
  o[1] = WI[1][0]*i[0] + WI[1][1]*i[1] + WI[1][2]*i[2];
}

//---------------------------------------------------------------------------
bool Plot3dWidget::setWidgetData(DataForSelection* dataforselection_in, int tmpw[3], QString _symbols, QColor*_colors)
{
    if(dataforselection_in == NULL) return false;
    dataforselection = dataforselection_in;
    featurestoshow[0] = tmpw[0];
    featurestoshow[1] = tmpw[1];
    featurestoshow[2] = tmpw[2];
    symbols = _symbols;
    colors = _colors;
    if(sorted != NULL) delete[] sorted;

    sorted = new int[3*dataforselection->vectornumber];
    classmembership = new int[3*dataforselection->vectornumber];
    sortFeatures();

    QFontMetrics fm(hfont);
    font_height = fm.height();
    update();

    return true;
}



/*
bool Plot3dWidget::setWidgetData(int _vectornumber,
                    int _classnumber,
                    int _featurenumber,
                    int *_vectorclassindex,
                    QString *_featurenames,
                    double *_values,
                    QString _symbols,
                    QColor* _colors)
{
    vectornumber = 0;
    classnumber = 0;
    featurenumber = 0;

    if(vectorclassindex != NULL) delete[] vectorclassindex;
    if(featurenames != NULL) delete[] featurenames;
    if(values != NULL) delete[] values;
    if(sorted != NULL) delete[] sorted;

    vectornumber = _vectornumber;
    classnumber = _classnumber;
    featurenumber = _featurenumber;
    vectorclassindex = _vectorclassindex;
    featurenames = _featurenames;
    values = _values;
    symbols = _symbols;
    colors = _colors;

    sorted = new int[featurenumber*vectornumber];
    sortFeatures();

    QFontMetrics fm(hfont);
    font_height = fm.height();
    update();

    return true;
}
*/
//---------------------------------------------------------------------------
void Plot3dWidget::setSymbols(QString _symbols)
{
    symbols = _symbols;
    update();
}

void Plot3dWidget::setColor(QColor color, int index)
{
    if(index >= 0 && index < dataforselection->classnumber) colors[index] = color;
    update();
}

//---------------------------------------------------------------------------
void Plot3dWidget::sortFeatures(void)
{
    int k1, k2;
    bool* checked = new bool[dataforselection->vectornumber];
    for(int f = 0; f < 3; f++)
    {
        for(k1 = 0; k1 < dataforselection->vectornumber; k1++) checked[k1] = false;
        for(k1 = 0; k1 < dataforselection->vectornumber; k1++)
        {
            bool first = true;
            double min = 0;
            int minindex = 0;
            for(k2 = 0; k2 < dataforselection->vectornumber; k2++)
            {
                if(!checked[k2])
                    if(dataforselection->values[featurestoshow[f]+k2*dataforselection->featurenumber] < min || first)
                {
                    min = dataforselection->values[featurestoshow[f]+k2*dataforselection->featurenumber];
                    minindex = k2;
                    first = false;
                }
            }
            checked[minindex] = true;
            sorted[f+k1*3] = minindex; //f+minindex*featurenumber;
        }
    }
    delete[] checked;

    int ss = 0;
    for(int cc = 0; cc < dataforselection->classnumber; cc++)
    {
        for(; ss < dataforselection->classendvectorindex[cc]; ss++)
        {
            classmembership[ss] = cc;
        }
    }

    for(int k = 0; k < 3; k++)
    {
        int sorted_ = sorted[k + (dataforselection->vectornumber-1)*3];
        int sortedk_ = sorted[k];
        int featurests_ = featurestoshow[k];
        int ii1 = featurests_ + sorted_*dataforselection->featurenumber;
        int ii2 = featurests_ + sortedk_*dataforselection->featurenumber;
        mean[k] = (dataforselection->values[ii1] + dataforselection->values[ii2])/2.0;
        span[k] = (dataforselection->values[ii1] - dataforselection->values[ii2])/2.0;
        if(span[k]>0.0000000001) span[k] = 1.0/span[k];
        else span[k] = 0;
    }
/*
    mean[1] = (values[values[featurestoshow[1] + sorted[1 + (vectornumber-1)*featurenumber]*featurenumber] + values[1+sorted[1]*featurenumber])/2.0;
    span[1] = (values[values[featurestoshow[1] + sorted[1 + (vectornumber-1)*featurenumber]*featurenumber] - values[1+sorted[1]*featurenumber])/2.0;
    if(span[1]>0.0000000001) span[1] = 1.0/span[1];
    else span[1] = 0;
    mean[2] = (values[values[featurestoshow[2] + sorted[2 + (vectornumber-1)*featurenumber]*featurenumber] + values[2+sorted[2]*featurenumber])/2.0;
    span[2] = (values[values[featurestoshow[2] + sorted[2 + (vectornumber-1)*featurenumber]*featurenumber] - values[2+sorted[2]*featurenumber])/2.0;
    if(span[2]>0.0000000001) span[2]= 1.0/span[2];
    else span[2] = 0;
*/
    //??? FIXME usunac wydruk
/*
    printf("=========Sorted \n");
    for(int f = 0; f < 3; f++)
    {
        for(k1 = 0; k1 < dataforselection->vectornumber; k1++)
        {
            printf("%f ", (float)(dataforselection->values[featurestoshow[f]+k1*dataforselection->featurenumber]));
        }
        printf("\n");
    }
    printf("\n");
    for(int f = 0; f < 3; f++)
    {
        for(k1 = 0; k1 < dataforselection->vectornumber; k1++)
        {
            printf("%i ", (sorted[f+k1*3]));
        }
        printf("\n");
    }
    printf("\n");
    for(int f = 0; f < 3; f++)
    {
        for(k1 = 0; k1 < dataforselection->vectornumber; k1++)
        {
            printf("%f ", (float)(dataforselection->values[featurestoshow[f] + sorted[f+k1*3]*dataforselection->featurenumber]));
        }
        printf("\n");
    }
    fflush(stdout);
*/
}


//---------------------------------------------------------------------------
void Plot3dWidget::selectFont(QFont font)
{
    hfont = font;
    QFontMetrics fm(hfont);
    font_height = fm.height();
    update();
}

void Plot3dWidget::animatePlot(void)
{
    Rotate(1, 0, 2);
    Rotate(1, 1, 2);
    Rotate(2, 0, 1);
    update();
}

void Plot3dWidget::toggleAnimate(bool checked)
{
    if(checked)
        plottimer->start();
    else
        plottimer->stop();

    animatePlot();
}

//---------------------------------------------------------------------------
void Plot3dWidget::Draw3DPlot(QPainter* hdc, QRect viewport, int color_set)
{
    if(dataforselection == NULL) return;

    double o1[2], i1[3];
    double o2[2], i2[3];
    double colabels[3][2][2];

    int shiftx = viewport.width()/2;//Image1->Width/2;
    int shifty = viewport.height()/2;//Image1->Height/2;
    int mainspan = 100;

    QColor color_front;
    QColor color_shade;

    switch(color_set)
    {
    case 1:
        color_front = Qt::black;
        color_shade = Qt::gray;
        break;
    default:
        color_front = this->palette().color(QPalette::WindowText);
        color_shade = this->palette().color(QPalette::Window);
        color_shade.setRed((3*color_shade.red()+color_front.red())/4);
        color_shade.setGreen((3*color_shade.green()+color_front.green())/4);
        color_shade.setBlue((3*color_shade.blue()+color_front.blue())/4);
    }

    mainspan = shiftx > shifty ? shifty : shiftx;
    mainspan /= 1.8;

    int xx, yy, zz;
    if(WI[2][0]<0) xx = -1;
    else xx = 1;
    if(WI[2][1]<0) yy = -1;
    else yy = 1;
    if(WI[2][2]<0) zz = -1;
    else zz = 1;

    int lista;
    int kierunek;
    if(fabs(WI[2][0])>fabs(WI[2][1]) && fabs(WI[2][0])>fabs(WI[2][2]))
    {
        lista = 0;
        if(WI[2][0]>0) kierunek = 1;
        else kierunek = -1;
    }
    else if(fabs(WI[2][1])>fabs(WI[2][0]))
    {
        lista = 1;
        if(WI[2][1]>0) kierunek = 1;
        else kierunek = -1;
    }
    else
    {
        lista = 2;
        if(WI[2][2]>0) kierunek = 1;
        else kierunek = -1;
    }

    //hdc->fillRect(viewport, Qt::white);

    //QPen hpen(Qt::gray);
    QPen hpen(color_shade);
    hpen.setWidth(1);
    hpen.setStyle(Qt::DashLine);
    hdc->setPen(hpen);
    i1[0] = -mainspan*xx; i1[1] = -mainspan*yy; i1[2] = -mainspan*zz;
    i2[0] = mainspan*xx; i2[1] = -mainspan*yy; i2[2] = -mainspan*zz;
    Project(i1, o1);
    Project(i2, o2);
    hdc->drawLine(shiftx+o1[0], shifty+o1[1], shiftx+o2[0], shifty+o2[1]);
    i1[0] = -mainspan*xx; i1[1] = -mainspan*yy; i1[2] = -mainspan*zz;
    i2[0] = -mainspan*xx; i2[1] = mainspan*yy; i2[2] = -mainspan*zz;
    Project(i1, o1);
    Project(i2, o2);
    hdc->drawLine(shiftx+o1[0], shifty+o1[1], shiftx+o2[0], shifty+o2[1]);
    i1[0] = -mainspan*xx; i1[1] = -mainspan*yy; i1[2] = -mainspan*zz;
    i2[0] = -mainspan*xx; i2[1] = -mainspan*yy; i2[2] = mainspan*zz;
    Project(i1, o1);
    Project(i2, o2);
    hdc->drawLine(shiftx+o1[0], shifty+o1[1], shiftx+o2[0], shifty+o2[1]);
    //int shiftxt = shiftx - sfont_size;
    //int shiftyt = shifty - sfont_size;


    //QFont hfont;
    hdc->setFont(hfont);


    for(int ss = 0; ss < dataforselection->vectornumber; ss++)
    //for(int ss = 0; ss < dataforselection->classendvectorindex[cc]; ss++)
    {
        int s;
        if(kierunek>0) s = sorted[lista+ss*3];
        else s = sorted[lista+(dataforselection->vectornumber-ss-1)*3];

        int cms = classmembership[s];
        QChar symb;

        if(symbols.size() <= cms) symb = QChar(' ');
        else symb = symbols.at(cms);
        QPen hpen(colors[cms]);
        hdc->setPen(hpen);

        for(int f = 0; f < 3; f++)
        {
            i1[f] = mainspan*span[f]*(dataforselection->values[s*dataforselection->featurenumber + featurestoshow[f]]-mean[f]);
        }
/*
        i1[0] = mainspan*span[0]*(dataforselection->values[s*dataforselection->featurenumber + featurestoshow[0]]-mean[0]);
        i1[1] = mainspan*span[1]*(dataforselection->values[s*dataforselection->featurenumber + featurestoshow[1]]-mean[1]);
        i1[2] = mainspan*span[2]*(dataforselection->values[s*dataforselection->featurenumber + featurestoshow[2]]-mean[2]);
*/

        Project(i1, o1);
        QRectF bound;
        bound.setRect(shiftx+o1[0]-font_height, shifty+o1[1]-font_height, font_height*2, font_height*2);
        hdc->drawText(bound, Qt::AlignCenter, symb);
    }
    //hpen.setColor(Qt::black);
    hpen.setColor(color_front);
    hpen.setWidth(1);
    hpen.setStyle(Qt::SolidLine);
    hdc->setPen(hpen);

/*
  i1[0] = mainspan*xx; i1[1] = mainspan*yy; i1[2] = mainspan*zz;
  i2[0] = mainspan*xx; i2[1] = -mainspan*yy; i2[2] = mainspan*zz;
    Project(i1, o1);
    Project(i2, o2);
    hdc->drawLine(shiftx+o1[0], shifty+o1[1], shiftx+o2[0], shifty+o2[1]);
  i1[0] = mainspan*xx; i1[1] = mainspan*yy; i1[2] = mainspan*zz;
  i2[0] = -mainspan*xx; i2[1] = mainspan*yy; i2[2] = mainspan*zz;
    Project(i1, o1);
    Project(i2, o2);
    hdc->drawLine(shiftx+o1[0], shifty+o1[1], shiftx+o2[0], shifty+o2[1]);
  i1[0] = mainspan*xx; i1[1] = mainspan*yy; i1[2] = mainspan*zz;
  i2[0] = mainspan*xx; i2[1] = mainspan*yy; i2[2] = -mainspan*zz;
    Project(i1, o1);
    Project(i2, o2);
    hdc->drawLine(shiftx+o1[0], shifty+o1[1], shiftx+o2[0], shifty+o2[1]);
*/

    i1[0] = -mainspan*xx; i1[1] = mainspan*yy; i1[2] = -mainspan*zz;
    i2[0] = mainspan*xx; i2[1] = mainspan*yy; i2[2] = -mainspan*zz;
    Project(i1, o1);
    Project(i2, o2);
    hdc->drawLine(shiftx+o1[0], shifty+o1[1], shiftx+o2[0], shifty+o2[1]);
    colabels[0][0][0] = o1[0];
    colabels[0][0][1] = o1[1];
    colabels[0][1][0] = o2[0];
    colabels[0][1][1] = o2[1];
    i1[0] = -mainspan*xx; i1[1] = -mainspan*yy; i1[2] = mainspan*zz;
    i2[0] = mainspan*xx; i2[1] = -mainspan*yy; i2[2] = mainspan*zz;
    Project(i1, o1);
    Project(i2, o2);
    hdc->drawLine(shiftx+o1[0], shifty+o1[1], shiftx+o2[0], shifty+o2[1]);
    if((o2[0]-o1[0])*((o2[0]-o1[0])*o2[1]-(o2[1]-o1[1])*o1[0])>=0)
    {
        colabels[0][0][0] = o1[0];
        colabels[0][0][1] = o1[1];
        colabels[0][1][0] = o2[0];
        colabels[0][1][1] = o2[1];
    }
    i1[0] = mainspan*xx; i1[1] = -mainspan*yy; i1[2] = -mainspan*zz;
    i2[0] = mainspan*xx; i2[1] = mainspan*yy; i2[2] = -mainspan*zz;
    Project(i1, o1);
    Project(i2, o2);
    hdc->drawLine(shiftx+o1[0], shifty+o1[1], shiftx+o2[0], shifty+o2[1]);
    colabels[1][0][0] = o1[0];
    colabels[1][0][1] = o1[1];
    colabels[1][1][0] = o2[0];
    colabels[1][1][1] = o2[1];
    i1[0] = -mainspan*xx; i1[1] = -mainspan*yy; i1[2] = mainspan*zz;
    i2[0] = -mainspan*xx; i2[1] = mainspan*yy; i2[2] = mainspan*zz;
    Project(i1, o1);
    Project(i2, o2);
    hdc->drawLine(shiftx+o1[0], shifty+o1[1], shiftx+o2[0], shifty+o2[1]);
    if((o2[0]-o1[0])*((o2[0]-o1[0])*o2[1]-(o2[1]-o1[1])*o1[0])>=0)
    {
        colabels[1][0][0] = o1[0];
        colabels[1][0][1] = o1[1];
        colabels[1][1][0] = o2[0];
        colabels[1][1][1] = o2[1];
    }
    i1[0] = mainspan*xx; i1[1] = -mainspan*yy; i1[2] = -mainspan*zz;
    i2[0] = mainspan*xx; i2[1] = -mainspan*yy; i2[2] = mainspan*zz;
    Project(i1, o1);
    Project(i2, o2);
    hdc->drawLine(shiftx+o1[0], shifty+o1[1], shiftx+o2[0], shifty+o2[1]);
    colabels[2][0][0] = o1[0];
    colabels[2][0][1] = o1[1];
    colabels[2][1][0] = o2[0];
    colabels[2][1][1] = o2[1];
    i1[0] = -mainspan*xx; i1[1] = mainspan*yy; i1[2] = -mainspan*zz;
    i2[0] = -mainspan*xx; i2[1] = mainspan*yy; i2[2] = mainspan*zz;
    Project(i1, o1);
    Project(i2, o2);
    hdc->drawLine(shiftx+o1[0], shifty+o1[1], shiftx+o2[0], shifty+o2[1]);
    if((o2[0]-o1[0])*((o2[0]-o1[0])*o2[1]-(o2[1]-o1[1])*o1[0])>=0)
    {
        colabels[2][0][0] = o1[0];
        colabels[2][0][1] = o1[1];
        colabels[2][1][0] = o2[0];
        colabels[2][1][1] = o2[1];
    }

    for(int f = 0; f < 3; f++)
    {
        double xx = (colabels[f][1][0] - colabels[f][0][0]);
        double yy = (colabels[f][1][1] - colabels[f][0][1]);
        double ll = sqrt(xx*xx + yy*yy);

        if(ll > 5*font_height)
        {

            if(colabels[f][0][0] > colabels[f][1][0])
            {
                double t = colabels[f][1][0];
                colabels[f][1][0] = colabels[f][0][0];
                colabels[f][0][0] = t;
                t = colabels[f][1][1];
                colabels[f][1][1] = colabels[f][0][1];
                colabels[f][0][1] = t;
            }


            double angle = atan2(colabels[f][1][1]-colabels[f][0][1],
                    colabels[f][1][0]-colabels[f][0][0]);
            angle*=180; angle/=M_PI;
            //if(angle > 180.0) angle -= 360.0;
            //if(angle < -180.0) angle += 360.0;

            char str[2][64];
            if(WI[0][f]<0)
            {
                sprintf(str[0], "%+.4g",
                        dataforselection->values[featurestoshow[f] + sorted[f + (dataforselection->vectornumber-1)*3]*dataforselection->featurenumber]);
                sprintf(str[1], "%+.4g",
                        dataforselection->values[featurestoshow[f] + sorted[f]*dataforselection->featurenumber]);
            }
            else
            {
                sprintf(str[1], "%+.4g",
                        dataforselection->values[featurestoshow[f] + sorted[f + (dataforselection->vectornumber-1)*3]*dataforselection->featurenumber]);
                sprintf(str[0], "%+.4g",
                        dataforselection->values[featurestoshow[f] + sorted[f]*dataforselection->featurenumber]);
            }

            QRectF bound;
            bound.setRect(0, 0, ll, font_height); //????? FIXME pt -> pix
            QTransform old_transform = hdc->transform();
            //hdc->save();
            hdc->translate(shiftx+colabels[f][0][0], shifty+colabels[f][0][1]);
            hdc->rotate(angle);
            hdc->drawText(bound, Qt::AlignTop | Qt::AlignHCenter, QString::fromStdString(dataforselection->featurenames[featurestoshow[f]]));
            hdc->drawText(bound, Qt::AlignTop | Qt::AlignLeft, str[0]);
            hdc->drawText(bound, Qt::AlignTop | Qt::AlignRight, str[1]);
            //hdc->restore();
            hdc->setTransform(old_transform);
        }
    }
}

/*===========================================================================*/
void Plot3dWidget::paintEvent(QPaintEvent*)
{
    QPainter painter(this);
    QRect viewport = geometry();
    Draw3DPlot(&painter, viewport, 0);
    //painter.drawPixmap(viewport, pixmap);
}

/*===========================================================================*/
void Plot3dWidget::mousePressEvent(QMouseEvent *ev)
{
    mousex = _ev_position_x;
    mousey = _ev_position_y;
    ev->accept();
}

void Plot3dWidget::mouseMoveEvent(QMouseEvent *ev)
{
    if(ev->buttons() & Qt::LeftButton)
    {
        Rotate(_ev_position_x-mousex, 0, 2);
        Rotate(_ev_position_y-mousey, 1, 2);
        mousex = _ev_position_x;
        mousey = _ev_position_y;
        update();
        ev->accept();
    }
    if(ev->buttons() & Qt::RightButton)
    {
        Rotate(mousex-_ev_position_x, 0, 1);
        mousex = _ev_position_x;
        mousey = _ev_position_y;
        update();
        ev->accept();
    }
    else ev->ignore();
}

void Plot3dWidget::mouseDoubleClickEvent(QMouseEvent *ev)
{
    if(ev->buttons() & Qt::LeftButton)
    {
        W[0][0] = 1; W[0][1] = 0; W[0][2] = 0;
        W[1][0] = 0; W[1][1] = 1; W[1][2] = 0;
        W[2][0] = 0; W[2][1] = 0; W[2][2] = 1;
        transpozycja(WI, W);
        update();
        ev->accept();
    }
}















