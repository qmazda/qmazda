/*
 * qMaZda - Image Analysis and Pattern Recognition
 * 
 * Copyright 2013  Piotr M. Szczypiński <piotr.szczypinski@p.lodz.pl>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef FOLDSDIALOG_H
#define FOLDSDIALOG_H

#include <QDialog>
#include "mztablemodel.h"

namespace Ui {
class FoldsDialog;
}

class FoldsDialog : public QDialog
{
    Q_OBJECT
    
public:
    explicit FoldsDialog(MzTableModel* model, QWidget *parent = 0);
    ~FoldsDialog();
    
private slots:
    void on_okPushButton_clicked();
    void on_applyPushButton_clicked();
    void on_cancelPushButton_clicked();
    void on_createPushButton_clicked();

private:
    void setListOfFolds();

    MzTableModel* model;
    Ui::FoldsDialog *ui;
};

#endif // FOLDSDIALOG_H
