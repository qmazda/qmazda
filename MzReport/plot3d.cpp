/*
 * qMaZda - Image Analysis and Pattern Recognition
 * 
 * Copyright 2013  Piotr M. Szczypiński <piotr.szczypinski@p.lodz.pl>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "plot3d.h"
#include "plot3dwidget.h"
#include "ui_plot3d.h"

Plot3D::Plot3D(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::Plot3D)
{
    ui->setupUi(this);
    dataforselection = NULL;

/*
    vectornumber = 0;
    classnumber = 0;
    featurenumber = 0;
    vectorclassindex = NULL;
    classnames = NULL;
    featurenames = NULL;
    values = NULL;
*/
    colors = NULL;
}

Plot3D::~Plot3D()
{
    delete ui;
/*
    if(vectorclassindex != NULL) delete[] vectorclassindex;
    if(classnames != NULL) delete[] classnames;
    if(featurenames != NULL) delete[] featurenames;
    if(values != NULL) delete[] values;
*/
    if(colors != NULL) delete[] colors;
}

void Plot3D::drawLegend(QString _symbols)
{
    if(dataforselection == NULL) return;
    QString legend;
    for(int i = 0; i < dataforselection->classnumber; i++)
    {
        uint col = 0xffffff & colors[i].rgb();
        QChar fil = QChar('0');
        QString line;

        if(_symbols.size() <= i)
            line = QString("&nbsp; &nbsp; %1").arg(QString::fromStdString(dataforselection->classnames[i]));
        else if(_symbols.at(i) == QChar(' '))
            line = QString("&nbsp; &nbsp; %1").arg(QString::fromStdString(dataforselection->classnames[i]));
        else
            line = QString("<span style=color:#%1>%2</span> &nbsp; %3").arg(col, 6, 16, fil).arg(_symbols.at(i)).arg(QString::fromStdString(dataforselection->classnames[i]));
        if(i)legend.append("<br>");
        legend.append(line);
    }
    ui->textEdit->setText(legend);
}

bool Plot3D::setDataForSelection(DataForSelection* dataforselection_in)
{
    int i;
    const char symbols_v1[3] = "+o";
    const char symbols_v2[34] = "123456789ABCDEFGHIJKLMNOPQRSTUVWZ";
    selfeat[0] = -1;
    selfeat[1] = -1;
    selfeat[2] = -1;

    dataforselection = NULL;

    if(dataforselection_in->featurenumber < 1 ||
       dataforselection_in->classnumber < 1 || dataforselection_in->vectornumber < 1 ||
       dataforselection_in->featureoriginalindex == NULL || dataforselection_in->values == NULL ||
       dataforselection_in->classendvectorindex == NULL || dataforselection_in->featurenames == NULL) return false;

    dataforselection = dataforselection_in;
    if(colors != NULL) delete[] colors;
    colors = NULL;
    QString _symbols;

    //if(classnumber<=2) _symbols.replace(0, classnumber, symbols_v1);
    if(dataforselection->classnumber <= 2) {_symbols = symbols_v1; _symbols.resize(dataforselection->classnumber);}
    else if(dataforselection->classnumber <= 33) {_symbols = symbols_v2; _symbols.resize(dataforselection->classnumber);}
    else _symbols.fill(QChar(183), dataforselection->classnumber);
    ui->lineEdit->setText(_symbols);

    colors = new QColor[dataforselection->classnumber];
    for(i = 0; i < dataforselection->classnumber; i++) colors[i] = QColor(default_colors[i % default_colors_number]);

    drawLegend(_symbols);

    ui->xComboBox->blockSignals(true);
    ui->yComboBox->blockSignals(true);
    ui->zComboBox->blockSignals(true);

    ui->xComboBox->clear();
    ui->yComboBox->clear();
    ui->zComboBox->clear();
    for(i = 0; i < dataforselection->featurenumber; i++) ui->xComboBox->addItem(QString::fromStdString(dataforselection->featurenames[i]));
    for(i = 0; i < dataforselection->featurenumber; i++) ui->yComboBox->addItem(QString::fromStdString(dataforselection->featurenames[i]));
    for(i = 0; i < dataforselection->featurenumber; i++) ui->zComboBox->addItem(QString::fromStdString(dataforselection->featurenames[i]));

    ui->xComboBox->setCurrentIndex(0 % dataforselection->featurenumber);
    ui->yComboBox->setCurrentIndex(1 % dataforselection->featurenumber);
    ui->zComboBox->setCurrentIndex(2 % dataforselection->featurenumber);

    setWidgetData();

    ui->xComboBox->blockSignals(false);
    ui->yComboBox->blockSignals(false);
    ui->zComboBox->blockSignals(false);

    return true;
}

bool Plot3D::getSelectedFeatures(SelectedFeatures* selected)
{
    if(selfeat[0] < 0 || selfeat[1] < 0 || selfeat[2] < 0) return false;
    selected->Reset();
    selected->featurenumber = 3;
    selected->featureoriginalindex = new int[3];

    selected->featureoriginalindex[0] = dataforselection->featureoriginalindex[selfeat[0]];
    selected->featureoriginalindex[1] = dataforselection->featureoriginalindex[selfeat[1]];
    selected->featureoriginalindex[2] = dataforselection->featureoriginalindex[selfeat[2]];
    return true;
}


/*
bool setDataForSelection(int _vectornumber,
                    int _classnumber,
                    int _featurenumber,
                    int *_vectorclassindex,
                    QString *_classnames,
                    QString *_featurenames,
                    double *_values)
{
    int i;
    const char symbols_v1[3] = "+o";
    const char symbols_v2[34] = "123456789ABCDEFGHIJKLMNOPQRSTUVWZ";

    vectornumber = 0;
    classnumber = 0;
    featurenumber = 0;

    if(vectorclassindex != NULL) delete[] vectorclassindex; vectorclassindex = NULL;
    if(classnames != NULL) delete[] classnames; classnames = NULL;
    if(featurenames != NULL) delete[] featurenames; featurenames = NULL;
    if(values != NULL) delete[] values; values = NULL;
    if(colors != NULL) delete[] colors; colors = NULL;

    vectornumber = _vectornumber;
    classnumber = _classnumber;
    featurenumber = _featurenumber;
    vectorclassindex = _vectorclassindex;
    classnames = _classnames;
    featurenames = _featurenames;
    values = _values;

    QString _symbols;

    //if(classnumber<=2) _symbols.replace(0, classnumber, symbols_v1);
    if(classnumber <= 2) {_symbols = symbols_v1; _symbols.resize(classnumber);}
    else if(classnumber <= 33) {_symbols = symbols_v2; _symbols.resize(classnumber);}
    else _symbols.fill(183, classnumber);
    ui->lineEdit->setText(_symbols);

    colors = new QColor[classnumber];
    for(i = 0; i < classnumber; i++) colors[i] = QColor(default_colors[i % default_colors_number]);

    drawLegend(_symbols);

    ui->xComboBox->blockSignals(true);
    ui->yComboBox->blockSignals(true);
    ui->zComboBox->blockSignals(true);

    ui->xComboBox->clear();
    ui->yComboBox->clear();
    ui->zComboBox->clear();
    for(i = 0; i < featurenumber; i++) ui->xComboBox->addItem(featurenames[i]);
    for(i = 0; i < featurenumber; i++) ui->yComboBox->addItem(featurenames[i]);
    for(i = 0; i < featurenumber; i++) ui->zComboBox->addItem(featurenames[i]);

    ui->xComboBox->setCurrentIndex(0 % featurenumber);
    ui->yComboBox->setCurrentIndex(1 % featurenumber);
    ui->zComboBox->setCurrentIndex(2 % featurenumber);

    setWidgetData();

    ui->xComboBox->blockSignals(false);
    ui->yComboBox->blockSignals(false);
    ui->zComboBox->blockSignals(false);

    return true;
}
*/
bool Plot3D::setWidgetData(void)
{
    if(dataforselection == NULL) return false;

    int selfeat[3];
    selfeat[0] = ui->xComboBox->currentIndex();
    if(selfeat[0] < 0) selfeat[0] = 0;
    selfeat[1] = ui->yComboBox->currentIndex();
    if(selfeat[1] < 0) selfeat[1] = 0;
    selfeat[2] = ui->zComboBox->currentIndex();
    if(selfeat[2] < 0) selfeat[2] = 0;

    ui->renderWidget->setWidgetData(dataforselection, selfeat, ui->lineEdit->text(), colors);
    return true;
}

void Plot3D::on_xComboBox_currentIndexChanged(int)
{
    setWidgetData();
}

void Plot3D::on_yComboBox_currentIndexChanged(int)
{
    setWidgetData();
}

void Plot3D::on_zComboBox_currentIndexChanged(int)
{
    setWidgetData();
}

void Plot3D::on_toolButtonSave_clicked()
{
    QString filter;
    QString fileName = QFileDialog::getSaveFileName(this,
                                                    tr("Save plot"),
                                                    NULL,//domyslna nazwa pliku
                                                    tr("Scalable vector graphics (*.svg) (*.svg);;Portable document format (*.pdf) (*.pdf);;Tagged image file format (*tiff) (*tiff);;JPEG image (*.jpeg) (*.jpeg)"),
                                                    &filter);
    if (!fileName.isEmpty())
    {
        QRect viewport = ui->renderWidget->geometry();

        if(filter == "Tagged image file format (*tiff) (*tiff)" || filter == "JPEG image (*.jpeg) (*.jpeg)")
        {
            QImage image(QSize(viewport.width(), viewport.height()), QImage::Format_RGB32);
            image.fill(Qt::white);
            QPainter painter;
            if(!painter.begin(&image)) return;
            ui->renderWidget->Draw3DPlot(&painter, painter.viewport(), 1);
            painter.end();
            image.save(fileName);
        }
        else if(filter == "Portable document format (*.pdf) (*.pdf)")
        {
            QPrinter printer;
            printer.setOutputFormat(QPrinter::PdfFormat);
            printer.setOutputFileName(fileName);
            //printer.setPaperSize(QSizeF(viewport.width(), viewport.height()), QPrinter::Point); //PMS obsolete
            QPageSize pageSize(QSizeF(viewport.width(), viewport.height()), QPageSize::Point);
            printer.setPageSize(pageSize);


            //printer.setPageMargins(0, 0, 0, 0, QPrinter::Point);

            QPageLayout pageLayout = printer.pageLayout();
            pageLayout.setMargins(QMarginsF(0, 0, 0, 0));
            printer.setPageLayout(pageLayout);


            QPainter painter;
            if(!painter.begin(&printer)) return;
            ui->renderWidget->Draw3DPlot(&painter, painter.viewport(), 1);
            painter.end();
        }
        else
        {
            QSvgGenerator generator;
            generator.setFileName(fileName);
            generator.setSize(QSize(viewport.width(), viewport.height()));
            generator.setViewBox(viewport);
            generator.setTitle(tr("MaZda"));
            generator.setDescription(tr("Scatter plot"));
            QPainter painter;
            if(!painter.begin(&generator)) return;
            ui->renderWidget->Draw3DPlot(&painter, viewport, 1);
            painter.end();
        }
    }
}

void Plot3D::on_toolButtonFont_clicked()
{

    bool ok;
    QFont font = QFontDialog::getFont(&ok, ui->renderWidget->getFont(), this);
    if(ok)
    {
        ui->textEdit->setFont(font);
        ui->renderWidget->selectFont(font);
    }
    update();
}

void Plot3D::on_toolButtonPlay_toggled(bool checked)
{
    ui->renderWidget->toggleAnimate(checked);
}

void Plot3D::on_lineEdit_editingFinished()
{
    drawLegend(ui->lineEdit->text());
    ui->renderWidget->setSymbols(ui->lineEdit->text());
}

void Plot3D::on_toolButton_clicked()
{

    int imax, i;
    QColorDialog colorDialog;
    for(i = 0; i < dataforselection->classnumber; i++)
    {
        colorDialog.setCustomColor(i, colors[i].rgb());
    }

    //colorDialog.setOptions(QColorDialog::DontUseNativeDialog);
    //colorDialog.setOptions(0);
    colorDialog.getColor();

    imax = dataforselection->classnumber < colorDialog.customCount() ? dataforselection->classnumber : colorDialog.customCount();

    for(i = 0; i < imax; i++)
    {
        colors[i] = QColor(colorDialog.customColor(i));
    }

    drawLegend(ui->lineEdit->text());
    ui->renderWidget->update();
    //QColor color = QColorDialog::getColor();
}

void Plot3D::on_closePushButton_clicked()
{
    selfeat[0] = -1;
    selfeat[1] = -1;
    selfeat[2] = -1;
    close();
}

void Plot3D::on_selectPushButton_clicked()
{
    selfeat[0] = ui->xComboBox->currentIndex();
    if(selfeat[0] < 0) selfeat[0] = 0;
    selfeat[1] = ui->yComboBox->currentIndex();
    if(selfeat[1] < 0) selfeat[1] = 0;
    selfeat[2] = ui->zComboBox->currentIndex();
    if(selfeat[2] < 0) selfeat[2] = 0;

    close();
}
