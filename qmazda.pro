TEMPLATE = subdirs
SUBDIRS = MaZda \
          MzReport \ 
          MzMaps \
          MzGengui \
          MzGenerator \
          MzPredict \
          MzTrainer \
          LdaPlugin \ 
          PcaPlugin \ 
          VschPlugin \ 
          SvmPlugin \
          QSortPlugin \
          DecisionTreePlugin

#TRANSLATIONS = ./Translations/qmazda_pl.ts
 
