TEMPLATE = app
CONFIG += console
CONFIG -= app_bundle
CONFIG -= qt

DESTDIR         = ../../Executables
TARGET = maps2csv

SOURCES += \
    ../MzShared/imageio.cpp \
    ../MzShared/dataforconsole.cpp \
    ../MzShared/mzmapio.cpp \
    maps2csv.cpp \
    ../MzShared/mzroi.cpp

HEADERS += \
    ../MzShared/imageio.h \
    ../MzShared/bitscopy.h \
    ../MzShared/colorconvert.h \
    ../MzShared/getoption.h \
    ../MzShared/mzmapio.h
    
include(../Pri/itk.pri)
include(../Pri/opencv.pri)
include(../Pri/tiff.pri)
