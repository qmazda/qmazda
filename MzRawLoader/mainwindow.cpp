#include "mainwindow.h"
#include "ui_mainwindow.h"

#include <QFileDialog>
#include <QByteArray>

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    renderer = new RawRenderer();
    //renderer->setSizePolicy(QSizePolicy::Ignored, QSizePolicy::Ignored);
    renderer->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Expanding);
    scrollArea = new QScrollArea;
    scrollArea->setAlignment(Qt::AlignCenter);

    scrollArea->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Expanding);
    scrollArea->setWidget(renderer);
    setCentralWidget(scrollArea);
}

MainWindow::~MainWindow()
{
    delete ui;
}

const int PixelTypeCount = 15;
const char* PixelTypeName[PixelTypeCount] =
{"U8", "S8", "U16LE", "U16BE", "S16LE",
 "S16BE", "RGB16", "RGB24", "BGR24", "RGB32",
 "BGR32", "F32LE", "F32BE", "F64LE", "F64BE"};


void MainWindow::buffer2QImage(QByteArray *buffer, qint64 isize, int i)
{
    qint64 ii;
    uchar* bits = image.bits();

    switch(i)
    {
    case 0: //U8
    {
        char* ptr = buffer->data();
        for(ii = 0; ii < isize; ii++)
        {
            *bits = *ptr; bits++;
            *bits = *ptr; bits++;
            *bits = *ptr; bits++;
            *bits = 0xff; bits++;
            ptr++;
        }
    }
        break;
    case 1: //S8
    {
        char* ptr = buffer->data();
        for(ii = 0; ii < isize; ii++)
        {
            *bits = *ptr+128; bits++;
            *bits = *ptr+128; bits++;
            *bits = *ptr+128; bits++;
            *bits = 0xff; bits++;
            ptr++;
        }
    }
        break;
    case 2: //U16LE
    {
        quint16* ptr = (quint16*) buffer->data();
        quint16 min = *ptr;
        quint16 max = *ptr;
        double mul = 0;
        for(ii = 0; ii < isize; ii++)
        {
            if(*ptr > max) max = *ptr;
            if(*ptr < min) min = *ptr;
            ptr++;
        }
        max -= min;
        if(max > 0) mul = (double)256/max;
        ptr = (quint16*) buffer->data();
        for(ii = 0; ii < isize; ii++)
        {
            unsigned char val = (*ptr - min) * mul;
            *bits = val; bits++;
            *bits = val; bits++;
            *bits = val; bits++;
            *bits = 0xff; bits++;
            ptr++;
        }

    }
        break;
    case 3: //U16BE
    {
        quint16* ptr = (quint16*) buffer->data();
        quint16 vali;
        ((unsigned char*)(&vali))[0] = ((unsigned char*)ptr)[1];
        ((unsigned char*)(&vali))[1] = ((unsigned char*)ptr)[0];
        quint16 min = vali;
        quint16 max = vali;
        double mul = 0;
        for(ii = 0; ii < isize; ii++)
        {
            ((unsigned char*)(&vali))[0] = ((unsigned char*)ptr)[1];
            ((unsigned char*)(&vali))[1] = ((unsigned char*)ptr)[0];
            if(vali > max) max = vali;
            if(vali < min) min = vali;
            ptr++;
        }
        max -= min;
        if(max > 0) mul = (double)256/max;
        ptr = (quint16*) buffer->data();
        for(ii = 0; ii < isize; ii++)
        {
            ((unsigned char*)(&vali))[0] = ((unsigned char*)ptr)[1];
            ((unsigned char*)(&vali))[1] = ((unsigned char*)ptr)[0];
            unsigned char val = (vali - min) * mul;
            *bits = val; bits++;
            *bits = val; bits++;
            *bits = val; bits++;
            *bits = 0xff; bits++;
            ptr++;
        }

    }
        break;
    case 4: //S16LE
    {
        qint16* ptr = (qint16*) buffer->data();
        qint16 min = *ptr;
        qint16 max = *ptr;
        double mul = 0;
        for(ii = 0; ii < isize; ii++)
        {
            if(*ptr > max) max = *ptr;
            if(*ptr < min) min = *ptr;
            ptr++;
        }
        int dif = (int) max-min;
        if(max > 0) mul = (double)256/dif;
        ptr = (qint16*) buffer->data();
        for(ii = 0; ii < isize; ii++)
        {
            unsigned char val = (*ptr - min) * mul;
            *bits = val; bits++;
            *bits = val; bits++;
            *bits = val; bits++;
            *bits = 0xff; bits++;
            ptr++;
        }

    }
        break;
    case 5: //S16BE
    {
        qint16* ptr = (qint16*) buffer->data();
        qint16 vali;
        ((unsigned char*)(&vali))[0] = ((unsigned char*)ptr)[1];
        ((unsigned char*)(&vali))[1] = ((unsigned char*)ptr)[0];
        qint16 min = vali;
        qint16 max = vali;
        double mul = 0;
        for(ii = 0; ii < isize; ii++)
        {
            ((unsigned char*)(&vali))[0] = ((unsigned char*)ptr)[1];
            ((unsigned char*)(&vali))[1] = ((unsigned char*)ptr)[0];
            if(vali > max) max = vali;
            if(vali < min) min = vali;
            ptr++;
        }
        int dif = (int) max-min;
        if(max > 0) mul = (double)256/dif;
        ptr = (qint16*) buffer->data();
        for(ii = 0; ii < isize; ii++)
        {
            ((unsigned char*)(&vali))[0] = ((unsigned char*)ptr)[1];
            ((unsigned char*)(&vali))[1] = ((unsigned char*)ptr)[0];
            unsigned char val = (vali - min) * mul;
            *bits = val; bits++;
            *bits = val; bits++;
            *bits = val; bits++;
            *bits = 0xff; bits++;
            ptr++;
        }

    }
        break;
    case 7:
    {
        char* ptr = buffer->data();
        for(ii = 0; ii < isize; ii++)
        {
            *bits = *ptr; bits++; ptr++;
            *bits = *ptr; bits++; ptr++;
            *bits = *ptr; bits++; ptr++;
            *bits = 0xff; bits++;

        }
    }
        break;
    case 8:
    {
        char* ptr = buffer->data();
        for(ii = 0; ii < isize; ii++)
        {
            *bits = ptr[2]; bits++;
            *bits = ptr[1]; bits++;
            *bits = ptr[0]; bits++;
            *bits = 0xff; bits++;
            ptr+=3;
        }
    }
        break;
    case 9:
    {
        quint32* ptr = (quint32*) buffer->data();
        quint32* bits32 = (quint32*) bits;
        for(ii = 0; ii < isize; ii++)
        {
            *bits32 = *ptr; bits32++; ptr++;
        }
    }
        break;
    case 10:
    {
        char* ptr = buffer->data();
        for(ii = 0; ii < isize; ii++)
        {
            *bits = ptr[3]; bits++;
            *bits = ptr[2]; bits++;
            *bits = ptr[1]; bits++;
            *bits = ptr[0]; bits++;
            ptr+=4;
        }
    }
        break;
    case 11:
    {
        float* ptr = (float*) buffer->data();
        float min = *ptr;
        float max = *ptr;
        ptr++;
        for(ii = 1; ii < isize; ii++)
        {
            if(*ptr > max) max = *ptr;
            if(*ptr < min) min = *ptr;
            ptr++;
        }

        max -= min;
        if(max > 0) max = (float)256/max;
        else max = 0;

        ptr = (float*) buffer->data();
        for(ii = 0; ii < isize; ii++)
        {
            unsigned char val = (*ptr - min) * max;
            *bits = val; bits++;
            *bits = val; bits++;
            *bits = val; bits++;
            *bits = 0xff; bits++;
            ptr++;
        }
    }
        break;
    case 12:
    {
        float* ptr = (float*) buffer->data();
        float valf;
        ((unsigned char*)(&valf))[0] = ((unsigned char*)ptr)[3];
        ((unsigned char*)(&valf))[1] = ((unsigned char*)ptr)[2];
        ((unsigned char*)(&valf))[2] = ((unsigned char*)ptr)[1];
        ((unsigned char*)(&valf))[3] = ((unsigned char*)ptr)[0];
        float min = valf;
        float max = valf;
        ptr++;
        for(ii = 1; ii < isize; ii++)
        {
            ((unsigned char*)(&valf))[0] = ((unsigned char*)ptr)[3];
            ((unsigned char*)(&valf))[1] = ((unsigned char*)ptr)[2];
            ((unsigned char*)(&valf))[2] = ((unsigned char*)ptr)[1];
            ((unsigned char*)(&valf))[3] = ((unsigned char*)ptr)[0];
            if(valf > max) max = valf;
            if(valf < min) min = valf;
            ptr++;
        }
        max -= min;
        if(max > 0) max = (float)256/max;
        else max = 0;
        ptr = (float*) buffer->data();
        for(ii = 0; ii < isize; ii++)
        {
            ((unsigned char*)(&valf))[0] = ((unsigned char*)ptr)[3];
            ((unsigned char*)(&valf))[1] = ((unsigned char*)ptr)[2];
            ((unsigned char*)(&valf))[2] = ((unsigned char*)ptr)[1];
            ((unsigned char*)(&valf))[3] = ((unsigned char*)ptr)[0];
            unsigned char val = (valf - min) * max;
            *bits = val; bits++;
            *bits = val; bits++;
            *bits = val; bits++;
            *bits = 0xff; bits++;
            ptr++;
        }
    }
        break;
    case 13:
    {
        double* ptr = (double*) buffer->data();
        double min = *ptr;
        double max = *ptr;
        ptr++;
        for(ii = 1; ii < isize; ii++)
        {
            if(*ptr > max) max = *ptr;
            if(*ptr < min) min = *ptr;
            ptr++;
        }

        max -= min;
        if(max > 0) max = (double)256/max;
        else max = 0;

        ptr = (double*) buffer->data();
        for(ii = 0; ii < isize; ii++)
        {
            unsigned char val = (*ptr - min) * max;
            *bits = val; bits++;
            *bits = val; bits++;
            *bits = val; bits++;
            *bits = 0xff; bits++;
            ptr++;
        }
    }
        break;
    default:
    {
        char* ptr = buffer->data();
        for(ii = 0; ii < isize; ii++)
        {
            *bits = *ptr; bits++;
            *bits = *ptr; bits++;
            *bits = *ptr; bits++;
            *bits = 0xff; bits++;
            ptr++;
        }
    }
        break;
    case 14:
    {
        double* ptr = (double*) buffer->data();
        double valf;
        ((unsigned char*)(&valf))[0] = ((unsigned char*)ptr)[7];
        ((unsigned char*)(&valf))[1] = ((unsigned char*)ptr)[6];
        ((unsigned char*)(&valf))[2] = ((unsigned char*)ptr)[5];
        ((unsigned char*)(&valf))[3] = ((unsigned char*)ptr)[4];
        ((unsigned char*)(&valf))[4] = ((unsigned char*)ptr)[3];
        ((unsigned char*)(&valf))[5] = ((unsigned char*)ptr)[2];
        ((unsigned char*)(&valf))[6] = ((unsigned char*)ptr)[1];
        ((unsigned char*)(&valf))[7] = ((unsigned char*)ptr)[0];
        double min = valf;
        double max = valf;
        ptr++;
        for(ii = 1; ii < isize; ii++)
        {
            ((unsigned char*)(&valf))[0] = ((unsigned char*)ptr)[7];
            ((unsigned char*)(&valf))[1] = ((unsigned char*)ptr)[6];
            ((unsigned char*)(&valf))[2] = ((unsigned char*)ptr)[5];
            ((unsigned char*)(&valf))[3] = ((unsigned char*)ptr)[4];
            ((unsigned char*)(&valf))[4] = ((unsigned char*)ptr)[3];
            ((unsigned char*)(&valf))[5] = ((unsigned char*)ptr)[2];
            ((unsigned char*)(&valf))[6] = ((unsigned char*)ptr)[1];
            ((unsigned char*)(&valf))[7] = ((unsigned char*)ptr)[0];
            if(valf > max) max = valf;
            if(valf < min) min = valf;
            ptr++;
        }
        max -= min;
        if(max > 0) max = (double)256/max;
        else max = 0;
        ptr = (double*) buffer->data();
        for(ii = 0; ii < isize; ii++)
        {
            ((unsigned char*)(&valf))[0] = ((unsigned char*)ptr)[7];
            ((unsigned char*)(&valf))[1] = ((unsigned char*)ptr)[6];
            ((unsigned char*)(&valf))[2] = ((unsigned char*)ptr)[5];
            ((unsigned char*)(&valf))[3] = ((unsigned char*)ptr)[4];
            ((unsigned char*)(&valf))[4] = ((unsigned char*)ptr)[3];
            ((unsigned char*)(&valf))[5] = ((unsigned char*)ptr)[2];
            ((unsigned char*)(&valf))[6] = ((unsigned char*)ptr)[1];
            ((unsigned char*)(&valf))[7] = ((unsigned char*)ptr)[0];
            unsigned char val = (valf - min) * max;
            *bits = val; bits++;
            *bits = val; bits++;
            *bits = val; bits++;
            *bits = 0xff; bits++;
            ptr++;
        }
    }
        break;
    }
}


unsigned int MainWindow::sizeOfPixel(int* index)
{
    int i;
    QString pttext = ui->comboBox_Pixel->currentText();
    for(i = 0; i < PixelTypeCount; i++)
    {
        if(pttext == PixelTypeName[i]) break;
    }
    if(i >= PixelTypeCount) i = 0;
    unsigned int ptsize = 1;
    if(i >= 2 && i <= 6) ptsize = 2;
    else if(i >= 7 && i <= 8) ptsize = 3;
    else if(i >= 9 && i <= 12) ptsize = 4;
    else if(i >= 13 && i <= 14) ptsize = 8;
    if(index != NULL) *index = i;
    return ptsize;
}

void MainWindow::reload(void)
{
    int i;
    unsigned int ptsize = sizeOfPixel(&i);

    if(image.height() != ui->spinBox_Height->value() || image.width() != ui->spinBox_Width->value())
        image = QImage(ui->spinBox_Width->value(), ui->spinBox_Height->value(), QImage::Format_RGB32);
    image.fill(0xff00007f);

    QFile file(filename);
    if (!file.open(QIODevice::ReadOnly)) return;

    qint64 s = file.size();
    if(s > 0)
    {
        if(ui->spinBox_Skip->maximum() != s-1)
            ui->spinBox_Skip->setMaximum(s-1);
        file.seek(ui->spinBox_Skip->value());
    }
    else  ui->spinBox_Skip->setMaximum(0);
    qint64 isize = (qint64) image.height() * image.width();
    qint64 fsize = s - ui->spinBox_Skip->value();
    fsize /= ptsize;

    QByteArray buffer = file.read(isize * ptsize);

    if(isize > fsize)
        isize = fsize;
    buffer2QImage(&buffer, isize, i);

    file.close();
    renderer->setImage(&image);
}

void MainWindow::loadImage(QString* fileName)
{
    filename = *fileName;

    printf("%s\n", filename.toStdString().c_str());
    fflush(stdout);

    reload();
//    if(loadImage(fileNameRemapRead(fileName).c_str()) <= 0) return false;
//    QString openFileName = QFileInfo(*fileName).fileName();
//    this->setWindowTitle(QString("%1 - %2").arg(openFileName).arg(tr("MzMaps")));
//    assimilateImage();
//    return true;
}

void MainWindow::dropEvent(QDropEvent* event)
{
    const QMimeData* mimeData = event->mimeData();
    if (mimeData->hasUrls())
    {
        QList<QUrl> urlList = mimeData->urls();
        if(urlList.size() >= 1)
        {
            QString filename = urlList.at(0).toLocalFile();
            loadImage(&filename);
            event->acceptProposedAction();
        }
    }
}

void MainWindow::on_actionLoad_triggered()
{
    QString fileName = QFileDialog::getOpenFileName(this,
                                                    tr("Load file"),
                                                    NULL,//domyslna nazwa pliku
                                                    tr("All Files (*)"));
    if (!fileName.isEmpty())
    {
        loadImage(&fileName);
    }
}

void MainWindow::on_spinBox_Width_valueChanged(int arg1)
{
    reload();
}

void MainWindow::on_spinBox_Height_valueChanged(int arg1)
{
    reload();
}

void MainWindow::on_comboBox_Pixel_currentIndexChanged(int index)
{
    reload();
}

void MainWindow::on_spinBox_Skip_valueChanged(int arg1)
{
    reload();
}

void MainWindow::on_toolButton_Left_clicked()
{
    unsigned int ptsize = sizeOfPixel(NULL);
    ui->spinBox_Skip->setValue(ui->spinBox_Skip->value() + ptsize);
}

void MainWindow::on_toolButton_Right_clicked()
{
    unsigned int ptsize = sizeOfPixel(NULL);
    ui->spinBox_Skip->setValue(ui->spinBox_Skip->value() - ptsize);
}

void MainWindow::on_toolButton_Dn_clicked()
{
    unsigned int ptsize = sizeOfPixel(NULL);
    ui->spinBox_Skip->setValue(ui->spinBox_Skip->value() - ptsize*ui->spinBox_Width->value());
}

void MainWindow::on_toolButton_Up_clicked()
{
    unsigned int ptsize = sizeOfPixel(NULL);
    ui->spinBox_Skip->setValue(ui->spinBox_Skip->value() + ptsize*ui->spinBox_Width->value());
}
