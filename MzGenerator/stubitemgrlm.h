/*
 * QMAZDA project: Image Analysis and Pattern Recognition
 *
 * Copyright 2013-2017 Piotr M. Szczypiński <piotr.szczypinski@p.lodz.pl>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef STUBITEMGRLM_H
#define STUBITEMGRLM_H

#include "genentry.h"

enum Direction {H, V, Z, N, X};

class StubItemGrlm : public StubItem
{
public:
    ~StubItemGrlm();
    static StubItem* New(unsigned int dimensions, StubItem* parent, std::vector<std::string>* nameStubs, unsigned int index, NotifyType *notify);
    void Connect(StubItem* item);
    void Execute(StubItemData* data);
    bool IsNotifier(){return true;}
private:

    inline static void rlmline(bool roiishere, short unsigned int imagevalue, unsigned long int** RL, int* count, int* glevel)
    {
        if(roiishere)
        {
            if(*glevel<0)
            {
                *glevel = imagevalue;
                (*count)++;
            }
            else
            {
                int l = imagevalue;
                if(*glevel != l)
                {
                    RL[*glevel][*count]++;
                    *count = 0;
                    *glevel = l;
                }
                else
                {
                    (*count)++;
                }
            }
        }
        else
        {
            if(*count>=0 && *glevel>=0) RL[*glevel][*count]++;
            *count = -1;
            *glevel = -1;
        }
    }

    template <unsigned int VDimensions>
    void inRoi(StubItemDataEntryRoi* datas)
    {
        if(bits < 1) return;
        if(bits > sizeof(MazdaImagePixelType)*8) return;
        if(bits < 1) return;
        unsigned int shift = sizeof(MazdaImagePixelType)*8 - bits;

        double* table = datas->table;
        if(direction == X && VDimensions < 3) return;

        typedef MazdaImage<MazdaImagePixelType, VDimensions> MITypeLocal;
        typedef MazdaRoi <MazdaRoiBlockType, VDimensions> MzRoiLocal;
        unsigned int size[VDimensions];
        double spacing[VDimensions];
        int begin[VDimensions];
        int end[VDimensions];
        MITypeLocal* image = static_cast<MITypeLocal*> (datas->image);
        image->GetSize(size);
        image->GetSpacing(spacing);
        MzRoiLocal* roi = static_cast<MzRoiLocal*> (datas->roi);
        if(roi->IsEmpty()) return;
        roi->GetBegin(begin);
        roi->GetEnd(end);
        bool quit = false;
        for(unsigned int d = 0; d < VDimensions; d++)
        {
            if(begin[d] >= (int)size[d]) {quit = true; break;}
            if(end[d] < 0) {quit = true; break;}
            if(begin[d] < 0) begin[d] = 0;
            if(end[d] >= (int)size[d]) end[d] = size[d]-1;
        }
        if(quit) return;

        int i, j, k;
        int levels = (int)1<<bits;
        unsigned long int **RL;
        int rlsize = 0;
        switch(direction)
        {
        case H: rlsize = end[0]-begin[0]+1; break;
        case V: rlsize = end[1]-begin[1]+1; break;
        case N:
        case Z: rlsize = end[0]-begin[0] < end[1]-begin[1] ? end[0]-begin[0]+1 : end[1]-begin[1]+1; break;
        case X: rlsize = end[2]-begin[2]+1; break;
        }

        RL = (unsigned long int**) malloc(sizeof(unsigned long int*)*levels);
        if(RL==NULL) return;
        for(k=0; k < levels; k++)
        {
            RL[k]=(unsigned long int*) malloc(sizeof(unsigned long int)*rlsize);
            if(RL[k]==NULL)
            {
                for(k--; k >= 0; k--) free(RL[k]);
                free(RL);
                return;
            }
            for(i = 0; i < rlsize; i++) RL[k][i] = 0;
        }
        switch(direction)
        {
            case H:
            {
                MazdaImageRegionIterator<MITypeLocal> ci = MazdaImageRegionIterator<MITypeLocal>(image, (unsigned int*) begin, (unsigned int*) end);
                MazdaRoiRegionIterator<MzRoiLocal> ri = MazdaRoiRegionIterator<MzRoiLocal>(roi, begin, end, true);
                while(!ri.IsBehind())
                {
                    int count = -1;
                    int glevel = -1;
                    for(int l = 0; l < rlsize; l++)
                    {
                        rlmline(ri.GetPixel(), (ci.GetPixel())>>shift, RL, &count, &glevel);
                        ++ri;
                        ++ci;
                    }
                    if(count>=0 && glevel>=0) RL[glevel][count]++;
                }
                break;
            }

            case V:
            {
                unsigned int order[VDimensions];
                order[0] = 1;
                order[1] = 0;
                for(k = 2; k < VDimensions; k++) order[k] = k;
                MazdaImageRegionFlippedIterator<MITypeLocal> ci = MazdaImageRegionFlippedIterator<MITypeLocal>(image, (unsigned int*) begin, (unsigned int*) end, order);
                MazdaRoiRegionFlippedIterator<MzRoiLocal> ri = MazdaRoiRegionFlippedIterator<MzRoiLocal>(roi, begin, end, order, true);
                while(!ri.IsBehind())
                {
                    int count = -1;
                    int glevel = -1;
                    for(int l = 0; l < rlsize; l++)
                    {
                        rlmline(ri.GetPixel(), (ci.GetPixel())>>shift, RL, &count, &glevel);
                        ++ri;
                        ++ci;
                    }
                    if(count>=0 && glevel>=0) RL[glevel][count]++;
                }
                break;
            }

            case X:
            {
                if(VDimensions < 3)
                {
                    for(k=0; k < levels; k++) free(RL[k]);
                    free(RL);
                    return;
                }
                unsigned int order[VDimensions];
                order[0] = 2;
                order[1] = 0;
                order[2] = 1;
                for(k = 3; k < VDimensions; k++) order[k] = k;
                MazdaImageRegionFlippedIterator<MITypeLocal> ci = MazdaImageRegionFlippedIterator<MITypeLocal>(image, (unsigned int*) begin, (unsigned int*) end, order);
                MazdaRoiRegionFlippedIterator<MzRoiLocal> ri = MazdaRoiRegionFlippedIterator<MzRoiLocal>(roi, begin, end, order, true);
                while(!ri.IsBehind())
                {
                    int count = -1;
                    int glevel = -1;
                    for(int l = 0; l < rlsize; l++)
                    {
                        rlmline(ri.GetPixel(), (ci.GetPixel())>>shift, RL, &count, &glevel);
                        ++ri;
                        ++ci;
                    }
                    if(count>=0 && glevel>=0) RL[glevel][count]++;
                }
                break;
            }

            case N:
            {
                int length;
                unsigned int step;
                int end2[VDimensions];
                unsigned int rindex[VDimensions];
                step = size[0]+1;
                for(k = 0; k < VDimensions; k++) end2[k] = end[k];
                end2[1] = begin[1];
                for(int kk = 0; kk < 2; kk++)
                {
                    MazdaImageRegionIterator<MITypeLocal> ci = MazdaImageRegionIterator<MITypeLocal>(image, (unsigned int*) begin, (unsigned int*) end2);
                    MazdaRoiRegionIterator<MzRoiLocal> ri = MazdaRoiRegionIterator<MzRoiLocal>(roi, begin, end2, true);
                    while(!ri.IsBehind())
                    {
                        MazdaImagePixelType* pimage = ci.GetPixelPointer();
                        for(k = 0; k < VDimensions; k++) rindex[k] = ri.GetIndexFromRoiBegin(k);
                        int i0 = end[0]-(int)ci.GetIndex(0);
                        int i1 = end[1]-(int)ci.GetIndex(1);
                        length = i0;
                        if(length > i1) length = i1;
                        int count = -1;
                        int glevel = -1;
                        for(int l = 0; l <= length; l++)
                        {
                            rlmline(roi->GetPixel((int*)rindex), (*pimage)>>shift, RL, &count, &glevel);
                            pimage += step;
                            rindex[0]++;
                            rindex[1]++;
                        }
                        if(count>=0 && glevel>=0) RL[glevel][count]++;
                        ++ri;
                        ++ci;
                    }
                    end2[1] = end[1];
                    end2[0] = begin[0];
                    begin[1]++;
                    if(begin[1] > end[1]) break;
                }
                break;
            }

            case Z:
            {
                int length;
                unsigned int step;
                int end2[VDimensions];
                unsigned int rindex[VDimensions];
                step = size[0]-1;
                for(k = 0; k < VDimensions; k++) end2[k] = end[k];
                end2[1] = begin[1];
                int begin0 = begin[0];

                for(int kk = 0; kk < 2; kk++)
                {
                    MazdaImageRegionIterator<MITypeLocal> ci = MazdaImageRegionIterator<MITypeLocal>(image, (unsigned int*) begin, (unsigned int*) end2);
                    MazdaRoiRegionIterator<MzRoiLocal> ri = MazdaRoiRegionIterator<MzRoiLocal>(roi, begin, end2, true);
                    while(!ri.IsBehind())
                    {
                        MazdaImagePixelType* pimage = ci.GetPixelPointer();
                        for(k = 0; k < VDimensions; k++) rindex[k] = ri.GetIndexFromRoiBegin(k);
                        int i0 = (int)ci.GetIndex(0)-begin0;
                        int i1 = end[1]-(int)ci.GetIndex(1);
                        length = i0;
                        if(length > i1) length = i1;
                        int count = -1;
                        int glevel = -1;

                        for(int l = 0; l <= length; l++)
                        {
                            rlmline(roi->GetPixel((int*)rindex), (*pimage)>>shift, RL, &count, &glevel);
                            pimage += step;
                            rindex[0]--;
                            rindex[1]++;
                        }
                        if(count>=0 && glevel>=0) RL[glevel][count]++;
                        ++ri;
                        ++ci;
                    }
                    end2[1] = end[1];
                    begin[0] = end[0];
                    begin[1]++;
                    if(begin[1] > end[1]) break;
                }
                break;
            }
        }

        double ddw, dw, c;
        c = 0;
        for (i = 0; i < levels; i++)
        {
            for (j = 0; j < rlsize; j++)
            {
                c+=RL[i][j];
            }
        }

#ifdef USE_DEBUG_LOG
        if(debugfile.is_open())
        {
            debugfile << "GreyLevels: " << levels << " Size: " << rlsize << std::endl;
            debugfile << "Direction: ";
            switch(direction)
            {
            case H: debugfile << "H"; break;
            case V: debugfile << "V"; break;
            case N: debugfile << "N"; break;
            case Z: debugfile << "Z"; break;
            case X: debugfile << "X"; break;
            }
            debugfile << std::endl;
            debugfile << "Divider: " << c << std::endl;
            unsigned int area = 0;
            for (i = 0; i < levels; i++)
            {
                area += RL[i][0];
                for (j = 1; j < rlsize; j++)
                {
                    area += (RL[i][j]*(j+1));
                }
            }
            debugfile << "Area: " << area << std::endl;
            debugfile << "Grlm:" << std::endl;
            for (i = 0; i < levels; i++)
            {
                debugfile << RL[i][0];
                for (j = 1; j < rlsize; j++)
                {
                    debugfile << "\t" << RL[i][j];
                }
                debugfile << std::endl;
            }
        }
#endif

        if(!c)
        {
            for(k=0; k < levels; k++) free(RL[k]);
            free(RL);
            return;
        }
        /*RLDist*/
        if(RLNonUni != (unsigned int)-1 || MRLNonUni != (unsigned int)-1)
        {
            ddw=0;
            for (j = 0; j < rlsize; j++)
            {
                dw = 0;
                for (i = 0; i < levels; i++)
                    dw += RL[i][j];
                ddw += (double) dw * dw;
            }
            if(RLNonUni != (unsigned int)-1) table[RLNonUni] = (double)ddw/c;
            if(MRLNonUni != (unsigned int)-1) table[MRLNonUni] = (double)ddw/(c*c);
        }
        /*GDist*/
        if(GLevNonUn != (unsigned int)-1 || MGLevNonUn != (unsigned int)-1)
        {
            ddw=0;
            for (i = 0; i < levels; i++)
            {
                dw = 0;
                for (j = 0; j < rlsize; j++)
                    dw += RL[i][j];
                ddw += (double) dw * dw;
            }
            if(GLevNonUn != (unsigned int)-1) table[GLevNonUn] = (double)ddw/c;
            if(MGLevNonUn != (unsigned int)-1) table[MGLevNonUn] = (double)ddw/(c*c);
        }
        /*LRE*/
        if(LngREmph != (unsigned int)-1)
        {
            ddw=0;
            for (i = 0; i < levels; i++)
                for (j = 0; j < rlsize; j++)
                    ddw += (double) (j+1)*(j+1)*RL[i][j];
            table[LngREmph] = (double)ddw/c;
        }
        /*Short Run Emphasis*/
        if(ShrtREmp != (unsigned int)-1)
        {
            ddw=0;
            for (i = 0; i < levels; i++)
                for (j = 0; j < rlsize; j++)
                    ddw += (double) RL[i][j]/((double)(j+1)*(j+1));
            table[ShrtREmp] = (double)ddw/c;
        }
        /*Fraction of Image in Runs*/
        if(Fraction != (unsigned int)-1)
        {
            ddw=0;
            for (i = 0; i < levels; i++)
                for (j = 0; j < rlsize; j++)
                    ddw += (double)(j+1)*RL[i][j];
            if(ddw != 0.0) table[Fraction] = (double)c/ddw;
        }
        for(k=0; k < levels; k++) free(RL[k]);
        free(RL);
    }
    unsigned int bits;

    unsigned int RLNonUni;
    unsigned int GLevNonUn;
    unsigned int LngREmph;
    unsigned int ShrtREmp;
    unsigned int Fraction;
    unsigned int MRLNonUni;
    unsigned int MGLevNonUn;
    Direction direction;

};

#endif // STUBITEMGRLM_H
