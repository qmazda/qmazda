/*
 * QMAZDA project: Image Analysis and Pattern Recognition
 *
 * Copyright 2013-2018 Piotr M. Szczypiński <piotr.szczypinski@p.lodz.pl>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef STUBITEMLBP_H
#define STUBITEMLBP_H

#include "genentry.h"
#include <math.h>

class StubItemLbp : public StubItem
{
public:
    ~StubItemLbp();
    static StubItem* New(unsigned int dimensions, StubItem* parent, std::vector<std::string>* nameStubs, unsigned int index, NotifyType *notify);
    void Connect(StubItem* item);
    void Execute(StubItemData* data);
    bool IsNotifier(){return true;}

    enum Algorithm {Oc4n, Oc8n, Tr4n, Tr8n, Cs4n, Cs8n, Cs12n};
    enum Pattern {Oc, Tr, Cs};

private:
    int dx_4n[4] = { 0, 1,-1,-1};
    int dy_4n[4] = {-1, 1, 1,-1};
    int dx_8n[8] = { 0, 1, 0, 0,-1,-1, 0, 0};
    int dy_8n[8] = {-1, 0, 1, 1, 0, 0,-1,-1};
    int dx_12n[12] = { 0, 1, 1, 0, 0,-1,-1,-1,-1, 0, 0, 1};
    int dy_12n[12] = {-2, 0, 1, 1, 1, 1, 0, 0,-1,-1,-1,-1};

    template <unsigned int VDimensions>
    void inRoi(StubItemDataEntryRoi* datas, unsigned int neighbors, int* dx, int* dy, Pattern mode)
    {
        unsigned int n;
        unsigned int ntries = 1;
        double* table = datas->table;
        typedef MazdaImage<MazdaImagePixelType, VDimensions> MITypeLocal;
        typedef MazdaRoi <MazdaRoiBlockType, VDimensions> MzRoiLocal;
        unsigned int size[VDimensions];
        int begin[VDimensions];
        int end[VDimensions];
        MITypeLocal* image = static_cast<MITypeLocal*> (datas->image);
        image->GetSize(size);
        MzRoiLocal* roi = static_cast<MzRoiLocal*> (datas->roi);
        if(roi->IsEmpty()) return;
        roi->GetBegin(begin);
        roi->GetEnd(end);
        bool quit = false;
        for(unsigned int d = 0; d < VDimensions; d++)
        {
            if(begin[d] >= (int)size[d]) {quit = true; break;}
            if(end[d] < 0) {quit = true; break;}
            if(begin[d] < 0) begin[d] = 0;
            if(end[d] >= (int)size[d]) end[d] = size[d]-1;
        }
        if(quit) return;

        if(neighbors > 8)
        {
            if(end[0] - begin[0] < 4) return;
            if(end[1] - begin[1] < 4) return;
        }
        else
        {
            if(end[0] - begin[0] < 2) return;
            if(end[1] - begin[1] < 2) return;
        }

        long int area = 0;
        unsigned int vals[256];
        for(n = 0; n < 256; n++) vals[n] = 0;
        MazdaImageRegionIterator<MITypeLocal>* ci[13];
        MazdaRoiRegionIterator<MzRoiLocal>* ri[13];
        if(neighbors > 8)
        {
            begin[1]+=2; end[1]-=2;
            begin[0]+=2; end[0]-=2;
        }
        else
        {
            begin[1]++; end[1]--;
            begin[0]++; end[0]--;
        }
        if(mode == Oc)
        {
            ci[0] = new MazdaImageRegionIterator<MITypeLocal>(image, (unsigned int*) begin, (unsigned int*) end);
            ri[0] = new MazdaRoiRegionIterator<MzRoiLocal>(roi, begin, end, true);
        }
        else
        {
            ci[0] = NULL;
            ri[0] = NULL;
        }
        for(n = 0; n < neighbors; n++)
        {
            begin[0]+=dx[n]; end[0]+=dx[n];
            begin[1]+=dy[n]; end[1]+=dy[n];
            ci[n+1] = new MazdaImageRegionIterator<MITypeLocal>(image, (unsigned int*) begin, (unsigned int*) end);
            ri[n+1] = new MazdaRoiRegionIterator<MzRoiLocal>(roi, begin, end, true);
        }
        for(; n < 12; n++)
        {
            ci[n+1] = NULL;
            ri[n+1] = NULL;
        }

        switch(mode)
        {
            case Oc:
            {
                ntries <<= neighbors;
                while(!ri[0]->IsBehind())
                {
                    for(n = 0; n <= neighbors; n++)
                    {
                        if(! ri[n]->GetPixel())
                            break;
                    }
                    if(n > neighbors)
                    {
                        unsigned int val = 0;
                        unsigned int bit = 1;
                        typename MazdaImageRegionIterator<MITypeLocal>::PixelType cent = ci[0]->GetPixel();

                        for(n = 1; n <= neighbors; n++)
                        {
                            if(ci[n]->GetPixel() > cent)
                                val |= bit;
                            bit <<= 1;
                        }
                        area++;
                        vals[val]++;
                    }
                    for(n = 0; n <= neighbors; n++)
                    {
                        ++(*(ri[n]));
                        ++(*(ci[n]));
                    }
                }
            }
            break;

            case Tr:
            {
                ntries <<= neighbors;
                while(!ri[1]->IsBehind())
                {
                    for(n = 1; n <= neighbors; n++)
                    {
                        if(! ri[n]->GetPixel())
                            break;
                    }
                    if(n > neighbors)
                    {
                        unsigned int val = 0;
                        unsigned int bit = 1;
                        typename MazdaImageRegionIterator<MITypeLocal>::PixelType cent = ci[neighbors]->GetPixel();
                        typename MazdaImageRegionIterator<MITypeLocal>::PixelType ncent;
                        for(n = 1; n <= neighbors; n++)
                        {
                            ncent = ci[n]->GetPixel();
                            if(ncent > cent)
                                val |= bit;
                            bit <<= 1;
                            cent = ncent;
                        }
                        area++;
                        vals[val]++;
                    }
                    for(n = 1; n <= neighbors; n++)
                    {
                        ++(*(ri[n]));
                        ++(*(ci[n]));
                    }
                }
            }
            break;

            case Cs:
            {
                unsigned int neighbors2 = neighbors/2;
                ntries <<= neighbors2;
                while(!ri[1]->IsBehind())
                {
                    for(n = 1; n <= neighbors; n++)
                    {
                        if(! ri[n]->GetPixel())
                            break;
                    }
                    if(n > neighbors)
                    {
                        unsigned int val = 0;
                        unsigned int bit = 1;

                        for(n = 1; n <= neighbors2; n++)
                        {
                            if(ci[n]->GetPixel() > ci[neighbors2+n]->GetPixel())
                                val |= bit;
                            bit <<= 1;
                        }
                        area++;
                        vals[val]++;
                    }
                    for(n = 1; n <= neighbors; n++)
                    {
                        ++(*(ri[n]));
                        ++(*(ci[n]));
                    }
                }
            }
            break;
        }

        for(n = neighbors; n <= neighbors; n--)
        {
            if(ri[n] != NULL) delete ri[n];
            if(ci[n] != NULL) delete ci[n];
        }
//        delete[] ri;
//        delete[] ci;

        for(n = 0; n < ntries; n++)
            if(Count[n] != (unsigned int)-1)
                table[Count[n]] = (double)vals[n]/(double)area;
    }





    /*
    template <unsigned int VDimensions>
    void inRoi(StubItemDataEntryRoi* datas)
    {
        double* table = datas->table;
        typedef MazdaImage<MazdaImagePixelType, VDimensions> MITypeLocal;
        typedef MazdaRoi <MazdaRoiBlockType, VDimensions> MzRoiLocal;
        unsigned int size[VDimensions];
        int begin[VDimensions];
        int end[VDimensions];
        MITypeLocal* image = static_cast<MITypeLocal*> (datas->image);
        image->GetSize(size);
        MzRoiLocal* roi = static_cast<MzRoiLocal*> (datas->roi);
        if(roi->IsEmpty()) return;
        roi->GetBegin(begin);
        roi->GetEnd(end);
        bool quit = false;
        for(unsigned int d = 0; d < VDimensions; d++)
        {
            if(begin[d] >= (int)size[d]) {quit = true; break;}
            if(end[d] < 0) {quit = true; break;}
            if(begin[d] < 0) begin[d] = 0;
            if(end[d] >= (int)size[d]) end[d] = size[d]-1;
        }
        if(quit) return;
        if(end[0] - begin[0] < 2) return;
        if(end[1] - begin[1] < 2) return;

        long int area = 0;
        unsigned int vals[16];
        for(unsigned int n = 0; n < 16; n++)
            vals[n] = 0;
        begin[1]++; end[1]--;
        begin[0]++; end[0]--;
        MazdaImageRegionIterator<MITypeLocal> cci = MazdaImageRegionIterator<MITypeLocal>(image, (unsigned int*) begin, (unsigned int*) end);
        MazdaRoiRegionIterator<MzRoiLocal> rci = MazdaRoiRegionIterator<MzRoiLocal>(roi, begin, end, true);
        begin[0]--; end[0]--;
        MazdaImageRegionIterator<MITypeLocal> cli = MazdaImageRegionIterator<MITypeLocal>(image, (unsigned int*) begin, (unsigned int*) end);
        MazdaRoiRegionIterator<MzRoiLocal> rli = MazdaRoiRegionIterator<MzRoiLocal>(roi, begin, end, true);
        begin[0]+=2; end[0]+=2;
        MazdaImageRegionIterator<MITypeLocal> cri = MazdaImageRegionIterator<MITypeLocal>(image, (unsigned int*) begin, (unsigned int*) end);
        MazdaRoiRegionIterator<MzRoiLocal> rri = MazdaRoiRegionIterator<MzRoiLocal>(roi, begin, end, true);
        begin[0]--; end[0]--;
        begin[1]--; end[1]--;
        MazdaImageRegionIterator<MITypeLocal> cti = MazdaImageRegionIterator<MITypeLocal>(image, (unsigned int*) begin, (unsigned int*) end);
        MazdaRoiRegionIterator<MzRoiLocal> rti = MazdaRoiRegionIterator<MzRoiLocal>(roi, begin, end, true);
        begin[1]+=2; end[1]+=2;
        MazdaImageRegionIterator<MITypeLocal> cbi = MazdaImageRegionIterator<MITypeLocal>(image, (unsigned int*) begin, (unsigned int*) end);
        MazdaRoiRegionIterator<MzRoiLocal> rbi = MazdaRoiRegionIterator<MzRoiLocal>(roi, begin, end, true);

        while(!rci.IsBehind())
        {
            if(rci.GetPixel() && rli.GetPixel() && rri.GetPixel() && rti.GetPixel() && rbi.GetPixel())
            {
                unsigned int val = 0;
                typename MazdaImageRegionIterator<MITypeLocal>::PixelType cent = cci.GetPixel();
                if(cti.GetPixel() > cent) val |= 1;
                if(cri.GetPixel() > cent) val |= 2;
                if(cbi.GetPixel() > cent) val |= 4;
                if(cli.GetPixel() > cent) val |= 8;
                area++;
                vals[val]++;
            }
            ++rci; ++rbi; ++rti; ++rli; ++rri;
            ++cci; ++cbi; ++cti; ++cli; ++cri;
        }

        for(unsigned int n = 0; n < 16; n++)
            if(Count[n] != (unsigned int)-1)
                table[Count[n]] = (double)vals[n]/(double)area;
    }

*/

    unsigned int Count[256];

    Algorithm algorithm;
};


#endif // STUBITEMLBP_H
