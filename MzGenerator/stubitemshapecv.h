/*
 * QMAZDA project: Image Analysis and Pattern Recognition
 *
 * Copyright 2013-2017 Piotr M. Szczypiński <piotr.szczypinski@p.lodz.pl>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef StubItemShapeCv_H
#define StubItemShapeCv_H

#include "genentry.h"
#include <math.h>
#ifdef USE_OPENCV_CPP_API
#include <opencv2/imgproc/imgproc.hpp>
#include <tuple>
#else
#include <opencv2/imgproc/imgproc_c.h>
#endif


class StubItemShapeCv : public StubItem
{
public:
    ~StubItemShapeCv();
    static StubItem* New(unsigned int dimensions, StubItem* parent, std::vector<std::string>* nameStubs, unsigned int index, NotifyType *notify);
    void Connect(StubItem* item);
    void Execute(StubItemData* data);
    bool IsNotifier(){return true;}
private:
    void inRoi2D(StubItemDataEntryRoi* datas);
    void WykonajOliczenia(MzRoi2D* roi, double *table);

    #ifdef USE_OPENCV_CPP_API
    std::tuple<double, std::vector<cv::Point>>
    segmentGetMaxAreaContour(std::vector<std::vector<cv::Point>> contours,
                             std::vector<cv::Point> contour);

    #else
    double segmentGetMaxAreaContour(CvSeq* contours, CvSeq** contour);

    #endif


    bool compute_elips;
    bool compute_convex;
    bool compute_radii;
    bool compute_moments;
    bool compute_humoments;

    unsigned int PixelsCount;
    unsigned int ProfileArea;
    unsigned int Perimeter;
    unsigned int Roundness;

    unsigned int ElipsAngle;
    unsigned int ElipsHeight;
    unsigned int ElipsWidth;
    unsigned int ElipsElong;

    unsigned int X;
    unsigned int Y;
    unsigned int Width;
    unsigned int Height;
    unsigned int EquivDiameter;

    unsigned int MomTheta;
    unsigned int MomElong;
    unsigned int HuMom1;
    unsigned int HuMom2;
    unsigned int HuMom3;
    unsigned int HuMom4;
    unsigned int HuMom5;
    unsigned int HuMom6;
    unsigned int HuMom7;

    unsigned int ChRectWidth;
    unsigned int ChRectHeight;
    unsigned int ChRectArea;
    unsigned int ChArea;
    unsigned int ChPerim;
    unsigned int ChMinFeret;
    unsigned int ChMaxFeret;

    unsigned int RadDist;
    unsigned int RadDistsqrd;
    unsigned int RadDistdev;
    unsigned int Radbb;
};


#endif // StubItemShapeCv_H
