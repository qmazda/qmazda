/*
 * QMAZDA project: Image Analysis and Pattern Recognition
 *
 * Copyright 2013-2017 Piotr M. Szczypiński <piotr.szczypinski@p.lodz.pl>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "stubitemshape.h"

StubItemShape::~StubItemShape()
{
}
StubItem* StubItemShape::New(unsigned int dimensions, StubItem* parent, std::vector<std::string>* nameStubs, unsigned int /*index*/, NotifyType* notify)
{
    StubItemShape* item = new StubItemShape;
    item->dimensions = dimensions;
    item->notify = notify;
    item->name = nameStubs->back();

    item->compute_perimeter = false;
    item->compute_feret = false;
    item->compute_connected = false;
    item->compute_skeleton = false;
    item->Area = (unsigned int) -1;
    item->FullyConnectedFalse = (unsigned int) -1;
    item->FullyConnectedTrue = (unsigned int) -1;
    item->SizeX = (unsigned int) -1;
    item->SizeY = (unsigned int) -1;
    item->SizeZ = (unsigned int) -1;
    item->FeretDiameter = (unsigned int) -1;
    item->CentroidX = (unsigned int) -1;
    item->CentroidY = (unsigned int) -1;
    item->CentroidZ = (unsigned int) -1;
    item->PrincipalMomentX = (unsigned int) -1;
    item->PrincipalMomentY = (unsigned int) -1;
    item->PrincipalMomentZ = (unsigned int) -1;
    item->Tilt = (unsigned int) -1;
    item->Elongation = (unsigned int) -1;
    item->Perimeter = (unsigned int) -1;
    item->Roundness = (unsigned int) -1;
    item->EquivalentSphericalRadius = (unsigned int) -1;
    item->EquivalentSphericalPerimeter = (unsigned int) -1;
    item->EquivalentEllipsoidDiameterX = (unsigned int) -1;
    item->EquivalentEllipsoidDiameterY = (unsigned int) -1;
    item->EquivalentEllipsoidDiameterZ = (unsigned int) -1;

    if(parent != NULL) parent->Connect(item);
    return item;
}
void StubItemShape::Connect(StubItem* item)
{
    StubItemFinal* itemf = static_cast <StubItemFinal*> (item);

    if(itemf->Name() == "Area") {Area = itemf->Index(); compute_shape = true;}
    else if(itemf->Name() == "FullyConnectedFalse") {FullyConnectedFalse = itemf->Index(); compute_connected = true;}
    else if(itemf->Name() == "FullyConnectedTrue") {FullyConnectedTrue = itemf->Index(); compute_connected = true;}
    else if(itemf->Name() == "SizeX") {SizeX = itemf->Index(); compute_shape = true;}
    else if(itemf->Name() == "SizeY") {SizeY = itemf->Index(); compute_shape = true;}
    else if(itemf->Name() == "SizeZ") {SizeZ = itemf->Index(); compute_shape = true;}
    else if(itemf->Name() == "FeretDiameter") {FeretDiameter = itemf->Index(); compute_shape = true; compute_feret = true;}
    else if(itemf->Name() == "CentroidX") {CentroidX = itemf->Index(); compute_shape = true;}
    else if(itemf->Name() == "CentroidY") {CentroidY = itemf->Index(); compute_shape = true;}
    else if(itemf->Name() == "CentroidZ") {CentroidZ = itemf->Index(); compute_shape = true;}
    else if(itemf->Name() == "PrincipalMomentX") {PrincipalMomentX = itemf->Index(); compute_shape = true;}
    else if(itemf->Name() == "PrincipalMomentY") {PrincipalMomentY = itemf->Index(); compute_shape = true;}
    else if(itemf->Name() == "PrincipalMomentZ") {PrincipalMomentZ = itemf->Index(); compute_shape = true;}
    else if(itemf->Name() == "Tilt") {Tilt = itemf->Index(); compute_shape = true;}
    else if(itemf->Name() == "Elongation") {Elongation = itemf->Index(); compute_shape = true;}
    else if(itemf->Name() == "Perimeter") {Perimeter = itemf->Index(); compute_shape = true; compute_perimeter = true;}
    else if(itemf->Name() == "Roundness") {Roundness = itemf->Index(); compute_shape = true;}
    else if(itemf->Name() == "EquivalentSphericalRadius") {EquivalentSphericalRadius = itemf->Index(); compute_shape = true;}
    else if(itemf->Name() == "EquivalentSphericalPerimeter") {EquivalentSphericalPerimeter = itemf->Index(); compute_shape = true;}
    else if(itemf->Name() == "EquivalentEllipsoidDiameterX") {EquivalentEllipsoidDiameterX = itemf->Index(); compute_shape = true;}
    else if(itemf->Name() == "EquivalentEllipsoidDiameterY") {EquivalentEllipsoidDiameterY = itemf->Index(); compute_shape = true;}
    else if(itemf->Name() == "EquivalentEllipsoidDiameterZ") {EquivalentEllipsoidDiameterZ = itemf->Index(); compute_shape = true;}
    children.push_back(item);
}

void StubItemShape::Execute(StubItemData* data)
{
    StubItemDataEntryRoi* datas = static_cast<StubItemDataEntryRoi*> (data);

    if(datas->roi != NULL)
    {
        try
        {
            switch(dimensions)
            {
            case 3: inRoi<3> (datas); break;
            case 2: inRoi<2> (datas); break;
            default: break;
            }
        }
        catch(...){}
    }
    if(notify!=NULL) (*notify)();
    return;
}

