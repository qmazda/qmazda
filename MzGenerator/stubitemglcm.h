/*
 * QMAZDA project: Image Analysis and Pattern Recognition
 *
 * Copyright 2013-2017 Piotr M. Szczypiński <piotr.szczypinski@p.lodz.pl>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef STUBITEMGLCM_H
#define STUBITEMGLCM_H

#include "genentry.h"
#include <math.h>

#define Eps 1E-20
#define Ln10 2.302585092994

class StubItemGlcm : public StubItem
{
public:
    ~StubItemGlcm();
    static StubItem* New(unsigned int dimensions, StubItem* parent, std::vector<std::string>* nameStubs, unsigned int index, NotifyType *notify);
    void Connect(StubItem* item);
    void Execute(StubItemData* data);
    bool IsNotifier(){return true;}
private:

    template <unsigned int VDimensions>
    void inRoi(StubItemDataEntryRoi* datas)
    {
        double* table = datas->table;

        if(bits > sizeof(MazdaImagePixelType)*8 || bits < 1) return;
        if(offset[2] != 0 && VDimensions < 3) return;
        unsigned int shift = sizeof(MazdaImagePixelType)*8 - bits;

        unsigned int** S;
        long int* RR;
        int l, k;
        int levels = (int)1<<bits;
        typedef MazdaImage<MazdaImagePixelType, VDimensions> MITypeLocal;
        typedef MazdaRoi <MazdaRoiBlockType, VDimensions> MzRoiLocal;
        unsigned int size[VDimensions];
        double spacing[VDimensions];
        int begin[VDimensions];
        int end[VDimensions];
        MITypeLocal* image = static_cast<MITypeLocal*> (datas->image);
        image->GetSize(size);
        image->GetSpacing(spacing);
        MzRoiLocal* roi = static_cast<MzRoiLocal*> (datas->roi);
        if(roi->IsEmpty()) return;
        roi->GetBegin(begin);
        roi->GetEnd(end);
        bool quit = false;
        for(unsigned int d = 0; d < VDimensions; d++)
        {
            if(begin[d] >= (int)size[d]) {quit = true; break;}
            if(end[d] < 0) {quit = true; break;}
            if(begin[d] < 0) begin[d] = 0;
            if(end[d] >= (int)size[d]) end[d] = size[d]-1;
            if(end[d]-begin[d]-abs(offset[d]) < 0) {quit = true; break;}
        }
        if(quit) return;

        RR = (long int*) malloc(sizeof(long int)*levels*2);
        if(RR == NULL) return;

        S = (unsigned int**) malloc(sizeof(unsigned int*)*levels);
        if(S == nullptr) return;
        for(k=0; k < levels; k++)
        {
            S[k]=(unsigned int*) malloc(sizeof(unsigned int)*levels);
            if(S[k]==NULL)
            {
                for(k--; k >= 0; k--)
                    free(S[k]);
                free(S);
                return;
            }
            for(l = 0; l < levels; l++)
                S[k][l] = 0;
        }

//        for(k=0; k<levels; k++)
//        {
//            for(l=0; l<levels; l++)
//            {
//                S[k][l] = 0;
//            }
//        }

        unsigned long int area = 0;
        for(k = 0; k < VDimensions && k < 3; k++)
        {
            if(offset[k] < 0) begin[k] -= offset[k];
            else end[k] -= offset[k];
        }
        MazdaImageRegionIterator<MITypeLocal> cli = MazdaImageRegionIterator<MITypeLocal>(image, (unsigned int*) begin, (unsigned int*) end);
        MazdaRoiRegionIterator<MzRoiLocal> rli = MazdaRoiRegionIterator<MzRoiLocal>(roi, begin, end, true);
        for(k = 0; k < VDimensions && k < 3; k++)
        {
            end[k] += offset[k];
            begin[k] += offset[k];
        }
        MazdaImageRegionIterator<MITypeLocal> cri = MazdaImageRegionIterator<MITypeLocal>(image, (unsigned int*) begin, (unsigned int*) end);
        MazdaRoiRegionIterator<MzRoiLocal> rri = MazdaRoiRegionIterator<MzRoiLocal>(roi, begin, end, true);

        if(symmetric)
        {
            while(!rli.IsBehind())
            {
                if(rli.GetPixel() && rri.GetPixel())
                {
                    int k = cli.GetPixel()>>shift;
                    int l = cri.GetPixel()>>shift;
                    S[k][l]++; S[l][k]++;
                    area += 2;
                }
                ++rli; ++rri; ++cli; ++cri;
            }
        }
        else
        {
            while(!rli.IsBehind())
            {
                if(rli.GetPixel() && rri.GetPixel())
                {
                    int k = cli.GetPixel()>>shift;
                    int l = cri.GetPixel()>>shift;
                    S[k][l]++;
                    area++;
                }
                ++rli; ++rri; ++cli; ++cri;
            }
        }

#ifdef USE_DEBUG_LOG
        if(debugfile.is_open())
        {
            debugfile << "GreyLevels: " << levels << std::endl;
            debugfile << "Glcm:" << std::endl;
            for (k = 0; k < levels; k++)
            {
                debugfile << S[k][0];
                for (l = 1; l < levels; l++)
                {
                    debugfile << "\t" << S[k][l];
                }
                debugfile << std::endl;
            }
        }
#endif

        if(area <= 0)
        {
            if(Area != (unsigned int)-1) table[Area] = 0;
        }
        else
        {
            if(Area != (unsigned int)-1) table[Area] = area;
            // AngScMom Moment zwykly 2 rzedu - cecha 0
            if(AngScMom != (unsigned int)-1)
            {
                double c=0;
                for(k=0; k<levels; k++)
                {
                    for(l=0; l<levels; l++)
                    {
                        c+=((double)S[k][l]*(double)S[k][l]);
                    }
                }
                table[AngScMom] = c/((double)area*area);
            }
            // Contrast Kontrast - cecha 1
            if(Contrast != (unsigned int)-1)
            {
                double c=0;
                for(k=0; k<levels; k++)
                {
                    for(l=0; l<levels; l++)
                    {
                        c+=(double)S[k][l]*((double)k-l)*((double)k-l);
                    }
                }
                table[Contrast] = c/(double)area;
            }
            // Correlat Korelacja - cecha 2
            if(Correlat != (unsigned int)-1 || SumOfSqs != (unsigned int)-1)
            {
                double mx=0; double my=0; double vx=0; double vy=0; double c=0;
                for(k=0; k<levels; k++)
                {
                    for(l=0; l<levels; l++)
                    {
                        mx+=((double)k+1)*(double)S[k][l];
                        my+=((double)l+1)*(double)S[k][l];
                    }
                }
                mx=mx/area;  my=my/area;
                for(k=0; k<levels; k++)
                {
                    for(l=0; l<levels; l++)
                    {
                        vx+=(mx-k-1)*(mx-k-1)*(double)S[k][l];
                        vy+=(my-l-1)*(my-l-1)*(double)S[k][l];
                    }
                }
                vx=vx/area;  vy=vy/area;

                for(k=0; k<levels; k++)
                {
                    for(l=0; l<levels; l++)
                    {
                        c+=(double)S[k][l]*((double)k+1)*((double)l+1);
                    }
                }
                if(Correlat != (unsigned int)-1) table[Correlat] = (c/(double)area-mx*my)/(vx*vy>Eps ?sqrt(vx*vy) :Eps);

                /* SumOfSqs Wariancja rozkladu brzegowego px - cecha 3*/
                if(SumOfSqs != (unsigned int)-1)table[SumOfSqs] = vx;
            }

                /* InvDfMom Odwrotny moment roznicowy - cecha 4*/
            if(InvDfMom != (unsigned int)-1)
            {
                double c=0;
                for(k=0; k<levels; k++)
                {
                    for(l=0; l<levels; l++)
                    {
                        c+=(double)S[k][l]/(1+(k-l)*(k-l));
                    }
                }
                table[InvDfMom] = c/area;
            }

                /* SumAverg Wartosc srednia rozkl. sumacyjnego - cecha 5*/
            if(SumAverg != (unsigned int)-1 || SumVarnc != (unsigned int)-1 || SumEntrp != (unsigned int)-1)
            {
                double mx=0;
                for(k=0; k<2*(levels)-1; k++)
                {
                    RR[k]=0;
                    for(l=(k-(levels)+1<0 ?0 :k-(levels)+1); ((l<(levels))&&(l<=k)); l++)
                        RR[k]+=S[l][k-l];
                    mx+=((double)(k+2)*RR[k])/(double)area;
                }
                if(SumAverg != (unsigned int)-1) table[SumAverg] = mx;
                /* SumVarnc Wariancja rozkl. sumacyjnego - cecha 6*/
                if(SumVarnc != (unsigned int)-1)
                {
                    double c=0;
                    for(k=0; k<2*(levels)-1; k++)
                    {
                        double cc=(double)(k+2-mx)*(k+2-mx);
                        c+=RR[k]*cc;
                    }
                    table[SumVarnc] = c/area;
                }
                /* SumEntrp Entropia rozkl. sumacyjnego - cecha 7*/
                if(SumEntrp != (unsigned int)-1)
                {
                    double c=0;
                    for(k=0; k<2*(levels)-1; k++)
                    {
                        double cc=RR[k]/(double)area;
                        c+=cc*(cc>0 ?log(cc)/Ln10 :(double)0);
                    }
                    table[SumEntrp] = -c;
                }

            }

                /* Entropy Entropia - cecha 8*/
            if(Entropy != (unsigned int)-1)
            {
                double c=0;
                for(k=0; k<(levels); k++)
                {
                    for(l=0; l<(levels); l++)
                    {
                        double cc=(double)S[k][l]/(double)area;
                        c+=cc*(cc>0 ?log(cc)/Ln10 :(double)0);
                    }
                }
                table[Entropy] = -c;
            }

            // Obliczenie rozkladu roznicowego i jego mediany
            if(DifVarnc != (unsigned int)-1 || DifEntrp != (unsigned int)-1)
            {
                double mx=0;
                for(k=0; k<(levels); k++)
                {
                    RR[k]=0;
                    for(l=0; l<(levels)-k; l++)
                        RR[k]+=(k==0 ?S[l][l] :S[l][l+k]+S[l+k][l]);
                    mx+=((double)RR[k]*(k+1))/(double)area;
                }
            // DifVarnc Wariancja rozkl. roznicowego - cecha 9
                double c=0;
                for(k=0; k<(levels); k++)
                {
                    c+=(double)RR[k]*((double)k-mx+1)*((double)k-mx+1);
                }
                if(DifVarnc != (unsigned int)-1) table[DifVarnc] = c/((double)area);
                /*  Entropia rozkl. roznicowego - cecha 10 */
                c=0;
                for(k=0; k<(levels); k++)
                {
                    double cc=(double)RR[k]/(double)area;
                    c+=cc*(cc>0 ?log(cc)/Ln10 :0);
                }
                if(DifEntrp != (unsigned int)-1)table[DifEntrp] = -c;
            }

            if(ClustShd != (unsigned int)-1 || ClustPrm != (unsigned int)-1)
            {
                double mx=0; double my=0;
                for(k=0; k<levels; k++)
                {
                    for(l=0; l<levels; l++)
                    {
                        mx+=((double)k+1)*(double)S[k][l];
                        my+=((double)l+1)*(double)S[k][l];
                    }
                }
                mx=mx/area;  my=my/area;
                double cs = 0;
                double cp = 0;
                for(k=0; k<levels; k++)
                {
                    for(l=0; l<levels; l++)
                    {
                        double c1 = ((double)k+(double)l + 2 - mx - my);
                        double c2 = c1*c1*c1*(double)S[k][l];
                        double c3 = c2*c1;
                        cs += c3;
                        cp += c2;
                    }
                }
                if(ClustShd != (unsigned int)-1)
                    table[ClustShd] = cs/area;
                if(ClustPrm != (unsigned int)-1)
                    table[ClustPrm] = cp/area;
            }
        }

        for(k=0; k < levels; k++) free(S[k]);
        free(S);
        free(RR);
    }




    unsigned int bits;
    bool symmetric;
    int offset[3];
    unsigned int Area;
    unsigned int AngScMom;
    unsigned int Contrast;
    unsigned int Correlat;
    unsigned int SumOfSqs;
    unsigned int InvDfMom;
    unsigned int SumAverg;
    unsigned int SumVarnc;
    unsigned int SumEntrp;
    unsigned int Entropy;
    unsigned int DifVarnc;
    unsigned int DifEntrp;

    unsigned int ClustShd;
    unsigned int ClustPrm;

};
#endif // STUBITEMGLCM_H
