/*
 * QMAZDA project: Image Analysis and Pattern Recognition
 *
 * Copyright 2013-2017 Piotr M. Szczypiński <piotr.szczypinski@p.lodz.pl>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef STUBITEMGABOR_H
#define STUBITEMGABOR_H

#include "genentry.h"
//#include <opencv2/core/core.hpp>
//#include <opencv2/imgproc/imgproc.hpp>

#include <itkImage.h>
#include <itkFFTConvolutionImageFilter.h>
#include <itkBinaryErodeImageFilter.h>
#include <itkBinaryBallStructuringElement.h>

const double epsilon = 0.00001;

class StubItemGabor : public StubItem
{
public:
    ~StubItemGabor(){}
    static StubItem* New(unsigned int dimensions, StubItem* parent, std::vector<std::string>* nameStubs, unsigned int index, NotifyType *notify);
    void Connect(StubItem* item);
    void Execute(StubItemData* data);
    bool IsNotifier(){return true;}
    void NewCommon(unsigned int dimensions, StubItem* parent, std::vector<std::string>* nameStubs);

protected:
    template <typename ImageType>
    void CreateKernels(typename ImageType::Pointer imkernel, typename ImageType::Pointer rekernel)
    {
        typename ImageType::IndexType start;
        start.Fill(0);
        typename ImageType::SizeType size;
        size.Fill(2*kernelsize+1);
        typename ImageType::RegionType region;
        region.SetSize(size);
        region.SetIndex(start);

        imkernel->SetRegions(region);
        imkernel->Allocate();
        rekernel->SetRegions(region);
        rekernel->Allocate();

        itk::ImageRegionIterator<ImageType> imkernelIterator(imkernel, region);
        itk::ImageRegionIterator<ImageType> rekernelIterator(rekernel, region);

        double kerker = (double)kernelsize + 0.5;
        kerker *= kerker;

        double ss2 = 2.0*stddev*stddev;

        if(angle[0] == 0.0 && angle[1] == 0.0 && angle[2] == 0.0) // O direction
        {
            while(!imkernelIterator.IsAtEnd())
            {
                double dist = 0.0;
                double phase = 0.0;
                typename ImageType::IndexType index = imkernelIterator.GetIndex();
                for(int d = 0; d < ImageType::ImageDimension; d++)
                {
                    int i = (int)index[d] - (int)kernelsize;
                    dist += (i*i);
                }
                if(dist > kerker)
                {
                    imkernelIterator.Set(0.0);
                    rekernelIterator.Set(0.0);
                }
                else
                {
                    phase = sqrt(dist)*freq;
                    imkernelIterator.Set(exp(-dist/ss2)*sin(phase));
                    rekernelIterator.Set(exp(-dist/ss2)*cos(phase));
                }
                ++imkernelIterator;
                ++rekernelIterator;
            }
        }
        else // X, Z, N, H, V directions
        {
            while(!imkernelIterator.IsAtEnd())
            {
                double dist = 0.0;
                double phase = 0.0;
                typename ImageType::IndexType index = imkernelIterator.GetIndex();
                for(int d = 0; d < ImageType::ImageDimension; d++)
                {
                    int i = (int)index[d] - (int)kernelsize;
                    dist += (i*i);
                    phase += (i*angle[d]);
                }
                if(dist > kerker)
                {
                    imkernelIterator.Set(0.0);
                    rekernelIterator.Set(0.0);
                }
                else
                {
                    phase *= freq;
                    imkernelIterator.Set(exp(-dist/ss2)*sin(phase));
                    rekernelIterator.Set(exp(-dist/ss2)*cos(phase));
                }
                ++imkernelIterator;
                ++rekernelIterator;
            }
        }
    }

    template <typename ImageType>
    typename ImageType::Pointer erode(typename ImageType::Pointer input)
    {
        typedef itk::BinaryBallStructuringElement< typename ImageType::PixelType, ImageType::ImageDimension > SEType;
        typedef itk::BinaryErodeImageFilter < ImageType, ImageType, SEType > F;
        typedef typename F::Pointer P;
        SEType se;
        se.SetRadius(kernelsize);
        se.CreateStructuringElement();
        P filter = F::New();
        filter->SetInput(input);
        filter->SetKernel(se);
        filter->Update();
        return filter->GetOutput();
    }


    template <unsigned int VDimensions>
    void inRoi(StubItemDataEntryRoi* datas,
               typename itk::Image< double, VDimensions > ::Pointer kernelim,
               typename itk::Image< double, VDimensions > ::Pointer kernelre)
    {
        if(bits < 1)
            return;
        if(bits > sizeof(MazdaImagePixelType)*8)
            return;
        unsigned int shift = sizeof(MazdaImagePixelType)*8 - bits;

        double* table = datas->table;
        typedef itk::Image< double, VDimensions > ImageType;
        typedef itk::Image< unsigned char, VDimensions > RegionType;

        typedef MazdaImage<MazdaImagePixelType, VDimensions> MITypeLocal;
        typedef MazdaRoi <MazdaRoiBlockType, VDimensions> MzRoiLocal;
        unsigned int size[VDimensions];
        double spacing[VDimensions];
        int begin[VDimensions];
        int end[VDimensions];
        MITypeLocal* image = static_cast<MITypeLocal*> (datas->image);
        image->GetSize(size);
        image->GetSpacing(spacing);
        MzRoiLocal* roi = static_cast<MzRoiLocal*> (datas->roi);
        roi->GetBegin(begin);
        roi->GetEnd(end);

        typename ImageType::IndexType start;
        typename ImageType::SizeType itkSize;
        for(int d = 0; d < VDimensions; d++)
        {
            if(end[d] >= size[d]) end[d] = size[d]-1;
            if(begin[d] < 0) begin[d] = 0;
            if(begin[d] > end[d])
                return;
            itkSize[d] = end[d]-begin[d]+1;
            start[d] = 0;
        }

        typename ImageType::Pointer itkImage;
        itkImage = ImageType::New();
        typename RegionType::Pointer itkRoi;
        itkRoi = RegionType::New();

        typename ImageType::RegionType region;
        region.SetSize(itkSize);
        region.SetIndex(start);
        itkImage->SetRegions(region);
        itkImage->Allocate();
        itkRoi->SetRegions(region);
        itkRoi->Allocate();
        itkImage->FillBuffer(0);

        itk::ImageRegionIterator<ImageType> itkIterator(itkImage, itkImage->GetRequestedRegion());
        itk::ImageRegionIterator<RegionType> itkRIterator(itkRoi, itkRoi->GetRequestedRegion());
        MazdaImageRegionIterator<MITypeLocal> ci = MazdaImageRegionIterator<MITypeLocal>(image, (unsigned int*) begin, (unsigned int*) end);
        MazdaRoiRegionIterator<MzRoiLocal> ri = MazdaRoiRegionIterator<MzRoiLocal>(roi, begin, end, true);

        while(!itkIterator.IsAtEnd())
        {
            itkIterator.Set(ci.GetPixel()>>shift);
            if(ri.GetPixel()) itkRIterator.Set(1);
            else itkRIterator.Set(0);
            ++ci;
            ++itkIterator;
            ++itkRIterator;
            ++ri;
        }

        typedef itk::FFTConvolutionImageFilter< ImageType > Filter;
        typename Filter::Pointer filterre = Filter::New();
        filterre->SetInput(itkImage);
        filterre->SetKernelImage(kernelre);
        filterre->Update();

        typename Filter::Pointer filterim = Filter::New();
        filterim->SetInput(itkImage);
        filterim->SetKernelImage(kernelim);
        filterim->Update();

        double magnitude = 0.0;
        unsigned int area = 0;
        typename RegionType::Pointer itkErodedRoi = erode< RegionType > (itkRoi);

        itk::ImageRegionIterator<RegionType> roiIterator(itkErodedRoi, itkErodedRoi->GetRequestedRegion());
        itk::ImageRegionIterator<ImageType> reIterator(filterre->GetOutput(), filterre->GetOutput()->GetRequestedRegion());
        itk::ImageRegionIterator<ImageType> imIterator(filterim->GetOutput(), filterim->GetOutput()->GetRequestedRegion());
        while(!roiIterator.IsAtEnd())
        {
            if(roiIterator.Get())
            {
                area++;
                double im = imIterator.Get();
                double re = reIterator.Get();
                magnitude += sqrt(re*re+im*im);
            }
            ++roiIterator;
            ++reIterator;
            ++imIterator;
        }
        if(Area != (unsigned int)-1) table[Area] = area;
        if(Mag != (unsigned int)-1 && area > 0) table[Mag] = magnitude/area;
    }

    unsigned int bits;
    double freq;
    double angle[3];
    double stddev;
    unsigned int kernelsize;

    unsigned int Area;
    unsigned int Mag;
    unsigned int Phase;
    unsigned int Re;
    unsigned int Im;

    itk::Image< double, 2 > ::Pointer kernel2re;
    itk::Image< double, 2 > ::Pointer kernel2im;
    itk::Image< double, 3 > ::Pointer kernel3re;
    itk::Image< double, 3 > ::Pointer kernel3im;
};


class StubItemGaborLocal : public StubItemGabor
{
public:
    ~StubItemGaborLocal(){}
    static StubItem* New(unsigned int dimensions, StubItem* parent, std::vector<std::string>* nameStubs, unsigned int index, NotifyType *notify);
    void Execute(StubItemData* data);
    bool IsNotifier(){return false;}
private:
    template <unsigned int VDimensions>
    void createMaps(StubItemDataEntryLocal* datas,
                    typename itk::Image< double, VDimensions > ::Pointer kernelim,
                    typename itk::Image< double, VDimensions > ::Pointer kernelre)
    {
        typedef itk::Image< double, VDimensions > ImageType;
        typedef MazdaImage<MazdaImagePixelType, VDimensions> MITypeLocal;
        unsigned int size[VDimensions];
        double spacing[VDimensions];
        MITypeLocal* image = static_cast<MITypeLocal*> (datas->image);
        image->GetSize(size);
        image->GetSpacing(spacing);

        if(bits < 1) return;
        if(bits > sizeof(MazdaImagePixelType)*8) return;
        unsigned int shift = sizeof(MazdaImagePixelType)*8 - bits;

        typename ImageType::Pointer itkImage;
        itkImage = ImageType::New();

        typename ImageType::IndexType start;
        typename ImageType::SizeType itkSize;
        for(int d = 0; d < VDimensions; d++)
        {
            itkSize[d] = size[d];
            start[d] = 0;
        }
        typename ImageType::RegionType region;
        region.SetSize(itkSize);
        region.SetIndex(start);
        itkImage->SetRegions(region);
        itkImage->Allocate();
        itkImage->FillBuffer(0);

        itk::ImageRegionIterator<ImageType> itkIterator(itkImage, itkImage->GetRequestedRegion());
        MazdaImageIterator<MITypeLocal> ci = MazdaImageIterator<MITypeLocal>(image);

        while(!itkIterator.IsAtEnd())
        {
            itkIterator.Set(ci.GetPixel()>>shift);
            ++ci;
            ++itkIterator;
        }

        typedef itk::FFTConvolutionImageFilter< ImageType > Filter;
        typename Filter::Pointer filterre = Filter::New();
        filterre->SetInput(itkImage);
        filterre->SetKernelImage(kernelre);
        filterre->Update();

        typename Filter::Pointer filterim = Filter::New();
        filterim->SetInput(itkImage);
        filterim->SetKernelImage(kernelim);
        filterim->Update();

        typedef MazdaImage <MazdaMapPixelType, VDimensions> MzMap;

        if(Re != (unsigned int)-1)
        {
            MzMap* map = static_cast<MzMap*>((*(datas->maps))[Re]);
            MazdaImageIterator<MzMap> mapi = MazdaImageIterator<MzMap>(map);
            itk::ImageRegionIterator<ImageType> gIterator(filterre->GetOutput(), filterre->GetOutput()->GetRequestedRegion());
            while(!mapi.IsBehind())
            {
//                typename ImageType::IndexType index = reIterator.GetIndex();
//                int d;
//                for(d = 0; d < VDimensions; d++)
//                {
//                    if(index[d] < kernelsize || index[d] >= size[d]-kernelsize)
//                    {
//                        mapi.SetPixel(mznan);
//                        break;
//                    }
//                }
//                if(d >= VDimensions)
                {
                    mapi.SetPixel(gIterator.Get());
                }
                ++gIterator;
                ++mapi;
            }
        }
        if(Im != (unsigned int)-1)
        {
            MzMap* map = static_cast<MzMap*>((*(datas->maps))[Im]);
            MazdaImageIterator<MzMap> mapi = MazdaImageIterator<MzMap>(map);
            itk::ImageRegionIterator<ImageType> gIterator(filterim->GetOutput(), filterim->GetOutput()->GetRequestedRegion());
            while(!mapi.IsBehind())
            {
//                typename ImageType::IndexType index = reIterator.GetIndex();
//                int d;
//                for(d = 0; d < VDimensions; d++)
//                {
//                    if(index[d] < kernelsize || index[d] >= size[d]-kernelsize)
//                    {
//                        mapi.SetPixel(mznan);
//                        break;
//                    }
//                }
//                if(d >= VDimensions)
                {
                    mapi.SetPixel(gIterator.Get());
                }
                ++gIterator;
                ++mapi;
            }
        }

        if((Phase != (unsigned int)-1) && (Mag != (unsigned int)-1))
        {
            MzMap* mmap = static_cast<MzMap*>((*(datas->maps))[Mag]);
            MzMap* pmap = static_cast<MzMap*>((*(datas->maps))[Phase]);
            MazdaImageIterator<MzMap> mmapi = MazdaImageIterator<MzMap>(mmap);
            MazdaImageIterator<MzMap> pmapi = MazdaImageIterator<MzMap>(pmap);
            itk::ImageRegionIterator<ImageType> reIterator(filterre->GetOutput(), filterre->GetOutput()->GetRequestedRegion());
            itk::ImageRegionIterator<ImageType> imIterator(filterim->GetOutput(), filterim->GetOutput()->GetRequestedRegion());
            while(!mmapi.IsBehind())
            {
//                typename ImageType::IndexType index = reIterator.GetIndex();
//                int d;
//                for(d = 0; d < VDimensions; d++)
//                {
//                    if(index[d] < kernelsize || index[d] >= size[d]-kernelsize)
//                    {
//                        mmapi.SetPixel(mznan);
//                        pmapi.SetPixel(mznan);
//                        break;
//                    }
//                }
//                if(d >= VDimensions)
                {
                    double im = imIterator.Get();
                    double re = reIterator.Get();
                    mmapi.SetPixel(sqrt(re*re+im*im));
                    if(fabs(im) > epsilon || fabs(re) > epsilon) pmapi.SetPixel(atan2(im, re));
                    else pmapi.SetPixel(mznan);
                }
                ++reIterator;
                ++imIterator;
                ++mmapi;
                ++pmapi;
            }
        }
        else if(Mag != (unsigned int)-1)
        {
            MzMap* mmap = static_cast<MzMap*>((*(datas->maps))[Mag]);
            MazdaImageIterator<MzMap> mmapi = MazdaImageIterator<MzMap>(mmap);
            itk::ImageRegionIterator<ImageType> reIterator(filterre->GetOutput(), filterre->GetOutput()->GetRequestedRegion());
            itk::ImageRegionIterator<ImageType> imIterator(filterim->GetOutput(), filterim->GetOutput()->GetRequestedRegion());
            while(!mmapi.IsBehind())
            {
//                typename ImageType::IndexType index = reIterator.GetIndex();
//                int d;
//                for(d = 0; d < VDimensions; d++)
//                {
//                    if(index[d] < kernelsize || index[d] >= size[d]-kernelsize)
//                    {
//                        mmapi.SetPixel(mznan);
//                        break;
//                    }
//                }
//                if(d >= VDimensions)
                {
                    double im = imIterator.Get();
                    double re = reIterator.Get();
                    mmapi.SetPixel(sqrt(re*re+im*im));
                }
                ++reIterator;
                ++imIterator;
                ++mmapi;
            }
        }
        else if(Phase != (unsigned int)-1)
        {
            MzMap* pmap = static_cast<MzMap*>((*(datas->maps))[Phase]);
            MazdaImageIterator<MzMap> pmapi = MazdaImageIterator<MzMap>(pmap);
            itk::ImageRegionIterator<ImageType> reIterator(filterre->GetOutput(), filterre->GetOutput()->GetRequestedRegion());
            itk::ImageRegionIterator<ImageType> imIterator(filterim->GetOutput(), filterim->GetOutput()->GetRequestedRegion());
            while(!pmapi.IsBehind())
            {
//                typename ImageType::IndexType index = reIterator.GetIndex();
//                int d;
//                for(d = 0; d < VDimensions; d++)
//                {
//                    if(index[d] < kernelsize || index[d] >= size[d]-kernelsize)
//                    {
//                        pmapi.SetPixel(mznan);
//                        break;
//                    }
//                }
//                if(d >= VDimensions)
                {
                    double im = imIterator.Get();
                    double re = reIterator.Get();
                    if(fabs(im) > epsilon || fabs(re) > epsilon) pmapi.SetPixel(atan2(im, re));
                    else pmapi.SetPixel(mznan);
                }
                ++reIterator;
                ++imIterator;
                ++pmapi;
            }
        }
    }

    template <unsigned int VDimensions>
    void createVectors(StubItemDataEntryLocal* datas,
                       typename itk::Image< double, VDimensions > ::Pointer kernelim,
                       typename itk::Image< double, VDimensions > ::Pointer kernelre)
    {
        typedef itk::Image< double, VDimensions > ImageType;
        typedef itk::Image< unsigned char, VDimensions > RegionType;

        typedef MazdaImage<MazdaImagePixelType, VDimensions> MITypeLocal;
        typedef MazdaRoi <MazdaRoiBlockType, VDimensions> MzRoiLocal;
        unsigned int size[VDimensions];
        double spacing[VDimensions];
        int begin[VDimensions];
        int end[VDimensions];
        int min[VDimensions];
        int max[VDimensions];
        MITypeLocal* image = static_cast<MITypeLocal*> (datas->image);
        image->GetSize(size);
        image->GetSpacing(spacing);
        MzRoiLocal* roi = static_cast<MzRoiLocal*> (datas->roi);
        roi->GetBegin(begin);
        roi->GetEnd(end);

        unsigned int shift = 16 - bits;
        typename ImageType::Pointer itkImage;
        itkImage = ImageType::New();
        //typename RegionType::Pointer itkRoi;
        //itkRoi = RegionType::New();

        typename ImageType::IndexType start;
        typename ImageType::SizeType itkSize;
        for(int d = 0; d < VDimensions; d++)
        {
            min[d] = begin[d]-kernelsize;
            max[d] = end[d]+kernelsize;
            if(min[d] < 0) min[d] = 0;
            if(max[d] >= size[d]) max[d] = size[d] - 1;
            itkSize[d] = max[d]-min[d]+1;
            start[d] = 0;
        }
        typename ImageType::RegionType region;
        region.SetSize(itkSize);
        region.SetIndex(start);
        itkImage->SetRegions(region);
        itkImage->Allocate();
        //itkRoi->SetRegions(region);
        //itkRoi->Allocate();
        //itkImage->FillBuffer(0);

        itk::ImageRegionIterator<ImageType> itkIterator(itkImage, itkImage->GetRequestedRegion());
        //itk::ImageRegionIterator<RegionType> itkRIterator(itkRoi, itkRoi->GetRequestedRegion());
        //MazdaImageRegionIterator<MITypeLocal> ci = MazdaImageRegionIterator<MITypeLocal>(image, (unsigned int*) begin, (unsigned int*) end);

        unsigned int coords[VDimensions];
        while(!itkIterator.IsAtEnd())
        {
            typename ImageType::IndexType index = itkIterator.GetIndex();
            for(int d = 0; d < VDimensions; d++)
            {
                coords[d] = index[d] + min[d];
            }
            itkIterator.Set(image->GetPixel(coords)>>shift);
            ++itkIterator;
        }

        typedef itk::FFTConvolutionImageFilter< ImageType > Filter;
        typename Filter::Pointer filterre = Filter::New();
        filterre->SetInput(itkImage);
        filterre->SetKernelImage(kernelre);
        filterre->Update();
        typename Filter::Pointer filterim = Filter::New();
        filterim->SetInput(itkImage);
        filterim->SetKernelImage(kernelim);
        filterim->Update();

        MazdaRoiRegionIterator<MzRoiLocal> ri = MazdaRoiRegionIterator<MzRoiLocal>(roi, begin, end, true);
        typename ImageType::Pointer remap = filterre->GetOutput();
        typename ImageType::Pointer immap = filterim->GetOutput();
        unsigned int line = 0;
        while(!ri.IsBehind())
        {
            if(ri.GetPixel())
            {
                for(int d = 0; d < VDimensions; d++)
                {
                    start[d] = ri.GetIndex(d)+begin[d]-min[d];
                }
                double* table = (*(datas->tables))[line];
                double im = immap->GetPixel(start);
                double re = remap->GetPixel(start);
                if(Mag != (unsigned int)-1) table[Mag] = sqrt(re*re+im*im);
                if(Phase != (unsigned int)-1)
                    if(fabs(im) > epsilon || fabs(re) > epsilon) table[Phase] = atan2(im, re);
                if(Re != (unsigned int)-1) table[Re] = re;
                if(Im != (unsigned int)-1) table[Im] = im;
                line++;
            }
            ++ri;
        }
    }
};

#endif // STUBITEMGABOR_H
