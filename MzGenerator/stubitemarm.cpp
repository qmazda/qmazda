/*
 * QMAZDA project: Image Analysis and Pattern Recognition
 *
 * Copyright 2013-2017 Piotr M. Szczypiński <piotr.szczypinski@p.lodz.pl>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "stubitemarm.h"

StubItemArm::~StubItemArm()
{
}
StubItem* StubItemArm::New(unsigned int dimensions, StubItem* parent, std::vector<std::string>* nameStubs, unsigned int /*index*/, NotifyType* notify)
{
    StubItemArm* item = new StubItemArm;
    item->dimensions = dimensions;
    item->notify = notify;

    item->bits = 0;
    if(nameStubs->size() >= 3)
    {
        item->name = nameStubs->back();
        if(isupper( (*nameStubs)[nameStubs->size()-3].at(0)) )
            item->bits = atoi((*nameStubs)[nameStubs->size()-2].c_str());
        else if(nameStubs->size() >= 5)
            item->bits = atoi((*nameStubs)[nameStubs->size()-4].c_str());
    }

    item->Teta1 = (unsigned int) -1;
    item->Teta2 = (unsigned int) -1;
    item->Teta3 = (unsigned int) -1;
    item->Teta4 = (unsigned int) -1;
    item->Sigma = (unsigned int) -1;
    item->Area = (unsigned int) -1;

    if(parent != NULL) parent->Connect(item);
    return item;
}
void StubItemArm::Connect(StubItem* item)
{
    StubItemFinal* itemf = static_cast <StubItemFinal*> (item);
    if(itemf->Name() == "Teta1") Teta1= itemf->Index();
    else if(itemf->Name() == "Teta2") Teta2= itemf->Index();
    else if(itemf->Name() == "Teta3") Teta3= itemf->Index();
    else if(itemf->Name() == "Teta4") Teta4= itemf->Index();
    else if(itemf->Name() == "Sigma") Sigma= itemf->Index();
    else if(itemf->Name() == "Area") Area= itemf->Index();
    children.push_back(item);
}

void StubItemArm::Execute(StubItemData* data)
{
    StubItemDataEntryRoi* datas = static_cast<StubItemDataEntryRoi*> (data);

    if(datas->roi != NULL)
    {
        try
        {
            switch(dimensions)
            {
            case 3: inRoi<3> (datas); break;
            case 2: inRoi<2> (datas); break;
            default: break;
            }
        }
        catch(...){}
    }
    if(notify!=NULL) (*notify)();
    return;
}




/*------------------------------------------------------------------------*/
double StubItemArm::wyzn4(double W[4][4])
{
  return
      W[0][0]*
      (
         W[1][1]*(W[2][2]*W[3][3]-W[2][3]*W[3][2])
        -W[1][2]*(W[2][1]*W[3][3]-W[2][3]*W[3][1])
        +W[1][3]*(W[2][1]*W[3][2]-W[2][2]*W[3][1])
      )
     -W[0][1]*
      (
         W[1][0]*(W[2][2]*W[3][3]-W[2][3]*W[3][2])
        -W[1][2]*(W[2][0]*W[3][3]-W[2][3]*W[3][0])
        +W[1][3]*(W[2][0]*W[3][2]-W[2][2]*W[3][0])
      )
     +W[0][2]*
      (
         W[1][0]*(W[2][1]*W[3][3]-W[2][3]*W[3][1])
        -W[1][1]*(W[2][0]*W[3][3]-W[2][3]*W[3][0])
        +W[1][3]*(W[2][0]*W[3][1]-W[2][1]*W[3][0])
      )
     -W[0][3]*
      (
         W[1][0]*(W[2][1]*W[3][2]-W[2][2]*W[3][1])
        -W[1][1]*(W[2][0]*W[3][2]-W[2][2]*W[3][0])
        +W[1][2]*(W[2][0]*W[3][1]-W[2][1]*W[3][0])
      );
}

/*------------------------------------------------------------------------*/
double StubItemArm::pod3wyzn4(double W[4][4], int w, int k)
{
  int w0, w1, w2;
  int k0, k1, k2;

  w0=0; if(w0==w) w0++;
  w1=w0+1; if(w1==w) w1++;
  w2=w1+1; if(w2==w) w2++;

  k0=0; if(k0==k) k0++;
  k1=k0+1; if(k1==k) k1++;
  k2=k1+1; if(k2==k) k2++;

  return
         W[w0][k0]*(W[w1][k1]*W[w2][k2]-W[w1][k2]*W[w2][k1])
        -W[w0][k1]*(W[w1][k0]*W[w2][k2]-W[w1][k2]*W[w2][k0])
        +W[w0][k2]*(W[w1][k0]*W[w2][k1]-W[w1][k1]*W[w2][k0]);
}

/*------------------------------------------------------------------------*/
void StubItemArm::ma_wekwek4(double W[4][4], double W1[4], double W2[4])
{
  int k, l;
  for(k=0; k<4; k++)
    for(l=0; l<4; l++)
      W[k][l]=W1[k]*W2[l];
}

/*------------------------------------------------------------------------*/
double StubItemArm::li_wekwek4(double W1[4], double W2[4])
{
  return W1[0]*W2[0]+W1[1]*W2[1]+W1[2]*W2[2]+W1[3]*W2[3];
}

/*------------------------------------------------------------------------*/
void StubItemArm::ma_ma4(double W[4][4], double W1[4][4], double W2[4][4])
{
  int k, l;

  for(k=0; k<4; k++)
    for(l=0; l<4; l++)
      W[k][l]=W1[k][0]*W2[0][l]+W1[k][1]*W2[1][l]
             +W1[k][2]*W2[2][l]+W1[k][3]*W2[3][l];
}

/*------------------------------------------------------------------------*/
void StubItemArm::wek_mawek4(double W[4], double W1[4][4], double W2[4])
{
  int k;

  for(k=0; k<4; k++)
    W[k]=W1[k][0]*W2[0]+W1[k][1]*W2[1]
             +W1[k][2]*W2[2]+W1[k][3]*W2[3];
}

/*------------------------------------------------------------------------*/
void StubItemArm::sum_ma4(double W[4][4], double W1[4][4])
{
  int k, l;
  for(k=0; k<4; k++)
    for(l=0; l<4; l++)
      W[k][l]+=W1[k][l];
}

/*------------------------------------------------------------------------*/
void StubItemArm::sum_wek4(double W[4], double W1[4])
{
  int l;
  for(l=0; l<4; l++)
    W[l]+=W1[l];
}

/*------------------------------------------------------------------------*/
int StubItemArm::invert4(double W[4][4], double W1[4][4])
{
  int k, l;
  double det;
  det=wyzn4(W1);
  if(det==0) return 1;

  for(k=0; k<4; k++)
    for(l=0; l<4; l++)
    {
      if((k+l)%2) W[l][k]=-pod3wyzn4(W1, k, l)/det;
      else W[l][k]=pod3wyzn4(W1, k, l)/det;
    }
  return 0;
}
/*------------------------------------------------------------------------*/
