/*
 * QMAZDA project: Image Analysis and Pattern Recognition
 *
 * Copyright 2013-2017 Piotr M. Szczypiński <piotr.szczypinski@p.lodz.pl>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "stubitemwindow.h"


StubItemWindow::~StubItemWindow()
{
}
StubItem* StubItemWindow::New(unsigned int dimensions, StubItem* parent, std::vector<std::string>* nameStubs, unsigned int /*index*/, NotifyType* notify)
{
    StubItemWindow* item = new StubItemWindow;
    item->dimensions = dimensions;
    item->notify = notify;
    item->name = nameStubs->back();
    item->shape = (*nameStubs)[nameStubs->size()-2].at(0);
    item->radius = atoi((*nameStubs)[nameStubs->size()-1].c_str());
    if(parent != NULL) parent->Connect(item);
    return item;
}

void StubItemWindow::Connecting(unsigned int index)
{
//    printf("StubItemWindow::Connecting(%i)\n", index);
//    fflush(stdout);
    indices.push_back(index);
}


void StubItemWindow::Execute(StubItemData* data)
{
    StubItemDataEntryLocal* datas = static_cast<StubItemDataEntryLocal*> (data);
    MazdaDummy* roi = NULL;

    switch(dimensions)
    {
    case 3: roi = createRoi<3> (); break;
    case 2: roi = createRoi<2> (); break;
    default: break;
    }

    if(datas->roi == NULL)
    {
        switch(dimensions)
        {
        case 3: createMaps<3>(datas, roi); break;
        case 2: createMaps<2>(datas, roi); break;
        default: break;
        }
    }
    else
    {
        switch(dimensions)
        {
        case 3: createVectors<3> (datas, roi); break;
        case 2: createVectors<2> (datas, roi); break;
        default: break;
        }
    }

    delete roi;
}
