/*
 * QMAZDA project: Image Analysis and Pattern Recognition
 *
 * Copyright 2013-2016 Piotr M. Szczypiński <piotr.szczypinski@p.lodz.pl>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef STUBITEMCOLORTRANSFORM_H
#define STUBITEMCOLORTRANSFORM_H

#include "stubitem.h"

class StubItemColorTransform : public StubItem
{
public:
    //StubItemColorTransform();
    ~StubItemColorTransform();
    static StubItem* New(unsigned int dimensions, StubItem* parent, std::vector<std::string>* nameStubs, unsigned int index, NotifyType* notify);
    void Execute(StubItemData* data);
    bool IsNotifier(){return false;}
private:
    char channel;
    //unsigned int imageid;
    //MazdaDummy* mono;
};

#endif // STUBITEMCOLORTRANSFORM_H
