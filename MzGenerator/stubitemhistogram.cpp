/*
 * QMAZDA project: Image Analysis and Pattern Recognition
 *
 * Copyright 2013-2017 Piotr M. Szczypiński <piotr.szczypinski@p.lodz.pl>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "stubitemhistogram.h"

StubItemHistogram::~StubItemHistogram()
{
}
StubItem* StubItemHistogram::New(unsigned int dimensions, StubItem* parent, std::vector<std::string>* nameStubs, unsigned int /*index*/, NotifyType* notify)
{
    StubItemHistogram* item = new StubItemHistogram;
    item->dimensions = dimensions;
    item->notify = notify;

    item->bits = 0;
    if(nameStubs->size() >= 3)
    {
        item->name = nameStubs->back();
        if(isupper( (*nameStubs)[nameStubs->size()-3].at(0)) )
            item->bits = atoi((*nameStubs)[nameStubs->size()-2].c_str());
        else if(nameStubs->size() >= 5)
            item->bits = atoi((*nameStubs)[nameStubs->size()-4].c_str());
    }

    item->compute_perc = false;
    item->compute_10 = false;
    item->compute_01 = false;
    item->compute_var = false;

    item->Area= (unsigned int) -1;
    item->Mean= (unsigned int) -1;
    item->Variance= (unsigned int) -1;
    item->StdDev= (unsigned int) -1;
    item->Skewness= (unsigned int) -1;
    item->Kurtosis= (unsigned int) -1;
    item->Perc01= (unsigned int) -1;
    item->Perc10= (unsigned int) -1;
    item->Perc50= (unsigned int) -1;
    item->Perc90= (unsigned int) -1;
    item->Perc99= (unsigned int) -1;
    item->Maxm01= (unsigned int) -1;
    item->Domn01= (unsigned int) -1;
    item->Maxm10= (unsigned int) -1;
    item->Domn10= (unsigned int) -1;

    if(parent != NULL) parent->Connect(item);
    return item;
}
void StubItemHistogram::Connect(StubItem* item)
{
    StubItemFinal* itemf = static_cast <StubItemFinal*> (item);

    if(itemf->Name() == "Area") {Area= itemf->Index(); compute_var = true;}
    else if(itemf->Name() == "Mean") {Mean= itemf->Index(); compute_var = true;}
    else if(itemf->Name() == "Variance") {Variance= itemf->Index(); compute_var = true;}
    else if(itemf->Name() == "StdDev") {StdDev= itemf->Index(); compute_var = true;}
    else if(itemf->Name() == "Skewness") {Skewness= itemf->Index(); compute_var = true;}
    else if(itemf->Name() == "Kurtosis") {Kurtosis= itemf->Index(); compute_var = true;}
    else if(itemf->Name() == "Perc01") {Perc01= itemf->Index(); compute_perc = true;}
    else if(itemf->Name() == "Perc10") {Perc10= itemf->Index(); compute_perc = true;}
    else if(itemf->Name() == "Perc50") {Perc50= itemf->Index(); compute_perc = true;}
    else if(itemf->Name() == "Perc90") {Perc90= itemf->Index(); compute_perc = true;}
    else if(itemf->Name() == "Perc99") {Perc99= itemf->Index(); compute_perc = true;}
    else if(itemf->Name() == "Maxm01") {Maxm01= itemf->Index(); compute_01 = true;}
    else if(itemf->Name() == "Domn01") {Domn01= itemf->Index(); compute_01 = true;}
    else if(itemf->Name() == "Maxm10") {Maxm10= itemf->Index(); compute_10 = true;}
    else if(itemf->Name() == "Domn10") {Domn10= itemf->Index(); compute_10 = true;}

    children.push_back(item);
}

void StubItemHistogram::Execute(StubItemData* data)
{
    StubItemDataEntryRoi* datas = static_cast<StubItemDataEntryRoi*> (data);
#ifdef USE_DEBUG_LOG
    if(debugfile.is_open())
    {
        debugfile << "########  Histogram::Execute()  ########"<< std::endl;
    }
#endif
    if(datas->roi != NULL)
    {
        try
        {
            switch(dimensions)
            {
            case 3: inRoi<3> (datas); break;
            case 2: inRoi<2> (datas); break;
            default: break;
            }
        }
        catch(...){}
    }
    if(notify!=NULL) (*notify)();
    return;
}

