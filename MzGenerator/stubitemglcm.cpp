/*
 * QMAZDA project: Image Analysis and Pattern Recognition
 *
 * Copyright 2013-2017 Piotr M. Szczypiński <piotr.szczypinski@p.lodz.pl>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "stubitemglcm.h"

StubItemGlcm::~StubItemGlcm()
{
}
StubItem* StubItemGlcm::New(unsigned int dimensions, StubItem* parent, std::vector<std::string>* nameStubs, unsigned int /*index*/, NotifyType* notify)
{
    StubItemGlcm* item = new StubItemGlcm;
    item->dimensions = dimensions;
    item->notify = notify;

    item->bits = 0;
    if(nameStubs->size() >= 5)
    {
        item->name = nameStubs->back();
        if(isupper( (*nameStubs)[nameStubs->size()-5].at(0)) )
            item->bits = atoi((*nameStubs)[nameStubs->size()-4].c_str());
        else if(nameStubs->size() >= 7)
            item->bits = atoi((*nameStubs)[nameStubs->size()-6].c_str());
    }

    std::string name = (*nameStubs)[nameStubs->size()-3];
    std::string direction = (*nameStubs)[nameStubs->size()-2];
    int off = atoi((*nameStubs)[nameStubs->size()-1].c_str());

    if(name == "Glch") item->symmetric = false;
    else item->symmetric = true;

    if(direction == "H")
    {
        item->offset[0] = off; item->offset[1] = 0; item->offset[2] = 0;
    }
    else if(direction == "Z")
    {
       item->offset[0] = off; item->offset[1] = -off; item->offset[2] = 0;
    }
    else if(direction == "N")
    {
       item->offset[0] = off; item->offset[1] = off; item->offset[2] = 0;
    }
    else if(direction == "X")
    {
       item->offset[0] = 0; item->offset[1] = 0; item->offset[2] = off;
    }
    else //"V"
    {
       item->offset[0] = 0; item->offset[1] = off; item->offset[2] = 0;
    }

    item->Area = (unsigned int) -1;
    item->AngScMom = (unsigned int) -1;
    item->Contrast = (unsigned int) -1;
    item->Correlat = (unsigned int) -1;
    item->SumOfSqs = (unsigned int) -1;
    item->InvDfMom = (unsigned int) -1;
    item->SumAverg = (unsigned int) -1;
    item->SumVarnc = (unsigned int) -1;
    item->SumEntrp = (unsigned int) -1;
    item->Entropy = (unsigned int) -1;
    item->DifVarnc = (unsigned int) -1;
    item->DifEntrp = (unsigned int) -1;
    item->ClustShd = (unsigned int) -1;
    item->ClustPrm = (unsigned int) -1;

    if(parent != NULL) parent->Connect(item);
    return item;
}
void StubItemGlcm::Connect(StubItem* item)
{
    StubItemFinal* itemf = static_cast <StubItemFinal*> (item);

    if(itemf->Name() == "Area") Area = itemf->Index();
    else if(itemf->Name() == "AngScMom") AngScMom = itemf->Index();
    else if(itemf->Name() == "Contrast") Contrast = itemf->Index();
    else if(itemf->Name() == "Correlat") Correlat = itemf->Index();
    else if(itemf->Name() == "SumOfSqs") SumOfSqs = itemf->Index();
    else if(itemf->Name() == "InvDfMom") InvDfMom = itemf->Index();
    else if(itemf->Name() == "SumAverg") SumAverg = itemf->Index();
    else if(itemf->Name() == "SumVarnc") SumVarnc = itemf->Index();
    else if(itemf->Name() == "SumEntrp") SumEntrp = itemf->Index();
    else if(itemf->Name() == "Entropy") Entropy = itemf->Index();
    else if(itemf->Name() == "DifVarnc") DifVarnc = itemf->Index();
    else if(itemf->Name() == "DifEntrp") DifEntrp = itemf->Index();
    else if(itemf->Name() == "ClustShd") ClustShd = itemf->Index();
    else if(itemf->Name() == "ClustPrm") ClustPrm = itemf->Index();

    children.push_back(item);
}

void StubItemGlcm::Execute(StubItemData* data)
{
    StubItemDataEntryRoi* datas = static_cast<StubItemDataEntryRoi*> (data);
#ifdef USE_DEBUG_LOG
    if(debugfile.is_open())
    {
        debugfile << "########  Glcm::Execute()  ########"<< std::endl;
//        debugfile << " ImageId: " << datas->imageid;
//        debugfile << " RoiId: " << datas->roiid <<" ########"<< std::endl;
        debugfile << "Symmetric: ";
        if(symmetric) debugfile << "Yes";
        else debugfile << "No";
        debugfile << std::endl;
        debugfile << "Offset: ";
        for(unsigned int d = 0; d < dimensions; d++)
            debugfile << offset[d] << " ";
        debugfile << std::endl;
    }
#endif
    if(datas->roi != NULL)
    {
        try
        {
            switch(dimensions)
            {
            case 3: inRoi<3> (datas); break;
            case 2: inRoi<2> (datas); break;
            default: break;
            }
        }
        catch(...){}
    }
    if(notify!=NULL) (*notify)();
    return;
}
