/*
 * QMAZDA project: Image Analysis and Pattern Recognition
 *
 * Copyright 2013-2016 Piotr M. Szczypiński <piotr.szczypinski@p.lodz.pl>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <string>
#include <iostream>
#include <cstdio>
#include "genentry.h"
#include "../MzShared/mzdefines.h"
#include "../MzShared/getoption.h"
#include "../SharedImage/mazdaimageio.h"
#include "../SharedImage/mazdaroiio.h"
#include "../MzShared/dataforconsole.h"
#include "../MzShared/csvio.h"
#include "../MzShared/filenameremap.h"
#include "featureio.h"
#include "stubitem.h"
#include "templates.h"
#include "time.h"
#include <sstream>

char* modestring = NULL;
char* input_image = NULL;
char* input_features = NULL;
char* input_regions = NULL;
char* output_data = NULL;
char* category_name = NULL;
char* debug_log = NULL;
char* execute_command = NULL;
unsigned int mapstep = 1;
bool query = false;
bool append = false;
bool single_thread = false;
bool hexadecimal = false;
bool isprinthelp = false;
bool delete_input_files = false;
unsigned long int notifications, maxnotifications;
#ifdef USE_DEBUG_LOG
std::ofstream debugfile;
#endif

/**
 * @brief scan_parameters parses command line arguments
 * @param argc
 * @param argv
 * @return
 */
int scan_parameters(int argc, char* argv[])
{
    int argi = 1;
    while(argi < argc)
    {
        GET_STRING_OPTION("-m", "--mode", modestring)
        else GET_STRING_OPTION("-i", "--input", input_image)
        else GET_STRING_OPTION("-f", "--features", input_features)
        else GET_STRING_OPTION("-r", "--regions", input_regions)
        else GET_STRING_OPTION("-o", "--output", output_data)
        else GET_STRING_OPTION("-c", "--category", category_name)
        else GET_NOARG_OPTION("-q", "--query", query, true)
        else GET_NOARG_OPTION("-x", "--save-hex", hexadecimal, true)
        else GET_NOARG_OPTION("-a", "--append", append, true)
        else GET_INT_OPTION("-s", "--step", mapstep)
        else GET_NOARG_OPTION("-k", "--single-thread", single_thread, true)
        else GET_STRING_OPTION("-e", "--execute", execute_command)
#ifdef USE_DEBUG_LOG
        else GET_STRING_OPTION("-d", "--debug-log", debug_log)
#endif
        else GET_NOARG_OPTION("--delete-input-files", "--delete-input-files", delete_input_files, true)
        else GET_NOARG_OPTION("/?", "--help", isprinthelp, true)
        else return argi;
        argi++;
    }
    if(query) single_thread = true;
    return 0;
}


void deleteInputFiles()
{
    if(delete_input_files)
    {
        if(input_image != NULL) std::remove(input_image);
        if(input_features != NULL) std::remove(input_features);
        if(input_regions != NULL) std::remove(input_regions);
    }
}


/**
 * @brief printhelp prints help information
 */
void printhelp(char* name)
{
    //------------------------------------------------------------------------------------------v
    printf("Usage: %s [OPTION]...\n", name);
    printf("Computes image attributes (features). In 'local' mode features are computed\n");
    printf("arround specified locations or arround every pixel of the input image. In 'roi'\n");
    printf("mode features are computed within specified regions of interest.\n");
    printf("%s\n", QMAZDA_VERSION);
    printf("%s\n", QMAZDA_COPYRIGHT);
    printf("Compilation date and time: %s %s\n", __DATE__, __TIME__);
    printf("Options:\n");
    printf("  -m, --mode <mode>       Set <mode> of the analysis to: 'local' or 'roi',\n");
    printf("  -q, --query             Queries for feature names template\n");
    printf("  -i, --input <file>      Load image from <file>. 16bit tiff is available\n");
    printf("  -f, --features <file>   Load feature list from <file>\n");
    printf("  -r, --regions <file>    Load masks or regions of interest form <file>\n");
    printf("  -o, --output <file>     Save results to csv or floating-point multipage tiff <file>\n");
    printf("  -c, --category <string> Force category name\n");
    printf("                          By default a category name is retrieved from regions file\n");
    printf("  -s, --step <int>        Step in pixels in map computation, default is 1\n");
#ifdef MZ_USE_THREADS
    printf("  -k, --single-thread     Use single thread (slower, requires less mempory)\n");
#endif
    printf("  -x, --save-hex          Save double precision results in hexadecimal format\n");
    printf("  -a, --append            Append data to output file if available\n");
#ifdef USE_DEBUG_LOG
    printf("  -d, --debug-log         Set filename for debug info\n");
#endif
    printf("  /?, --help              Display this help and exit\n\n");
}



int gentryScan(int argc, char* argv[])
{
    int ret = scan_parameters(argc, argv);
    if(isprinthelp || argc <= 1)
    {
        printhelp(argv[0]);
        return 1;
    }
    if(ret != 0)
    {
        if(ret < argc) fprintf(stderr, "Incorrect operand: %s\n", argv[ret]);
        fprintf(stderr, "Try --help for more information.\n");
        return ret;
    }
    return 0;
}


int gentryFeatureHeader(std::vector <std::string>* names)
{
    StubItemBuilder* featureComputationObject;
    std::vector <std::string> names_temp;
    NameStubTree templateTree;
    StubItemBuilder::CreateTemplate(&templateTree, roiTreeTemplate, sizeof(roiTreeTemplate)/sizeof(ItemTemplate));
    featureComputationObject = new StubItemBuilder(0, &templateTree);
//    featureComputationObject = new StubItemBuilder(0, &templateTree, roiTreeTemplate, sizeof(roiTreeTemplate)/sizeof(ItemTemplate));
    unsigned int i, f;
    f = 0;
    for(i = 0; i < names->size(); i++)
    {
        if(featureComputationObject->addFeature((*names)[i], f, notify))
        {
            names_temp.push_back((*names)[i]);
            f++;
        }
    }
    names->clear();
    *names = names_temp;
    names_temp.clear();
    unsigned int imax = names->size();
    if(imax <= 0) {fprintf(stderr, "Invalid feature names\n"); return -3;}
    std::ofstream file;
    if(append)
    {
#if defined _MZ_UTF8_TO_U16
        file.open(_mz_utf8_to_u16(output_data).c_str(), std::ofstream::out | std::ofstream::app);
#else
        file.open(output_data, std::ofstream::out | std::ofstream::app);
#endif
    }
    else
    {
#if defined _MZ_UTF8_TO_U16
        file.open(_mz_utf8_to_u16(output_data).c_str());
#else
        file.open(output_data);
#endif
    }
    if (!file.is_open()) return -4;
    if (!file.good()) return -4;
    char *prevloc = setlocale(LC_ALL, NULL);
    if(prevloc) prevloc = strdup(prevloc);
    setlocale(LC_ALL, "C");
    if(!append)
    {
        std::vector<std::string>::iterator nameit;
        for(nameit = names->begin(); nameit != names->end(); ++nameit) file << *nameit << ",";
        if(category_name == nullptr)
            file << "Category" << std::endl;
        else
            file << category_name << std::endl;
    }
    setlocale(LC_ALL, prevloc);
    free(prevloc);
    return 0;
}

void startNotifier(const char* mode)
{
    time_t currenttime = time(NULL);
    char* ct = ctime(&currenttime);
    strtok(ct, "\n\r");
    std::stringstream ss;
    ss << mode << ", " << ct;
    notifyText(ss.str().c_str());
    if(input_image != NULL)
    {
        ss.str("");
        ss << "Input: " << input_image;
        notifyText(ss.str().c_str());
    }
//    if(input_regions != NULL)
//    {
//        ss.str("");
//        ss << "Regions: " << input_regions;
//        notifyText(ss.str().c_str());
//    }
    if(output_data != NULL)
    {
        ss.str("");
        ss << "Output: " << output_data;
        notifyText(ss.str().c_str());
    }
}


int gentryRoi(std::vector <std::string>* names)
{
    unsigned int size[4] = {0, 0, 0, 0};
    int begin[4] = {1, 1, 1, 1};
    int end[4] = {0, 0, 0, 0};
    std::vector <MzRoi2D*> rois2;
    std::vector <MzRoi3D*> rois3;
    if(input_regions == NULL) {fprintf(stderr, "Unspecified input regions (-r)\n"); return -5;}

    MazdaDummy* image = MazdaImageIO< MIType3D, MazdaImagePixelType >::Read(input_image);

    MIType2D* isbw = dynamic_cast < MIType2D* >(image);
    MIType2DRGB* isrgb = dynamic_cast < MIType2DRGB* >(image);
    MIType3D* isbw3 = dynamic_cast < MIType3D* >(image);
    MIType3DRGB* isrgb3 = dynamic_cast < MIType3DRGB* >(image);
    if(isbw==NULL && isrgb==NULL && isbw3==NULL && isrgb3==NULL)
        {fprintf(stderr, "Input image format not supported in this mode (-i)\n"); return -2;}
    if(isbw3 != NULL)
        isbw3->GetSize(size);
    else if(isrgb3 != NULL)
        isrgb3->GetSize(size);
    else if(isbw != NULL)
        isbw->GetSize(size);
    else if(isrgb != NULL)
        isrgb->GetSize(size);

    unsigned int rmax = 0;
    bool is3d = isbw3!=NULL || isrgb3!=NULL;
    if(is3d)
    {
        startNotifier("Volume-based feature extraction (3D)");
        rois3  = MazdaRoiIO<MzRoi3D>::Read(input_regions);
        rmax = rois3.size();
    }
    else
    {
        startNotifier("Region-based feature extraction (2D)");
        rois2  = MazdaRoiIO<MzRoi2D>::Read(input_regions);
        rmax = rois2.size();
    }
    if(rmax <= 0) {fprintf(stderr, "Invalid input regions (-r)\n"); return -5;}

    StubItemBuilder* featureComputationObject;
    std::vector <std::string> names_temp;
    NameStubTree templateTree;
    StubItemBuilder::CreateTemplate(&templateTree, roiTreeTemplate, sizeof(roiTreeTemplate)/sizeof(ItemTemplate));
    featureComputationObject = new StubItemBuilder((is3d ? 3 : 2), &templateTree);
//    featureComputationObject = new StubItemBuilder((is3d ? 3 : 2), &templateTree, roiTreeTemplate, sizeof(roiTreeTemplate)/sizeof(ItemTemplate));
    unsigned int i, f;
    f = 0;
    for(i = 0; i < names->size(); i++)
    {
//        cout << (*names)[i] << " ";
        if(featureComputationObject->addFeature((*names)[i], f, notify))
        {
            names_temp.push_back((*names)[i]);
            f++;
        }
    }
    names->clear();
    *names = names_temp;
    names_temp.clear();
    unsigned int imax = names->size();
    if(imax <= 0) {fprintf(stderr, "Invalid feature names\n"); return -3;}

    maxnotifications = featureComputationObject->getNotificationsNumber();
    maxnotifications *= rmax;
    double* table = new double[imax];
    std::ofstream file;
    if(append)
    {
#if defined _MZ_UTF8_TO_U16
        file.open(_mz_utf8_to_u16(output_data).c_str(), std::ofstream::out | std::ofstream::app);
#else
        file.open(output_data, std::ofstream::out | std::ofstream::app);
#endif
    }
    else
    {
#if defined _MZ_UTF8_TO_U16
        file.open(_mz_utf8_to_u16(output_data).c_str());
#else
        file.open(output_data);
#endif
    }
    if (!file.is_open()) return -4;
    if (!file.good()) return -4;

    char *prevloc = setlocale(LC_ALL, NULL);
    if(prevloc) prevloc = strdup(prevloc);
    setlocale(LC_ALL, "C");

    if(!append)
    {
        std::vector<std::string>::iterator nameit;
        for(nameit = names->begin(); nameit != names->end(); ++nameit) file << *nameit << ",";
        if(category_name == nullptr)
            file << "Category" << std::endl;
        else
            file << category_name << std::endl;
    }

    StubItemDataEntryRoi genData;
    genData.image = image;
//    genData.is3D = is3d;
    genData.isRGB = isrgb;
    genData.table = table;

    for(unsigned int r = 0; r < rmax; r++)
    {
        std::string name;
        bool roiok = true;

        if(is3d)
        {
            MzRoi3D* roi = rois3[r];
            genData.roi = roi;
            name = roi->GetName();
            if(roi->GetDataPointer() == NULL) roiok = false;
            roi->GetBegin(begin);
            roi->GetEnd(end);
            for(int d = 0; d < 3; d++)
                if(begin[d] >= (int)size[d] || end[d] < 0 || end[d] < begin[d])
                    roiok = false;
        }
        else
        {
            MzRoi2D* roi = rois2[r];
            genData.roi = roi;
            name = roi->GetName();
            if(roi->GetDataPointer() == NULL) roiok = false;
            roi->GetBegin(begin);
            roi->GetEnd(end);
            for(int d = 0; d < 2; d++)
                if(begin[d] >= (int)size[d] || end[d] < 0 || end[d] < begin[d])
                    roiok = false;
        }

        std::stringstream ss;
        if(category_name != nullptr)
            ss << "Roi " << r+1 << "/" << rmax << ": "<< category_name;
        else
            ss << "Roi " << r+1 << "/" << rmax << ": "<< name;
        notifyText(ss.str().c_str());

        for(unsigned int i = 0; i < imax; i++) table[i] = mznan;
        if(roiok) featureComputationObject->Execute(&genData);

        if(hexadecimal)
        {
            for(unsigned int i = 0; i < imax; i++)
            {
                char* valuec = CsvIo::doubleToHex(table[i]);
                file << valuec << ",";
            }
        }
        else
        {
            for(unsigned int i = 0; i < imax; i++)
            {
                file << table[i] << ",";
            }
        }
        if(category_name != nullptr)
            file << category_name << std::endl;
        else
            file << name << std::endl;
    }
    if(table != NULL) delete[] table;
    setlocale(LC_ALL, prevloc);
    free(prevloc);

    notifications = 0;
    maxnotifications = 1;
    notify();
    return 0;
}

int gentryMap(std::vector <std::string>* names)
{
    if(mapstep <= 0) mapstep = 1;
    if(mapstep > 128) mapstep = 128;

    MazdaDummy* image = MazdaImageIO< MIType3D, MazdaImagePixelType >::Read(input_image);
    bool isbw = dynamic_cast < MIType2D* >(image) != NULL;
    bool isrgb = dynamic_cast < MIType2DRGB* >(image) != NULL;
    bool isbw3 = dynamic_cast < MIType3D* >(image) != NULL;
    bool isrgb3 = dynamic_cast < MIType3DRGB* >(image) != NULL;
    if(!isbw && !isrgb && !isbw3 && !isrgb3)
        {fprintf(stderr, "Input image format not supported in this mode (-i)\n"); return -2;}

    bool is3d = isbw3 || isrgb3;

    StubItemBuilder* featureComputationObject;
    std::vector <std::string> names_temp;
    NameStubTree templateTree;

    StubItemBuilder::CreateTemplate(&templateTree, localTreeTemplate, sizeof(localTreeTemplate)/sizeof(ItemTemplate));
    featureComputationObject = new StubItemBuilder((is3d ? 3 : 2), &templateTree);
//    featureComputationObject = new StubItemBuilder((is3d ? 3 : 2), &templateTree, localTreeTemplate, sizeof(localTreeTemplate)/sizeof(ItemTemplate));
    unsigned int i, f;
    f = 0;
    for(i = 0; i < names->size(); i++)
        if(featureComputationObject->addFeature((*names)[i], f, notify))
        {
            names_temp.push_back((*names)[i]);
            f++;
        }
    names->clear();
    *names = names_temp;
    names_temp.clear();

    maxnotifications = featureComputationObject->getNotificationsNumber();

    unsigned int dimensions = 0;
    unsigned int size[4];
    double spacing[4];
    if(isbw)
    {
        static_cast < MIType2D* >(image)->GetSize(size);
        static_cast < MIType2D* >(image)->GetSpacing(spacing);
    }
    if(isrgb)
    {
        static_cast < MIType2DRGB* >(image)->GetSize(size);
        static_cast < MIType2DRGB* >(image)->GetSpacing(spacing);
    }
    if(isbw3)
    {
        static_cast < MIType3D* >(image)->GetSize(size);
        static_cast < MIType3D* >(image)->GetSpacing(spacing);
    }
    if(isrgb3)
    {
        static_cast < MIType3DRGB* >(image)->GetSize(size);
        static_cast < MIType3DRGB* >(image)->GetSpacing(spacing);
    }

    if(is3d) dimensions = 3;
    else dimensions = 2;
    unsigned long int sizeall = size[0];
    for(unsigned int d = 1; d < dimensions; d++) sizeall *= size[d];

    maxnotifications *= sizeall;

    unsigned int imax = names->size();
    if(imax <= 0) {fprintf(stderr, "Invalid feature names\n"); return -3;}

    StubItemDataEntryLocal genData;
    genData.image = image;
//    genData.is3D = is3d;
    genData.isRGB = isrgb;

    std::vector<MazdaDummy*> maxmaps;
    if(is3d)
    {
        startNotifier("Map computation (3D)");
        for(unsigned int d = 0; d < imax; d++)
        {

            MapType3D* map = new MapType3D(size, spacing);
            map->SetName((*names)[d]);
            maxmaps.push_back(map);
        }
    }
    else
    {
        startNotifier("Map computation (2D)");
        for(unsigned int d = 0; d < imax; d++)
        {
            MapType2D* map = new MapType2D(size, spacing);
            map->SetName((*names)[d]);
            maxmaps.push_back(map);
        }
    }

    genData.maps = &maxmaps;
    genData.roi = NULL;
    genData.step = mapstep;
    genData.tables = NULL;

    featureComputationObject->Execute(&genData);

    if(is3d)
    {
        std::vector<MapType3D*> maxmaps3;
        for(unsigned int r = 0; r < maxmaps.size(); r++)
            maxmaps3.push_back(static_cast<MapType3D*>(maxmaps[r]));
        MazdaMapIO<MapType3D>::Write(output_data, &maxmaps3);
    }
    else
    {
        std::vector<MapType2D*> maxmaps2;
        for(unsigned int r = 0; r < maxmaps.size(); r++)
            maxmaps2.push_back(static_cast<MapType2D*>(maxmaps[r]));
        MazdaMapIO<MapType2D>::Write(output_data, &maxmaps2);
    }

    for(unsigned int r = 0; r < maxmaps.size(); r++)
    {
        delete maxmaps[r];
    }
    maxmaps.clear();
    notifications = 0;
    maxnotifications = 1;
    notify();
    return 0;
}

int gentryLocal(std::vector <std::string>* names)
{
    unsigned int size[4] = {0, 0, 0, 0};
    int begin[4] = {1, 1, 1, 1};
    int end[4] = {0, 0, 0, 0};
    std::vector <MzRoi2D*> rois2;
    std::vector <MzRoi3D*> rois3;

    MazdaDummy* image = MazdaImageIO< MIType3D, MazdaImagePixelType >::Read(input_image);

    MIType2D* isbw = dynamic_cast < MIType2D* >(image);
    MIType2DRGB* isrgb = dynamic_cast < MIType2DRGB* >(image);
    MIType3D* isbw3 = dynamic_cast < MIType3D* >(image);
    MIType3DRGB* isrgb3 = dynamic_cast < MIType3DRGB* >(image);
    if(isbw==NULL && isrgb==NULL && isbw3==NULL && isrgb3==NULL)
        {fprintf(stderr, "Input image format not supported in this mode (-i)\n"); return -2;}
    if(isbw3 != NULL)
        isbw3->GetSize(size);
    else if(isrgb3 != NULL)
        isrgb3->GetSize(size);
    else if(isbw != NULL)
        isbw->GetSize(size);
    else if(isrgb != NULL)
        isrgb->GetSize(size);

    unsigned int rmax = 0;
    bool is3d = isbw3!=NULL || isrgb3!=NULL;
    if(is3d)
    {
        startNotifier("Point attribute computation (3D)");
        rois3  = MazdaRoiIO<MzRoi3D>::Read(input_regions);
        rmax = rois3.size();
    }
    else
    {
        startNotifier("Point attribute computation (2D)");
        rois2  = MazdaRoiIO<MzRoi2D>::Read(input_regions);
        rmax = rois2.size();
    }
    if(rmax <= 0) {fprintf(stderr, "Invalid input regions (-r)\n"); return -5;}


    StubItemBuilder* featureComputationObject;
    std::vector <std::string> names_temp;
    NameStubTree templateTree;

    StubItemBuilder::CreateTemplate(&templateTree, localTreeTemplate, sizeof(localTreeTemplate)/sizeof(ItemTemplate));
    featureComputationObject = new StubItemBuilder((is3d ? 3 : 2), &templateTree);

//    featureComputationObject = new StubItemBuilder((is3d ? 3 : 2), &templateTree, localTreeTemplate, sizeof(localTreeTemplate)/sizeof(ItemTemplate));
    unsigned int i, f;
    f = 0;
    for(i = 0; i < names->size(); i++)
        if(featureComputationObject->addFeature((*names)[i], f, notify))
        {
            names_temp.push_back((*names)[i]);
            f++;
        }
    names->clear();
    *names = names_temp;
    names_temp.clear();
    maxnotifications = featureComputationObject->getNotificationsNumber();

    unsigned long int* dots = new unsigned long int[rmax];
    for(unsigned int r = 0; r < rmax; r++)
    {
        dots[r] = 0;
        if(is3d)
        {
            MzRoi3D* roi = rois3[r];
            if(roi->GetDataPointer() != nullptr)
            {
                MazdaRoiIterator<MzRoi3D> it(roi);
                while(! it.IsBehind())
                {
                    if(it.GetPixel()) dots[r]++;
                    ++it;
                }
            }
        }
        else
        {
            MzRoi2D* roi = rois2[r];
            if(roi->GetDataPointer() != nullptr)
            {
                MazdaRoiIterator<MzRoi2D> it(roi);
                while(! it.IsBehind())
                {
                    if(it.GetPixel()) dots[r]++;
                    ++it;
                }
            }
        }
    }
    unsigned long int alldots = 0;
    unsigned long int maxdots = 0;
    for(unsigned int r = 0; r < rmax; r++)
    {
        alldots += dots[r];
        if(maxdots < dots[r]) maxdots = dots[r];
    }
    maxnotifications *= alldots;

    unsigned int imax = names->size();
    if(imax <= 0) {fprintf(stderr, "Invalid feature names\n"); return -3;}

    std::ofstream file;
    if(append)
    {
#if defined _MZ_UTF8_TO_U16
        file.open(_mz_utf8_to_u16(output_data).c_str(), std::ofstream::out | std::ofstream::app);
#else
        file.open(output_data, std::ofstream::out | std::ofstream::app);
#endif
    }
    else
    {
#if defined _MZ_UTF8_TO_U16
        file.open(_mz_utf8_to_u16(output_data).c_str());
#else
        file.open(output_data);
#endif
    }

    if (!file.is_open()) return -4;
    if (!file.good()) return -4;

    char *prevloc = setlocale(LC_ALL, NULL);
    if(prevloc) prevloc = strdup(prevloc);
    setlocale(LC_ALL, "C");

    if(!append)
    {
        std::vector<std::string>::iterator nameit;
        for(nameit = names->begin(); nameit != names->end(); ++nameit) file << *nameit << ",";
        if(category_name == nullptr)
            file << "Category" << std::endl;
        else
            file << category_name << std::endl;
    }

    StubItemDataEntryLocal genData;
    genData.image = image;
//    genData.is3D = is3d;
    genData.isRGB = isrgb;

    std::vector<double*> maxtable;
    for(unsigned int r = 0; r < maxdots; r++)
    {
        double* mtline = new double[imax];
        maxtable.push_back(mtline);
    }

    for(unsigned int r = 0; r < rmax; r++)
    {
        bool roiok = true;
        std::string name;
        if(is3d)
        {
            MzRoi3D* roi = rois3[r];
            genData.roi = roi;
            name = roi->GetName();
            if(roi->GetDataPointer() == NULL)
                roiok = false;
            roi->GetBegin(begin);
            roi->GetEnd(end);
            for(int d = 0; d < 3; d++)
                if(begin[d] >= (int)size[d] || end[d] < 0 || end[d] < begin[d])
                    roiok = false;
        }
        else
        {
            MzRoi2D* roi = rois2[r];
            genData.roi = roi;
            name = roi->GetName();
            if(roi->GetDataPointer() == NULL)
                roiok = false;
            roi->GetBegin(begin);
            roi->GetEnd(end);
            for(int d = 0; d < 2; d++)
                if(begin[d] >= (int)size[d] || end[d] < 0 || end[d] < begin[d])
                    roiok = false;
        }
        for(unsigned int dot = 0; dot < dots[r]; dot++)
        {
            for(unsigned int i = 0; i < imax; i++)
            {
                maxtable[dot][i] = mznan;
            }
        }

        genData.maps = NULL;
        genData.step = mapstep;
        genData.tables = &maxtable;

        if(roiok)
            featureComputationObject->Execute(&genData);

        for(unsigned int dot = 0; dot < dots[r]; dot++)
        {
            if(hexadecimal)
            {
                for(unsigned int i = 0; i < imax; i++)
                {
                    char* valuec = CsvIo::doubleToHex(maxtable[dot][i]);
                    file << valuec << ",";
                }
            }
            else
            {
                for(unsigned int i = 0; i < imax; i++)
                {
                    file << maxtable[dot][i] << ",";
                }
            }
            file << name << std::endl;
        }
    }

    setlocale(LC_ALL, prevloc);
    free(prevloc);

    for(unsigned int r = 0; r < maxtable.size(); r++)
    {
        delete[] maxtable[r];
    }
    maxtable.clear();
    delete[] dots;
    notifications = 0;
    maxnotifications = 1;
    notify();
    return 0;
}

std::vector <std::string> gentryGetTemplate(GentryMode mode)
{
    unsigned int imax = 0;
    unsigned int i;
    ItemTemplate* itemTemplate = NULL;
    std::vector <std::string> tree;
    switch(mode)
    {
    case roi:
        itemTemplate = roiTreeTemplate;
        imax = sizeof(roiTreeTemplate)/sizeof(ItemTemplate);
        break;
    case local:
        itemTemplate = localTreeTemplate;
        imax = sizeof(localTreeTemplate)/sizeof(ItemTemplate);
        break;
    default: return tree;
    }
    for(i = 0; i < imax; i++)
    {
        tree.push_back(itemTemplate[i].codeLine);
    }
    return tree;
}

int gentryRun(void)
{

#ifdef MZ_USE_THREADS
//    int available_threads = StubItemThrough::InitializeThreads();
//    fprintf(stdout, "Available threads %i\n", available_threads);
//    fflush(stdout);
#endif

    GentryMode mode;
    std::vector <std::string> names;
    notifications = 0;
    if(modestring == NULL) {fprintf(stderr, "Unspecified mode (-m)\n"); return -1;}
    std::string ms = modestring;

    if(ms == "roi") mode = roi;
    else if(ms == "local") mode = local;
    else
    {
        fprintf(stderr, "Invalid mode (-m)\n");
        return -1;
    }
    if(query)
    {
        std::vector <std::string> stringresults = gentryGetTemplate(mode);
        for(unsigned int i = 0; i < stringresults.size(); i++) std::cout << stringresults[i] << std::endl;
        return 0;
    }
    if(input_image == NULL && input_features != NULL && output_data != NULL && !append)
    {
        names = loadFeatureList(input_features);
        gentryFeatureHeader(&names);
        return 0;
    }
    if(input_image == NULL) {fprintf(stderr, "Unspecified input image (-i)\n"); return -2;}
    if(input_features == NULL && !append) {fprintf(stderr, "Unspecified feature list file (-f)\n"); return -3;}
    if(input_features != NULL && append) {fprintf(stderr, "Cannot use feature list file in append mode (-f, -a)\n"); return -3;}
    if(output_data == NULL) {fprintf(stderr, "Unspecified output file (-o)\n"); return -4;}

#ifdef USE_DEBUG_LOG
    if(debug_log != NULL) debugfile.open(debug_log);
#endif
    if(append)
        names = loadFeatureList(output_data, true);
    else
        names = loadFeatureList(input_features);

    if(mode == roi) //Region computation
    {
        int r = gentryRoi(&names);
        if(r != 0) return r;
    }
    else if(mode == local && input_regions == NULL) // Map computation
    {
        int r = gentryMap(&names);
        if(r != 0) return r;
    }
    else if(mode == local) // Local in regions
    {
        int r = gentryLocal(&names);
        if(r != 0) return r;
    }
    deleteInputFiles();
/*
#ifdef MZ_USE_THREADS
    fprintf(stdout, "Available threads %i\n", available_threads);
#endif
*/
    return 0;
}
