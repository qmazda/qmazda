TEMPLATE = app
CONFIG += console
CONFIG -= app_bundle
CONFIG -= qt


# -m roi -r ./ziarna20.roi -i ./ziarna20.png -o ./ziarna20.csv -f ./ziarna20.txt
# -m local -i ./ziarna20.png -f ./ziarna20.txt -o ./ziarna20.tiff

DESTDIR         = ../../Executables
TARGET = MzGenerator
INCLUDEPATH += $$PWD/SharedImage

# DEFINES += MAZDA_IMAGE_8BIT_PIXEL

SOURCES += \
    ../MzShared/parametertreetemplate.cpp \
    ../MzShared/dataforconsole.cpp \
    ../MzShared/csvio.cpp \
    stubitem.cpp \
    genentry.cpp \
    templates.cpp \
    stubitemnormalization.cpp \
    stubitemcolortransform.cpp \
    stubitemhistogram.cpp \
    stubitemgradient.cpp \
    stubitemarm.cpp \
    stubitemgrlm.cpp \
    stubitemglcm.cpp \
    stubitemgabor.cpp \
    stubitemshape.cpp \
    stubitemwindow.cpp \
    main.cpp \
    featureio.cpp \
    stubitemshapemz.cpp \
    stubitemshapecv.cpp \
    stubitemlbp.cpp \
    stubitemhog.cpp \
    stubitemhaar.cpp

HEADERS += \
    ../MzShared/getoption.h \
    stubitem.h \
    ../MzShared/parametertreetemplate.h \
    genentry.h \
    ../SharedImage/mazdaimage.h \
    ../SharedImage/mazdaroi.h \
    templates.h \
    stubitemnormalization.h \
    stubitemcolortransform.h \
    stubitemhistogram.h \
    stubitemgradient.h \
    stubitemarm.h \
    stubitemgrlm.h \
    stubitemglcm.h \
    stubitemgabor.h \
    stubitemshape.h \
    stubitemwindow.h \
    featureio.h \
    stubitemshapemz.h \
    stubitemshapecv.h \
    stubitemlbp.h \
    stubitemhog.h \
    stubitemhaar.h

    DEFINES += USE_DEBUG_LOG
#    DEFINES += USE_NOTIFICATIONS

include(../Pri/config.pri)
include(../Pri/itk.pri)
include(../Pri/opencv.pri)
include(../Pri/tiff.pri)
